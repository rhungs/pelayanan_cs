/*
Navicat MySQL Data Transfer

Source Server         : MYSQL
Source Server Version : 50141
Source Host           : localhost:3306
Source Database       : dispendabogor_kepuasan

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2018-07-31 09:09:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `audit_trail`
-- ----------------------------
DROP TABLE IF EXISTS `audit_trail`;
CREATE TABLE `audit_trail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `menu` varchar(20) DEFAULT NULL,
  `activity` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `ipaddress` varchar(30) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of audit_trail
-- ----------------------------

-- ----------------------------
-- Table structure for `indikator_penilaian`
-- ----------------------------
DROP TABLE IF EXISTS `indikator_penilaian`;
CREATE TABLE `indikator_penilaian` (
  `instansi_id` int(11) NOT NULL,
  `indikator_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `no_antrian` varchar(30) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `id_ref_jenisantrian` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`created_date`,`no_antrian`,`instansi_id`,`indikator_category_id`,`user_id`),
  KEY `idx_indikator_cat` (`indikator_category_id`,`user_id`,`created_date`,`no_antrian`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=587 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of indikator_penilaian
-- ----------------------------
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:50:57', '1', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:10', '2', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:19', '3', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:31', '4', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 17:51:47', '5', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-10-31 17:52:01', '7', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:37:09', '45', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:38:03', '47', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-10-31 18:38:21', '48', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 18:38:46', '49', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:39:25', '50', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:39:50', '51', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 18:40:23', '51', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:21:55', '1', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:22:08', '2', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:22:26', '3', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:23:34', '4', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:26:29', '5', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:26:48', '6', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:04', '7', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:27:18', '8', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:36', '9', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:58', '10', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:28:37', '11', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 16:29:33', '12', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:31:33', '13', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:32:27', '12', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:39:23', '13', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:15', '14', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:30', '16', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:45', '17', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 16:42:10', '18', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:45:01', '19', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:45:46', '20', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:53:22', '21', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:53:40', '22', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:55:33', '23', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:56:00', '24', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:00:08', '25', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:05:50', '26', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:10:23', '27', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 17:16:04', '28', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:18:50', '29', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:19:06', '30', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:20:53', '31', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:21:18', '32', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 17:21:46', '33', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:04:13', '002', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:04:52', '005', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:42:47', '006', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:44:29', '007', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:52:23', '009', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 10:58:22', '001', null, null, '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:00:59', '002', '2016-11-03 11:00:52', '2016-11-03 11:00:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:32:59', '003', '2016-11-03 11:32:50', '2016-11-03 11:32:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:33:23', '004', '2016-11-03 11:33:10', '2016-11-03 11:33:23', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:46:29', '005', '2016-11-03 11:45:54', '2016-11-03 11:46:29', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:52:09', '006', '2016-11-03 11:51:57', '2016-11-03 11:52:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 13:48:09', '007', '2016-11-03 13:48:00', '2016-11-03 13:48:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:23:09', '008', '2016-11-03 14:22:07', '0000-00-00 00:00:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:24:35', '009', '2016-11-03 14:24:16', '2016-11-03 14:23:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:25:51', '010', '2016-11-03 14:25:34', '2016-11-03 14:24:35', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:26:15', '011', '2016-11-03 14:26:07', '2016-11-03 14:25:51', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 14:26:58', '012', '2016-11-03 14:26:40', '2016-11-03 14:26:15', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:27:20', '013', '2016-11-03 14:27:07', '2016-11-03 14:26:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:27:45', '014', '2016-11-03 14:27:31', '2016-11-03 14:27:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:31:52', '015', '2016-11-03 14:31:38', '2016-11-03 14:27:45', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:32:15', '016', '2016-11-03 14:32:04', '2016-11-03 14:31:52', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:29:58', '017', '2016-11-03 15:29:42', '0000-00-00 00:00:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:30:24', '018', '2016-11-03 15:30:07', '2016-11-03 15:29:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:31:22', '019', '2016-11-03 15:31:03', '2016-11-03 15:30:24', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 15:32:59', '020', '2016-11-03 15:32:47', '2016-11-03 15:31:22', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 15:41:28', '021', '2016-11-03 15:41:12', '0000-00-00 00:00:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '3', '2016-11-03 17:42:03', '022', '2016-11-03 17:41:47', '0000-00-00 00:00:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 17:55:03', '023', '2016-11-03 17:52:00', '0000-00-00 00:00:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 17:56:48', '024', '2016-11-03 17:55:07', '2016-11-03 17:56:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:23:43', '025', '2016-11-03 17:59:59', '2016-11-03 18:23:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:24:58', '026', '2016-11-03 18:24:40', '2016-11-03 18:24:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:26:09', '027', '2016-11-03 18:25:52', '2016-11-03 18:26:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:26:23', '028', '2016-11-03 18:26:13', '2016-11-03 18:26:23', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:31:57', '029', '2016-11-03 18:31:48', '2016-11-03 18:31:57', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:50:26', '030', '2016-11-03 18:50:16', '2016-11-03 18:50:26', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:35:48', '001', '2016-11-04 13:31:13', '2016-11-04 13:35:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:36:04', '002', '2016-11-04 13:35:53', '2016-11-04 13:36:04', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-04 13:38:12', '003', '2016-11-04 13:37:43', '2016-11-04 13:38:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:38:37', '004', '2016-11-04 13:38:23', '2016-11-04 13:38:37', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:39:03', '005', '2016-11-04 13:38:50', '2016-11-04 13:39:03', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:39:43', '006', '2016-11-04 13:39:14', '2016-11-04 13:39:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:41:33', '007', '2016-11-04 13:41:22', '2016-11-04 13:41:33', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:50:14', '008', '2016-11-04 13:50:01', '2016-11-04 13:50:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 23:38:53', '002', '2016-11-04 23:38:38', '2016-11-04 23:38:53', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:07:29', '001', '2016-11-07 11:06:37', '2016-11-07 11:07:29', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:08:03', '002', '2016-11-07 11:07:51', '2016-11-07 11:08:03', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:08:38', '003', '2016-11-07 11:08:16', '2016-11-07 11:08:38', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 11:09:11', '004', '2016-11-07 11:09:04', '2016-11-07 11:09:11', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 11:09:56', '005', '2016-11-07 11:09:43', '2016-11-07 11:09:56', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:11:27', '006', '2016-11-07 11:10:47', '2016-11-07 11:11:27', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:11:58', '001', '2016-11-07 11:11:18', '2016-11-07 11:11:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 11:12:26', '002', '2016-11-07 11:12:12', '2016-11-07 11:12:26', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:14:09', '003', '2016-11-07 11:13:27', '2016-11-07 11:14:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:15:06', '004', '2016-11-07 11:14:28', '2016-11-07 11:15:06', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:15:41', '007', '2016-11-07 11:11:40', '2016-11-07 11:15:41', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 12:36:30', '008', '2016-11-07 11:30:01', '2016-11-07 12:36:30', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:37:26', '009', '2016-11-07 12:37:12', '2016-11-07 12:37:26', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:46:37', '010', '2016-11-07 12:46:27', '2016-11-07 12:46:37', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:46:50', '011', '2016-11-07 12:46:41', '2016-11-07 12:46:50', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:47:14', '012', '2016-11-07 12:47:02', '2016-11-07 12:47:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:48:57', '013', '2016-11-07 12:48:47', '2016-11-07 12:48:57', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 14:11:34', '014', '2016-11-07 14:11:24', '2016-11-07 14:11:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 14:11:53', '015', '2016-11-07 14:11:43', '2016-11-07 14:11:53', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 14:12:16', '016', '2016-11-07 14:12:05', '2016-11-07 14:12:16', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 14:12:42', '017', '2016-11-07 14:12:31', '2016-11-07 14:12:42', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:10', '018', '2016-11-07 16:12:26', '2016-11-07 16:14:10', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:25', '019', '2016-11-07 16:14:15', '2016-11-07 16:14:25', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:45', '020', '2016-11-07 16:14:36', '2016-11-07 16:14:45', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:15:47', '021', '2016-11-07 16:15:40', '2016-11-07 16:15:47', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:16:00', '022', '2016-11-07 16:15:52', '2016-11-07 16:16:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:23:01', '023', '2016-11-07 16:22:46', '2016-11-07 16:23:01', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:27:22', '024', '2016-11-07 16:25:38', '2016-11-07 16:27:22', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:30:09', '025', '2016-11-07 16:29:58', '2016-11-07 16:30:09', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:30:34', '026', '2016-11-07 16:30:19', '2016-11-07 16:30:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:32:51', '027', '2016-11-07 16:32:32', '2016-11-07 16:32:51', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:33:19', '028', '2016-11-07 16:33:10', '2016-11-07 16:33:19', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:34:07', '029', '2016-11-07 16:33:56', '2016-11-07 16:34:07', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:34:36', '009', '2016-11-07 16:34:16', '2016-11-07 16:34:36', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:41:56', '102', '2016-11-07 16:41:31', '2016-11-07 16:41:56', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:41:56', '099', '2016-11-07 16:41:29', '2016-11-07 16:41:56', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:42:12', '30', '2016-11-07 16:34:37', '2016-11-07 16:42:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:42:25', '031', '2016-11-07 16:42:16', '2016-11-07 16:42:25', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:42:29', '100', '2016-11-07 16:42:19', '2016-11-07 16:42:29', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:42:35', '113', '2016-11-07 16:42:22', '2016-11-07 16:42:35', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:42:59', '101', '2016-11-07 16:42:49', '2016-11-07 16:42:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:43:12', '134', '2016-11-07 16:42:54', '2016-11-07 16:43:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:43:37', '102', '2016-11-07 16:43:29', '2016-11-07 16:43:37', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:43:44', '123', '2016-11-07 16:43:31', '2016-11-07 16:43:44', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:44:02', '103', '2016-11-07 16:43:54', '2016-11-07 16:44:02', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:44:14', '134', '2016-11-07 16:44:01', '2016-11-07 16:44:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:44:57', '001', '2016-11-07 16:44:43', '2016-11-07 16:44:57', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:45:29', '032', '2016-11-07 16:45:19', '2016-11-07 16:45:29', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:45:29', '110', '2016-11-07 16:45:18', '2016-11-07 16:45:29', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:45:30', '156', '2016-11-07 16:45:09', '2016-11-07 16:45:30', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:45:31', '001', '2016-11-07 16:45:18', '2016-11-07 16:45:31', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:45:51', '001', '2016-11-07 16:45:41', '2016-11-07 16:45:51', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:45:52', '033', '2016-11-07 16:45:31', '2016-11-07 16:45:52', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:45:52', '235', '2016-11-07 16:45:40', '2016-11-07 16:45:52', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:45:52', '112', '2016-11-07 16:45:41', '2016-11-07 16:45:52', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:46:20', '113', '2016-11-07 16:46:10', '2016-11-07 16:46:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:46:20', '034', '2016-11-07 16:46:08', '2016-11-07 16:46:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:46:21', '223', '2016-11-07 16:46:12', '2016-11-07 16:46:21', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:46:21', '001', '2016-11-07 16:46:09', '2016-11-07 16:46:21', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:46:38', '035', '2016-11-07 16:46:29', '2016-11-07 16:46:38', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '2', '6', '2016-11-07 16:46:49', '002', '2016-11-07 16:46:39', '2016-11-07 16:46:49', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:47:06', '120', '2016-11-07 16:46:51', '2016-11-07 16:47:06', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:47:12', '036', '2016-11-07 16:46:59', '2016-11-07 16:47:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:47:13', '005', '2016-11-07 16:47:01', '2016-11-07 16:47:13', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:47:31', '006', '2016-11-07 16:47:21', '2016-11-07 16:47:31', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:47:33', '037', '2016-11-07 16:47:24', '2016-11-07 16:47:33', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:47:34', '90', '2016-11-07 16:47:18', '2016-11-07 16:47:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '2', '6', '2016-11-07 16:48:01', '008', '2016-11-07 16:47:44', '2016-11-07 16:48:01', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:48:03', '86', '2016-11-07 16:47:47', '2016-11-07 16:48:03', '0');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:48:30', '005', '2016-11-07 16:48:20', '2016-11-07 16:48:30', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:48:47', '78', '2016-11-07 16:48:36', '2016-11-07 16:48:47', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:49:07', '67', '2016-11-07 16:48:54', '2016-11-07 16:49:07', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:49:40', '90', '2016-11-07 16:49:23', '2016-11-07 16:49:40', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:50:12', '60', '2016-11-07 16:49:52', '2016-11-07 16:50:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:00:30', '038', '2016-11-07 17:00:21', '2016-11-07 17:00:30', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:02:01', '039', '2016-11-07 17:01:03', '2016-11-07 17:02:01', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:02:20', '40', '2016-11-07 17:02:11', '2016-11-07 17:02:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:15:55', '041', '2016-11-07 17:07:50', '2016-11-07 17:15:55', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:16:07', '042', '2016-11-07 17:15:59', '2016-11-07 17:16:07', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 17:18:48', '043', '2016-11-07 17:18:24', '2016-11-07 17:18:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:04', '044', '2016-11-07 17:18:53', '2016-11-07 17:19:04', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 17:19:16', '045', '2016-11-07 17:19:07', '2016-11-07 17:19:16', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:31', '045', '2016-11-07 17:19:18', '2016-11-07 17:19:31', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:43', '046', '2016-11-07 17:19:33', '2016-11-07 17:19:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:54', '047', '2016-11-07 17:19:45', '2016-11-07 17:19:54', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:20:15', '048', '2016-11-07 17:19:57', '2016-11-07 17:20:15', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:20:28', '049', '2016-11-07 17:20:19', '2016-11-07 17:20:28', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:20:44', '050', '2016-11-07 17:20:36', '2016-11-07 17:20:44', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:20:59', '051', '2016-11-07 17:20:48', '2016-11-07 17:20:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:14', '052', '2016-11-07 17:21:05', '2016-11-07 17:21:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:24', '053', '2016-11-07 17:21:17', '2016-11-07 17:21:24', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:44', '054', '2016-11-07 17:21:35', '2016-11-07 17:21:44', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:22:36', '055', '2016-11-07 17:21:47', '2016-11-07 17:22:36', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:23:04', '056', '2016-11-07 17:22:52', '2016-11-07 17:23:04', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:23:53', '057', '2016-11-07 17:23:34', '2016-11-07 17:23:53', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:31:13', '001', '2016-11-08 10:26:00', '2016-11-08 10:31:13', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:31:26', '002', '2016-11-08 10:31:15', '2016-11-08 10:31:26', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:32:28', '003', '2016-11-08 10:31:28', '2016-11-08 10:32:28', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:33:00', '004', '2016-11-08 10:32:30', '2016-11-08 10:33:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:34:10', '005', '2016-11-08 10:33:02', '2016-11-08 10:34:10', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:35:13', '006', '2016-11-08 10:34:12', '2016-11-08 10:35:13', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:35:27', '007', '2016-11-08 10:35:14', '2016-11-08 10:35:27', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:03', '008', '2016-11-08 10:35:29', '2016-11-08 10:36:03', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:40', '009', '2016-11-08 10:36:05', '2016-11-08 10:36:40', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:59', '010', '2016-11-08 10:36:43', '2016-11-08 10:36:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:37:48', '011', '2016-11-08 10:37:01', '2016-11-08 10:37:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:44:14', '012', '2016-11-08 10:37:52', '2016-11-08 10:44:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:46:13', '013', '2016-11-08 10:44:16', '2016-11-08 10:46:13', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:46:55', '014', '2016-11-08 10:46:15', '2016-11-08 10:46:55', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:48:47', '015', '2016-11-08 10:46:58', '2016-11-08 10:48:47', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:48:58', '016', '2016-11-08 10:48:49', '2016-11-08 10:48:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:49:08', '016', '2016-11-08 10:49:00', '2016-11-08 10:49:08', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:32:26', '017', '2016-11-08 11:18:10', '2016-11-08 11:32:26', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:32:35', '18', '2016-11-08 11:32:28', '2016-11-08 11:32:35', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:03', '019', '2016-11-08 11:32:56', '2016-11-08 11:33:03', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:31', '020', '2016-11-08 11:33:06', '2016-11-08 11:33:31', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:42', '021', '2016-11-08 11:33:34', '2016-11-08 11:33:42', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:55', '022', '2016-11-08 11:33:44', '2016-11-08 11:33:55', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:34:34', '022', '2016-11-08 11:33:58', '2016-11-08 11:34:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:34:45', '023', '2016-11-08 11:34:36', '2016-11-08 11:34:45', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:03:58', '024', '2016-11-08 14:03:03', '2016-11-08 14:03:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:05:05', '025', '2016-11-08 14:04:48', '2016-11-08 14:05:05', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:07:00', '026', '2016-11-08 14:06:50', '2016-11-08 14:07:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:07:21', '026', '2016-11-08 14:07:09', '2016-11-08 14:07:21', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:07:39', '027', '2016-11-08 14:07:28', '2016-11-08 14:07:39', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:08:04', '028', '2016-11-08 14:07:46', '2016-11-08 14:08:04', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:10:20', '029', '2016-11-08 14:10:13', '2016-11-08 14:10:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:10:35', '030', '2016-11-08 14:10:27', '2016-11-08 14:10:35', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:10:47', '031', '2016-11-08 14:10:38', '2016-11-08 14:10:47', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:11:00', '032', '2016-11-08 14:10:51', '2016-11-08 14:11:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:11:38', '033', '2016-11-08 14:11:04', '2016-11-08 14:11:38', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:22:11', '034', '2016-11-08 14:22:02', '2016-11-08 14:22:11', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 14:26:33', '101', '2016-11-08 14:26:03', '2016-11-08 14:26:33', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:27:56', '103', '2016-11-08 14:27:37', '2016-11-08 14:27:56', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:33:33', '104', '2016-11-08 14:29:44', '2016-11-08 14:33:33', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:33:34', '104', '2016-11-08 14:29:44', '2016-11-08 14:33:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:44:01', '035', '2016-11-08 14:43:53', '2016-11-08 14:44:01', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:44:21', '36', '2016-11-08 14:44:07', '2016-11-08 14:44:21', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 14:44:43', '111', '2016-11-08 14:44:30', '2016-11-08 14:44:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 14:45:18', '113', '2016-11-08 14:45:10', '2016-11-08 14:45:18', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:47:48', '037', '2016-11-08 14:47:32', '2016-11-08 14:47:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:50:00', '114', '2016-11-08 14:49:46', '2016-11-08 14:50:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:50:52', '038', '2016-11-08 14:50:39', '2016-11-08 14:50:52', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:02:17', '039', '2016-11-08 14:51:58', '2016-11-08 15:02:17', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:03:11', '041', '2016-11-08 15:02:28', '2016-11-08 15:03:11', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:07:02', '042', '2016-11-08 15:03:21', '2016-11-08 15:07:02', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:07:20', '043', '2016-11-08 15:07:08', '2016-11-08 15:07:20', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:17:58', '044', '2016-11-08 15:17:46', '2016-11-08 15:17:58', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:18:19', '045', '2016-11-08 15:18:08', '2016-11-08 15:18:19', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 15:18:43', '046', '2016-11-08 15:18:26', '2016-11-08 15:18:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:19:30', '047', '2016-11-08 15:19:09', '2016-11-08 15:19:30', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 15:19:54', '048', '2016-11-08 15:19:43', '2016-11-08 15:19:54', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 15:19:57', '115', '2016-11-08 15:19:48', '2016-11-08 15:19:57', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:20:43', '049', '2016-11-08 15:20:16', '2016-11-08 15:20:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:21:10', '050', '2016-11-08 15:20:53', '2016-11-08 15:21:10', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:23:40', '051', '2016-11-08 15:23:31', '2016-11-08 15:23:40', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 15:25:27', '114', '2016-11-08 15:25:16', '2016-11-08 15:25:27', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:25:39', '052', '2016-11-08 15:25:29', '2016-11-08 15:25:39', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:45:31', '053', '2016-11-08 15:45:21', '2016-11-08 15:45:31', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 16:04:18', '115', '2016-11-08 16:03:52', '2016-11-08 16:04:18', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 16:05:43', '054', '2016-11-08 16:05:28', '2016-11-08 16:05:43', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 16:06:07', '055', '2016-11-08 16:05:57', '2016-11-08 16:06:07', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 16:07:14', '056', '2016-11-08 16:07:01', '2016-11-08 16:07:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 16:08:49', '116', '2016-11-08 16:08:37', '2016-11-08 16:08:49', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 17:31:39', '110', '2016-11-08 17:29:33', '2016-11-08 17:31:39', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 17:32:44', '057', '2016-11-08 17:32:36', '2016-11-08 17:32:44', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 17:45:53', '117', '2016-11-08 17:45:37', '2016-11-08 17:45:53', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 17:46:23', '112', '2016-11-08 17:31:49', '2016-11-08 17:46:23', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 17:47:14', '120', '2016-11-08 17:47:01', '2016-11-08 17:47:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 18:27:40', '121', '2016-11-08 18:27:27', '2016-11-08 18:27:40', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 18:33:35', '101', '2016-11-08 18:33:22', '2016-11-08 18:33:35', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 18:41:19', '115', '2016-11-08 18:40:53', '2016-11-08 18:41:19', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 18:46:44', '001', '2016-11-08 18:46:26', '2016-11-08 18:46:44', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 18:47:14', '002', '2016-11-08 18:46:55', '2016-11-08 18:47:14', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-08 18:52:13', '003', '2016-11-08 18:51:44', '2016-11-08 18:52:13', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 18:59:16', '004', '2016-11-08 18:58:04', '2016-11-08 18:59:16', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 19:43:34', '004', '2016-11-08 19:43:24', '2016-11-08 19:43:34', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 19:45:39', '112', '2016-11-08 19:45:30', '2016-11-08 19:45:39', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 19:59:15', '110', '2016-11-08 19:58:33', '2016-11-08 19:59:15', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 20:00:54', '113', '2016-11-08 20:00:30', '2016-11-08 20:00:54', '0');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 20:11:00', '110', '2016-11-08 20:10:22', '2016-11-08 20:11:00', '0');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 20:11:41', '116', '2016-11-08 20:09:28', '2016-11-08 20:11:41', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-09 10:44:59', '001', '2016-11-09 10:44:51', '2016-11-09 10:44:59', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-09 20:29:50', '001', '2016-11-09 20:29:41', '2016-11-09 20:29:50', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-15 12:40:19', '001', '2016-11-15 12:39:51', '2016-11-15 12:40:19', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-18 17:34:56', '001', '2016-11-18 17:34:50', '2016-11-18 17:34:56', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-24 11:00:12', '001', '2016-11-24 10:59:39', '2016-11-24 11:00:12', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-09-18 14:39:18', '001', '2017-09-18 14:38:58', '2017-09-18 14:39:18', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-09-18 14:39:48', '002', '2017-09-18 14:39:31', '2017-09-18 14:39:48', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-09-18 14:48:24', '003', '2017-09-18 14:48:08', '2017-09-18 14:48:24', '0');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 14:40:08', '005', '2017-10-09 14:40:26', '2017-10-09 14:42:02', '4');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 14:37:49', '004', '2017-10-09 14:38:06', '2017-10-09 14:40:06', '3');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 14:06:16', '001', '2017-10-09 14:22:24', '2017-10-09 14:22:24', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 14:06:16', '002', '2017-10-09 14:15:20', '2017-10-09 14:15:24', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-09 14:06:36', '003', '2017-10-09 14:15:20', '2017-10-09 14:15:24', '2');
INSERT INTO `indikator_penilaian` VALUES ('7', '586', '0', '2018-01-12 14:23:43', '007', null, null, '5');
INSERT INTO `indikator_penilaian` VALUES ('6', '585', '0', '2018-01-12 14:23:39', '006', null, null, '4');
INSERT INTO `indikator_penilaian` VALUES ('5', '584', '0', '2018-01-12 14:23:36', '005', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('4', '583', '0', '2018-01-12 14:23:33', '004', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('3', '582', '0', '2018-01-12 14:23:30', '003', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '581', '0', '2018-01-12 14:23:27', '002', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '580', '0', '2018-01-12 14:23:05', '001', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('9', '579', '0', '2017-12-18 14:11:03', '020', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('8', '578', '0', '2017-12-18 12:26:44', '019', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('7', '577', '0', '2017-12-18 12:26:20', '018', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('6', '576', '0', '2017-12-18 12:25:51', '017', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '575', '0', '2017-12-18 12:23:29', '016', null, null, '5');
INSERT INTO `indikator_penilaian` VALUES ('4', '574', '0', '2017-12-18 12:23:19', '015', null, null, '4');
INSERT INTO `indikator_penilaian` VALUES ('3', '573', '0', '2017-12-18 12:23:00', '014', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('2', '572', '0', '2017-12-18 12:22:43', '013', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '571', '0', '2017-12-18 12:22:31', '012', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('11', '570', '0', '2017-12-18 12:22:12', '011', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('10', '569', '0', '2017-12-18 12:21:55', '010', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('9', '568', '0', '2017-12-18 12:21:41', '009', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('8', '567', '0', '2017-12-18 12:21:24', '008', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('7', '566', '0', '2017-12-18 12:20:02', '007', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('6', '565', '0', '2017-12-18 12:19:33', '006', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '564', '0', '2017-12-18 12:18:51', '005', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('4', '563', '0', '2017-12-18 12:18:20', '004', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('3', '562', '0', '2017-12-18 12:17:47', '003', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('2', '561', '0', '2017-12-18 12:17:18', '002', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '560', '0', '2017-12-18 12:16:58', '001', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-10 09:03:38', '003', '2017-11-10 09:03:58', '2017-11-10 09:05:33', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-10 08:56:12', '002', '2017-11-10 08:56:22', '2017-11-10 08:57:14', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-10 08:55:24', '001', '2017-11-10 08:55:43', '2017-11-10 08:55:54', '1');
INSERT INTO `indikator_penilaian` VALUES ('3', '556', '0', '2017-11-09 14:16:34', '003', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('2', '555', '0', '2017-11-09 14:02:08', '002', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-09 09:15:00', '001', '2017-11-09 09:16:22', '2017-11-09 09:27:58', '1');
INSERT INTO `indikator_penilaian` VALUES ('10', '553', '0', '2017-11-08 13:19:34', '011', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '552', '0', '2017-11-08 13:19:05', '010', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-08 10:55:31', '001', '2017-11-08 10:55:53', '2017-11-08 10:57:23', '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2017-11-08 10:55:34', '002', '2017-11-08 10:56:59', '2017-11-08 11:12:31', '2');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2017-11-08 10:55:37', '003', '2017-11-08 10:57:09', '2017-11-08 11:12:21', '3');
INSERT INTO `indikator_penilaian` VALUES ('4', '1', '6', '2017-11-08 10:55:40', '004', '2017-11-08 10:57:12', '2017-11-08 11:12:13', '4');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-08 10:55:43', '005', '2017-11-08 11:11:44', '2017-11-08 11:14:18', '1');
INSERT INTO `indikator_penilaian` VALUES ('6', '548', '0', '2017-11-08 11:12:56', '006', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('7', '549', '0', '2017-11-08 11:13:01', '007', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('8', '550', '0', '2017-11-08 11:13:04', '008', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('9', '551', '0', '2017-11-08 11:13:06', '009', null, null, '4');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-03 10:01:08', '006', '2017-11-03 10:01:33', '2017-11-03 10:01:37', '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '531', '0', '2017-11-03 10:01:05', '005', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-03 09:59:37', '004', '2017-11-03 10:01:24', '2017-11-03 10:01:30', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-03 09:59:35', '003', '2017-11-03 10:00:03', '2017-11-03 10:01:20', '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '528', '0', '2017-11-03 09:58:16', '002', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-11-03 09:51:38', '001', '2017-11-03 09:51:54', '2017-11-03 09:58:09', '1');
INSERT INTO `indikator_penilaian` VALUES ('9', '526', '0', '2017-11-01 18:27:11', '012', null, null, '4');
INSERT INTO `indikator_penilaian` VALUES ('4', '525', '0', '2017-11-01 18:26:50', '011', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('3', '524', '0', '2017-11-01 18:22:01', '010', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('2', '523', '0', '2017-11-01 18:21:16', '009', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('8', '522', '0', '2017-11-01 18:03:05', '008', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('7', '521', '0', '2017-11-01 18:03:01', '007', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('6', '520', '0', '2017-11-01 18:02:57', '006', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '519', '0', '2017-11-01 18:02:53', '005', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-11-01 18:02:44', '004', '2017-11-01 18:05:24', '2017-11-01 18:06:08', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-01 18:02:39', '003', '2017-11-01 18:04:41', '2017-11-01 18:05:05', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-11-01 18:02:21', '002', '2017-11-01 18:03:46', '2017-11-01 18:04:31', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-11-01 18:02:12', '001', '2017-11-01 18:03:32', '2017-11-01 18:03:38', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:33:45', '013', '2017-10-27 17:33:56', '2017-10-27 17:49:51', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:33:12', '012', '2017-10-27 17:33:19', '2017-10-27 17:33:42', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:32:17', '011', '2017-10-27 17:32:30', '2017-10-27 17:33:02', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:28:29', '010', '2017-10-27 17:28:49', '2017-10-27 17:32:26', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:27:59', '009', '2017-10-27 17:28:13', '2017-10-27 17:28:40', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:22:59', '008', '2017-10-27 17:27:34', '2017-10-27 17:28:07', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 17:12:47', '007', '2017-10-27 17:12:56', '2017-10-27 17:13:19', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:49:57', '006', '2017-10-27 14:52:26', '2017-10-27 14:52:34', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:48:28', '005', '2017-10-27 14:48:40', '2017-10-27 14:49:04', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:46:29', '004', '2017-10-27 14:46:59', '2017-10-27 14:48:36', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:45:37', '003', '2017-10-27 14:46:45', '2017-10-27 14:46:54', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:42:01', '002', '2017-10-27 14:45:01', '2017-10-27 14:45:24', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-27 14:18:19', '001', '2017-10-27 14:41:37', '2017-10-27 14:41:55', '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '501', '0', '2017-10-26 17:44:29', '028', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:43:44', '027', '2017-10-26 17:43:50', '2017-10-26 17:44:27', '1');
INSERT INTO `indikator_penilaian` VALUES ('4', '499', '0', '2017-10-26 17:43:03', '026', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('3', '498', '0', '2017-10-26 17:42:32', '025', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:41:40', '024', '2017-10-26 17:41:50', '2017-10-26 17:42:28', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:40:35', '023', '2017-10-26 17:40:42', '2017-10-26 17:41:37', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:39:38', '022', '2017-10-26 17:39:44', '2017-10-26 17:40:32', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:39:05', '021', '2017-10-26 17:39:17', '2017-10-26 17:39:35', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:38:42', '020', '2017-10-26 17:38:54', '2017-10-26 17:39:12', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:38:01', '019', '2017-10-26 17:38:12', '2017-10-26 17:38:51', '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '491', '0', '2017-10-26 17:35:03', '018', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:34:04', '017', '2017-10-26 17:34:11', '2017-10-26 17:34:22', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:23:31', '016', '2017-10-26 17:23:59', '2017-10-26 17:24:06', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:23:02', '015', '2017-10-26 17:24:13', '2017-10-26 17:24:17', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:22:20', '014', '2017-10-26 17:22:31', '2017-10-26 17:22:46', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:20:42', '013', '2017-10-26 17:21:02', '2017-10-26 17:21:11', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:20:21', '012', '2017-10-26 17:20:25', '2017-10-26 17:20:31', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:19:51', '011', '2017-10-26 17:19:58', '2017-10-26 17:20:17', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:17:30', '010', '2017-10-26 17:19:05', '2017-10-26 17:19:10', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:17:07', '009', '2017-10-26 17:17:12', '2017-10-26 17:17:17', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 17:16:49', '008', '2017-10-26 17:16:54', '2017-10-26 17:16:59', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-26 17:15:50', '007', '2017-10-26 17:16:20', '2017-10-26 17:16:25', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-26 17:14:07', '006', '2017-10-26 17:15:55', '2017-10-26 17:16:17', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-26 15:35:36', '005', '2017-10-26 16:05:39', '2017-10-26 16:05:53', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 15:35:33', '004', '2017-10-26 17:14:41', '2017-10-26 17:14:51', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 15:35:30', '003', '2017-10-26 17:14:14', '2017-10-26 17:14:28', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-26 15:33:09', '002', '2017-10-26 15:33:17', '2017-10-26 15:35:26', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-26 14:36:06', '001', '2017-10-26 14:36:25', '2017-10-26 14:44:31', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-24 17:11:08', '005', '2017-10-24 17:15:03', '2017-10-24 17:15:13', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-24 17:10:15', '004', '2017-10-24 17:10:23', '2017-10-24 17:10:36', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-24 17:09:54', '003', '2017-10-24 17:10:03', '2017-10-24 17:10:09', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-24 17:09:20', '002', '2017-10-24 17:09:25', '2017-10-24 17:09:32', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-24 17:08:29', '001', '2017-10-24 17:08:35', '2017-10-24 17:08:58', '1');
INSERT INTO `indikator_penilaian` VALUES ('5', '468', '0', '2017-10-23 13:30:11', '006', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('4', '467', '0', '2017-10-23 13:28:17', '005', null, null, '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2017-10-23 10:49:29', '004', '2017-10-23 10:51:50', '2017-10-23 10:52:04', '1');
INSERT INTO `indikator_penilaian` VALUES ('3', '465', '0', '2017-10-23 10:49:25', '003', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('2', '459', '0', '2017-10-09 17:13:50', '010', null, null, '3');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-10 08:49:05', '001', '2017-10-10 08:49:14', '2017-10-10 08:50:03', '5');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-10 13:30:36', '002', '2017-10-10 13:31:18', '2017-10-10 13:31:54', '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '462', '0', '2017-10-10 13:32:22', '003', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-23 10:47:46', '001', '2017-10-23 13:31:23', '2017-10-23 13:31:34', '1');
INSERT INTO `indikator_penilaian` VALUES ('2', '464', '0', '2017-10-23 10:47:53', '002', null, null, '2');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 14:42:52', '006', '2017-10-09 14:43:24', '2017-10-09 14:43:39', '5');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 16:34:40', '007', '2017-10-09 16:34:55', '2017-10-09 16:35:11', '4');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 16:35:27', '008', '2017-10-09 16:36:23', '2017-10-09 16:36:30', '1');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2017-10-09 17:00:29', '009', '2017-10-09 17:00:40', '2017-10-09 17:00:52', '3');

-- ----------------------------
-- Table structure for `log_akses`
-- ----------------------------
DROP TABLE IF EXISTS `log_akses`;
CREATE TABLE `log_akses` (
  `tanggal` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_akses
-- ----------------------------
INSERT INTO `log_akses` VALUES ('2016-11-08 11:42:44', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 12:01:15', '3', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 12:01:32', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 13:55:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:01:57', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:02:57', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:06:37', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:21:40', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:25:45', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:27:30', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:32:38', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:38:49', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:40:00', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:43:51', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:44:15', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:25:11', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:45:46', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:46:07', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:50:22', '5', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 16:04:51', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 16:19:02', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:27:37', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:32:32', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:43:53', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:45:34', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 18:15:18', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:44:06', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:55:09', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:57:46', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:59:37', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 20:10:17', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-09 09:17:06', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-09 15:27:59', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-09 20:29:28', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-15 12:39:31', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-17 14:57:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-18 17:30:46', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-24 10:57:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-18 14:38:31', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-18 15:28:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-18 15:38:05', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-18 15:44:33', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-19 09:43:52', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-22 10:49:34', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-22 11:21:54', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-28 09:13:27', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-29 09:03:44', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-29 09:40:23', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-09-29 11:21:27', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-04 11:32:26', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-09 09:37:30', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-09 13:12:11', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-10 08:48:27', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-10 13:26:47', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-23 10:43:05', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-23 10:47:37', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-24 17:08:09', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-26 10:33:33', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-10-27 09:02:29', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-01 17:40:57', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-03 09:51:23', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 09:23:03', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 09:42:53', '4', '2');
INSERT INTO `log_akses` VALUES ('2017-11-08 09:44:19', '4', '2');
INSERT INTO `log_akses` VALUES ('2017-11-08 09:50:11', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:19:23', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:47:50', '5', '3');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:48:30', '5', '3');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:49:39', '6', '4');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:50:02', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 10:56:52', '4', '2');
INSERT INTO `log_akses` VALUES ('2017-11-08 11:13:49', '5', '3');
INSERT INTO `log_akses` VALUES ('2017-11-08 13:18:17', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-08 13:19:23', '4', '2');
INSERT INTO `log_akses` VALUES ('2017-11-09 09:01:03', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-09 14:16:05', '4', '2');
INSERT INTO `log_akses` VALUES ('2017-11-09 14:26:26', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-11-10 08:54:38', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-12-18 10:27:40', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-12-18 12:16:41', '3', '1');
INSERT INTO `log_akses` VALUES ('2017-12-18 14:18:23', '3', '11');
INSERT INTO `log_akses` VALUES ('2017-12-19 11:44:34', '3', '1');
INSERT INTO `log_akses` VALUES ('2018-01-12 14:22:54', '3', '1');

-- ----------------------------
-- Table structure for `ref_indikator_cat`
-- ----------------------------
DROP TABLE IF EXISTS `ref_indikator_cat`;
CREATE TABLE `ref_indikator_cat` (
  `indikator_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `indikator_category_name` varchar(255) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`indikator_category_id`),
  KEY `idx_indikator_cat` (`indikator_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_indikator_cat
-- ----------------------------
INSERT INTO `ref_indikator_cat` VALUES ('1', 'Sangat Puas', '', 'small-box bg-green');
INSERT INTO `ref_indikator_cat` VALUES ('2', 'Puas', '', 'small-box bg-yellow');
INSERT INTO `ref_indikator_cat` VALUES ('3', 'Tidak Puas', '', 'small-box bg-red');

-- ----------------------------
-- Table structure for `ref_jenisantrian`
-- ----------------------------
DROP TABLE IF EXISTS `ref_jenisantrian`;
CREATE TABLE `ref_jenisantrian` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_jenisantrian
-- ----------------------------
INSERT INTO `ref_jenisantrian` VALUES ('-1', '-');
INSERT INTO `ref_jenisantrian` VALUES ('1', 'PBB');
INSERT INTO `ref_jenisantrian` VALUES ('2', 'BPHTB');
INSERT INTO `ref_jenisantrian` VALUES ('3', 'PRINT OUT / SALINAN');
INSERT INTO `ref_jenisantrian` VALUES ('4', 'CUSTOMER SERVICE');
INSERT INTO `ref_jenisantrian` VALUES ('5', 'BJB');

-- ----------------------------
-- Table structure for `res_ref_instansi`
-- ----------------------------
DROP TABLE IF EXISTS `res_ref_instansi`;
CREATE TABLE `res_ref_instansi` (
  `instansi_id` int(10) NOT NULL AUTO_INCREMENT,
  `id_ref_jenisantrian` int(11) DEFAULT NULL,
  `instansi_name` varchar(255) NOT NULL,
  `instansi_address` varchar(255) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `instansi_email` varchar(200) NOT NULL,
  `instansi_phone` varchar(20) NOT NULL,
  `instansi_fax` varchar(20) DEFAULT NULL,
  `instansi_city` varchar(200) DEFAULT NULL,
  `instansi_province` varchar(200) DEFAULT NULL,
  `instansi_country` varchar(200) DEFAULT NULL,
  `aktif` bit(1) DEFAULT b'1',
  `kategori` tinyint(4) DEFAULT NULL,
  `stat` tinyint(4) DEFAULT '0',
  `stat_open` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`instansi_id`),
  KEY `idxrefisntansi` (`instansi_id`,`instansi_name`,`aktif`,`kategori`,`id_ref_jenisantrian`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of res_ref_instansi
-- ----------------------------
INSERT INTO `res_ref_instansi` VALUES ('1', '2', 'LOKET 1', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '1', '1');
INSERT INTO `res_ref_instansi` VALUES ('2', '2', 'LOKET 2', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('3', '3', 'LOKET 3', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('4', '4', 'LOKET 4', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('5', '3', 'LOKET 5', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('6', '4', 'LOKET 6', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('7', '5', 'LOKET 7', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('8', '5', 'LOKET 8', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('9', '3', 'LOKET 9', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('10', '4', 'LOKET 10', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('11', '5', 'LOKET 11', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8322 871', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0', '1');
INSERT INTO `res_ref_instansi` VALUES ('12', null, 'LOKET 12', null, null, '', '', null, null, null, null, '', null, '0', '0');

-- ----------------------------
-- Table structure for `res_sec_login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_login_attempts`;
CREATE TABLE `res_sec_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for `res_sec_module`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_module`;
CREATE TABLE `res_sec_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) DEFAULT NULL,
  `module_alias` varchar(100) DEFAULT NULL,
  `module_url` varchar(255) DEFAULT NULL,
  `mod_icon_cls` varchar(50) DEFAULT NULL,
  `mod_seq` int(2) DEFAULT '0',
  `module_pid` int(11) DEFAULT NULL,
  `publish` bit(1) DEFAULT b'1',
  `mod_group` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `idx_uniq` (`module_alias`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_module
-- ----------------------------
INSERT INTO `res_sec_module` VALUES ('1', 'Dashboard', 'MOD_HOME', 'main/dashboard', 'fa-fw fa-dashboard', '1', null, '', null);
INSERT INTO `res_sec_module` VALUES ('2', 'Utility', 'MOD_UTILITY', '#', 'fa-share', '99', '0', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('3', 'Daftar User', 'MOD_USERMANAGE', 'utility/user_manage', 'fa-user', '1', '2', '', null);
INSERT INTO `res_sec_module` VALUES ('41', 'Indikator Penilaian', 'MOD_CORE', 'indikatorcore/Cindikatorcore', 'fa-file', '1', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('7', 'Referensi', 'MOD_REF', '#', 'fa-gears', '5', null, '', 'reference');
INSERT INTO `res_sec_module` VALUES ('39', 'Pengisian 360', 'MOD_PENILAIAN360', 'penilaian/Cpenilaian', 'fa-list', '5', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('36', 'Setting Nilai', 'MOD_ALUMNI', 'alumni/alumni', 'fa-file', '4', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('40', 'Referensi Indikator 360', 'MOD_REF_INDIKATOR', 'indikator360/Cindikator360', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('14', 'Daftar Loket', 'MOD_REF_INSTANSI', 'reference/ref_instansi', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('15', 'Referensi Penilaian', 'MOD_FPP', '#', 'fa-fw fa-files-o', '3', '0', '', 'main');
INSERT INTO `res_sec_module` VALUES ('32', 'Module Management', 'MOD_MODULEMANAGE', 'utility/Module_manage', 'fa-file-archive-o', '3', '2', '', null);
INSERT INTO `res_sec_module` VALUES ('18', 'Setting Penilai', 'MOD_SETTING_PENILAI', 'setting_penilai/setting_penilai', 'fa-file', '2', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('38', 'Thn.Anggaran', 'MOD_TAHUNANGGARAN', 'thang/Cthang', 'fa-circle-o', '4', '21', '', '');
INSERT INTO `res_sec_module` VALUES ('21', 'Data Referensi', 'MOD_MASTER', '#', 'fa-database', '2', '0', '', '');
INSERT INTO `res_sec_module` VALUES ('22', 'Data Karyawan', 'MOD_DATA_KARYAWAN', 'data_karyawan/Cdata_karyawan', 'fa-circle-o', '1', '21', '', '');
INSERT INTO `res_sec_module` VALUES ('23', 'Log User Loket', 'MOD_LOG', 'log/Clog', 'fa-circle-o', '3', '2', '', '');
INSERT INTO `res_sec_module` VALUES ('24', 'Laporan', 'MOD_LAP', '#', 'fa-fw fa-dashboard', '4', '0', '', '');
INSERT INTO `res_sec_module` VALUES ('25', 'Laporan Penilaian', 'MOD_LAP_PEN', 'lap_data_anak/lap_data_anak', 'fa-list', '1', '24', '', '');
INSERT INTO `res_sec_module` VALUES ('26', 'Laporan', 'MOD_LAP_KEPUASAN', 'laporankepuasan/Laporankepuasan', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('27', 'Laporan Karyawan', 'MOD_LAP_GERAK_ANAK', 'lap_gerak_anak/lap_gerak_anak', 'fa-list', '3', '24', '', '');
INSERT INTO `res_sec_module` VALUES ('29', 'Pengisian Outcome Data Anak', 'MOD_OUTCOME_HIST', 'outcome/history', 'fa-file', '1', '28', '', 'outcome');
INSERT INTO `res_sec_module` VALUES ('30', 'Indikator Penilaian', 'MOD_PEN_BALITA', 'pendaftaranbalita/Pendaftaranbalita', 'fa-file', '1', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('31', 'Skala Penilaian', 'MOD_PEN_COTA', 'pendaftarancota/Pendaftarancota', 'fa-file', '3', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('33', 'Role Management', 'MOD_ROLEMANAGE', 'utility/role_manage', 'fa-file-archive-o', '4', '2', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('34', 'Role Akses', 'MOD_PRIVMANAGE', 'utility/Privilege_manage', 'fa-file-archive-o', '5', '2', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('42', 'Data Penilai', 'MOD_DATA_PENILAI', 'data_penilai/Cdata_penilai', 'fa-file', '5', '15', '', null);
INSERT INTO `res_sec_module` VALUES ('43', 'Daftar User CS', 'MODUSRCS', 'utility/Cs_manage', 'fa-user', '2', '2', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('44', 'Cetak Struk', 'MOD_CETAKSTRUK', 'main/cetakstruk', 'fa-print', '1', null, '', null);

-- ----------------------------
-- Table structure for `res_sec_role`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_role`;
CREATE TABLE `res_sec_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `role_alias` varchar(100) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`role_id`),
  KEY `idx_role` (`role_id`,`role_name`,`role_alias`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_role
-- ----------------------------
INSERT INTO `res_sec_role` VALUES ('1', 'Super Administrator', 'superadmin', '');
INSERT INTO `res_sec_role` VALUES ('2', 'Administrator', 'admin', '');
INSERT INTO `res_sec_role` VALUES ('3', 'Loket', 'loket', '');

-- ----------------------------
-- Table structure for `res_sec_role_priv`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_role_priv`;
CREATE TABLE `res_sec_role_priv` (
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `allow_view` bit(1) DEFAULT NULL,
  `allow_new` bit(1) DEFAULT b'1',
  `allow_edit` bit(1) DEFAULT b'1',
  `allow_delete` bit(1) DEFAULT b'1',
  `allow_print` bit(1) DEFAULT b'0',
  PRIMARY KEY (`role_id`,`module_id`),
  KEY `idxrolepriv` (`role_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_role_priv
-- ----------------------------
INSERT INTO `res_sec_role_priv` VALUES ('1', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '8', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '9', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '10', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '11', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '12', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '13', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '16', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '8', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '9', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '10', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '11', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '12', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '13', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '16', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '32', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '33', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '34', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '33', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '34', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '42', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '32', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '43', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '43', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '43', '', '', '', '', '');

-- ----------------------------
-- Table structure for `res_sec_user`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_user`;
CREATE TABLE `res_sec_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `ip_address` varchar(20) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `password_changed` tinyint(4) DEFAULT NULL,
  `flag_penilaian` int(11) DEFAULT '0',
  `no_antrian` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_users` (`id`,`username`,`full_name`,`email`,`password`,`role_id`,`active`,`ip_address`,`flag_penilaian`,`no_antrian`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of res_sec_user
-- ----------------------------
INSERT INTO `res_sec_user` VALUES ('-1', '', '', '', '', '0', null, null, '', null, null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('1', 'nuriyanto', 'nuriyanto', 'nuriyanto@gmail.com', '$2a$08$7QDN21vCWdNcBO3hN7XxoeFnDShOpPnwJv38j0Va7P6XBaQ6q85q.', '2', '2015-12-22 14:43:44', '2016-12-09 21:27:41', '', null, null, null, '', null, '', null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('2', 'admin', 'Developer', 'developer@gmail.com', '$2a$08$7QDN21vCWdNcBO3hN7XxoeFnDShOpPnwJv38j0Va7P6XBaQ6q85q.', '2', '2015-12-22 16:19:22', '2018-07-11 06:11:44', '', null, null, null, '', null, 'agl27CwebosHvOkyTQV5YO', null, '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `res_sec_user` VALUES ('3', 'cs1', 'Susan', 'cs1@gmail.com', '$2a$08$alx/LDOE35vdbPl6N5YAFeq4pTIjcsgCIcifTz1Vhue1W2vXzTWpW', '3', '2016-08-15 08:23:56', '2018-01-12 14:22:54', '', '::1', null, null, null, null, 'Nm0G68SMKIUnxvAi5UEhWu', null, '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `res_sec_user` VALUES ('4', 'cs2', 'Asep ', 'cs2@gmail.com', '$2a$08$Qq4GJJxjuWC6/VWu5aQsB.vD3m/V/NF8sdAh5qV5o6bjJA2FkJ3PW', '3', '2016-08-23 15:25:30', '2017-11-09 14:16:05', '', '::1', null, null, null, null, 'MkmZAOW18d4q6zn4bdU2X.', null, '1', '003 ', '2017-11-09 14:16:38', '0000-00-00 00:00:00');
INSERT INTO `res_sec_user` VALUES ('5', 'cs3', 'Agus', 'cs3@gmail.com', '$2a$08$.7Bc5uW6MkFkQUvExFkg4OdFY7csywXLFeNxweWW8VfFH8wjslc2W', '3', '2016-08-29 15:34:03', '2017-11-08 11:13:49', '', '::1', null, null, null, null, 'A5BoMVB4qfEnlN0FHXvvIe', null, '1', '008 ', '2017-11-08 11:13:55', '0000-00-00 00:00:00');
INSERT INTO `res_sec_user` VALUES ('6', 'cs4', 'Dewi', 'cs4@gmail.com', '$2a$08$2dRfPmmD7rRI2ltWJYOvru.4Ho2lTaJNZserbj7jpyPNsYEIbWh5e', '3', '2016-08-29 16:00:06', '2017-11-08 10:49:39', '', '::1', null, null, null, null, null, null, '1', '009 ', '2017-11-08 11:13:11', '2017-11-08 11:12:13');
INSERT INTO `res_sec_user` VALUES ('7', 'dispenda', 'Admin Dispenda', 'admin@dispenda.com', '$2a$08$4UIhu9PKxzapAbn3zV9MC.mGMvaxMcIe3CdUEEmEHi4/3dsnyMUzW', '2', '2016-11-02 17:02:23', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('8', 'cs5', 'Dede', 'cs5@gmail.com', '$2a$08$Z.rZTjDFXfuA31gIpcL1d.xd7cealrAW5VUa1gqwZSOwDL9on4x0u', '3', '2016-11-02 17:04:32', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('9', 'cs6', 'Heru', 'cs6@gmail.com', '$2a$08$Scc/fc24MOehfbdoUoCs8ejumpxUnKCZGa3A/.XonUsa8MmndVvcK', '3', '2016-11-02 17:12:54', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('10', 'cs7', 'Asep', 'cs7@gmail.com', '$2a$08$.9eg3rZsCIzGmFiyZuUb3u34VSo6MFlTcGC.8wSBjyCaKoNYa8c8q', '3', '2016-11-02 17:16:54', '2016-11-02 17:20:28', '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('11', 'cs8', 'Usep', 'cs@gmail.com', '$2a$08$1Cce.kVcvpHiapBYM2mfpuTPvOFJFi5f/NXD8tvfpOwuFKm8R36qS', '3', '2016-11-02 17:24:10', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('12', 'dispenda2', 'Admin Dispenda 2', 'dispenda2@gmai.com', '$2a$08$4g2UxAJYPm/S/3Z6Sk2wMOPxyW6JNMNVfh6EAKVNmCIOwiITRsugi', '2', '2016-11-02 17:30:33', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('13', 'cs9', 'Dendi', 'cs9@gmail.com', '$2a$08$f/9yFCT260ySmvpxeQSB3OydXDuQQyBHq4mkEqm78sAcOJaV8Cq/O', '3', '2016-11-02 17:36:08', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('14', 'cs10', 'Sukmara', 'cs10@gmail.com', '$2a$08$sDiESQyup7SaLFHF8e8ZMeg5FOQO9MUtck/CZ/RRwk9hTd4caaQGm', '3', '2016-11-02 17:38:04', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('15', 'cs11', 'Tatang', 'cs11@gmail.com', '$2a$08$PX3jMA56uglju9a44WUHzuEiIbbkFi4TPlnxlF2MBhyAyEzkKn5MG', '3', '2016-11-03 08:40:55', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('16', 'cs12', 'Hikmat', 'cs12@gmail.com', '$2a$08$1b0cYfKulqlGLhI40s1RNup..8M5VV3ZwIBkvZfqc./A3OJX3pMNG', '3', '2016-11-03 08:42:13', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('17', 'cs13', 'Yudi', 'cs13@gmail.com', '$2a$08$vV4CIS3T9WSxRGYgBcb.EuCQXvkNlH6g4l1nNH0.6xnhlRrqJ1/rK', '3', '2016-11-03 08:43:31', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('18', 'cs14', 'Lucy', 'cs14@gmail.com', '$2a$08$WsaFdzqyPN5azr..Bgh2rezdJ3ncZSYm0wIjC0pTDgOssW0/4zrNG', '3', '2016-11-03 09:00:29', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('19', 'cs15', 'Totong', 'cs15@gmail.com', '$2a$08$SMeDTPTJK2qVr4Zs5iwqPu17Kdyf1r3dOhW6ANSkjre7U0rk0rTAS', '3', '2016-11-03 09:11:46', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('20', 'loket1', 'Loket 1', 'loket1@gmail.com', '$2a$08$206MlJV89.ANa9biP.0nROQz0Ziv7oKLOS60NpYBu3cpGJialAptG', '3', '2017-09-18 14:34:57', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);

-- ----------------------------
-- Table structure for `res_sec_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_user_role`;
CREATE TABLE `res_sec_user_role` (
  `userid` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`userid`,`role_id`),
  KEY `idxrole` (`userid`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_user_role
-- ----------------------------
INSERT INTO `res_sec_user_role` VALUES ('1', '2');
INSERT INTO `res_sec_user_role` VALUES ('2', '2');
INSERT INTO `res_sec_user_role` VALUES ('3', '3');
INSERT INTO `res_sec_user_role` VALUES ('4', '3');
INSERT INTO `res_sec_user_role` VALUES ('5', '3');
INSERT INTO `res_sec_user_role` VALUES ('6', '3');
INSERT INTO `res_sec_user_role` VALUES ('7', '2');
INSERT INTO `res_sec_user_role` VALUES ('8', '0');
INSERT INTO `res_sec_user_role` VALUES ('9', '0');
INSERT INTO `res_sec_user_role` VALUES ('10', '3');
INSERT INTO `res_sec_user_role` VALUES ('11', '0');
INSERT INTO `res_sec_user_role` VALUES ('12', '2');
INSERT INTO `res_sec_user_role` VALUES ('13', '0');
INSERT INTO `res_sec_user_role` VALUES ('14', '3');
INSERT INTO `res_sec_user_role` VALUES ('15', '0');
INSERT INTO `res_sec_user_role` VALUES ('16', '0');
INSERT INTO `res_sec_user_role` VALUES ('17', '0');
INSERT INTO `res_sec_user_role` VALUES ('18', '0');
INSERT INTO `res_sec_user_role` VALUES ('19', '0');
INSERT INTO `res_sec_user_role` VALUES ('20', '0');

-- ----------------------------
-- Table structure for `res_user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `res_user_profile`;
CREATE TABLE `res_user_profile` (
  `userid` int(11) NOT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `tgl_lahir` datetime DEFAULT NULL,
  `alamat` text CHARACTER SET utf8,
  `no_telp` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_kelamin` tinyint(1) DEFAULT NULL,
  `asal_daerah` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `agama` tinyint(4) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  KEY `idxprofile` (`userid`,`instansi_id`,`address`,`phone`,`photo`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_user_profile
-- ----------------------------
INSERT INTO `res_user_profile` VALUES ('1', '1', 'Bandung', '08xxxxxxxxxx', 'avatar1.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('2', '1', 'Bogor', '087823339007', 'avatar1.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('3', '1', 'Bogor', '087823339007', 'avatar2.png', '0000-00-00 00:00:00', null, null, null, '', null, '3');
INSERT INTO `res_user_profile` VALUES ('4', '2', 'Bogor', '087823339007', 'avatar1.png', '0000-00-00 00:00:00', null, null, null, '', null, '3');
INSERT INTO `res_user_profile` VALUES ('5', '3', 'Bogor', '08782390980808', 'avatar1.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('6', '4', 'Bogor', '085722609190', 'avatar4.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('7', '1', 'Kopo', '082783312312', '160x160.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('8', '1', 'Kopo', '082783312312', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('9', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('10', '7', 'Kopo', '087823339007', '160x160.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('11', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('12', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('13', '1', 'Kopo', '087823339019', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('14', '1', 'Kopo', '087823339019', '160x160.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('15', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('16', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('17', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('18', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('19', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('20', '1', '-', '-', '', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `tbantrian`
-- ----------------------------
DROP TABLE IF EXISTS `tbantrian`;
CREATE TABLE `tbantrian` (
  `instansi_id` int(11) DEFAULT NULL,
  `jml_pelayanan` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbantrian
-- ----------------------------
INSERT INTO `tbantrian` VALUES ('1', '1');
INSERT INTO `tbantrian` VALUES ('2', '1');
INSERT INTO `tbantrian` VALUES ('3', '1');
INSERT INTO `tbantrian` VALUES ('4', '1');
INSERT INTO `tbantrian` VALUES ('5', '1');
INSERT INTO `tbantrian` VALUES ('6', '1');
INSERT INTO `tbantrian` VALUES ('7', '0');
INSERT INTO `tbantrian` VALUES ('8', '0');
INSERT INTO `tbantrian` VALUES ('9', '0');
INSERT INTO `tbantrian` VALUES ('10', '0');
INSERT INTO `tbantrian` VALUES ('11', '0');

-- ----------------------------
-- Table structure for `t_queue`
-- ----------------------------
DROP TABLE IF EXISTS `t_queue`;
CREATE TABLE `t_queue` (
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_antrian` varchar(10) NOT NULL DEFAULT '',
  `loket` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`created_date`,`loket`,`no_antrian`),
  KEY `idx_queue` (`created_date`,`no_antrian`,`loket`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_queue
-- ----------------------------
INSERT INTO `t_queue` VALUES ('2017-11-08 11:13:11', '009 ', 'LOKET 4');
INSERT INTO `t_queue` VALUES ('2017-11-08 11:13:55', '008 ', 'LOKET 3');
INSERT INTO `t_queue` VALUES ('2017-11-09 14:16:38', '003 ', 'LOKET 2');
INSERT INTO `t_queue` VALUES ('2017-12-18 14:11:44', '001 ', 'LOKET 1');
INSERT INTO `t_queue` VALUES ('2017-12-18 14:18:29', '016 ', 'LOKET 11');

-- ----------------------------
-- Table structure for `t_queue_sound`
-- ----------------------------
DROP TABLE IF EXISTS `t_queue_sound`;
CREATE TABLE `t_queue_sound` (
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `no_antrian` varchar(10) NOT NULL DEFAULT '',
  `loket` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`created_date`,`loket`,`no_antrian`),
  KEY `idx_queue_sound` (`created_date`,`no_antrian`,`loket`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_queue_sound
-- ----------------------------

-- ----------------------------
-- View structure for `v_sec_role_priv`
-- ----------------------------
DROP VIEW IF EXISTS `v_sec_role_priv`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sec_role_priv` AS select `srp`.`role_id` AS `role_id`,`srp`.`module_id` AS `module_id`,`srp`.`allow_view` AS `allow_view`,`srp`.`allow_new` AS `allow_new`,`srp`.`allow_edit` AS `allow_edit`,`srp`.`allow_delete` AS `allow_delete`,`srp`.`allow_print` AS `allow_print`,`sm`.`module_name` AS `module_name`,`sm`.`module_alias` AS `module_alias`,`sm`.`module_url` AS `module_url`,`sm`.`module_pid` AS `module_pid`,`sm`.`mod_icon_cls` AS `mod_icon_cls`,`sm`.`mod_seq` AS `mod_seq`,`sm`.`publish` AS `publish`,`sm`.`mod_group` AS `mod_group` from ((`res_sec_role_priv` `srp` left join `res_sec_module` `sm` on((`srp`.`module_id` = `sm`.`module_id`))) left join `res_sec_role` `sr` on((`srp`.`role_id` = `sr`.`role_id`))) ;

-- ----------------------------
-- Procedure structure for `FN_NoAntrian`
-- ----------------------------
DROP PROCEDURE IF EXISTS `FN_NoAntrian`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `FN_NoAntrian`(p_id_ref_jenisantrian int)
BEGIN 

DECLARE autono VARCHAR(11); /* ID to create */
DECLARE v_max_id  VARCHAR(11);  /* Max ID */
DECLARE v_max_tmp_id  VARCHAR(11);  /* Max ID */
DECLARE p_alias VARCHAR(15);
DECLARE strtemp VARCHAR(10);
DECLARE v_jenis_antrian VARCHAR(20);
DECLARE strval , v_instansi_id INT;

		 /*CREATE TEMPORARY TABLE IF NOT EXISTS tbAntrian(
					instansi_id INT,
					jml_pelayanan INT
			);*/
		
		DELETE FROM tbAntrian;
		-- Insert tbAntrian
		INSERT INTO tbAntrian (instansi_id, jml_pelayanan)
		SELECT a.instansi_id, 
		(SELECT COUNT(x1.no_antrian) FROM indikator_penilaian x1 
		WHERE DATE_FORMAT(x1.created_date, '%Y-%m-%d') =  DATE_FORMAT(NOW(), '%Y-%m-%d') AND a.instansi_id=x1.instansi_id)
		AS jml_pelayanan
		FROM res_ref_instansi a WHERE a.stat_open=1;
		-- End Insert tbAntrian

		SET v_instansi_id = (SELECT instansi_id FROM tbAntrian WHERE jml_pelayanan=(SELECT MIN(jml_pelayanan) FROM tbAntrian LIMIT 1) LIMIT 1);
		SET v_jenis_antrian = (SELECT nama FROM ref_jenisantrian WHERE id=p_id_ref_jenisantrian);

    SET v_max_id= (SELECT MAX(no_antrian) 
									 FROM indikator_penilaian 
                   WHERE DATE_FORMAT(created_date, '%Y-%m-%d') =  DATE_FORMAT(NOW(), '%Y-%m-%d'));

		IF v_max_id IS NOT NULL THEN
				IF v_max_id='099' THEN
					SET autono = '100';
				ELSEIF LEFT(v_max_id,1) <> '0'  THEN
					SET autono = v_max_id + 1;
				ELSE 	
					SET strtemp = SUBSTR(v_max_id, 1, 3);
					SET strval = strtemp + 1;
					SET autono = CONCAT((SUBSTR('00', 1, 3 - LENGTH(strval))) , strval);
				END IF;
		ELSE
				-- SET p_alias = (SELECT DATE_FORMAT(NOW(), '%Y-%m-%d'));
				SET autono = CONCAT('001');
		END IF;

		INSERT INTO indikator_penilaian (instansi_id, no_antrian, created_date, id_ref_jenisantrian) VALUES
		(v_instansi_id, autono, NOW(), p_id_ref_jenisantrian );
		 
	  SELECT autono, v_instansi_id AS loket, v_jenis_antrian AS jenis_antrian;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `sp_SecRoleModule_get`
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_SecRoleModule_get`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_SecRoleModule_get`(IN prole_id int)
BEGIN 
 
		-- CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul as (select * from table1 where 1=0);		
		-- CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul(
	CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul(
					module_id INT, module_name varchar(200), module_alias varchar(200),
					module_pid INT, module_url varchar(255),  
					mod_icon_cls varchar(100), mod_seq INT, mod_group varchar(100)
			);
		

		TRUNCATE TABLE tbSecModul;

		/* insert role modul */
		INSERT INTO tbSecModul(module_id, module_name, module_alias, module_pid, module_url, mod_icon_cls, mod_seq, mod_group)
			SELECT module_id, module_name, module_alias, COALESCE(module_pid,0), LTRIM(RTRIM(module_url)),
				mod_icon_cls, mod_seq, mod_group
			FROM v_sec_role_priv
			WHERE publish=1 AND allow_view=1 AND role_id= prole_id
			ORDER BY module_pid, mod_seq, module_id;
 
		SELECT * FROM tbSecModul;

END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `FN_NoAntrian`
-- ----------------------------
DROP FUNCTION IF EXISTS `FN_NoAntrian`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FN_NoAntrian`(p_alias VARCHAR (2)) RETURNS varchar(11) CHARSET utf8
BEGIN
DECLARE autono VARCHAR(11); /* ID to create */
DECLARE v_max_id  VARCHAR(11);  /* Max ID */
DECLARE v_max_tmp_id  VARCHAR(11);  /* Max ID */
DECLARE strtemp VARCHAR(10);
DECLARE strval INT;

    SET v_max_id= (SELECT MAX(no_antrian) 
									 FROM indikator_penilaian 
                   WHERE DATE_FORMAT(created_date, '%Y-%m-%d') =  DATE_FORMAT(NOW(), '%Y-%m-%d'));

		IF v_max_id IS NOT NULL THEN
				IF v_max_id='099' THEN
					SET autono = '100';
				ELSEIF LEFT(v_max_id,1) <> '0'  THEN
					SET autono = v_max_id + 1;
				ELSE 	
					SET strtemp = SUBSTR(v_max_id, 1, 3);
					SET strval = strtemp + 1;
					SET autono = CONCAT((SUBSTR('00', 1, 3 - LENGTH(strval))) , strval);
				END IF;
		ELSE
				SET autono = CONCAT(p_alias,'001');
		END IF;
RETURN autono;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `SPLIT_STR`
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(

  x LONGTEXT,

  delim VARCHAR(12),

  pos INT

) RETURNS longtext CHARSET utf8
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),

       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),

       delim, '')
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `ufn_SplitStr`
-- ----------------------------
DROP FUNCTION IF EXISTS `ufn_SplitStr`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ufn_SplitStr`(
  x LONGTEXT,
  delim VARCHAR(12),
  pos INT
) RETURNS longtext CHARSET utf8
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '')
;;
DELIMITER ;
