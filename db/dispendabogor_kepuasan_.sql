/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50141
Source Host           : 127.0.0.1:3306
Source Database       : dispendabogor_kepuasan

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2017-01-18 14:37:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `audit_trail`
-- ----------------------------
DROP TABLE IF EXISTS `audit_trail`;
CREATE TABLE `audit_trail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `menu` varchar(20) DEFAULT NULL,
  `activity` varchar(100) DEFAULT NULL,
  `activity_date` datetime DEFAULT NULL,
  `ipaddress` varchar(30) DEFAULT NULL,
  `browser` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of audit_trail
-- ----------------------------

-- ----------------------------
-- Table structure for `indikator_penilaian`
-- ----------------------------
DROP TABLE IF EXISTS `indikator_penilaian`;
CREATE TABLE `indikator_penilaian` (
  `instansi_id` int(11) NOT NULL,
  `indikator_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `no_antrian` varchar(30) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`created_date`,`no_antrian`,`instansi_id`,`indikator_category_id`,`user_id`),
  KEY `idx_indikator_cat` (`indikator_category_id`,`user_id`,`created_date`,`no_antrian`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of indikator_penilaian
-- ----------------------------
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:50:57', '1', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:10', '2', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:19', '3', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 17:51:31', '4', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 17:51:47', '5', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-10-31 17:52:01', '7', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:37:09', '45', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:38:03', '47', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-10-31 18:38:21', '48', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 18:38:46', '49', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:39:25', '50', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-10-31 18:39:50', '51', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-10-31 18:40:23', '51', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:21:55', '1', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:22:08', '2', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:22:26', '3', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:23:34', '4', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:26:29', '5', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:26:48', '6', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:04', '7', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:27:18', '8', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:36', '9', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:27:58', '10', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:28:37', '11', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 16:29:33', '12', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:31:33', '13', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:32:27', '12', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:39:23', '13', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:15', '14', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:30', '16', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:41:45', '17', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 16:42:10', '18', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:45:01', '19', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:45:46', '20', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:53:22', '21', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:53:40', '22', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 16:55:33', '23', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 16:56:00', '24', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:00:08', '25', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:05:50', '26', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:10:23', '27', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-01 17:16:04', '28', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:18:50', '29', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:19:06', '30', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:20:53', '31', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-01 17:21:18', '32', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-01 17:21:46', '33', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:04:13', '002', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:04:52', '005', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:42:47', '006', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:44:29', '007', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-02 13:52:23', '009', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 10:58:22', '001', null, null);
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:00:59', '002', '2016-11-03 11:00:52', '2016-11-03 11:00:59');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:32:59', '003', '2016-11-03 11:32:50', '2016-11-03 11:32:59');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:33:23', '004', '2016-11-03 11:33:10', '2016-11-03 11:33:23');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:46:29', '005', '2016-11-03 11:45:54', '2016-11-03 11:46:29');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 11:52:09', '006', '2016-11-03 11:51:57', '2016-11-03 11:52:09');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 13:48:09', '007', '2016-11-03 13:48:00', '2016-11-03 13:48:09');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:23:09', '008', '2016-11-03 14:22:07', '0000-00-00 00:00:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:24:35', '009', '2016-11-03 14:24:16', '2016-11-03 14:23:09');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:25:51', '010', '2016-11-03 14:25:34', '2016-11-03 14:24:35');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:26:15', '011', '2016-11-03 14:26:07', '2016-11-03 14:25:51');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 14:26:58', '012', '2016-11-03 14:26:40', '2016-11-03 14:26:15');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:27:20', '013', '2016-11-03 14:27:07', '2016-11-03 14:26:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:27:45', '014', '2016-11-03 14:27:31', '2016-11-03 14:27:20');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-03 14:31:52', '015', '2016-11-03 14:31:38', '2016-11-03 14:27:45');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 14:32:15', '016', '2016-11-03 14:32:04', '2016-11-03 14:31:52');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:29:58', '017', '2016-11-03 15:29:42', '0000-00-00 00:00:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:30:24', '018', '2016-11-03 15:30:07', '2016-11-03 15:29:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-03 15:31:22', '019', '2016-11-03 15:31:03', '2016-11-03 15:30:24');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 15:32:59', '020', '2016-11-03 15:32:47', '2016-11-03 15:31:22');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 15:41:28', '021', '2016-11-03 15:41:12', '0000-00-00 00:00:00');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '3', '2016-11-03 17:42:03', '022', '2016-11-03 17:41:47', '0000-00-00 00:00:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 17:55:03', '023', '2016-11-03 17:52:00', '0000-00-00 00:00:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 17:56:48', '024', '2016-11-03 17:55:07', '2016-11-03 17:56:48');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:23:43', '025', '2016-11-03 17:59:59', '2016-11-03 18:23:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:24:58', '026', '2016-11-03 18:24:40', '2016-11-03 18:24:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:26:09', '027', '2016-11-03 18:25:52', '2016-11-03 18:26:09');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:26:23', '028', '2016-11-03 18:26:13', '2016-11-03 18:26:23');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:31:57', '029', '2016-11-03 18:31:48', '2016-11-03 18:31:57');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-03 18:50:26', '030', '2016-11-03 18:50:16', '2016-11-03 18:50:26');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:35:48', '001', '2016-11-04 13:31:13', '2016-11-04 13:35:48');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:36:04', '002', '2016-11-04 13:35:53', '2016-11-04 13:36:04');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-04 13:38:12', '003', '2016-11-04 13:37:43', '2016-11-04 13:38:12');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:38:37', '004', '2016-11-04 13:38:23', '2016-11-04 13:38:37');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:39:03', '005', '2016-11-04 13:38:50', '2016-11-04 13:39:03');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:39:43', '006', '2016-11-04 13:39:14', '2016-11-04 13:39:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 13:41:33', '007', '2016-11-04 13:41:22', '2016-11-04 13:41:33');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-04 13:50:14', '008', '2016-11-04 13:50:01', '2016-11-04 13:50:14');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-04 23:38:53', '002', '2016-11-04 23:38:38', '2016-11-04 23:38:53');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:07:29', '001', '2016-11-07 11:06:37', '2016-11-07 11:07:29');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:08:03', '002', '2016-11-07 11:07:51', '2016-11-07 11:08:03');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:08:38', '003', '2016-11-07 11:08:16', '2016-11-07 11:08:38');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 11:09:11', '004', '2016-11-07 11:09:04', '2016-11-07 11:09:11');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 11:09:56', '005', '2016-11-07 11:09:43', '2016-11-07 11:09:56');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:11:27', '006', '2016-11-07 11:10:47', '2016-11-07 11:11:27');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:11:58', '001', '2016-11-07 11:11:18', '2016-11-07 11:11:58');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 11:12:26', '002', '2016-11-07 11:12:12', '2016-11-07 11:12:26');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:14:09', '003', '2016-11-07 11:13:27', '2016-11-07 11:14:09');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-07 11:15:06', '004', '2016-11-07 11:14:28', '2016-11-07 11:15:06');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 11:15:41', '007', '2016-11-07 11:11:40', '2016-11-07 11:15:41');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 12:36:30', '008', '2016-11-07 11:30:01', '2016-11-07 12:36:30');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:37:26', '009', '2016-11-07 12:37:12', '2016-11-07 12:37:26');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:46:37', '010', '2016-11-07 12:46:27', '2016-11-07 12:46:37');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:46:50', '011', '2016-11-07 12:46:41', '2016-11-07 12:46:50');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:47:14', '012', '2016-11-07 12:47:02', '2016-11-07 12:47:14');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 12:48:57', '013', '2016-11-07 12:48:47', '2016-11-07 12:48:57');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 14:11:34', '014', '2016-11-07 14:11:24', '2016-11-07 14:11:34');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 14:11:53', '015', '2016-11-07 14:11:43', '2016-11-07 14:11:53');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 14:12:16', '016', '2016-11-07 14:12:05', '2016-11-07 14:12:16');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 14:12:42', '017', '2016-11-07 14:12:31', '2016-11-07 14:12:42');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:10', '018', '2016-11-07 16:12:26', '2016-11-07 16:14:10');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:25', '019', '2016-11-07 16:14:15', '2016-11-07 16:14:25');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:14:45', '020', '2016-11-07 16:14:36', '2016-11-07 16:14:45');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:15:47', '021', '2016-11-07 16:15:40', '2016-11-07 16:15:47');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:16:00', '022', '2016-11-07 16:15:52', '2016-11-07 16:16:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:23:01', '023', '2016-11-07 16:22:46', '2016-11-07 16:23:01');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:27:22', '024', '2016-11-07 16:25:38', '2016-11-07 16:27:22');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:30:09', '025', '2016-11-07 16:29:58', '2016-11-07 16:30:09');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:30:34', '026', '2016-11-07 16:30:19', '2016-11-07 16:30:34');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:32:51', '027', '2016-11-07 16:32:32', '2016-11-07 16:32:51');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:33:19', '028', '2016-11-07 16:33:10', '2016-11-07 16:33:19');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:34:07', '029', '2016-11-07 16:33:56', '2016-11-07 16:34:07');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:34:36', '009', '2016-11-07 16:34:16', '2016-11-07 16:34:36');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:41:56', '102', '2016-11-07 16:41:31', '2016-11-07 16:41:56');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:41:56', '099', '2016-11-07 16:41:29', '2016-11-07 16:41:56');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:42:12', '30', '2016-11-07 16:34:37', '2016-11-07 16:42:12');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:42:25', '031', '2016-11-07 16:42:16', '2016-11-07 16:42:25');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:42:29', '100', '2016-11-07 16:42:19', '2016-11-07 16:42:29');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:42:35', '113', '2016-11-07 16:42:22', '2016-11-07 16:42:35');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:42:59', '101', '2016-11-07 16:42:49', '2016-11-07 16:42:59');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:43:12', '134', '2016-11-07 16:42:54', '2016-11-07 16:43:12');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:43:37', '102', '2016-11-07 16:43:29', '2016-11-07 16:43:37');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:43:44', '123', '2016-11-07 16:43:31', '2016-11-07 16:43:44');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:44:02', '103', '2016-11-07 16:43:54', '2016-11-07 16:44:02');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:44:14', '134', '2016-11-07 16:44:01', '2016-11-07 16:44:14');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:44:57', '001', '2016-11-07 16:44:43', '2016-11-07 16:44:57');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:45:29', '032', '2016-11-07 16:45:19', '2016-11-07 16:45:29');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:45:29', '110', '2016-11-07 16:45:18', '2016-11-07 16:45:29');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:45:30', '156', '2016-11-07 16:45:09', '2016-11-07 16:45:30');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:45:31', '001', '2016-11-07 16:45:18', '2016-11-07 16:45:31');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:45:51', '001', '2016-11-07 16:45:41', '2016-11-07 16:45:51');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:45:52', '033', '2016-11-07 16:45:31', '2016-11-07 16:45:52');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:45:52', '235', '2016-11-07 16:45:40', '2016-11-07 16:45:52');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-07 16:45:52', '112', '2016-11-07 16:45:41', '2016-11-07 16:45:52');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-07 16:46:20', '113', '2016-11-07 16:46:10', '2016-11-07 16:46:20');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:46:20', '034', '2016-11-07 16:46:08', '2016-11-07 16:46:20');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:46:21', '223', '2016-11-07 16:46:12', '2016-11-07 16:46:21');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:46:21', '001', '2016-11-07 16:46:09', '2016-11-07 16:46:21');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 16:46:38', '035', '2016-11-07 16:46:29', '2016-11-07 16:46:38');
INSERT INTO `indikator_penilaian` VALUES ('4', '2', '6', '2016-11-07 16:46:49', '002', '2016-11-07 16:46:39', '2016-11-07 16:46:49');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:47:06', '120', '2016-11-07 16:46:51', '2016-11-07 16:47:06');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 16:47:12', '036', '2016-11-07 16:46:59', '2016-11-07 16:47:12');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:47:13', '005', '2016-11-07 16:47:01', '2016-11-07 16:47:13');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:47:31', '006', '2016-11-07 16:47:21', '2016-11-07 16:47:31');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 16:47:33', '037', '2016-11-07 16:47:24', '2016-11-07 16:47:33');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:47:34', '90', '2016-11-07 16:47:18', '2016-11-07 16:47:34');
INSERT INTO `indikator_penilaian` VALUES ('4', '2', '6', '2016-11-07 16:48:01', '008', '2016-11-07 16:47:44', '2016-11-07 16:48:01');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-07 16:48:03', '86', '2016-11-07 16:47:47', '2016-11-07 16:48:03');
INSERT INTO `indikator_penilaian` VALUES ('4', '3', '6', '2016-11-07 16:48:30', '005', '2016-11-07 16:48:20', '2016-11-07 16:48:30');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:48:47', '78', '2016-11-07 16:48:36', '2016-11-07 16:48:47');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:49:07', '67', '2016-11-07 16:48:54', '2016-11-07 16:49:07');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-07 16:49:40', '90', '2016-11-07 16:49:23', '2016-11-07 16:49:40');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-07 16:50:12', '60', '2016-11-07 16:49:52', '2016-11-07 16:50:12');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:00:30', '038', '2016-11-07 17:00:21', '2016-11-07 17:00:30');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:02:01', '039', '2016-11-07 17:01:03', '2016-11-07 17:02:01');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:02:20', '40', '2016-11-07 17:02:11', '2016-11-07 17:02:20');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:15:55', '041', '2016-11-07 17:07:50', '2016-11-07 17:15:55');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:16:07', '042', '2016-11-07 17:15:59', '2016-11-07 17:16:07');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 17:18:48', '043', '2016-11-07 17:18:24', '2016-11-07 17:18:48');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:04', '044', '2016-11-07 17:18:53', '2016-11-07 17:19:04');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-07 17:19:16', '045', '2016-11-07 17:19:07', '2016-11-07 17:19:16');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:31', '045', '2016-11-07 17:19:18', '2016-11-07 17:19:31');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:43', '046', '2016-11-07 17:19:33', '2016-11-07 17:19:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:19:54', '047', '2016-11-07 17:19:45', '2016-11-07 17:19:54');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:20:15', '048', '2016-11-07 17:19:57', '2016-11-07 17:20:15');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-07 17:20:28', '049', '2016-11-07 17:20:19', '2016-11-07 17:20:28');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:20:44', '050', '2016-11-07 17:20:36', '2016-11-07 17:20:44');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:20:59', '051', '2016-11-07 17:20:48', '2016-11-07 17:20:59');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:14', '052', '2016-11-07 17:21:05', '2016-11-07 17:21:14');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:24', '053', '2016-11-07 17:21:17', '2016-11-07 17:21:24');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:21:44', '054', '2016-11-07 17:21:35', '2016-11-07 17:21:44');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:22:36', '055', '2016-11-07 17:21:47', '2016-11-07 17:22:36');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:23:04', '056', '2016-11-07 17:22:52', '2016-11-07 17:23:04');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-07 17:23:53', '057', '2016-11-07 17:23:34', '2016-11-07 17:23:53');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:31:13', '001', '2016-11-08 10:26:00', '2016-11-08 10:31:13');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:31:26', '002', '2016-11-08 10:31:15', '2016-11-08 10:31:26');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:32:28', '003', '2016-11-08 10:31:28', '2016-11-08 10:32:28');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:33:00', '004', '2016-11-08 10:32:30', '2016-11-08 10:33:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:34:10', '005', '2016-11-08 10:33:02', '2016-11-08 10:34:10');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:35:13', '006', '2016-11-08 10:34:12', '2016-11-08 10:35:13');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:35:27', '007', '2016-11-08 10:35:14', '2016-11-08 10:35:27');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:03', '008', '2016-11-08 10:35:29', '2016-11-08 10:36:03');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:40', '009', '2016-11-08 10:36:05', '2016-11-08 10:36:40');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:36:59', '010', '2016-11-08 10:36:43', '2016-11-08 10:36:59');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:37:48', '011', '2016-11-08 10:37:01', '2016-11-08 10:37:48');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:44:14', '012', '2016-11-08 10:37:52', '2016-11-08 10:44:14');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:46:13', '013', '2016-11-08 10:44:16', '2016-11-08 10:46:13');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:46:55', '014', '2016-11-08 10:46:15', '2016-11-08 10:46:55');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:48:47', '015', '2016-11-08 10:46:58', '2016-11-08 10:48:47');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:48:58', '016', '2016-11-08 10:48:49', '2016-11-08 10:48:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 10:49:08', '016', '2016-11-08 10:49:00', '2016-11-08 10:49:08');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:32:26', '017', '2016-11-08 11:18:10', '2016-11-08 11:32:26');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:32:35', '18', '2016-11-08 11:32:28', '2016-11-08 11:32:35');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:03', '019', '2016-11-08 11:32:56', '2016-11-08 11:33:03');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:31', '020', '2016-11-08 11:33:06', '2016-11-08 11:33:31');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:42', '021', '2016-11-08 11:33:34', '2016-11-08 11:33:42');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:33:55', '022', '2016-11-08 11:33:44', '2016-11-08 11:33:55');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:34:34', '022', '2016-11-08 11:33:58', '2016-11-08 11:34:34');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 11:34:45', '023', '2016-11-08 11:34:36', '2016-11-08 11:34:45');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:03:58', '024', '2016-11-08 14:03:03', '2016-11-08 14:03:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:05:05', '025', '2016-11-08 14:04:48', '2016-11-08 14:05:05');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:07:00', '026', '2016-11-08 14:06:50', '2016-11-08 14:07:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:07:21', '026', '2016-11-08 14:07:09', '2016-11-08 14:07:21');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:07:39', '027', '2016-11-08 14:07:28', '2016-11-08 14:07:39');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:08:04', '028', '2016-11-08 14:07:46', '2016-11-08 14:08:04');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:10:20', '029', '2016-11-08 14:10:13', '2016-11-08 14:10:20');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:10:35', '030', '2016-11-08 14:10:27', '2016-11-08 14:10:35');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:10:47', '031', '2016-11-08 14:10:38', '2016-11-08 14:10:47');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:11:00', '032', '2016-11-08 14:10:51', '2016-11-08 14:11:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 14:11:38', '033', '2016-11-08 14:11:04', '2016-11-08 14:11:38');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:22:11', '034', '2016-11-08 14:22:02', '2016-11-08 14:22:11');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 14:26:33', '101', '2016-11-08 14:26:03', '2016-11-08 14:26:33');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:27:56', '103', '2016-11-08 14:27:37', '2016-11-08 14:27:56');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:33:33', '104', '2016-11-08 14:29:44', '2016-11-08 14:33:33');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:33:34', '104', '2016-11-08 14:29:44', '2016-11-08 14:33:34');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:44:01', '035', '2016-11-08 14:43:53', '2016-11-08 14:44:01');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:44:21', '36', '2016-11-08 14:44:07', '2016-11-08 14:44:21');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 14:44:43', '111', '2016-11-08 14:44:30', '2016-11-08 14:44:43');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 14:45:18', '113', '2016-11-08 14:45:10', '2016-11-08 14:45:18');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 14:47:48', '037', '2016-11-08 14:47:32', '2016-11-08 14:47:48');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 14:50:00', '114', '2016-11-08 14:49:46', '2016-11-08 14:50:00');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 14:50:52', '038', '2016-11-08 14:50:39', '2016-11-08 14:50:52');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:02:17', '039', '2016-11-08 14:51:58', '2016-11-08 15:02:17');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:03:11', '041', '2016-11-08 15:02:28', '2016-11-08 15:03:11');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:07:02', '042', '2016-11-08 15:03:21', '2016-11-08 15:07:02');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:07:20', '043', '2016-11-08 15:07:08', '2016-11-08 15:07:20');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:17:58', '044', '2016-11-08 15:17:46', '2016-11-08 15:17:58');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:18:19', '045', '2016-11-08 15:18:08', '2016-11-08 15:18:19');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 15:18:43', '046', '2016-11-08 15:18:26', '2016-11-08 15:18:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:19:30', '047', '2016-11-08 15:19:09', '2016-11-08 15:19:30');
INSERT INTO `indikator_penilaian` VALUES ('1', '3', '3', '2016-11-08 15:19:54', '048', '2016-11-08 15:19:43', '2016-11-08 15:19:54');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 15:19:57', '115', '2016-11-08 15:19:48', '2016-11-08 15:19:57');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:20:43', '049', '2016-11-08 15:20:16', '2016-11-08 15:20:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:21:10', '050', '2016-11-08 15:20:53', '2016-11-08 15:21:10');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:23:40', '051', '2016-11-08 15:23:31', '2016-11-08 15:23:40');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 15:25:27', '114', '2016-11-08 15:25:16', '2016-11-08 15:25:27');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 15:25:39', '052', '2016-11-08 15:25:29', '2016-11-08 15:25:39');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 15:45:31', '053', '2016-11-08 15:45:21', '2016-11-08 15:45:31');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 16:04:18', '115', '2016-11-08 16:03:52', '2016-11-08 16:04:18');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 16:05:43', '054', '2016-11-08 16:05:28', '2016-11-08 16:05:43');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 16:06:07', '055', '2016-11-08 16:05:57', '2016-11-08 16:06:07');
INSERT INTO `indikator_penilaian` VALUES ('1', '2', '3', '2016-11-08 16:07:14', '056', '2016-11-08 16:07:01', '2016-11-08 16:07:14');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 16:08:49', '116', '2016-11-08 16:08:37', '2016-11-08 16:08:49');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 17:31:39', '110', '2016-11-08 17:29:33', '2016-11-08 17:31:39');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-08 17:32:44', '057', '2016-11-08 17:32:36', '2016-11-08 17:32:44');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 17:45:53', '117', '2016-11-08 17:45:37', '2016-11-08 17:45:53');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 17:46:23', '112', '2016-11-08 17:31:49', '2016-11-08 17:46:23');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 17:47:14', '120', '2016-11-08 17:47:01', '2016-11-08 17:47:14');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 18:27:40', '121', '2016-11-08 18:27:27', '2016-11-08 18:27:40');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 18:33:35', '101', '2016-11-08 18:33:22', '2016-11-08 18:33:35');
INSERT INTO `indikator_penilaian` VALUES ('2', '3', '4', '2016-11-08 18:41:19', '115', '2016-11-08 18:40:53', '2016-11-08 18:41:19');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 18:46:44', '001', '2016-11-08 18:46:26', '2016-11-08 18:46:44');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 18:47:14', '002', '2016-11-08 18:46:55', '2016-11-08 18:47:14');
INSERT INTO `indikator_penilaian` VALUES ('3', '3', '5', '2016-11-08 18:52:13', '003', '2016-11-08 18:51:44', '2016-11-08 18:52:13');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 18:59:16', '004', '2016-11-08 18:58:04', '2016-11-08 18:59:16');
INSERT INTO `indikator_penilaian` VALUES ('3', '1', '5', '2016-11-08 19:43:34', '004', '2016-11-08 19:43:24', '2016-11-08 19:43:34');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 19:45:39', '112', '2016-11-08 19:45:30', '2016-11-08 19:45:39');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 19:59:15', '110', '2016-11-08 19:58:33', '2016-11-08 19:59:15');
INSERT INTO `indikator_penilaian` VALUES ('2', '1', '4', '2016-11-08 20:00:54', '113', '2016-11-08 20:00:30', '2016-11-08 20:00:54');
INSERT INTO `indikator_penilaian` VALUES ('2', '2', '4', '2016-11-08 20:11:00', '110', '2016-11-08 20:10:22', '2016-11-08 20:11:00');
INSERT INTO `indikator_penilaian` VALUES ('3', '2', '5', '2016-11-08 20:11:41', '116', '2016-11-08 20:09:28', '2016-11-08 20:11:41');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-09 10:44:59', '001', '2016-11-09 10:44:51', '2016-11-09 10:44:59');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-09 20:29:50', '001', '2016-11-09 20:29:41', '2016-11-09 20:29:50');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-15 12:40:19', '001', '2016-11-15 12:39:51', '2016-11-15 12:40:19');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-18 17:34:56', '001', '2016-11-18 17:34:50', '2016-11-18 17:34:56');
INSERT INTO `indikator_penilaian` VALUES ('1', '1', '3', '2016-11-24 11:00:12', '001', '2016-11-24 10:59:39', '2016-11-24 11:00:12');

-- ----------------------------
-- Table structure for `log_akses`
-- ----------------------------
DROP TABLE IF EXISTS `log_akses`;
CREATE TABLE `log_akses` (
  `tanggal` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of log_akses
-- ----------------------------
INSERT INTO `log_akses` VALUES ('2016-11-08 11:42:44', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 12:01:15', '3', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 12:01:32', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 13:55:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:01:57', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:02:57', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:06:37', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:21:40', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:25:45', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:27:30', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:32:38', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:38:49', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:40:00', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:43:51', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 14:44:15', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:25:11', '5', '3');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:45:46', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:46:07', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 15:50:22', '5', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 16:04:51', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 16:19:02', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:27:37', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:32:32', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:43:53', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 17:45:34', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 18:15:18', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:44:06', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:55:09', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:57:46', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-08 19:59:37', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-08 20:10:17', '4', '2');
INSERT INTO `log_akses` VALUES ('2016-11-09 09:17:06', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-09 15:27:59', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-09 20:29:28', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-15 12:39:31', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-17 14:57:49', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-18 17:30:46', '3', '1');
INSERT INTO `log_akses` VALUES ('2016-11-24 10:57:49', '3', '1');

-- ----------------------------
-- Table structure for `ref_indikator_cat`
-- ----------------------------
DROP TABLE IF EXISTS `ref_indikator_cat`;
CREATE TABLE `ref_indikator_cat` (
  `indikator_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `indikator_category_name` varchar(255) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `icon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`indikator_category_id`),
  KEY `idx_indikator_cat` (`indikator_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ref_indikator_cat
-- ----------------------------
INSERT INTO `ref_indikator_cat` VALUES ('1', 'Sangat Puas', '', 'small-box bg-green');
INSERT INTO `ref_indikator_cat` VALUES ('2', 'Puas', '', 'small-box bg-yellow');
INSERT INTO `ref_indikator_cat` VALUES ('3', 'Tidak Puas', '', 'small-box bg-red');

-- ----------------------------
-- Table structure for `res_ref_instansi`
-- ----------------------------
DROP TABLE IF EXISTS `res_ref_instansi`;
CREATE TABLE `res_ref_instansi` (
  `instansi_id` int(10) NOT NULL AUTO_INCREMENT,
  `instansi_name` varchar(255) NOT NULL,
  `instansi_address` varchar(255) DEFAULT NULL,
  `postal_code` varchar(10) DEFAULT NULL,
  `instansi_email` varchar(200) NOT NULL,
  `instansi_phone` varchar(20) NOT NULL,
  `instansi_fax` varchar(20) DEFAULT NULL,
  `instansi_city` varchar(200) DEFAULT NULL,
  `instansi_province` varchar(200) DEFAULT NULL,
  `instansi_country` varchar(200) DEFAULT NULL,
  `aktif` bit(1) DEFAULT b'1',
  `kategori` tinyint(4) DEFAULT NULL,
  `stat` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`instansi_id`),
  KEY `idxrefisntansi` (`instansi_id`,`instansi_name`,`aktif`,`kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of res_ref_instansi
-- ----------------------------
INSERT INTO `res_ref_instansi` VALUES ('1', 'LOKET 1', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '1');
INSERT INTO `res_ref_instansi` VALUES ('2', 'LOKET 2', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('3', 'LOKET 3', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('4', 'LOKET 4', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('5', 'LOKET 5', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('6', 'LOKET 6', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('7', 'LOKET 7', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('8', 'LOKET 8', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('9', 'LOKET 9', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('10', 'LOKET 10', 'Dispenda Bogor', '16912', 'dispenda@kotabogor.go.id', '(0251) 8322 871', '(0251) 8321 075', 'Bogor', 'Jawa Barat', 'Indonesia', '', '1', '0');
INSERT INTO `res_ref_instansi` VALUES ('11', 'LOKET 11', 'Dispenda Bogor', null, '', '', null, null, null, null, '', '1', '0');

-- ----------------------------
-- Table structure for `res_sec_login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_login_attempts`;
CREATE TABLE `res_sec_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for `res_sec_module`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_module`;
CREATE TABLE `res_sec_module` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) DEFAULT NULL,
  `module_alias` varchar(100) DEFAULT NULL,
  `module_url` varchar(255) DEFAULT NULL,
  `mod_icon_cls` varchar(50) DEFAULT NULL,
  `mod_seq` int(2) DEFAULT '0',
  `module_pid` int(11) DEFAULT NULL,
  `publish` bit(1) DEFAULT b'1',
  `mod_group` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`module_id`),
  KEY `idx_uniq` (`module_alias`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_module
-- ----------------------------
INSERT INTO `res_sec_module` VALUES ('1', 'Dashboard', 'MOD_HOME', 'main/dashboard', 'fa-fw fa-dashboard', '1', null, '', null);
INSERT INTO `res_sec_module` VALUES ('2', 'Utility', 'MOD_UTILITY', '#', 'fa-share', '99', '0', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('3', 'Daftar User', 'MOD_USERMANAGE', 'utility/user_manage', 'fa-user', '1', '2', '', null);
INSERT INTO `res_sec_module` VALUES ('41', 'Indikator Penilaian', 'MOD_CORE', 'indikatorcore/Cindikatorcore', 'fa-file', '1', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('7', 'Referensi', 'MOD_REF', '#', 'fa-gears', '5', null, '', 'reference');
INSERT INTO `res_sec_module` VALUES ('39', 'Pengisian 360', 'MOD_PENILAIAN360', 'penilaian/Cpenilaian', 'fa-list', '5', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('36', 'Setting Nilai', 'MOD_ALUMNI', 'alumni/alumni', 'fa-file', '4', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('40', 'Referensi Indikator 360', 'MOD_REF_INDIKATOR', 'indikator360/Cindikator360', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('14', 'Daftar Loket', 'MOD_REF_INSTANSI', 'reference/ref_instansi', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('15', 'Referensi Penilaian', 'MOD_FPP', '#', 'fa-fw fa-files-o', '3', '0', '', 'main');
INSERT INTO `res_sec_module` VALUES ('32', 'Module Management', 'MOD_MODULEMANAGE', 'utility/Module_manage', 'fa-file-archive-o', '3', '2', '', null);
INSERT INTO `res_sec_module` VALUES ('18', 'Setting Penilai', 'MOD_SETTING_PENILAI', 'setting_penilai/setting_penilai', 'fa-file', '2', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('38', 'Thn.Anggaran', 'MOD_TAHUNANGGARAN', 'thang/Cthang', 'fa-circle-o', '4', '21', '', '');
INSERT INTO `res_sec_module` VALUES ('21', 'Data Referensi', 'MOD_MASTER', '#', 'fa-database', '2', '0', '', '');
INSERT INTO `res_sec_module` VALUES ('22', 'Data Karyawan', 'MOD_DATA_KARYAWAN', 'data_karyawan/Cdata_karyawan', 'fa-circle-o', '1', '21', '', '');
INSERT INTO `res_sec_module` VALUES ('23', 'Log User Loket', 'MOD_LOG', 'log/Clog', 'fa-circle-o', '3', '2', '', '');
INSERT INTO `res_sec_module` VALUES ('24', 'Laporan', 'MOD_LAP', '#', 'fa-fw fa-dashboard', '4', '0', '', '');
INSERT INTO `res_sec_module` VALUES ('25', 'Laporan Penilaian', 'MOD_LAP_PEN', 'lap_data_anak/lap_data_anak', 'fa-list', '1', '24', '', '');
INSERT INTO `res_sec_module` VALUES ('26', 'Laporan', 'MOD_LAP_KEPUASAN', 'laporankepuasan/Laporankepuasan', 'fa-file', '3', '15', '', '');
INSERT INTO `res_sec_module` VALUES ('27', 'Laporan Karyawan', 'MOD_LAP_GERAK_ANAK', 'lap_gerak_anak/lap_gerak_anak', 'fa-list', '3', '24', '', '');
INSERT INTO `res_sec_module` VALUES ('29', 'Pengisian Outcome Data Anak', 'MOD_OUTCOME_HIST', 'outcome/history', 'fa-file', '1', '28', '', 'outcome');
INSERT INTO `res_sec_module` VALUES ('30', 'Indikator Penilaian', 'MOD_PEN_BALITA', 'pendaftaranbalita/Pendaftaranbalita', 'fa-file', '1', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('31', 'Skala Penilaian', 'MOD_PEN_COTA', 'pendaftarancota/Pendaftarancota', 'fa-file', '3', '15', '', 'main');
INSERT INTO `res_sec_module` VALUES ('33', 'Role Management', 'MOD_ROLEMANAGE', 'utility/role_manage', 'fa-file-archive-o', '4', '2', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('34', 'Role Akses', 'MOD_PRIVMANAGE', 'utility/Privilege_manage', 'fa-file-archive-o', '5', '2', '', 'utility');
INSERT INTO `res_sec_module` VALUES ('42', 'Data Penilai', 'MOD_DATA_PENILAI', 'data_penilai/Cdata_penilai', 'fa-file', '5', '15', '', null);
INSERT INTO `res_sec_module` VALUES ('43', 'Daftar User CS', 'MODUSRCS', 'utility/Cs_manage', 'fa-user', '2', '2', '', 'utility');

-- ----------------------------
-- Table structure for `res_sec_role`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_role`;
CREATE TABLE `res_sec_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `role_alias` varchar(100) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  PRIMARY KEY (`role_id`),
  KEY `idx_role` (`role_id`,`role_name`,`role_alias`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_role
-- ----------------------------
INSERT INTO `res_sec_role` VALUES ('1', 'Super Administrator', 'superadmin', '');
INSERT INTO `res_sec_role` VALUES ('2', 'Administrator', 'admin', '');
INSERT INTO `res_sec_role` VALUES ('3', 'Loket', 'loket', '');

-- ----------------------------
-- Table structure for `res_sec_role_priv`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_role_priv`;
CREATE TABLE `res_sec_role_priv` (
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `allow_view` bit(1) DEFAULT NULL,
  `allow_new` bit(1) DEFAULT b'1',
  `allow_edit` bit(1) DEFAULT b'1',
  `allow_delete` bit(1) DEFAULT b'1',
  `allow_print` bit(1) DEFAULT b'0',
  PRIMARY KEY (`role_id`,`module_id`),
  KEY `idxrolepriv` (`role_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_role_priv
-- ----------------------------
INSERT INTO `res_sec_role_priv` VALUES ('1', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '8', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '9', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '10', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '11', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '12', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '13', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '16', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '8', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '9', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '10', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '11', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '12', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '13', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '16', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '2', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '3', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '41', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '40', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '7', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '14', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '17', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '32', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '33', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '34', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '36', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '1', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '24', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '21', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '23', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '29', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '39', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('4', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '38', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '22', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '35', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '25', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '26', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('6', '27', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '33', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '34', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '18', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '15', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '30', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('5', '31', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '42', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '32', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('1', '43', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('2', '43', '', '', '', '', '');
INSERT INTO `res_sec_role_priv` VALUES ('3', '43', '', '', '', '', '');

-- ----------------------------
-- Table structure for `res_sec_user`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_user`;
CREATE TABLE `res_sec_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `ip_address` varchar(20) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `password_changed` tinyint(4) DEFAULT NULL,
  `flag_penilaian` int(11) DEFAULT '0',
  `no_antrian` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_users` (`id`,`username`,`full_name`,`email`,`password`,`role_id`,`active`,`ip_address`,`flag_penilaian`,`no_antrian`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of res_sec_user
-- ----------------------------
INSERT INTO `res_sec_user` VALUES ('-1', '', '', '', '', '0', null, null, '', null, null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('1', 'nuriyanto', 'nuriyanto', 'nuriyanto@gmail.com', '$2a$08$sZDQ20EF1mCqqjQ2xhnHq.2dE03H6kWM/F31on/4.PXFC/uPff0bi', '2', '2015-12-22 14:43:44', '2016-12-09 21:27:41', '', null, null, null, '', null, '', null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('2', 'admin', 'Developer', 'developer@gmail.com', '$2a$08$NWmUoWFM7DLNylo8vKQRku.8gQwjfZu5CAWecDJg.QiZKV8sJlKj6', '2', '2015-12-22 16:19:22', '2016-11-09 15:54:44', '', null, null, null, '', null, 'agl27CwebosHvOkyTQV5YO', null, '0', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `res_sec_user` VALUES ('3', 'cs1', 'Susan', 'cs1@gmail.com', '$2a$08$AYSQsT0D.B8QVoMzqvwGauXpBF7vU5SV/j89hp3b1qppUhEKRrQDu', '3', '2016-08-15 08:23:56', '2016-11-24 10:57:49', '', '::1', null, null, null, null, 'IM9nVoS2/QuA25qRldiGiu', null, '1', '', '2016-11-24 11:00:20', '2016-11-24 11:00:12');
INSERT INTO `res_sec_user` VALUES ('4', 'cs2', 'Asep ', 'cs2@gmail.com', '$2a$08$AYSQsT0D.B8QVoMzqvwGauXpBF7vU5SV/j89hp3b1qppUhEKRrQDu', '3', '2016-08-23 15:25:30', '2016-11-08 20:10:17', '', '::1', null, null, null, null, 'MkmZAOW18d4q6zn4bdU2X.', null, '0', '110', '2016-11-08 20:10:22', '2016-11-08 20:11:00');
INSERT INTO `res_sec_user` VALUES ('5', 'cs3', 'Agus', 'cs3@gmail.com', '$2a$08$liKQYXup3TtEzTWIzl3rBO43LJajzLK1S6M5gRZcqCAFfZVJQxcry', '3', '2016-08-29 15:34:03', '2016-11-08 15:50:22', '', '::1', null, null, null, null, 'A5BoMVB4qfEnlN0FHXvvIe', null, '0', '116', '2016-11-08 20:09:28', '2016-11-08 20:11:41');
INSERT INTO `res_sec_user` VALUES ('6', 'cs4', 'Dewi', 'cs4@gmail.com', '$2a$08$rh0efbcRDEOdb6lYgM8ZHOpxIs1mSVRheV0s4k5dNGrCDYq.3ZFoO', '3', '2016-08-29 16:00:06', '2016-11-07 16:44:18', '', '::1', null, null, null, null, null, null, '0', '005', '2016-11-07 16:48:20', '2016-11-07 16:48:30');
INSERT INTO `res_sec_user` VALUES ('7', 'dispenda', 'Admin Dispenda', 'admin@dispenda.com', '$2a$08$4UIhu9PKxzapAbn3zV9MC.mGMvaxMcIe3CdUEEmEHi4/3dsnyMUzW', '2', '2016-11-02 17:02:23', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('8', 'cs5', 'Dede', 'cs5@gmail.com', '$2a$08$Z.rZTjDFXfuA31gIpcL1d.xd7cealrAW5VUa1gqwZSOwDL9on4x0u', '3', '2016-11-02 17:04:32', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('9', 'cs6', 'Heru', 'cs6@gmail.com', '$2a$08$Scc/fc24MOehfbdoUoCs8ejumpxUnKCZGa3A/.XonUsa8MmndVvcK', '3', '2016-11-02 17:12:54', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('10', 'cs7', 'Asep', 'cs7@gmail.com', '$2a$08$.9eg3rZsCIzGmFiyZuUb3u34VSo6MFlTcGC.8wSBjyCaKoNYa8c8q', '3', '2016-11-02 17:16:54', '2016-11-02 17:20:28', '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('11', 'cs8', 'Usep', 'cs@gmail.com', '$2a$08$1Cce.kVcvpHiapBYM2mfpuTPvOFJFi5f/NXD8tvfpOwuFKm8R36qS', '3', '2016-11-02 17:24:10', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('12', 'dispenda2', 'Admin Dispenda 2', 'dispenda2@gmai.com', '$2a$08$4g2UxAJYPm/S/3Z6Sk2wMOPxyW6JNMNVfh6EAKVNmCIOwiITRsugi', '2', '2016-11-02 17:30:33', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('13', 'cs9', 'Dendi', 'cs9@gmail.com', '$2a$08$f/9yFCT260ySmvpxeQSB3OydXDuQQyBHq4mkEqm78sAcOJaV8Cq/O', '3', '2016-11-02 17:36:08', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('14', 'cs10', 'Sukmara', 'cs10@gmail.com', '$2a$08$sDiESQyup7SaLFHF8e8ZMeg5FOQO9MUtck/CZ/RRwk9hTd4caaQGm', '3', '2016-11-02 17:38:04', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('15', 'cs11', 'Tatang', 'cs11@gmail.com', '$2a$08$PX3jMA56uglju9a44WUHzuEiIbbkFi4TPlnxlF2MBhyAyEzkKn5MG', '3', '2016-11-03 08:40:55', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('16', 'cs12', 'Hikmat', 'cs12@gmail.com', '$2a$08$1b0cYfKulqlGLhI40s1RNup..8M5VV3ZwIBkvZfqc./A3OJX3pMNG', '3', '2016-11-03 08:42:13', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('17', 'cs13', 'Yudi', 'cs13@gmail.com', '$2a$08$vV4CIS3T9WSxRGYgBcb.EuCQXvkNlH6g4l1nNH0.6xnhlRrqJ1/rK', '3', '2016-11-03 08:43:31', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('18', 'cs14', 'Lucy', 'cs14@gmail.com', '$2a$08$WsaFdzqyPN5azr..Bgh2rezdJ3ncZSYm0wIjC0pTDgOssW0/4zrNG', '3', '2016-11-03 09:00:29', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);
INSERT INTO `res_sec_user` VALUES ('19', 'cs15', 'Totong', 'cs15@gmail.com', '$2a$08$SMeDTPTJK2qVr4Zs5iwqPu17Kdyf1r3dOhW6ANSkjre7U0rk0rTAS', '3', '2016-11-03 09:11:46', null, '', '::1', null, null, null, null, null, null, '0', null, null, null);

-- ----------------------------
-- Table structure for `res_sec_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `res_sec_user_role`;
CREATE TABLE `res_sec_user_role` (
  `userid` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`userid`,`role_id`),
  KEY `idxrole` (`userid`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_sec_user_role
-- ----------------------------
INSERT INTO `res_sec_user_role` VALUES ('1', '2');
INSERT INTO `res_sec_user_role` VALUES ('2', '2');
INSERT INTO `res_sec_user_role` VALUES ('3', '3');
INSERT INTO `res_sec_user_role` VALUES ('4', '3');
INSERT INTO `res_sec_user_role` VALUES ('5', '3');
INSERT INTO `res_sec_user_role` VALUES ('6', '3');
INSERT INTO `res_sec_user_role` VALUES ('7', '2');
INSERT INTO `res_sec_user_role` VALUES ('8', '0');
INSERT INTO `res_sec_user_role` VALUES ('9', '0');
INSERT INTO `res_sec_user_role` VALUES ('10', '3');
INSERT INTO `res_sec_user_role` VALUES ('11', '0');
INSERT INTO `res_sec_user_role` VALUES ('12', '2');
INSERT INTO `res_sec_user_role` VALUES ('13', '0');
INSERT INTO `res_sec_user_role` VALUES ('14', '3');
INSERT INTO `res_sec_user_role` VALUES ('15', '0');
INSERT INTO `res_sec_user_role` VALUES ('16', '0');
INSERT INTO `res_sec_user_role` VALUES ('17', '0');
INSERT INTO `res_sec_user_role` VALUES ('18', '0');
INSERT INTO `res_sec_user_role` VALUES ('19', '0');

-- ----------------------------
-- Table structure for `res_user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `res_user_profile`;
CREATE TABLE `res_user_profile` (
  `userid` int(11) NOT NULL,
  `instansi_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `tgl_lahir` datetime DEFAULT NULL,
  `alamat` text CHARACTER SET utf8,
  `no_telp` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `jenis_kelamin` tinyint(1) DEFAULT NULL,
  `asal_daerah` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `agama` tinyint(4) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  KEY `idxprofile` (`userid`,`instansi_id`,`address`,`phone`,`photo`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of res_user_profile
-- ----------------------------
INSERT INTO `res_user_profile` VALUES ('1', '1', 'Bandung', '08xxxxxxxxxx', 'avatar1.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('2', '1', 'Bogor', '087823339007', 'avatar1.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('3', '1', 'Bogor', '087823339007', 'avatar2.png', '0000-00-00 00:00:00', null, null, null, '', null, '3');
INSERT INTO `res_user_profile` VALUES ('4', '2', 'Bogor', '087823339007', 'avatar1.png', '0000-00-00 00:00:00', null, null, null, '', null, '3');
INSERT INTO `res_user_profile` VALUES ('5', '1', 'Bogor', '08782390980808', 'avatar1.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('6', '4', 'Bogor', '085722609190', 'avatar4.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('7', '1', 'Kopo', '082783312312', '160x160.png', null, null, null, null, null, null, '2');
INSERT INTO `res_user_profile` VALUES ('8', '1', 'Kopo', '082783312312', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('9', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('10', '7', 'Kopo', '087823339007', '160x160.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('11', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('12', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('13', '1', 'Kopo', '087823339019', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('14', '1', 'Kopo', '087823339019', '160x160.png', null, null, null, null, null, null, '3');
INSERT INTO `res_user_profile` VALUES ('15', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('16', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('17', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('18', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);
INSERT INTO `res_user_profile` VALUES ('19', '1', 'Kopo', '087823339007', '', null, null, null, null, null, null, null);

-- ----------------------------
-- View structure for `v_sec_role_priv`
-- ----------------------------
DROP VIEW IF EXISTS `v_sec_role_priv`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sec_role_priv` AS select `srp`.`role_id` AS `role_id`,`srp`.`module_id` AS `module_id`,`srp`.`allow_view` AS `allow_view`,`srp`.`allow_new` AS `allow_new`,`srp`.`allow_edit` AS `allow_edit`,`srp`.`allow_delete` AS `allow_delete`,`srp`.`allow_print` AS `allow_print`,`sm`.`module_name` AS `module_name`,`sm`.`module_alias` AS `module_alias`,`sm`.`module_url` AS `module_url`,`sm`.`module_pid` AS `module_pid`,`sm`.`mod_icon_cls` AS `mod_icon_cls`,`sm`.`mod_seq` AS `mod_seq`,`sm`.`publish` AS `publish`,`sm`.`mod_group` AS `mod_group` from ((`res_sec_role_priv` `srp` left join `res_sec_module` `sm` on((`srp`.`module_id` = `sm`.`module_id`))) left join `res_sec_role` `sr` on((`srp`.`role_id` = `sr`.`role_id`))) ;

-- ----------------------------
-- Procedure structure for `sp_SecRoleModule_get`
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_SecRoleModule_get`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_SecRoleModule_get`(IN prole_id int)
BEGIN 
 
		-- CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul as (select * from table1 where 1=0);		
		-- CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul(
	CREATE TEMPORARY TABLE IF NOT EXISTS tbSecModul(
					module_id INT, module_name varchar(200), module_alias varchar(200),
					module_pid INT, module_url varchar(255),  
					mod_icon_cls varchar(100), mod_seq INT, mod_group varchar(100)
			);
		

		TRUNCATE TABLE tbSecModul;

		/* insert role modul */
		INSERT INTO tbSecModul(module_id, module_name, module_alias, module_pid, module_url, mod_icon_cls, mod_seq, mod_group)
			SELECT module_id, module_name, module_alias, COALESCE(module_pid,0), LTRIM(RTRIM(module_url)),
				mod_icon_cls, mod_seq, mod_group
			FROM v_sec_role_priv
			WHERE publish=1 AND allow_view=1 AND role_id= prole_id
			ORDER BY module_pid, mod_seq, module_id;
 
		SELECT * FROM tbSecModul;

END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `SPLIT_STR`
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STR`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(

  x LONGTEXT,

  delim VARCHAR(12),

  pos INT

) RETURNS longtext CHARSET utf8
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),

       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),

       delim, '')
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `ufn_SplitStr`
-- ----------------------------
DROP FUNCTION IF EXISTS `ufn_SplitStr`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `ufn_SplitStr`(
  x LONGTEXT,
  delim VARCHAR(12),
  pos INT
) RETURNS longtext CHARSET utf8
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '')
;;
DELIMITER ;
