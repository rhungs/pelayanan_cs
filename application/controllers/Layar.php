<?php


class Layar extends Bss_controller {
        
	public function __construct(){
        parent::__construct();
        //$this->MOD_ALIAS = "MOD_HOME";
        //$this->checkAuthorization($this->MOD_ALIAS);
        $this->load->model("Mcetakstruk","mcom"); 
    }

	public function index(){            
        $data['titlehead'] = "";
        //$this->load->model("mcommon","mcom");
		//$this->load->model("Mcetakstruk","mcom"); 
		// custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.min.js',
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.resize.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.pie.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.categories.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.axislabels.js',
            HTTP_ASSET_PATH.'plugins/voices/responsivevoice.js',
            //'http://code.responsivevoice.org/responsivevoice.js', 
            HTTP_MOD_JS.'modules/layar/layar.js'            
        );
	

        $data['loadfoot'] = $loadfoot;     
        $this->template->load('tmpl/vwfrontcetakstruk','v_layar',$data);                 
	}

    public function DeleteSound(){
        $this->mcom->delete_queue_sound();
        exit;
    }

    public function listantrian(){

        $results=$this->mcom->get_list();
        // $ttl=count($results);
        // $build_array = array ("total"=>$ttl,"data"=>array());

        //     foreach($results as $row) {
        //         array_push($build_array["data"]
        //             ,array(
        //                 "no_antrian" => $row->no_antrian,
        //                 "loket"      => $row->loket
        //             )
        //         );
        //     }

            $build_array = array();

            foreach($results as $row) {
                array_push($build_array
                    ,array(
                        "no_antrian"        => $row->no_antrian,
                        "no_antrian_sound"  => $row->no_antrian_sound,
                        "loket"             => $row->loket
                    )
                );
            }


            //return $build_array; // 200 being the HTTP response code   
            echo json_encode($build_array);
            exit;
        // }else{
        //     $res = array('error'=>'Data could not be found');
        //     return $res;
        // }
    }

    function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    function konversi_hari()
    {
        switch(date("l"))
        {
            case 'Monday':$nmh="Senin";break; 
            case 'Tuesday':$nmh="Selasa";break; 
            case 'Wednesday':$nmh="Rabu";break; 
            case 'Thursday':$nmh="Kamis";break; 
            case 'Friday':$nmh="Jum'at";break; 
            case 'Saturday':$nmh="Sabtu";break; 
            case 'Sunday':$nmh="minggu";break; 
        }
        return $nmh;
    }

        
}
