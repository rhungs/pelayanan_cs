<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
            parent::__construct();
            $this->load->model('reference/mcommon','mcom');
	}
	public function index(){                                        
            $data['mcom'] = $this->mcom;
            $data['titlehead'] = ":: DISPENDA BOGOR ::";
			
			if (!$this->ion_auth->logged_in())
			{
				//redirect them to the login page
				redirect('auth/login', 'refresh');
			}
			elseif (!$this->ion_auth->is_admin()) //remove this elseif if you want to enable this for non-admins
			{
				redirect('penilaian/Cpenilaian/form_penilaian_list');
				//redirect them to the home page because they must be an administrator to view this
				return show_error('You must be an administrator to view this page.');
			}
			else
			{
				/** remark by kiq
							//set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				//list the users
				$this->data['users'] = $this->ion_auth->users()->result();
				foreach ($this->data['users'] as $k => $user)
				{
					$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
				}

				$this->_render_page('auth/index', $this->data);
							*/
						  redirect('auth/login', 'refresh');
			}  
				//redirect('main/dashboard', 'refresh');
		
	}               
               
}
