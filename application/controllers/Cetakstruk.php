<?php


class Cetakstruk extends Bss_controller {
        
	public function __construct(){
        parent::__construct();
        //$this->MOD_ALIAS = "MOD_HOME";
        //$this->checkAuthorization($this->MOD_ALIAS);
    }

	public function index(){            
        $data['titlehead'] = "";
        //$this->load->model("mcommon","mcom");
		$this->load->model("Mcetakstruk","mcom"); 
		// custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.min.js',
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.resize.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.pie.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.categories.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.axislabels.js',
            HTTP_MOD_JS.'modules/cetakstruk/cetakstruk.js'            
        );
	

        $data['loadfoot'] = $loadfoot;     
        $this->template->load('tmpl/vwfrontcetakstruk','v_cetakstruk',$data);                 
	}

    function lastno($filename){
        // $this->load->model("Mcetakstruk","mcom"); 
        // $antrian = $this->mcom->getlastno();
        // $date = date("Ymd");
        // $file =  $date.$antrian->no_antrian;
        // //echo $file;
        /* contoh text */ 
        //$printer = "\\\\192.168.0.26\\EPSON L120 Series";
        //if($ph = printer_open($printer))
        //{  
            // $html = "<html>
            //             <body>
            //                 <div style='font-size:16;'>
            //                     DISPENDA BOGOR
            //                 </div>
            //                 <div>
            //                     Tanggal : ".$date." <br/>
            //                     Antrian : ".$antrian->no_antrian."
            //                 </div>
            //             </body>
            //         </html>";     
            /* tulis dan buka koneksi ke printer */  


               // Get file contents
           $fh = fopen("./tmp_print/".$filename.".pdf", "rb");
           $content = fread($fh, filesize("./tmp_print/".$filename.".pdf"));
           
           //$fh = fopen("./tmp_print/201709222039.html", "r");
           //$content = fread($fh, filesize("./tmp_print/201709222039.html"));
           //fclose($fh);
               
           // Set print mode to RAW and send PDF to printer
           $printer = printer_open("\\\\192.168.0.26\\EPSON L120 Series");
           printer_set_option($printer, PRINTER_MODE, "RAW");
           printer_write($printer, $content);
           printer_close($printer);


             
            // printer_set_option($printer, PRINTER_MODE, "RAW");   
            // printer_write($printer, $html);   
            // printer_close($printer);    
        //}else{ 
        //    echo "gagal";
        //}

    }


    function printstruk() {



        // if (!class_exists('mpdf')) {
        //     $this->load->library('mpdf');
        // }

        $this->load->model("Mcetakstruk","mcom"); 
        $id_ref_jenisantrian = $this->input->post('id_ref_jenisantrian');
        $antrian = $this->mcom->getantrian($id_ref_jenisantrian);

        $date = date("Ymd");
        $filename = $date."".$antrian->autono.".label";
        $filestruk = "./tmp_print/".$filename;


        // $pdfFilePath = base_url()."/tmp_print/".$filename.".pdf";
        // ini_set('memory_limit','32M'); // boost the memory limit if it's low ;)
        // $html = $this->load->view('pdf_report', $data, true); // render the view into    HTML
        // $this->load->library('pdf');
        // $pdf = $this->pdf->load();
        // $pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure ;)
        // $pdf->WriteHTML($html); // write the HTML into the PDF
        // $pdf->Output($pdfFilePath, 'F'); // save to file because we can
        $html = "";
        $html ="
                    <!DOCTYPE html>
                    <html lang='en'>
                    <head>
                    </head>
                    <style type='text/css' media='print'>
                    @page 
                    {
                        size:  320px;   /* auto is the initial value */
                        margin: 0mm;  /* this affects the margin in the printer settings */
                    }

                    html
                    {
                        background-color: #FFFFFF; 
                        margin: 0px;  /* this affects the margin on the html before sending to printer */
                    }

                    body
                    {
                        //border: solid 1px blue ;
                        height:320px;
                        margin: 5mm 3mm 5mm 3mm; /* margin you want for the content */
                    }
                    </style>


                        <script>
                        function loaded()
                        {
                            window.print();
                            window.setTimeout(CloseMe, 10);
                        }

                        function CloseMe() 
                        {
                            window.close();
                        }
                        </script>

                    <body onLoad=loaded()>";

        $imgpath = base_url()."assets/images/logostruk.png";
        $pdfFilePath = "tmp_print/".$date."".$antrian->autono.".pdf";
        $html .="<div align='left' style='width:240px;' id='SelectorToPrint'>";
        $html .="       <div style='padding-top:-10px;'>
                            <table width='250px'>
                                <tr>
                                    <td valign='top' width='50px'>
                                        <img src='".$imgpath."' width='30' height='40'>
                                    </td>
                                    <td valign='top' width='150px'>
                                        <p style='font-size:11px; letter-spacing: 0px; line-height:1px;'><b>BAPENDA KOTA BOGOR</b></p>
                                        <p style='font-size:9px; letter-spacing: 0px; line-height:1.5px;'>Jalan Pemuda No. 31 - Bogor </p>
                                        <p style='font-size:9px; letter-spacing: 0px; line-height:1.5px;'>Phone (0251) 835-0505 </p>
                                    </td>
                                </tr>
                            </table>
                        </div>";  
                $html .="<div align='center' style='font-size:9px;line-height:1.5px;'>==================================================</div>";
                $html .="<div align='center' style='font-size:10px;'>".$antrian->jenis_antrian."</div>";
                $html .="<div align='center' style='font-size:80px;padding-top:-5px;'><b>".$antrian->autono."</b></div>";
                $html .="<div align='center' style='font-size:10px;'>".$this->konversi_hari().", ".$this->tanggal_indo(date("Y-m-d"))."</div>";
                $html .="<div align='center' style='font-size:9px;'>Pukul ".date("H:i:s")."</div>";
                $html .="<div align='center' style='font-size:9px;'>&nbsp;</div>";
                //$html .="<div align='center'><b>Loket :".$antrian->loket."</b></div>";
                $html .="<div align='center' style='font-size:9px;'>Pajak Anda Membangun Bangsa</div>";
                $html .="<div align='center' style='font-size:9px;'>-- Terima Kasih --</div>";
                $html .="<div align='center' style='font-size:9px;'><br/></div>";
                $html .="<div align='center' style='font-size:9px;'><br/></div>";
        $html .="</div>";

        $html .="
                </body>
                </html>

                ";


        echo  $html;


       // $printer = printer_open("\\\\DESKTOP-BA1ARI4\\EPSON L120 Series");
       // printer_set_option($printer, PRINTER_MODE, "RAW");
       // printer_write($printer, $html);
       // printer_close($printer);


        // $this->mpdf->mPDF('',    // mode - default ''
        //         array(77,60),    // format - A4, for example, default '
        //           12,       // font size - default 0
        //           'Arial',  // default font family
        //           3,    // margin_left
        //           3,    // margin right
        //           8,    // margin top
        //           3,    // margin bottom
        //           1,    // margin header
        //           1,    // margin footer
        //           'p'); // L - landscape, P - portrait
        // $this->mpdf->WriteHTML($html);
        // $this->mpdf->Output($pdfFilePath,'I');

        //echo  $html;


        // //$filestruk = "./tmp_print/struk1.label";
        // $fb = fopen($filestruk, "w");
        // $sp_number  =$date."".$antrian->autono;
        // // ---- garis ------//
        // $dash = $this->spcText("-","-",30,1);
        // $dash_double = $this->spcText("-","-",30,1);
        // $br = "\n";
        // $len = 30 - strlen($sp_number);
        // fwrite($fb, $br);
        // //$len = 30 - strlen($date);
        // //fwrite($fb, $date);
        // fwrite($fb, $br);
        // fwrite($fb, $br);
        // fwrite($fb, " DISPENDA KOTA BOGOR".$br);
        // fwrite($fb, $dash . $br);
        // fwrite($fb, $sp_number . $br);
        // fwrite($fb, $dash . $br);
        // fwrite($fb, " NOMOR ANTRIAN : ". $this->strLeftRight(false, $antrian->autono, 11, "").$br);
        // fwrite($fb, " LOKET         : ". $this->strLeftRight(false, $antrian->loket, 11, "").$br);
        // fwrite($fb, $br);
        // fclose($fb);

        //$this->lastno($date."".$antrian->autono);
        // $data = file_get_contents($filestruk);
        // $name = $filename;
        // $this->force_download($name, $data);
        // echo  $filename;
        // exit;
    }


    function printIssue()
    {
        $your_printer_name = "\\\\DESKTOP-BA1ARI4\\EPSON L120 Series";
        $handle=printer_open($your_printer_name);
        //set the font characteristics here
        $font_face = "Draft Condensed";
        $font_height = 20;
        $font_width = 6;
        $font=printer_create_font($font_face,$font_height,$font_width,PRINTER_FW_THIN,false,false,false,0);
        printer_select_font($handle,$font);
        printer_write($handle,"My PDF file content below");
        //here loop through your pdf file and print the line by line or else get the entire content inside the string at once and print
        $your_pdf_file = "somethng.pdf";
        if(!eof($file_handle))
        {
            //do something
            printer_write($handle,$name);
        }
        printer_delete_font($font);
        printer_close($handle);
    }


    function open_with($filename){
        $data = file_get_contents("./tmp_print/".$filename.".label");
        $name = $filename.".label";

        print_r($name);
        exit;
        $this->force_download($name, $data);
    }

    function force_download($filename = '', $data = '')
    {

        if ($filename == '' OR $data == '')
        {
            return FALSE;
        }

        // Try to determine if the filename includes a file extension.
        // We need it in order to set the MIME type
        if (FALSE === strpos($filename, '.'))
        {
            return FALSE;
        }

    
        // Grab the file extension
        $x = explode('.', $filename);
        $extension = end($x);

        // Load the mime types
        @include(APPPATH.'config/mimes'.EXT);
    
        // Set a default mime if we can't find it
        if ( ! isset($mimes[$extension]))
        {
            $mime = 'application/octet-stream';
        }
        else
        {
            $mime = (is_array($mimes[$extension])) ? $mimes[$extension][0] : $mimes[$extension];
        }

     
    
        // Generate the server headers
        if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE"))
        {
            header('Content-Type: "'.$mime.'"');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header("Content-Transfer-Encoding: binary");
            header('Pragma: public');
            header("Content-Length: ".strlen($data));
        }
        else
        {
            header('Content-Type: "'.$mime.'"');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            header("Content-Transfer-Encoding: binary");
            header('Expires: 0');
            header('Pragma: no-cache');
            header("Content-Length: ".strlen($data));
        }
    
        exit($data);


    }

    function spcText($txt, $spasi, $spc, $orn){
        $fill = "";
        for($x=strlen($txt); $x<$spc;$x++){
            $fill .= $spasi;
        }
        if ($orn == 1){ $hsl = $txt . $fill; }
        else{ $hsl = $fill . $txt; }
        return $hsl;
    }

    function strLeftRight($isLeft, $str, $digit, $ch){
        $hsl = ""; $chr = "";

        $lengthStr = strlen($str);
        if ($digit > $lengthStr){
            $diff = $digit - $lengthStr;
            for ($i =1; $i<= $diff; $i++) {
                $chr .= $ch;
            }
            if($isLeft){
                $hsl = $str.$chr;
            } else{
                $hsl = $chr.$str;
            }
        }else {
            $hsl = substr($str, 0, $digit);
        }
        return $hsl;
    }

    function tanggal_indo($tanggal)
    {
        $bulan = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    function konversi_hari()
    {
        switch(date("l"))
        {
            case 'Monday':$nmh="Senin";break; 
            case 'Tuesday':$nmh="Selasa";break; 
            case 'Wednesday':$nmh="Rabu";break; 
            case 'Thursday':$nmh="Kamis";break; 
            case 'Friday':$nmh="Jum'at";break; 
            case 'Saturday':$nmh="Sabtu";break; 
            case 'Sunday':$nmh="minggu";break; 
        }
        return $nmh;
    }

        
}
