<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Fpp extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_FPP";
        $this->checkAuthorization($this->MOD_ALIAS);            
        $this->load->model("mfpp","mfpp");
        $this->load->config('email_cnf', TRUE);
    }
    
    public function index(){
        $this->list_fpp();
    }
    
    public function list_fpp(){
        $data['titlehead'] = "Daftar Reservasi Permohonan Pengujian :: PULSA";
        $flag_status = $this->uri->segment(4);
        $status_id = (int)$this->uri->segment(5);
        $by=null;
        if ($flag_status == "status" && $status_id == 2){
           $by["f.fpp_status_id >="]  = 2;
           $by["f.fpp_status_id !="]  = 6;
        }
        else if ($flag_status == "status" && $status_id == 7){
           $by['f.fpp_status_id']  = 7;           
        }
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'                            
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            HTTP_MOD_JS.'modules/main/fpp_list_js.js'            
        );

        // load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mfpp->get_fpp_list(null,$by);
        $data['res_cnt'] = count($data['result']);        
        
        $this->template->load('tmpl/vwbacktmpl','vfpp_list',$data);     
    }
    
    public function form_fpp(){
        $id = $this->uri->segment(4); 
        if ($id == "0") { $id = ""; }
        
        //--== parameetr redirect reservasi halaman home
        $_puslit_id = $this->uri->segment(5); // puslit id
        $_balai_id = $this->uri->segment(6); // balai id
        $_ujikat_id = $this->uri->segment(7); // kategori id                
        if (trim($_puslit_id) != "") {
            $_puslit_id = $this->qsecure->decrypt($_puslit_id);            
            $data['puslit_id'] = $_puslit_id;           
        }
        if (trim($_balai_id) != "") {
            $_balai_id = $this->qsecure->decrypt($_balai_id);            
            $data['balai_id'] = $_balai_id;
        }
        if (trim($_ujikat_id) != "") {
            $_ujikat_id = $this->qsecure->decrypt($_ujikat_id);            
            $data['ujikat_id'] = $_ujikat_id;           
        }
        
        $data['fpp_id'] = $id;
        if ( is_null($id) || trim($id) == '' ) {
            $data['titlehead'] = 'Form Reservasi Permohonan Pengujian :: PULSA';
            $data['fpp_tgl'] = date("Y-m-d");
            $data['fpp_status_id'] = -1;
        } else {
            $data['titlehead'] = 'Edit Reservasi Permohonan Pengujian :: PULSA';
            $id = $this->qsecure->decrypt($id);            
        }
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',                        
            HTTP_MOD_JS.'modules/main/fpp_form_js.js',            
        );
             
        //$loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
        
        // config rules validation
        $config = array (		
            array(
                    'field'=>'fpp_tgl',
                    'label'=>'Tgl Permohonan',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'puslit_id',
                    'label'=>'Nama Litbang',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'balai_id',
                    'label'=>'Balai Pelaksana',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'nama_sampel',
                    'label'=>'Nama Sampel',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'ujikat_id',
                    'label'=>'Kategori Pengujian',
                    'rules'=>'trim|required'
                    ), 
            array(
                    'field'=>'nama_proyek',
                    'label'=>'Nama Pekerjaan',
                    'rules'=>'trim|required'
                    ),
        );
 
	                    
       if( ! is_null($id) && (trim($id) != '') && ! $this->input->post('ujikat_id')) {
            // retrieve data;
            $result = $this->mfpp->get_fpp_list($id);
            if (count($result) > 0 ) {
                $data['fpp_kode'] = $result->fpp_kode;
                $data['fpp_tgl'] = $result->fpp_tgl;
                $data['instansi_id'] = $result->instansi_id;
                $data['nama_proyek'] = $result->nama_proyek;
                $data['ujikat_id'] = $result->ujikat_id;
                $data['fpp_status_id'] = $result->fpp_status_id;
                $data['total_lamauji'] = $result->total_lamauji;
                $data['total_harga'] = $result->total_harga;
                $data['nama_sampel'] = $result->nama_sampel;
                $data['jum_sampel'] = $result->jum_sampel;
                $data['sat_sampel'] = $result->sat_sampel;
                $data['puslit_id'] = $result->puslit_id;
                $data['balai_id'] = $result->dest_balai_id;
                $data['pay_method_id'] = $result->pay_method_id;
                $data['flag_kirimhasil'] = $result->flag_kirimhasil;
                $data['catatan'] = $result->catatan;
                $data['total_harga'] = format_angka($result->total_harga);
                $data['total_lamauji'] = $result->total_lamauji;
                
                //$this->set_sess_balai($result->dest_balai_id); // set session balai_id
                
                // load detail 
                $data['fpp_detail'] = $this->mfpp->get_fpp_detail_list($id);
                
            }
                        
       } 
       
       if ($_POST) { // POST
          
            // Form submited, check rules
            $this->form_validation->set_rules($config); 
            $this->form_validation->set_message('required', '%s harus diisi !');
            if($this->form_validation->run() === FALSE) {
                $data['err_msg'] = validation_errors();
            }else{
                $res = $this->mfpp->fpp_save($id);
                if ($res["success"] == true)
                    $this->session->set_flashdata('info', $res['msg']);
                else
                    $this->session->set_flashdata('err', $res['msg']);
                redirect('main/fpp/list_fpp');                                
            }
                
            
       }
                                           
        // option satuan sampel
        $data_satuan  = $this->mfpp->get_satuan_sampel();
        $list_satuan [''] = '- pilih satuan -';
        foreach ($data_satuan as $row) {
            $list_satuan[$row->ref_code] = $row->ref_name;
        }
        $data['list_satuan'] = $list_satuan;
        
        // option select puslit
        $data_puslit = $this->mfpp->get_puslit();
        $list_puslit [''] = '- pilih puslitbang -';
        foreach ($data_puslit as $row) {
            $list_puslit[$row->puslit_id] = $row->puslit_shortname;
        }
        $data['list_puslit'] = $list_puslit;
        
        // option select balai
        //$data_balai = $this->mfpp->get_puslit();
        $list_balai [''] = '- pilih balai -';        
        $data['list_balai'] = $list_balai;
        
         // option select kategori uji
        //$data_ujikat = $this->mfpp->get_kategori_uji();
        $list_ujikat [''] = '- pilih kategori -';
        //foreach ($data_ujikat as $row) {
        //    $list_ujikat[$row->ujikat_id] = $row->ujikat_name;
        //}
        $data['list_ujikat'] = $list_ujikat;
        
        // get profile user
        $data_profile = $this->mfpp->get_user_profile();
        $data['profile'] = $data_profile;
        
        // list jenis uji untuk popup modal
        $data['jenis_uji_list'] = $this->mfpp->get_ujijenis_list(null);
                            
        
        $this->template->load('tmpl/vwbacktmpl','vfpp_form',$data);     
    }
    
    public function get_balai_by($puslit_id)
    {            
        $data = $this->mfpp->get_balai(null,$puslit_id);
        //$build_array = array ("success"=>true,"results"=>$ttl,"data"=>array());
        $build_array = array ();
        if (count($data) > 0) {
            foreach($data as $row) {
                array_push($build_array,
                    array(
                        "balai_id" => $row->balai_id,
                        "balai_shortname" => $row->balai_shortname
                    )
                );
            }
        }    
        //$this->set_sess_puslit($puslit_id); // set session puslit_id
        echo json_encode($build_array);        
    }
    
    public function get_ujikat_by($balai_id)
    {            
        $data = $this->mfpp->get_kategori_uji_by($balai_id);        
        $build_array = array ();
        if (count($data) > 0) {
            foreach($data as $row) {
                array_push($build_array,
                    array(
                        "ujikat_id" => $row->ujikat_id,
                        "ujikat_name" => $row->ujikat_name
                    )
                );
            }
        }    
        //$this->set_sess_balai($balai_id); // set session balai_id
        echo json_encode($build_array);        
    }
         
    public function get_ujijenis_vw($ujikat_id)
    {            
        $data["jenis_uji_list"] = $this->mfpp->get_ujijenis_list($ujikat_id);
        $this->load->view("main/vfpp_popup_jenisuji", $data);
    }
    
    public function get_ujijenis_by()
    {            
        $ujikat_id = $this->uri->segment(4,true);
        //echo $ujikat_id; exit;
        $data = $this->mfpp->get_ujijenis_list($ujikat_id);        
        $build_array = array ("data"=>array());
        if (count($data) > 0) {            
            $i=1;
            foreach($data as $row) {
               $link = $row->jenisuji_id . ";".
                        $row->jenisuji_name  . ";".
                        $row->jenisuji_standard  . ";".
                        $row->jenisuji_harga  . ";".
                        $row->jenisuji_lama;
                
               $param = "slju_".$i;
                
               $jenisuji_name = "<a id='slju_".$i."' href='javascript:GetSelectedJU(\"".$param."\");' data-select='".$link."'>".$row->jenisuji_name."</a>";
                
                array_push($build_array["data"],
                    array(
                        //$row->jenisuji_id, 
                        //$row->jenisuji_name,
                        $jenisuji_name,
                        $row->jenisuji_standard, 
                        $row->jenisuji_harga,
                        $row->jenisuji_lama                      
                    )
                );
                $i++;
            }
        }  else{
            
        }       
        echo json_encode($build_array);        
    }
    
    public function delete_fpp($id){
        $id = $this->qsecure->decrypt($id);                 
        $res = $this->mfpp->delete_fpp($id);
        if ($res["success"] == true)
            $this->session->set_flashdata('info', $res['msg']);
        else
            $this->session->set_flashdata('err', $res['msg']);
        redirect('main/fpp/list_fpp');
    } 
    
     public function update_fpp($type,$id){                      
        $id = $this->qsecure->decrypt($id);       
        $dest_uid = 0;
        $link = base_url() . "main/fpp/list_fpp";
        if ($type == "request" AND $this->session->userdata(sess_prefix() . "rolealias") == "registered"){
            $dataUp["fpp_status_id"] = 2;            
            $msg = "Kirim permohonan uji %s";                                                            
            $row_fpp = $this->mfpp->get_fpp_list($id);
            $notif_subj = "Permohonan pengujian baru " . $row_fpp->fpp_kode;
            
            //-- send email            
            $subjectEmail = "[".$this->config->item('sitename', 'email_cnf')."] PERMOHONAN UJI BARU " .$row_fpp->fpp_kode;                
            $message = "<p>Kepada<br/>".$row_fpp->balai_name."<br/></br>"
                    . "Anda mendapatkan reservasi permohonan pengujian baru, berikut informasinya :</p>"
                    . "<table border='0'>"
                    . "   <tr>
                            <td width='200'>No Permohonan</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->fpp_kode."</td>
                          </tr>
                          <tr>
                            <td width='200'>Tgl. Permohonan</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->fpp_tgl."</td>
                          </tr>
                          <tr>
                            <td width='200'>Nama Perusahaan</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->instansi_name."</td>
                          </tr>
                          <tr>
                            <td width='200'>Alamat Perusahaan</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->instansi_address."</td>
                          </tr>
                          <tr>
                            <td width='200'>Email Perusahaan</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->instansi_email."</td>
                          </tr>
                          <tr>
                            <td width='200'>Kontak Person</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->created_by."</td>
                          </tr>
                          <tr>
                            <td width='200'>Telp/HP</td>
                            <td width='10'>:</td>
                            <td width='500'>".$row_fpp->phone."</td>
                          </tr>"
                    . "</table>";            
            $layoutEmail = get_layout_email($message,$link,true);            
        }
        else if ($type == "received" AND ( $this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR 
                 $this->session->userdata(sess_prefix() . "rolealias") == "admin") ){
            $dataUp["fpp_status_id"] = 3;                
            $dataUp["receipt_id"] = $this->session->userdata( sess_prefix()."userid");
            $dataUp["receipt_date"] = date("Y-m-d H:i:s");
            $msg = "Terima permohonan uji %s";   
            
            $row_fpp = $this->mfpp->get_fpp_list($id);
            $dest_uid = $row_fpp->userid;
            $notif_subj = "Permohonan uji " . $row_fpp->fpp_kode ." telah diterima";
            
            //-- send email            
            $subjectEmail = "[".$this->config->item('sitename', 'email_cnf')."] PERMOHONAN UJI " .$row_fpp->fpp_kode . " DITERIMA";
            $message = "<p>Kepada Bpk/Ibu<br/>".$row_fpp->created_by."<br/></br>"
                    . "Reservasi permohonan pengujian anda dengan nomor ".$row_fpp->fpp_kode." tanggal ".$row_fpp->fpp_tgl." telah kami terima, silakan lakukan pembayaran sejumlah "
                    . "Rp. <b>".  format_angka($row_fpp->total_harga)."</b> ditujukan ke rekening Bank BRI no rek. 987615211. "
                    . "Jika telah melakukan pembayaran silakan informasikan bukti transfer kepada kami dengan mengklik tombol "
                    . "<i>Konfirmasi Pembayaran</i> pada menu <i>Daftar Permohonan Uji</i></p>";
            $layoutEmail = get_layout_email($message,$link,true);
        }
                 
        else if ($type == "verified"  AND ( $this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR 
                 $this->session->userdata(sess_prefix() . "rolealias") == "admin") ){
            $dataUp["fpp_status_id"] = 5; 
            $dataUp["verified_id"] = $this->session->userdata( sess_prefix()."userid");
            $dataUp["verified_date"] = date("Y-m-d H:i:s");
            $msg = "Verifikasi pembayaran %s ";
            
            $row_fpp = $this->mfpp->get_fpp_list($id);
            $dest_uid = $row_fpp->userid;
            $notif_subj = "Pembayaran order " . $row_fpp->fpp_kode ." telah diverifikasi";
            
             //-- send email            
            $subjectEmail = "[".$this->config->item('sitename', 'email_cnf')."] PEMBAYARAN PERMOHONAN UJI " .$row_fpp->fpp_kode . " DIVERIFIKASI";
            $message = "<p>Kepada Bpk/Ibu<br/>".$row_fpp->created_by."<br/></br>"
                    . "Terima kasih telah melakukan pembayaran untuk permohonan pengujian nomor ".$row_fpp->fpp_kode." tanggal ".$row_fpp->fpp_tgl.", "
                    . "silakan cetak dokumen permohonan pengujian dengan mengklik icon print. "
                    . "Lampirkan dokumen hasil cetak saat ada mengirimkan sampel pengujian ke tempat kami."                    
                    . "</p>";
            $layoutEmail = get_layout_email($message,$link,true);
        }
         else if ($type == "finished"  AND ( $this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR 
                 $this->session->userdata(sess_prefix() . "rolealias") == "admin") ){
            $dataUp["fpp_status_id"] = 7; 
            $dataUp["verified_id"] = $this->session->userdata( sess_prefix()."userid");
            $dataUp["verified_date"] = date("Y-m-d H:i:s");
            $msg = "Update status selesai %s ";
            
            $row_fpp = $this->mfpp->get_fpp_list($id);
            $dest_uid = $row_fpp->userid;
            $notif_subj = "Pengujian untuk nomor permohonan " . $row_fpp->fpp_kode ." telah selesai dilakukan";
            
             //-- send email            
            $subjectEmail = "[".$this->config->item('sitename', 'email_cnf')."] PERMOHONAN UJI " .$row_fpp->fpp_kode . " SELESAI";
            $message = "<p>Kepada Bpk/Ibu<br/>".$row_fpp->created_by."<br/></br>"
                    . "Pengujian untuk nomor permohonan ".$row_fpp->fpp_kode." tanggal ".$row_fpp->fpp_tgl.", "
                    . "sudah selesai dilakukan, hasil pengujian sudah dapat diambil. Terima Kasih."
                    . "</p>";
            $layoutEmail = get_layout_email($message,$link,true);
        }
        if (! empty($dataUp)){            
            $res = $this->mfpp->update_fpp($id, $dataUp);
        }else{
            $res["success"] = false;
            $msg = "Maaf proses update status tidak dapat dilakukan !";
        }
        if ($res["success"] == true){                                   
            $res_msg = sprintf($msg, "berhasil..");
            $this->session->set_flashdata('info', $res_msg);
            
            $notif_date = date ("Y-m-d H:i:s");
            if ($dest_uid <= 0) {
                $notifTo = $this->mfpp->get_uid_byrole(2); // role admin                          
                foreach($notifTo as $row) {  //--== insert notif to all user admin
                    $dataNotif = array(
                        'notif_date' => $notif_date,
                        'src_uid' => $this->session->userdata( sess_prefix()."userid"),
                        'dest_uid' => $row->userid,
                        'notif_subj' => $notif_subj,
                        'notif_msg' => $notif_subj,
                        'is_read' => 0
                    );                
                    $resnotif = $this->mfpp->insert_notif($dataNotif);
                    
                    // send email
                    $this->mfpp->send_email($this->config->item('fromemail', 'email_cnf'),
                                      $this->config->item('fromname', 'email_cnf'),
                                      $row->email , "",
                                      $subjectEmail, $layoutEmail);
                }//endforeach insert notif
            }else{
                $dataNotif = array(
                    'notif_date' => $notif_date,
                    'src_uid' => $this->session->userdata( sess_prefix()."userid"),
                    'dest_uid' => $dest_uid,
                    'notif_subj' => $notif_subj,
                    'notif_msg' => $notif_subj,
                    'is_read' => 0
                );                
                $resnotif = $this->mfpp->insert_notif($dataNotif); 
                
                 // send email
                 $this->mfpp->send_email($this->config->item('fromemail', 'email_cnf'),
                                      $this->config->item('fromname', 'email_cnf'),
                                      $row_fpp->email , $row_fpp->created_by,
                                      $subjectEmail, $layoutEmail);
            }
            
            
        }else{
            $res_msg = sprintf($msg, "gagal !!");
            $this->session->set_flashdata('err', $res_msg);
        }
        redirect('main/fpp/list_fpp');
    }
    
     public function confirm_pay(){                      
        $id = $this->qsecure->decrypt( $this->input->post("confirmpay_fppid"));
        $ref_transfer_no = $this->input->post("no_bukti");                        
        if (trim($id) != "" && trim($ref_transfer_no) != "") {
            $dataUp["fpp_status_id"] = 4;             
            $dataUp["ref_transfer_no"] = $ref_transfer_no;             
            $msg = "Konfirmasi pembayaran %s";        

            $res = $this->mfpp->update_fpp($id, $dataUp);           
            if ($res["success"] == true){
                $res["msg"] = sprintf($msg, "berhasil..");  
                
                // insert notif
                $row_fpp = $this->mfpp->get_fpp_list($id);              
                $notif_subj = "Konfirmasi pembayaran " . $row_fpp->fpp_kode;             
                $notifTo = $this->mfpp->get_uid_byrole(2); // role admin
                $notif_date = date ("Y-m-d H:i:s");
                foreach($notifTo as $row) {    //--== insert notif to all user admin
                    $dataNotif = array(
                        'notif_date' => $notif_date,
                        'src_uid' => $this->session->userdata( sess_prefix()."userid"),
                        'dest_uid' => $row->userid,
                        'notif_subj' => $notif_subj,
                        'notif_msg' => $notif_subj,
                        'is_read' => 0
                    );                
                    $resnotif = $this->mfpp->insert_notif($dataNotif);                
                }//endforeach insert notif
                
            }else{
                $res["msg"] = sprintf($msg, "gagal, silakan coba kembali.. !!");                
            }
        }else{
            $res["success"] = false;
            $res["msg"] = "Konfirmasi pembayaran gagal, nomor bukti transfer harus diisi !";
        }
        
        echo json_encode($res);
    }
    
    public function print_fpp($id){        
        $fpp_id = $this->qsecure->decrypt(trim($id));
        if (trim($fpp_id) != "")
            $this->mfpp->print_fpp($fpp_id); 
        else
            redirect('main/fpp');
    }
    
}
