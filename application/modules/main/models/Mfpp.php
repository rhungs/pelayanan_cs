<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mfpp extends BSS_model
{
    protected $_table = "res_fpp";
    protected $_table_det = "res_fpp_det";
    protected $_id = "fpp_id";
    protected $_data = null;
                
    public function get_fpp_list($id=null,$by=null){                 
          $this->db->select("f.fpp_id, f.fpp_kode, f.fpp_tgl, f.instansi_id, f.nama_proyek, f.catatan, f.userid, f.created_date, f.ujikat_id, bl.puslit_id,
                            f.modified_date, f.receipt_id, f.receipt_date, f.verified_id, f.verified_date, f.fpp_status_id, f.total_lamauji,
                            f.total_harga, f.ref_transfer_no, f.nama_sampel, f.jum_sampel, f.sat_sampel, f.dest_balai_id, f.pay_method_id, f.flag_kirimhasil,
                            st.status_name, st.status_cls, i.instansi_name, i.instansi_address, i.instansi_email, i.instansi_phone, i.instansi_fax,  
                            bl.balai_name, pl.puslit_shortname, us1.full_name as created_by, us1.email, usp.phone, usp.address, uk.ujikat_name");
          $this->db->from($this->_table. " f");        
          $this->db->join("res_ref_instansi i","i.instansi_id=f.instansi_id","inner");
          $this->db->join("res_ref_balai bl","bl.balai_id=f.dest_balai_id","inner");
          $this->db->join("res_ref_puslit pl","pl.puslit_id=bl.puslit_id","inner");
          $this->db->join("res_ref_fpp_status st","st.status_id=f.fpp_status_id","inner");
          $this->db->join("res_sec_user us1","f.userid=us1.id","inner");
          $this->db->join("res_user_profile usp","us1.id=usp.userid","inner");
          $this->db->join("res_ref_ujikategori uk","uk.ujikat_id=f.ujikat_id","inner");
          
          if ($by != null) {
            $bye = $this->_by($by);          
            $this->db->where($bye,"",false);
          }
          
          if ($this->session->userdata(sess_prefix() . "rolealias") != "superadmin" AND
              $this->session->userdata(sess_prefix() . "rolealias") != "admin" AND $id == null) 
          {          
              $this->db->where("f.userid", $this->session->userdata(sess_prefix() . "userid"));
          }
          
          if ($id == null && $id == ""){
              //if ($offset != null && $limit != null){
              //  $this->db->limit($limit,$offset);
              //}                            
              $this->db->order_by("f.fpp_status_id","asc");          
              $this->db->order_by("f.fpp_kode","desc");
          }else{
                $this->db->where("f.fpp_id",$id);
          }
          
        
          
          $Q = $this->db->get();
          if ($id == null && $id == ""){
                $this->_data = $Q->result();
          }else{
                $this->_data = $Q->row();
          }
          $Q->free_result();
          return $this->_data;
    }

    public function get_fpp_list_count($by=null){
        $this->db->select("count(a.jenisuji_id) as _cnt");
        $this->db->from($this->_table . " a");
        $this->db->join("res_ref_ujikategori b","b.ujikat_id=a.ujikat_id","inner");
        $this->db->join("res_ref_ujiklasifikasi c","c.ujiklas_id=a.ujiklas_id","inner");
        if ($this->session->userdata(sess_prefix() . "rolealias") != "superadmin" AND
             $this->session->userdata(sess_prefix() . "rolealias") != "admin") 
        {          
            $this->db->where("f.userid", $this->session->userdata(sess_prefix() . "userid"));
        }
        
        if ($by != null) {
            $bye = $this->_by($by);
            $this->db->where($bye,"",false);
        }
        
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
     public function get_fpp_detail_list($fpp_id=null){
          $this->db->select("a.fpp_id, a.jenisuji_id, b.jenisuji_code, b.jenisuji_name, b.jenisuji_shortname, b.jenisuji_standard, 
                             b.jenisuji_lama, a.harga_sat");
          $this->db->from($this->_table_det ." a");        
          $this->db->join("res_ref_ujijenis b","b.jenisuji_id=a.jenisuji_id","inner");
          $this->db->where('a.fpp_id', $fpp_id);  
          $this->db->order_by("a.seq_id","asc");          
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;
     }
    
    public function get_ujijenis_list($ujikat_id=null){
          $this->db->select("a.jenisuji_id, a.jenisuji_code, a.jenisuji_name, a.jenisuji_shortname, a.jenisuji_standard, 
                             a.jenisuji_lama, a.ujikat_id, a.ujikat_id, b.ujikat_name, c.ujiklas_name, a.jenisuji_harga");
          $this->db->from("res_ref_ujijenis a");        
          $this->db->join("res_ref_ujikategori b","b.ujikat_id=a.ujikat_id","inner");
          $this->db->join("res_ref_ujiklasifikasi c","c.ujiklas_id=a.ujiklas_id","inner");
          if (! is_null($ujikat_id) && $ujikat_id != ""){
              $this->db->where('a.ujikat_id', $ujikat_id);                          
          }
                              
          $this->db->order_by("a.jenisuji_code","asc");          
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }
    
    public function get_satuan_sampel($id=null,$kode=null){
          $this->db->select("a.ref_id, a.ref_code, a.ref_name");
          $this->db->from("res_ref_param a");
          $this->db->where("a.ref_type", "SATUAN_QTY");
          if (! is_null($id) && $id != ""){
              $this->db->where('a.ref_id', $id);             
          }
          if (! is_null($kode) && $kode != ""){
              $this->db->where('a.ref_code', $kode);
          }
                              
          $this->db->order_by("a.ref_seq","asc");          
          $Q = $this->db->get();
          if ((! is_null($id) && $id != "") || (! is_null($kode) && $kode != "")){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          
          $Q->free_result();
          return $this->_data;
    }
    
    public function get_balai($balai_id=null,$puslit_id=null){
          $this->db->select("a.balai_id, a.balai_alias, a.balai_shortname");
          $this->db->from("res_ref_balai a");          
          $this->db->where('a.aktif', 1);
          if (! is_null($puslit_id) && $puslit_id != ""){
              $this->db->where('a.puslit_id', $puslit_id);
          }          
          if (! is_null($balai_id) && $balai_id != ""){
              $this->db->where('a.balai_id', $balai_id);             
          }
                                        
          $this->db->order_by("a.seq_balai","asc");          
          $Q = $this->db->get();
          if (! is_null($balai_id) && $balai_id != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }
    
    public function get_puslit($puslit_id=null){
          $this->db->select("a.puslit_id, a.puslit_alias, a.puslit_name, a.puslit_shortname");
          $this->db->from("res_ref_puslit a");          
          $this->db->where('a.aktif', 1);
          if (! is_null($puslit_id) && $puslit_id != ""){
              $this->db->where('a.puslit_id', $puslit_id);
          }          
                                                                    
          $Q = $this->db->get();
          if (! is_null($puslit_id) && $puslit_id != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }
    
    public function get_kategori_uji($ujikat_id=null){       
          $this->db->select("a.ujikat_id, a.ujikat_name, a.ujikat_desc");
          $this->db->from("res_ref_ujikategori a");          
          $this->db->where('a.aktif', 1);
          if (! is_null($ujikat_id) && $ujikat_id != ""){
              $this->db->where('a.ujikat_id', $ujikat_id);
          }          
                                                                    
          $Q = $this->db->get();
          if (! is_null($ujikat_id) && $ujikat_id != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }
    
    public function get_kategori_uji_by($balai_id=0){       
        $SQL = "SELECT a.ujikat_id, a.ujikat_name, a.ujikat_desc
                FROM res_ref_ujikategori a 
                WHERE a.aktif = 1 AND EXISTS (
                  select 1 from res_ref_balai_ujijenis bu inner join res_ref_ujijenis ju on ju.jenisuji_id = bu.jenisuji_id
                  where bu.balai_id = ? and ju.ujikat_id = a.ujikat_id
                )";                                              
        $Q = $this->db->query($SQL, array($balai_id));
        $this->_data = $Q->result();        
        $Q->free_result();
        return $this->_data;
    }
    
    public function fpp_save($id){
        if ($id == null OR $id == "") $id = 0;
        // POSTING VARIABLE
        $seq = 0;
        $det_item = "";
        foreach($_POST["ju_id"] as $rju) :
           $harga_satk = 0;
           $col = $rju . "~" . $harga_satk;
           $det_item .= $col . ";";
           $seq++;
        endforeach;
         
                                        
        $dataIn = array(
              'fpp_id' => $id,              
              'fpp_tgl' => date("Y-m-d",strtotime($this->input->post("fpp_tgl"))),
              'instansi_id' => $this->input->post("instansi_id"),
              'nama_proyek' => $this->input->post("nama_proyek"),
              'catatan' => $this->input->post("catatan"),                                 
              'ujikat_id' => $this->input->post("ujikat_id"),
              'total_lamauji' => $this->input->post("total_lamauji"),
              'total_harga' => remove_format_angka($this->input->post("total_harga")),
              'nama_sampel' => $this->input->post("nama_sampel"),
              'jum_sampel' => $this->input->post("jum_sampel"),
              'sat_sampel' => $this->input->post("sat_sampel"),
              'dest_balai_id' => $this->input->post("balai_id"),
              'pay_method_id' => "0",
              'flag_kirimhasil' => "0",
              'userid' => $this->session->userdata( sess_prefix()."userid"),
              'det_item' => $det_item,
              'det_cnt' => $seq                         
        );
        
        $SQL = "CALL usp_res_fpp_insupd (?,?,?,?,?,?,?,?,?,?,
                                        ?,?,?,?,?,?,?);";
        $query = $this->db->query($SQL, $dataIn);        
        $res = $query->row();
        
        if (is_object($res) && isset($res->v_retval) && $res->v_retval == "1")
        {
            $return["success"]=true;
            $return["msg"]="Permohonan pengujian berhasil disimpan...";
        }
        else
        {
            $return["success"]=false;
            $return["msg"]="Permohonan pengujian gagal disimpan !";
        }
        return $return;                                                                 
    }
        
    function update_fpp($id,$data){
        $this->db->trans_begin();
        //$this->db->where($id);
         $this->db->where("fpp_id",$id);
        $this->db->update($this->_table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    
    function delete_fpp($id){
        $this->db->trans_begin();
        
        //delete detail first
        $this->db->where("fpp_id",$id);
        $this->db->delete($this->_table_det);
        
        //delete header
        $this->db->where("fpp_id",$id);
        $this->db->delete($this->_table);
               
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="Permohonan pengujian gagal dihapus !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="Permohonan pengujian berhasil dihapus...";
        }
        return $return;
    }
    
    
    function insert_notif($data){
        $this->db->trans_begin();                 
        $this->db->insert("res_notif", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    
     public function get_uid_byrole($role_id=0){           
        $SQL = "SELECT u.id as userid, u.email FROM res_sec_user u where EXISTS(
                    select 1 from res_sec_user_role where role_id = ? and userid = u.id
               )";        
        $Q = $this->db->query($SQL, array($role_id));
        $this->_data = $Q->result();        
        $Q->free_result();
        return $this->_data;
    }
    
    function print_fpp($id){
        if (!class_exists('mpdf')) {
            $this->load->library('mpdf');
        }

         $hhtml = '
                <htmlpageheader name="myHTMLHeaderOdd" style="display:none">
                    <div align="left"><b>Reservasi Permohonan Pengujian - PULSA</b></div>
                    <hr/>
                </htmlpageheader>
                <htmlpagefooter name="myHTMLFooterOdd" style="display:none">                    
                    <table width="100%" style="vertical-align: bottom; font-family: Tahoma; font-size: 7pt; color: #000000; font-weight: normal; font-style: italic;">
                        <tr>
                            <td width="33%"><span style="font-weight: normal; font-style: italic;">Printed On {DATE j/m/Y h:i:s a}</span></td>
                            <td width="33%" align="center" style="font-weight: normal; font-style: italic;"></td>
                            <td width="33%" style="text-align: right;font-style: italic; ">{PAGENO}/{nbpg}</td>
                        </tr>
                    </table>
                </htmlpagefooter>
                <sethtmlpageheader name="myHTMLHeaderOdd" page="O" value="on" show-this-page="1" />
                <sethtmlpagefooter name="myHTMLFooterOdd" page="O" value="on" show-this-page="1" />
                ';
           //$mpdf=new mPDF('en-GB','A4');
           $this->mpdf->useOnlyCoreFonts = true;
           $this->mpdf->WriteHTML($hhtml);

           // get data co header
           $resHead = $this->get_fpp_list($id);
           $html ="
                <style type='text/css'>
                   td.title {
                        border-top: 1px solid #000;
                        border-bottom: 1px solid #000;
                        border-right: 0px;
                        border-left: 1px solid #000;                        
                    }
                    td.title_rgt {
                        border-top: 1px solid #000;
                        border-bottom: 1px solid #000;
                        border-right: 1px solid #000;
                        border-left: 1px solid #000;                        
                    }                                        
                    td.isi {
                        border-top: 0px;
                        border-bottom: 1px solid #000;
                        border-right: 0px;
                        border-left: 1px solid #000;
                    }
                    td.isi_rgt {
                        border-top: 0px;
                        border-bottom: 1px solid #000;
                        border-right: 1px solid #000;
                        border-left: 1px solid #000;
                    }
                 </style>
                 <br/>
                 <table width='800' border='0' style='font-size:14px;font-family:Tahoma,Arial;'>                    
                    <tr>
                        <td colspan='6' align='left' style='font-weight:bold;'>
                             &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align='left' colspan='3' valign='top'>DATA PERUSAHAAN</td>
                        <td align='left' colspan='3' valign='top'>KONTAK PERSON</td>
                    </tr>
                    <tr>
                        <td align='left' width='100' valign='top'>Nama</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' valign='top'>".$resHead->instansi_name."</td>
                        <td align='left' width='100' valign='top'>Nama</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280'>".$resHead->created_by."</td>
                    </tr>
                     <tr>
                        <td align='left' width='100' valign='top'>Alamat</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' valign='top'>".$resHead->instansi_address."</td>
                        <td align='left' width='100' valign='top'>Telp/HP</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280'>".$resHead->phone."</td>
                    </tr>
                    <tr>
                        <td align='left' width='100' valign='top'>Telp/Fax</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->instansi_phone." / ".$resHead->instansi_fax."</td>
                        <td align='left' width='100' valign='top'>Email</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280'>".$resHead->email."</td>
                    </tr>
                    <tr>
                        <td align='left' width='100' valign='top'>Email</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->instansi_email."</td>
                        <td align='left' width='100' valign='top'></td>
                        <td align='center' width='30' valign='top'></td>
                        <td align='left' width='280'></td>
                    </tr>

                    <tr>
                        <td colspan='6' align='left' style='font-weight:bold;'>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='6' align='left' style='font-weight:bold;'>
                            DATA ORDER
                        </td>
                    </tr>
                    <tr>
                        <td align='left' colspan='3'>
                            <barcode code='".$resHead->fpp_kode."' type='C128A' class='barcode' size='0.6' height='1.5' />
                        </td>
                    </tr>
                    <tr>
                        <td align='left' width='100'>Nomor</td>
                        <td align='center' width='30'>:</td>
                        <td align='left' width='290'><b>".$resHead->fpp_kode."</b></td>
                            <td align='left' width='100' valign='top'>Nama Sampel</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->nama_sampel."</td>
                    </tr>                    
                    <tr>
                        <td align='left' width='100' valign='top'>Pekerjaan</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' valign='top'>".$resHead->nama_proyek."</td>
                        <td align='left' width='100' valign='top'>Jumlah Sampel</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->jum_sampel." ".$resHead->sat_sampel."</td>
                    </tr>
                    <tr>
                        <td align='left' width='100' valign='top'>Puslitbang</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' valign='top'>".$resHead->puslit_shortname."</td>
                        <td align='left' width='100' valign='top'>Kategori Uji</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->ujikat_name."</td>
                    </tr>
                     <tr>
                        <td align='left' width='100' valign='top'>Balai</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' valign='top'>".$resHead->balai_name."</td>
                        <td align='left' width='100' valign='top'>Status</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='280' valign='top'>".$resHead->status_name."</td>
                    </tr>
                    <tr>
                        <td colspan='6' align='left' style='font-weight:bold;'>
                            <br/>
                        </td>
                    </tr>";
           
           // detail pengujian   
           $resUji = $this->get_fpp_detail_list($id);
           $html .= "
                    <tr>
                        <td colspan='6' align='left' style='font-weight:bold;'>
                            JENIS PENGUJIAN YANG DIMINTA
                        </td>
                    </tr>
                     <tr>
                        <td colspan='6' align='left'>
                            <table width='800' border='0' cellpadding='0' cellspacing='0'>
                                 <tr>
                                    <td class='title' align='center' width='30'>No</td>
                                    <td class='title' align='center' width='280'>Jenis Pengujian</td>
                                    <td class='title' align='center' width='170'>Standar Pengujian</td>
                                    <td class='title' align='center' width='80'>Lama<br>Uji/hari</td>
                                    <td class='title_rgt' align='right' width='120'>Harga&nbsp;<br>Satuan/Rp&nbsp;</td>                                    
                                 </tr>";                                 
                                 $j = 1;                                 
                                 foreach($resUji as $row){                                     
                                        $html .= "
                                            <tr>
                                               <td class='isi' align='center'>".$j."</td>
                                               <td class='isi' align='left'>".$row->jenisuji_shortname."</td>
                                               <td class='isi' align='center'>".$row->jenisuji_standard."</td>
                                               <td class='isi' align='center'>".$row->jenisuji_lama."</td>
                                               <td class='isi_rgt' align='right'>".format_angka($row->harga_sat)."&nbsp;&nbsp;</td>                                               
                                            </tr>";
                                        $j++;  
                                        
                                 }// endfor 
                  $html .= "
                                 <tr>
                                    <td class='isi' align='center' colspan='3'>TOTAL</td>
                                    <td class='isi' align='center'>".$resHead->total_lamauji."</td>                                    
                                    <td class='isi_rgt' align='right'>".format_angka($resHead->total_harga)."&nbsp;&nbsp;</td>
                                 </tr>";
           $html .= "
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' width='100' valign='top'>Catatan</td>
                        <td align='center' width='30' valign='top'>:</td>
                        <td align='left' width='290' colspan='4' valign='top'>".$resHead->catatan."</td>                        
                    </tr>
                </table>";                      
                  
           $this->mpdf->WriteHTML($html);
          // $this->mpdf->writeBarcode('978-1234-567-890');
           $this->mpdf->Output();           
    }
}
