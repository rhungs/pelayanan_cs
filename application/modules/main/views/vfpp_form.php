<div class="content-wrapper">
<section class="content">
    <script type="text/javascript">
     <!--
        var vlPUSLIT_ID = "<?=(isset($puslit_id) ? $puslit_id:'0')?>";var vlBALAI_ID = "<?=(isset($balai_id) ? $balai_id:'0')?>";var vlUJIKAT_ID = "<?=(isset($ujikat_id) ? $ujikat_id:'0')?>";
     -->
    </script>
    <div class="row">
        <div class="col-xs-12">
            
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Permohonan Pengujian</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="<?php echo base_url() . "main/fpp/form_fpp/" . (isset($fpp_id) ? $fpp_id :''); ?>" >
              <div class="box-body">
                <div class="row">
                    <?php
                        if (isset($err_msg) && $err_msg != "") { ?>
                            <div class="alert alert-danger">
                                <?=$err_msg?> 
                            </div>
                    <?php
                        }
                    ?>
                  <div class="col-md-6">
                        <input name="fpp_id" type="hidden" value="<?php echo set_value('fpp_id', (isset($fpp_id) ? $fpp_id :'')) ; ?>">
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_kode">No Permohonan</label>
                          <div class="col-sm-8">
                              <input type="text"  id="fpp_kode" name="fpp_kode" class="form-control" readonly="true" value="<?=(isset($fpp_kode) ? $fpp_kode : "")?>" placeholder="Otomatis">
                              <input type="hidden" name="instansi_id" id="instansi_id" value="<?php echo set_value('instansi_id', (isset($profile) ? $profile->instansi_id : '')) ?>" readonly />
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_tgl">Tgl Permohonan*</label>
                          <div class="col-sm-8">
                              <input name="fpp_tgl" id="fpp_tgl" class="form-control" readonly="true" value="<?=(isset($fpp_tgl) ? $fpp_tgl : "")?>" 
                                  <?php echo (isset($fpp_status_id) AND $fpp_status_id  > 1 ) ? 'disabled' : ''; ?> />
                          </div>
                        </div>
                    
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_tgl">Nama Pekerjaan*</label>
                          <div class="col-sm-8">
                              <input name="nama_proyek" id="nama_proyek" class="form-control" value="<?=(isset($nama_proyek) ? $nama_proyek : "")?>"
                                   <?php echo (isset($fpp_status_id) AND $fpp_status_id  > 1 ) ? 'disabled' : ''; ?> />
                          </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="puslit_id">Nama Litbang*</label>
                            <div class="col-md-8">
                                <?php   
                                    $disabled_puslit = '';
                                    if ($this->uri->segment(6) != "" OR ((isset($fpp_status_id) AND $fpp_status_id  > 1 )) ){
                                        $disabled_puslit = "disabled";
                                    } 
                                    $selectedlitbang = '';
                                    if(isset($puslit_id) && trim($puslit_id) != '') $selectedlitbang = $puslit_id;
                                    $js = 'id="puslit_name" class="form-control" '.$disabled_puslit;
                                    echo form_dropdown('puslit_name', $list_puslit, $selectedlitbang, $js);
                                ?>
                                <input name="puslit_id" id="puslit_id" value="<?=set_value('puslit_id',(isset($puslit_id) ? $puslit_id : ""))?>" type="hidden">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="balai_id">Balai Pelaksana*</label>
                            <div class="col-md-8">                  
                                 <?php                                 
                                    $disabled_balai = '';
                                    if ($this->uri->segment(6) != "" OR ((isset($fpp_status_id) AND $fpp_status_id  > 1 )) ){
                                        $disabled_balai = "disabled";
                                    } 
                                    $selected_balai = '';
                                    if(isset($balai_id) && trim($balai_id) != '') $selected_balai = $balai_id;
                                    $js = 'id="balai_name" class="form-control" '.$disabled_balai;
                                    echo form_dropdown('balai_name', $list_balai, $selected_balai, $js);
                                ?>
                                <input name="balai_id" id="balai_id" value="<?=set_value('balai_id',(isset($balai_id) ? $balai_id : ""))?>" type="hidden">
                            </div>
                        </div>
                                             
                  </div><!-- /.col-md-6 -->
                  
                  <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="nama_sampel">Nama Sampel*</label>
                          <div class="col-sm-8">
                            <input id="nama_sampel" name="nama_sampel" class="form-control" value="<?=(isset($nama_sampel) ? $nama_sampel : "")?>"
                                   <?php echo (isset($fpp_status_id) AND $fpp_status_id  > 1 ) ? 'disabled' : ''; ?> />
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="sampel">Jumlah Sampel*</label>
                            <div class="col-sm-4">
                                <input id="jum_sampel" name="jum_sampel" class="form-control" value="<?=(isset($jum_sampel) ? $jum_sampel : "")?>" onblur="checkNumber(this)" onkeyup="checkNumber(this)" 
                                       <?php echo (isset($fpp_status_id) AND $fpp_status_id  > 1 ) ? 'disabled' : ''; ?> />
                            </div>
                            <div class="col-sm-4">                    
                                <?php
                                    $disabled_satsampel = '';
                                    if ((isset($fpp_status_id) AND $fpp_status_id  > 1 ) ){
                                        $disabled_satsampel = "disabled";
                                    } 
                                    $selected_satsampel = '';
                                    if(isset($sat_sampel) && trim($sat_sampel) != '') $selected_satsampel = $sat_sampel;
                                    $js = 'id="sat_sampel" class="form-control" '.$disabled_satsampel;
                                    echo form_dropdown('sat_sampel', $list_satuan, $selected_satsampel, $js);
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4" for="ujikat_id">Kategori Pengujian*</label>
                            <div class="col-md-8">
                                <?php        
                                    $disabled_ujikat = '';
                                    //if ($this->uri->segment(7) != ""){
                                    //    $disabled_ujikat = "disabled";
                                    //} 
                                    if ((isset($fpp_status_id) AND $fpp_status_id  > 1 ) ){
                                        $disabled_ujikat = "disabled";
                                    } 
                                    $selected_ujikat = '';
                                    if(isset($ujikat_id) && trim($ujikat_id) != '') $selected_ujikat = $ujikat_id;
                                    $js = 'id="ujikat_id" class="form-control" onchange="UjiKatChange(this.value)" '.$disabled_ujikat ;
                                    echo form_dropdown('ujikat_id', $list_ujikat, $selected_ujikat, $js);
                                ?>
                            </div>
                        </div>                                                         
                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="catatan">Catatan</label>
                          <div class="col-sm-8">
                            <textarea class="form-control" name="catatan" id="catatan"><?=set_value('catatan',isset($catatan) ? $catatan : "")?></textarea>
                          </div>
                        </div>
                      
                  </div><!-- /.col-md-6 -->
                                                                
                </div>
                
                <!-- data-target="#modal-ju" -->                
                <div class="row" style="margin-top:10px;">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table id="tableDet" class="table table-bordered table-striped">
                          <thead>
                            <tr>                      
                              <th>Jenis Pengujian</th>
                              <th>Standar Pengujian</th>
                              <th>Harga/Rp</th>
                              <th>Lama Uji/Hari</th>
                              <th>
                                  <?php if (isset($fpp_status_id) AND $fpp_status_id  <= 1 ) { ?>
                                    <a id="tambah_ju"  data-title="Tambah" title="Tambah" data-toggle="modal"  class="btn btn-primary btn-xs pull-left"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
                                  <?php } ?>                                        
                              </th>
                            </tr>
                          </thead>
                          <tbody id="daftar_det"> 
                            <?php       
                                $det_ju_id = "";
                                if (isset($fpp_detail) && count($fpp_detail) > 0 ) {
                                    $seq_id = 1;                                    
                                    foreach($fpp_detail as $fd) { 
                                        $_attrju = $fd->harga_sat . "~" . $fd->jenisuji_lama;
                                        ?>
                                        <tr>
                                            <td><input id='ju_id<?=$seq_id?>' type='hidden' name='ju_id[]' value='<?php echo $fd->jenisuji_id; ?>' /><?php echo $fd->jenisuji_name ?></td>
                                            <td><?php echo $fd->jenisuji_standard; ?></td>
                                            <td><?php echo $fd->harga_sat; ?></td>
                                            <td><?php echo $fd->jenisuji_lama; ?></td>
                                            <td>
                                            <?php if (isset($fpp_status_id) AND $fpp_status_id  == 1 ) { ?>
                                                <button data-id='<?php echo $fd->jenisuji_id; ?>' data-attrju='<?=$_attrju?>' type='button' class='removebutton btn btn-danger btn-xs' title='Remove this row'><span class='glyphicon glyphicon-trash'></span> Delete</button>
                                            <?php } ?>
                                            </td>
                                        </tr>
                            <?php       
                                        $det_ju_id .= $fd->jenisuji_id . ";";
                                        $seq_id++;
                                    }//endforeach
                                 }//endif ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                <input type="hidden" id="det_ju_id" name="det_ju_id" value="<?=set_value('det_ju_id',isset($det_ju_id) ? $det_ju_id : '')?>" >
                 <div class="col-md-6" >
                 </div>
                 <div class="col-md-6" >
                    <div class="form-group">
                      <label class="control-label col-sm-6" for="telp">Total Harga :</label>
                      <div class="col-sm-6">
                          <div class="input-group">
                            <span class="input-group-addon">Rp. </span>
                            <input type="text" style="text-align:right;color:red;font-weight:bold;" class="form-control" name="total_harga" id="total_harga" value="<?php echo set_value('total_harga', (isset($total_harga) ? $total_harga : '')) ?>" readonly />
                          </div>
                      </div>
                    </div>
                     
                    <div class="form-group">
                      <label class="control-label col-sm-6" for="telp">Estimasi Waktu :</label>
                      <div class="col-sm-6">
                          <div class="input-group">
                            <input type="text" style="text-align:right;color:red;font-weight:bold;" class="form-control" name="total_lamauji" id="total_lamauji" value="<?php echo set_value('total_lamauji', (isset($total_lamauji) ? $total_lamauji : '')) ?>" readonly />
                            <span class="input-group-addon">hari</span>
                          </div>
                      </div>                      
                    </div> 
                </div>
                
              </div><!-- /.box-body -->
              <div class="box-footer">
                <?php if (isset($fpp_status_id) AND $fpp_status_id  <= 1 ) {  ?>
                    <button type="submit" id="simpan" class="btn btn-info" onclick="return confirm('Simpan data permohonan ?')">Simpan</button>
                    &nbsp;
                <?php } ?>
                <a onclick="return confirm('Batalkan pengisian data permohonan ?')" href="<?php echo base_url()."main/fpp/list_fpp"?>" class="btn btn-default" type="submit">Batal</a>                
              </div><!-- /.box-footer -->
              
              
              
            </form>
          </div><!-- /.box-info -->
          
          <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Identitas pengguna/pemohon</h3>
                </div>
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="col-md-6" >
                             <div class="form-group">
                                  <label class="control-label col-sm-4" for="instansi">Nama Instansi :</label>
                                  <div class="col-sm-8">                    
                                      <input type="text" class="form-control input-sm" name="instansi" id="instansi" value="<?php echo set_value('instansi', (isset($profile) ? $profile->instansi_name : '')) ?>" readonly />                            
                                  </div>
                             </div>

                            <div class="form-group">
                                <label class="control-label col-sm-4" for="kontak">Kontak Person :</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="contact" id="contact" value="<?php echo set_value('instansi', (isset($profile) ? $profile->full_name : '')) ?>" readonly />
                                </div>
                            </div>
                           
                        </div>
                        
                        <div class="col-md-6" >
                            <div class="form-group">
                              <label class="control-label col-sm-4" for="telp">Telepon :</label>
                              <div class="col-sm-4">
                                  <input type="text" class="form-control input-sm" name="telp" id="telp" value="<?php echo set_value('telp', (isset($profile) ? $profile->phone : '')) ?>" readonly />
                              </div>

                            </div>

                            <div class="form-group">
                              <label class="control-label col-sm-4" for="telp">Email :</label>

                              <div class="col-sm-8">                            
                                  <input type="email" class="form-control input-sm" name="email" id="email" value="<?php echo set_value('email', (isset($profile) ? $profile->email : '')) ?>" readonly />
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div><!-- /.box-success -->
          
          
        </div><!-- /col-xs-12 -->
     </div>

    <!-- modal-content Popup --> 
   <div class="modal fade" id="modal-ju" tabindex="-1" role="dialog" aria-labelledby="modal-ju" aria-hidden="true">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
           <h4 class="modal-title custom_align" id="Heading">Pilih jenis pengujian</h4>
         </div>
         <div class="modal-body overflow-edit">
             <div class="row" >

               <div class="box-body">
                 <table id="dt-popupju" class="table table-striped table-bordered table-hover" data-page-length="10">
                       <thead>
                       <tr>                      
                         <th>Jenis Pengujian</th>
                         <th>Standar Pengujian</th>
                         <th>Harga</th>
                         <th>Lama Uji</th>                      
                       </tr>
                       </thead>
                       <tbody style="font-size: 12px">                       
                       </tbody>
                 </table>
               </div>

           </div>

         </div>
         <div class="modal-footer ">
            <!--<button id="btnSelectJu" type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Pilih</button>-->
           <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <!-- /.modal-content --> 

</section>
</div>