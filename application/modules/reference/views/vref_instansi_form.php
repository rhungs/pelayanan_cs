<div id="ModalBalai" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">
                    Form Tambah Daftar Instansi
                </h3>
            </div><!-- /.modal-header -->
            
            <div class="modal-body">
                <?php echo form_open( 'reference/ref_instansi/insert_instansi', array( 'class'=>'form-horizontal' ) );?>
                    <?php
                        $label = array(
                            'class' => 'control-label col-md-3'
                        );
                        $input = array(
                            'class' => 'form-control'
                        );
                    ?>
                    <div class="form-group">
                        <?php echo form_label('Nama Instansi', 'nama_instansi', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'nama_instansi', 'id'=>'nama_instansi' ), set_value('nama_instansi'), $input); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Alamat', 'alamat', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'alamat', 'id'=>'alamat' ), set_value('alamat'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Telepon', 'telp', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'telp', 'id'=>'telp' ), set_value('telp'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Fax', 'fax', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'fax', 'id'=>'fax' ), set_value('fax'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Kota', 'kota', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'kota', 'id'=>'kota' ), set_value('kota'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Kode Pos', 'kodepos', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'kodepos', 'id'=>'kodepos' ), set_value('kodepos'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Provinsi', 'provinsi', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'provinsi', 'id'=>'provinsi' ), set_value('provinsi'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <?php echo form_label('Negara', 'negara', $label); ?>
                        <div class="col-md-6">
                            <?php echo form_input( array( 'name'=> 'negara', 'id'=>'negara' ), set_value('negara'), $input ); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <?php
                            echo form_reset( array('id'=>'reset'), 'Reset', array( 'class'=>'btn btn-danger' ) );
                            echo form_submit( array('id'=>'submit'), 'Submit', array( 'class'=>'btn btn-primary' ) );
                        ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->
                <?php echo form_close(); ?>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->