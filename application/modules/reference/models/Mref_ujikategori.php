<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_ujikategori extends BSS_model
{
    var $_table = "res_ref_ujikategori";
    var $_id = "ujikat_id";
    protected $_data = null;
                
    public function get_ujikat_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.ujikat_id, a.ujikat_name, a.aktif");
          $this->db->from($this->_table. " a");        
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.ujikat_name', $search_string);              
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.ujikat_id","asc");          
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_ujikat_count($search_string=null){
        $this->db->select("count(a.ujikat_id) as _cnt");
        $this->db->from($this->_table . " a");        
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.ujikat_name', $search_string);              
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
 
}
