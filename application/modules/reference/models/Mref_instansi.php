<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_instansi extends BSS_model
{
    var $_table = "res_ref_instansi";
    var $_id = "instansi_id";
    protected $_data = null;
                
    public function get_instansi_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.instansi_id, a.instansi_name, a.instansi_address, a.instansi_email, a.instansi_phone, a.instansi_fax, a.postal_code,
                             a.instansi_city, a.instansi_province, a.instansi_country, a.aktif");
          $this->db->from($this->_table. " a");         
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.instansi_name', $search_string);                          
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.instansi_name","asc");          
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_instansi_count($search_string=null){
        $this->db->select("count(a.instansi_id) as _cnt");
        $this->db->from($this->_table . " a");
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.instansi_name', $search_string);                          
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }  
    
}
