<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_ujijenisbalai extends BSS_model
{
    var $_table = "res_ref_balai_ujijenis";
    var $_id = null;
    protected $_data = null;
                
    public function get_ujijenisbalai_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.balai_id, b.balai_name, a.jenisuji_id, a.seq_id, ju.jenisuji_name, ku.ujikat_name, 
                            kl.ujiklas_name, pl.puslit_shortname");
          $this->db->from($this->_table. " a");        
          $this->db->join("res_ref_balai b","b.balai_id=a.balai_id","inner");
          $this->db->join("res_ref_ujijenis ju","ju.jenisuji_id=a.jenisuji_id","inner");
          $this->db->join("res_ref_ujikategori ku","ku.ujikat_id=ju.ujikat_id","inner");
          $this->db->join("res_ref_ujiklasifikasi kl","kl.ujiklas_id=ju.ujiklas_id","inner");
          $this->db->join("res_ref_puslit pl","pl.puslit_id=b.puslit_id","inner");
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.jenisuji_name', $search_string);
              $this->db->or_like('a.balai_name', $search_string);              
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.balai_id","asc");
          $this->db->order_by("a.seq_id","asc");
          $Q = $this->db->get();
          
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_ujijenisbalai_count($search_string=null){
        $this->db->select("count(a.balai_id) as _cnt");
        $this->db->from($this->_table. " a");        
        $this->db->join("res_ref_balai b","b.balai_id=a.balai_id","inner");
        $this->db->join("res_ref_ujijenis ju","ju.jenisuji_id=a.jenisuji_id","inner");
        $this->db->join("res_ref_ujikategori ku","ku.ujikat_id=ju.ujikat_id","inner");
        $this->db->join("res_ref_ujiklasifikasi kl","kl.ujiklas_id=ju.ujiklas_id","inner");
        $this->db->join("res_ref_puslit pl","pl.puslit_id=b.puslit_id","inner");
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.jenisuji_name', $search_string);
              $this->db->or_like('a.balai_name', $search_string);              
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
    
    public function get_balaiuji($balai_id){
        $this->db->where( 'balai_id', $balai_id );
        $data = $this->db->get($this->_table);
        $return = $data->result();
        $data->free_result();
        return $return;
        
    }
    
    public function delete_balaiuji($balai_id, $uji_id){
        $this->db->where('balai_id', $balai_id);
        $this->db->where('jenisuji_id', $uji_id);
        $exe = $this->db->delete($this->_table);
        return $exe;
    }
    
    public function get_ujijenisbalai($bid,$uid){
        $this->db->where( 'balai_id', $bid );
        $this->db->where( 'jenisuji_id', $uid );
        $data = $this->db->get( $this->_table );
        $return  = $data->result();
        $data->free_result();
        return $return;
    }
    
    
    
}
