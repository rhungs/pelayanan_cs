<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcommon extends BSS_Model{
     protected $_data = null;
     public function get_instansi_list($by=null, $sort=null, $dir="asc"){
        $this->db->select("instansi_id, instansi_name, instansi_address, instansi_email, instansi_phone, instansi_fax, postal_code,
                         instansi_city, instansi_province, instansi_country, aktif");
        $this->db->from("res_ref_instansi");        
        if($by!=null){
            $this->db->where($this->_by($by),"",false);
        }
        
        if ($sort == null OR $sort == ""){
            $this->db->order_by("instansi_name","asc"); 
        }else{
            $this->db->order_by($sort,$dir);
        }

        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;
    } 
}