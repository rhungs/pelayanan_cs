<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_ujikategori extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_KATUJI";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_ujikategori","mref");
        //$this->load->model('mref_base', 'mbase');
    }
    
    public function index($msg=''){
        $this->list_ujikategori($msg);
    }
    
    public function list_ujikategori($msg){
        $data['titlehead'] = "Daftar Kategori Uji :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'             
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',            
            HTTP_MOD_JS.'modules/reference/ref_ujikategori.js'
            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                        $('#dataTables-listujikat').DataTable({
                            responsive: true,                                
                            searching: true,
                            paging : false
                        });
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_ujikat_list();
        $data['res_cnt'] = count($data['result']);
        if( !empty($msg) ){
            $data['message'] = $this->get_message($msg);
        }
        $this->template->load('tmpl/vwbacktmpl','vref_ujikategori',$data);     
    }
    
    
    public function add_kategori(){
        
        $input = $this->input->post();
        $data = array(
            'ujikat_name' => $input['cat_name'],
            'ujikat_desc' => $input['cat_desc']
        );
        
        $exe = $this->mref->insert_record( $this->mref->_table, $data );
        if($exe){
            redirect('reference/ref_ujikategori/index/inserted');
        }else{
            redirect('reference/ref_ujikategori/index/add_failed');
        }
        
    }
    
    public function delete($id){
        $del_id = $this->qsecure->decrypt($id);
        $exec = $this->mref->delete_record($this->mref->_table, $this->mref->_id, $del_id);
        if($exec){
            redirect('reference/ref_ujikategori/index/deleted');
        }else{
            redirect('reference/ref_ujikategori/index/del_failed');
        }
    }
    
    public function edit($id){
        $ref_id = $this->qsecure->decrypt($id);
        $ref = $this->mref->get_record( $this->mref->_table, $this->mref->_id, $ref_id );
        foreach($ref as $item){
            $item->ujikat_id = $this->qsecure->encrypt($item->ujikat_id);
        }
        echo json_encode($ref);
    }
    
    public function update($id){
        $ref_id = $this->qsecure->decrypt($id);
        $input = $this->input->post();
        $data = array(
            'ujikat_name' => $input['cat_name_edit'],
            'ujikat_desc' => $input['cat_desc_edit']
        );
        $update = $this->mref->update_record( $this->mref->_table, $data, $this->mref->_id, $ref_id );
        if($update){
            redirect('reference/ref_ujikategori/index/updated');
        }else{
            redirect('reference/ref_ujikategori/index/upd_failed');
        }
    }
    function get_message($message){
        
        switch($message){
            
            case 'deleted':
                $title = 'Hapus Berhasil';
                $message = 'Penghapusan Daftar Kategori Uji berhasil dilakukan.';
                break;
            
            case 'inserted':
                $title = 'Tambah Berhasil';
                $message = 'Penambahan Daftar Kategori Uji berhasil dilakukan.';
                break;
            
            case 'updated':
                $title = 'Update Berhasil';
                $message = 'Pembaharuan Daftar Kategori Uji berhasil dilakukan.';
                break;
        }
        $icon = 'info';
        return message_box($title, $message, 'success', 'check', true );
    }
}
