<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cpenilaian extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_PENILAIAN360";
        $this->checkAuthorization($this->MOD_ALIAS);            
        $this->load->model("Mpenilaian","mfpp");
    }
    
    public function index()
    {
        $data['titlehead'] = 'Form Penilaian : BAPENDA KOTA BOGOR';
        $loadhead['stylesheet'] = array(
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );
        $loadfoot['javascript'] = array(        
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',               
            HTTP_MOD_JS.'modules/penilaian/form_customer.js',            
        );
        $uID=$this->session->userdata( sess_prefix()."userid");
        $data['flag_penilaian'] = $this->mfpp->CekStatusFlag($uID);
             
        //$loadfoot['tagjs'] = array($tagjs1);
        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;

        $this->template->load('tmpl/vwfrontpenilaian','v_form_customer',$data);           
    }


    public function datediff($tgl1,$tgl2){

        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        /*$data = array( "Tahun" => date("Y", $diff) - $base_year, 
                      "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, 
                      "Bulan" => date("n", $diff) - 1, 
                      "days_total" => floor($diff_secs / (3600 * 24)), 
                      "Hari" => date("j", $diff) - 1, 
                      "hours_total" => floor($diff_secs / 3600), 
                      "Jam" => date("G", $diff), 
                      "minutes_total" => floor($diff_secs / 60), 
                      "Menit" => (int) date("i", $diff), 
                      "seconds_total" => $diff_secs, 
                      "Detik" => (int) date("s", $diff) 
                    );*/

        $hasil = floor($diff_secs / 60)." Menit ".(int) date("s", $diff)." Detik";
        return  $hasil;
    }



    public function list_penilaian(){
        $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
        $role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
        
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
        
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mfpp->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res;
        $totalfiltered =  $this->mfpp->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
        $btn_edit = "";
        $btn_group ="";
        $stat_visiting="";
        $btn_delete="";
        $btn_mulai ="";

        $no=1;
        foreach($results as $row) {     

            $btn_mulai =" 
            <button  class='btn btn-block btn-success' onClick='f_mulai_penilaian(\"".$row->no_antrian." \");' type='button'>
                PILIH ANTRIAN
            </button>
            ";
            
            array_push($build_array["data"],
                array(  
                    $btn_mulai,  
                    $row->created_date,
                    $row->jenis_antrian,
                    $row->no_antrian,
                    $row->start_time,
                    $row->end_time,
                    $this->datediff($row->start_time,$row->end_time),
                    $row->indikator_category_name
                )
            );   
            $no++;          
        }      
                    
        echo json_encode($build_array);   
    }


    public function penilaian()
    {
        $data['titlehead'] = 'Form Penilaian : BAPENDA KOTA BOGOR';
        $loadhead['stylesheet'] = array(
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );
        $loadfoot['javascript'] = array(        
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',               
            HTTP_MOD_JS.'modules/penilaian/form_penilaian_list_js.js',            
        );
             
        //$loadfoot['tagjs'] = array($tagjs1);
        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
        $this->template->load('tmpl/vwfrontpenilaian','v_form_penilaian_list',$data);           
    }

    public function save_penilaian()
    {           

    }
     
    public function statuspenilaian()
    {
        $id = $this->input->post('flag_penilaian');    
        $no_antrian = $this->input->post('no_antrian');    
        $userid=$this->session->userdata( sess_prefix()."userid");
        $flag=$this->input->post('flag');   



        if($flag=="mulai"){
            $cat = array('flag_penilaian' => $id,
            'no_antrian' => $no_antrian,
            'start_time' => date('Y-m-d H:i:s'));
        }else if($flag=="refresh"){
            $cat = array('flag_penilaian' => $id,
            'no_antrian' => $no_antrian,
            'end_time' => date('Y-m-d H:i:s'));
        }else if($flag=="cancel"){
            $cat = array('flag_penilaian' => $id,
            'no_antrian' => "",
            'start_time' => "",
            'end_time' => "");
        }else{
            $cat = array('flag_penilaian' => $id,
            'no_antrian' => $no_antrian);
        }
        $this->mfpp->UpdateFlag($userid,$cat);

        if($flag=="cancel"){
            if($id==0){ 
                 $catins = array('flag_penilaian' => $id,
                        'no_antrian' => "",
                        'start_time' => "",
                        'end_time' => ""
                        );
                $this->mfpp->UpdateFlag($catins);
            }
        }else{
            if($id==0){ 
                $myString = $this->mfpp->CekLastNomorAntrian($userid);
                $myArray = explode(';', $myString);
                $no_antrian =$myArray[0];
                $start_time =$myArray[1];
                $end_time =$myArray[2];

                $catins = array('indikator_category_id' => 1,
                             'created_date' => date('Y-m-d H:i:s'),
                             'user_id' => $userid,
                             'no_antrian' => $no_antrian,
                             'start_time' => $start_time,
                             'end_time' => $end_time,
                             'instansi_id' => $this->session->userdata( sess_prefix()."instansi_id")
                            );
                $this->mfpp->insert_penilaian($catins);
            }
        }


        
        $res['status'] = 'update berhasil';
        echo json_encode($res);
    }

    public function post()
    {
        $userid = $this->session->userdata( sess_prefix()."userid");
        $myString = $this->mfpp->CekLastNomorAntrian($userid);

        $myArray = explode(';', $myString);
        $no_antrian =$myArray[0];
        $start_time =$myArray[1];


        $id = $this->input->post('indikator_id');    
        $cat = array('indikator_category_id' => $id,
                     'created_date' => date('Y-m-d H:i:s'),
                     'user_id' => $userid,
                     'no_antrian' => $no_antrian,
                     'start_time' => $start_time,
                     'end_time' => date('Y-m-d H:i:s'),
                     'instansi_id' => $this->session->userdata( sess_prefix()."instansi_id")
                    );
        $this->mfpp->insert_penilaian($cat);

        $cat_status = array('flag_penilaian' => 0,
                            'end_time' => date('Y-m-d H:i:s'));
        $this->mfpp->UpdateFlag($userid,$cat_status);

        $res['status'] = 'Approve berhasil';
        echo json_encode($res);
    }


    public function cek_session(){
        $uID = $this->session->userdata( sess_prefix()."userid");
        $x = $this->mfpp->CekStatusFlag($uID);
        echo $x;
    }

    

}
