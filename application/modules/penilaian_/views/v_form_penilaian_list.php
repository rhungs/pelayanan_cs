<body >

<div class="content" id="maincontent_default">
		<div class="col-md-12">
			<br/><br/>
			<div class="row">
	        	<div class="box-header" style="text-align: center;">
		            <h1><b>Selamat Datang di Bapenda Kota Bogor</b></h1>
		        </div>
	      	</div>
	       	<div class="row">
	       		<br/><br/><br/><br/>
	       	</div>
	       	</div>
	       	<div class="footer" style="text-align: center;">
				<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
				<h1 class="pull-left">LOKET <?=$this->session->userdata(sess_prefix() . "loket_id")?></h1>
			</div>
	</div>
</div>




<div class="content" id="maincontent">
		<div class="col-md-12">
			<br/><br/><br/>
			<div class="row">
	        	<div class="box-header">
		            <h1><b>Apakah anda puas dengan layanan kami ?</b></h1>
		        </div>
	      	</div>
		       	<div class="row">
		       		<table class="table no-border text-center tblpenilaian">
			       		<td>
			       			<button id="sangatpuas" class="btn btn-block btn-success btn-lg" type="button"> 
			       				<div><img src="<?php echo base_url().'assets/images/sangat_puas.png' ?>"></img></div>
			       				<div><h2>SANGAT PUAS</h2></div>
			       			</button>
			       		</td>
			       		<td>
			       			<button id="puas" class="btn btn-block bg-yellow btn-lg" type="button">
			       				<div><img src="<?php echo base_url().'assets/images/puas.png' ?>"></img></div>
			       				<div><h2>PUAS</h2></div>
			       			</button></td>
			       		<td>
			       			<button id="tidakpuas" class="btn btn-block btn-danger btn-lg" type="button">
			       				<div><img src="<?php echo base_url().'assets/images/tidak_puas.png' ?>"></img></div>
			       				<div><h2>TIDAK PUAS</h2></div>
			       			</button>
			       		</td>
		       		</table>
		       	</div>
	       	</div>
	       	<div class="footer">
				<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
				<h1 class="pull-right">LOKET <?=$this->session->userdata(sess_prefix() . "loket_id")?></h1>
			</div>
	</div>
</div>


<div class="modal fade" id="msginfo" tabindex="-1" role="dialog" aria-labelledby="ct_masalah" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	  </div>
	  <div class="modal-body">
	  	<div class="row">
			<span>
				<center>
					<h3>
					Terima Kasih atas partisipasi Anda dalam penilaian kepuasan pelanggan 
					<img class="img-responsive"  src="<?=base_url('assets/images/jempol.gif');?>"  alt="logo">
					</h3>
				</center>
			</span>
		</div>
	  </div>
	</div>
  </div>
</div>

</body>


