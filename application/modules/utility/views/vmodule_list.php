<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
    <li><i class="fa fa-share"></i> Utility</li>
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
<section class="content">   
    <div class="box box-primary">
        <div class="box-header with-border">           
            <a href="<?= base_url('utility/module_manage/edit_form')?>" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Tambah Modul</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <?php if($this->session->flashdata('message')) { ?>
                <script type="text/javascript">
                    window.setTimeout(function(){
                        $(".alert").alert('close');
                    },3000);
                </script>
                <div class="alert alert-success">            
                    <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                    <strong>Info! </strong><?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <script type="text/javascript">
                    window.setTimeout(function(){
                        $(".alert").alert('close');
                    },5000);
                </script>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
            <?php } ?>
       
            <table class="table table-striped table-bordered table-hover" id="dt-listmodule" data-page-length="50">
                <thead>
                    <tr>                                                                                                    
                        <th>Nama Modul</th>
                        <th>Alias</th>                       
                        <th>URL</th>                      
                        <th class="align-center">Urutan</th>
                        <th class="align-center">Icon</th>                      
                        <th>Kelompok Modul</th>                        
                        <th class="align-center">Aktif</th>
                        <th class="align-center">#</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $seq = 1;
                foreach($modules as $row) : ?>

                    <tr>                                                                   
                        <td><?php echo $row->treename; ?></td>
                        <td><?php echo $row->module_alias; ?></td>                    
                        <td><?php echo $row->module_url; ?></td>                    
                        <td  class="align-center"><?php echo $row->mod_seq; ?></td>
                        <td class="align-center"><i class="fa <?php echo $row->mod_icon_cls ?>" title="<?php echo $row->mod_icon_cls ?>"></i></td>
                        <td><?php echo $row->mod_group; ?></td>
                        <td class="align-center"><?php echo ($row->publish) ? anchor("utility/module_manage/deactivate/".$this->qsecure->encrypt($row->module_id), "<span class='fa fa-check text-green'>&nbsp;</span>".lang('index_active_link')."", array("onclick"=>"return confirm('Non-aktifkan modul ?');") ) : 
                                                         anchor("utility/module_manage/activate/". $this->qsecure->encrypt($row->module_id), "<span class='fa fa-times text-red'>&nbsp;".lang('index_inactive_link')."</span>", array("onclick"=>"return confirm('Aktifkan modul ?');"));?></td>                                                 
                        <td class="align-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-gear"></i> Aksi <span class="caret"></span>
                                </button>                             
                                <ul class="dropdown-menu" role="menu">                                 
                                    <li>                                    
                                        <?php
                                            $id = $this->qsecure->encrypt($row->module_id);
                                            $att = array(
                                                'data-toggle' => 'tooltip',
                                                'data-placement' => 'top',
                                            );
                                            $att['title'] = 'Edit';                                        
                                            echo anchor('utility/module_manage/edit_form/'.$id, '<i class="fa fa-fw fa-edit"></i>Edit',$att);
                                        ?>
                                    </li>   
                                    <?php
                                    if ($this->session->userdata (sess_prefix() . "rolealias") == "superadmin") { ?>
                                    <li>
                                        <?php                                                                               
                                            $att['title'] = 'Hapus';
                                            $att['onclick'] = "return confirm('Hapus modul ?');";
                                            echo anchor('utility/module_manage/delete/'.$id, '<i class="fa fa-fw fa-trash"></i>Hapus', $att);
                                        ?>
                                    </li>                                    
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>    
                        </td>
                    </tr>

                <?php    
                    $seq++;
                endforeach;

                ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</section><!-- /.content -->

