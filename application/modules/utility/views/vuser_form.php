<section class="content-header">
  <h1>   
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>   
    <li><i class="fa fa-share"></i> Utility</li>   
    <li class="active"><?=($titlehead)?></li>
  </ol>
</section>
    <section class="content">
        <div class="row">
            <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
                    <div class="alert alert-info alert-dismissable">
                          <a class="close" aria-hidden="true" data-dismiss="alert">x</a>                       
                        <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
                    </div>
                 <?php } ?>
                 <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <a class="close" aria-hidden="true" data-dismiss="alert">x</a>
                        <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
                    </div>
            <?php } ?>
           
            <?php echo form_open(uri_string(), array( 'class'=>'form-horizontal' ) ); ?>
            <?php
                $input = array(
                    'class' => 'form-control'
                );

                $label = array(
                    'class' => 'control-label col-md-4'
                );
            ?>
           
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Form Edit/Tambah User</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      
                        <div class="form-group">
                            <?php echo form_label( 'Username', 'username', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_input($username);
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                         <div class="form-group">
                            <?php echo form_label( 'Email', 'email', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_input( $email );
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Nama Lengkap', 'fullname', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_input( $full_name );
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Password', 'password', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_password($password);
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Konfirmasi Password', 'repassword', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_password( $password_confirm );
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Alamat', 'address', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                 echo form_textarea($address);
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Telp/HP', 'telephone', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                echo form_input($phone);
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                                                                                                                                                                                               
                       
                        <div class="form-group">
                            <?php echo form_label( 'User Role', 'userrole', $label ) ?>
                            <div class="col-md-8">
                            <?php
                                /*$role = array(
                                    'id' => 'userrole',
                                    'name' => 'userrole'
                                );
                               
                                $data = array(
                                    'value' => 'Label'
                                );
                                echo form_dropdown($role, $data, set_select('userrole'), $input);
                                */
                                $selectedrole = '';
                                if(isset($role_id) && trim($role_id) != '') $selectedrole = $role_id;
                                $js = 'id="role_id" class="form-control" ';
                                echo form_dropdown('role_id', $list_role, $selectedrole, $js);
                            ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <?php echo form_label( 'Avatar', 'avatar', $label ) ?>
                            <div class="col-md-8">
                                <select name="photo" id="photo"  class="form-control" >
                                  <?php
                                    foreach ($list_avatar as $la){                                                                                                               
                                        if ($la['img_name'] == (isset($user_photo) ? $user_photo : '')) {
                                            echo "<option value='".$la['img_name']."' data-icon='". HTTP_ASSET_IMG . $la['img_ico'] ."' selected>".$la['img_title']."</option>";
                                        }else{
                                            echo "<option value='".$la['img_name']."' data-icon='". HTTP_ASSET_IMG . $la['img_ico'] ."' >".$la['img_title']."</option>";
                                        }
                                    }
                                  ?>
                              </select>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                       
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                                <?php
                                    //$btn = array(
                                    //    'class'=>'btn btn-danger'
                                    //);
                                    //echo form_reset(array('name'=>'reset','id'=>'reset'), 'Reset', $btn);
                                    $btn = array(
                                        'class'=>'btn btn-primary'
                                    );
                                    echo form_submit(array('name'=>'Submit', 'id'=>'submit'), 'Submit', $btn);
                                ?>
                                <a href="<?php echo base_url()."utility/user_manage"?>" class="btn btn-default" type="submit">Batal</a>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->
                                                                      
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
           
                              <div class="box box-info"><!-- collapsed-box -->
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            Informasi Loket
                        </h4>
                        <!--div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-plus"></i></button>
                        </div><!-- /.box-tools -->
                       
                    </div><!-- /.box-header -->
                    <div class="box-body">
                                                 
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="nama_perusahaan">Nama Loket</label>
                          <div class="col-sm-8">
                                <div class="input-group">
                                    <?php echo form_input($instansi_name);?>
                                    <?php echo form_input($instansi_id);?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" id="btn_cari_ins_mu">
                                            <span class="fa fa-search"></span>
                                            Cari
                                        </button>
                                    </span>
                                </div>
                          </div>
                        </div>

                        <div class="form-group">
                            <?php echo form_label('Alamat', 'alamat', $label); ?>
                            <div class="col-md-8">
                                <?php echo form_input($instansi_address); ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <?php echo form_label('Telepon', 'telp', $label); ?>
                            <div class="col-md-8">
                                <?php echo form_input($instansi_phone); ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <?php echo form_label('Fax', 'fax', $label); ?>
                            <div class="col-md-8">
                                <?php echo form_input($instansi_fax); ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->

                        <div class="form-group">
                            <?php echo form_label('Email', 'instansi_email', $label); ?>
                            <div class="col-md-8">
                                <?php echo form_input($instansi_email); ?>
                            </div><!-- /.col -->
                        </div><!-- /.form-group -->

                        
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
           
   
               
            </div><!-- /.col -->
            <?php echo form_hidden('email_edit',$email_edit); ?>
            <?php echo form_hidden('username_edit',$username_edit); ?>
           
            <?php echo form_hidden('id', $id);?>
            <?php echo form_hidden($csrf); ?>
           
           <?php echo form_close(); ?>
           
           
        </div><!-- /.row -->
    </section><!-- /.content -->




<!-- modal-content Popup -->
   <div class="modal fade" id="modal-instansi-mu" tabindex="-1" role="dialog" aria-labelledby="modal-instansi-mu" aria-hidden="true">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
           <h4 class="modal-title custom_align" id="Heading">Pilih Instansi</h4>
         </div>
         <div class="modal-body overflow-edit">
             <div class="row" >

               <div class="box-body">
                 <table id="dt-popup-instansi-mu" class="table table-striped table-bordered table-hover" data-page-length="10">
                       <thead>
                       <tr>                     
                         <th>Nama Instansi</th>
                         <th>Alamat</th>
                         <th>Kota</th>
                         <th>Telp</th>                     
                       </tr>
                       </thead>
                       <tbody style="font-size: 12px">                      
                       </tbody>
                 </table>
               </div>

           </div>

         </div>
         <div class="modal-footer ">
           <button id="btn_select_ins_mu" type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Pilih</button>
           <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <!-- /.modal-content --> 