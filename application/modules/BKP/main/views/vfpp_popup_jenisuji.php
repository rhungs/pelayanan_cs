 
    <!-- modal-content Popup --> 
   <div class="modal fade" id="modal-ju" tabindex="-1" role="dialog" aria-labelledby="modal-ju" aria-hidden="true">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
           <h4 class="modal-title custom_align" id="Heading">Pilih jenis pengujian</h4>
         </div>
         <div class="modal-body overflow-edit">
             <div class="row" >

               <div class="box-body">
                 <table id="dt-popupju" class="table table-striped table-bordered table-hover" data-page-length="10">
                       <thead>
                       <tr>                      
                         <th>Jenis Pengujian</th>
                         <th>Standar Pengujian</th>
                         <th>Harga</th>
                         <th>Lama Uji</th>                      
                       </tr>
                       </thead>
                       <tbody style="font-size: 12px">
                       <?php
                           $i=1;
                           foreach($jenis_uji_list as $rju) {                               
                               $link =  $rju->jenisuji_id . ";".
                                        $rju->jenisuji_name  . ";".
                                        $rju->jenisuji_standard  . ";".
                                        $rju->jenisuji_harga  . ";".
                                        $rju->jenisuji_lama;
                               $param = "slju_".$i;
                       ?>

                               <tr>                              
                                 <td>
                                     <a id="slju_<?php echo $i; ?>" href='javascript:GetSelectedJU("<?=$param?>");' data-select='<?php echo $link ?>' >
                                         <?php echo $rju->jenisuji_name;?></a>
                                 </td>
                                 <td><?php echo $rju->jenisuji_standard ?></td>
                                 <td align="right"><?php echo $rju->jenisuji_harga ?></th>
                                 <td><?php echo $rju->jenisuji_lama ?></td>                      
                               </tr>
                       <?php
                           $i++;
                           }
                       ?>
                       </tbody>
                 </table>
               </div>

           </div>

         </div>
         <div class="modal-footer ">
           <button id="btnSelectJu" type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Pilih</button>
           <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <!-- /.modal-content --> 
 