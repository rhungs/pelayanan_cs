<div id="ModalFppConfPay" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">          
            <form id="FormConfirm">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Konfirmasi Pembayaran</h4>
            </div>
            <div class="modal-body">                
                <div class="form-group" id="frmg_no_bukti">
                    <input type="hidden" id="confirmpay_fppid" name="confirmpay_fppid" />
                <?php
                    $input_data = array(
                        'name' => 'no_bukti',
                        'id' => 'no_bukti',
                        'value' => set_value('no_bukti'),
                        'class' => 'form-control'
                    );
                    echo form_label('Nomor bukti transfer', 'no_bukti');
                    echo form_input($input_data);
                ?>
                </div><!-- /.form-group -->
                
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="submit_confirmpay" type="button" class="submit-btn btn btn-primary">Submit</button>                
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="content-wrapper">
    <section class="content">
         <?php if($this->session->flashdata('info')) { ?>
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">x</a>
                <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert">x</a>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">DAFTAR PERMOHONAN PENGUJIAN</h3>
               <?php 
                    if ($this->session->userdata(sess_prefix() . "rolealias") == "registered") { ?>
                        <a href="<?= base_url('main/fpp/form_fpp')?>" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
               <?php } ?>
            </div>
            <div class="box-header">
                <?php 
                    if ($this->session->userdata(sess_prefix() . "rolealias") == "registered") { ?>
                        <button onclick="btnSendOrder_Click()" type="button" class='btn btn-info btn-sm' id="btnSendOrder" title="Kirim Permohonan" >
                            <span class='fa fa-send-o'></span>&nbsp;Kirim Permohonan
                        </button>
                
                        <button onclick="btnConfirmPay_Click()" class='btn btn-warning btn-sm' href="#" id="btnConfirmPay" title="Konfirmasi Permohonan" disabled >
                            <span class='fa fa-shield'></span>&nbsp;Konfirmasi Pembayaran
                        </button>
                <?php } ?>
                
                <?php 
                    if ($this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR
                        $this->session->userdata(sess_prefix() . "rolealias") == "admin") { ?>
                            <button onclick="btnReceipt_Click()" type="button" class='btn btn-success btn-sm' id="btnReceipt" title="Terima Permohonan" disabled >
                                <span class='fa fa-check-circle'></span>&nbsp;Terima Permohonan
                            </button>
                
                            <button onclick="btnVerify_Click()" class='btn btn-warning btn-sm' href="#" id="btnVerify" title="Verifikasi Permohonan" disabled >
                                   <span class='fa fa-shield'></span>&nbsp;Verifikasi Pembayaran
                            </button>
                            
                            <button onclick="btnFinish_Click()" class='btn btn-danger btn-sm' href="#" id="btnFinish" title="Pengujian Selesai" disabled >
                                   <span class='fa fa-flag'></span>&nbsp;Pengujian Selesai
                            </button>
                <?php } ?>
                                                         
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
              <table class="table table-bordered table-hover" id="dataTables-listfpp" data-page-length='10'>
                <thead>
                <tr>                    
                    <th>#</th>
                    <th>No.</th>
                    <th>No Permohonan</th>
                    <th>Tanggal</th>                    
                    <th>Nama Sampel</th>
                    <th>Nama Pekerjaan</th>
                    <th>Kontak Person</th>
                    <th>Nama Instansi</th>
                    <th>Status</th>
                </tr>
                </thead>
                </tbody>
                <?php
                $seq = 1;
                foreach($result as $row) : ?>
                
                    <tr data-id="<?php echo $this->qsecure->encrypt($row->fpp_id) ?>" data-status="<?php echo $row->fpp_status_id ?>" data-code="<?php echo $row->fpp_kode ?>" >                         
                        <td class="align-center">                           
                            <?php echo anchor("main/fpp/form_fpp/".$this->qsecure->encrypt($row->fpp_id), "<span class='fa fa-edit'></span>", array('class'=>'btn btn-info btn-xs','title'=>'Edit')) ;?>                                                            
                            
                            <?php if ($row->fpp_status_id <= 3) {// DELETE ?>
                                <a class='btn btn-danger btn-xs' href="#" onClick="if(confirm('Delete permohonan pengujian ? ')){document.location='<?php echo base_url() ."main/fpp/delete_fpp/".$this->qsecure->encrypt($row->fpp_id); ?>'; }" title="Delete" >
                                    <span class='fa fa-trash-o'></span>&nbsp;
                                </a>
                            <?php } ?>
                            <a class='btn btn-primary btn-xs' href="#" onClick="if(confirm('Cetak permohonan pengujian ? ')){window.open('<?php echo base_url() ."main/fpp/print_fpp/".$this->qsecure->encrypt($row->fpp_id); ?>'); }" title="Print" >
                                <span class='fa fa-print'></span>&nbsp;
                            </a> 
                        </td>
                        <td><?php echo $seq; ?></td>
                        <td><?php echo $row->fpp_kode; ?></td>
                        <td><?php echo $row->fpp_tgl; ?></td>
                        <td><?php echo $row->nama_sampel; ?></td> 
                        <td><?php echo $row->nama_proyek; ?></td>                         
                        <td><?php echo $row->created_by; ?></td>
                        <td><?php echo $row->instansi_name; ?></td> 
                        <td class="align-center"><span class="<?php echo $row->status_cls; ?>"><?php echo $row->status_name; ?></span></td> 
                    </tr>
                
                <?php    
                    $seq++;
                endforeach;
                
                ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

