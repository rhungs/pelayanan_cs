<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
	<li><i class="fa fa-share"></i> Utility</li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>


    <section class="content">
       <?php if($this->session->flashdata('message')) { ?>
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">x</a>
                <strong>Info! </strong><?php echo $this->session->flashdata('message'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert">x</a>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">DAFTAR USER</h3>
                <a href="<?= base_url('utility/user_manage/form_user')?>" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Tambah User</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
                <!-- <table class="table table-condensed table-striped table-bordered table-hover" id="dataTables-listuser" data-page-length='25'>-->
				<table class="table table-striped table-bordered table-hover" id="dt-listuser" data-page-length="50">
                    <thead>
                        <tr>                    
                            <th>Username</th>                                        
                            <th>Nama Lengkap</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Alamat</th>                            
                            <th>Loket</th>
                            <th>Aktif</th>
                            <th class="align-center"># Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->


