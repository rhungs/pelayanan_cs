<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
    <li><i class="fa fa-share"></i> Utility</li>
    <li><a href="<?php echo base_url() ?>utility/role_manage" > Pengaturan Role</a></li>
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
<section class="content">
    <div class="row">       
        <?php echo form_open(uri_string(), array( 'class'=>'form-horizontal' ) ); ?>
        <?php
            $input = array(
                'class' => 'form-control'
            );

            $label = array(
                'class' => 'control-label col-sm-2'
            );
        ?>

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-info-circle"></i> <i><b>(*)</b> harus diisi !</i>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
                            <div class="alert alert-info alert-dismissable">
                                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                                <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
                            </div>
                         <?php } ?>
                         <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                                <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
                            </div>
                    <?php } ?>

                    <div class="row">                   
                        <div class="col-md-9"> 
                    
                            <div class="form-group">
                                <?php echo form_label( 'Nama Role*', 'role_name', $label ) ?>
                                <div class="col-sm-8">
                                <?php
                                    echo form_input($role_name);
                                ?>
                                </div><!-- /.col -->
                            </div><!-- /.form-group -->
                            
                            <div class="form-group">
                                <?php echo form_label( 'Alias*', 'role_alias', $label ) ?>
                                <div class="col-sm-8">
                                <?php
                                    echo form_input($role_alias);
                                ?>
                                </div><!-- /.col -->
                            </div><!-- /.form-group -->
                            
                        </div><!-- /.col-md-9 -->                     
                    </div> 
                    
                </div><!-- /.box-body -->
                
                <div class="box-footer">
                     <button id="submit" type="submit" class='btn btn-primary pull-left btn-sm'><span class="fa fa-save"></span> Simpan</button>
                     &nbsp;&nbsp;<a href="<?php echo base_url()."utility/role_manage"?>" class="btn btn-default btn-sm" type="submit"><span class="fa fa-close"></span> Batal</a>
                </div><!-- /.box-footer -->
                 
            </div><!-- /.box -->
        </div><!-- /col-xs-12 -->
                
        <?php echo form_hidden('id', $id);?>
        <?php echo form_hidden('role_alias_edit', $role_alias_edit);?>
        <?php echo form_hidden($csrf); ?>
        <?php echo form_close(); ?>
        
    </div><!-- /.row -->
</section><!-- /.content -->