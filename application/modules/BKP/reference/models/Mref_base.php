<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is base model for reference model.
 * Contain common function to reduce repeated same function writing such as inserting, editing and deleting records.
 * 
 * @author Fazri Alfan Muaz <fazri@inov8-software.com>
 * @since 1.0
 */

class Mref_base extends CI_Model{
    
    /**
     * All method move to BSS_model() by kiQ     
     */
     
    
}