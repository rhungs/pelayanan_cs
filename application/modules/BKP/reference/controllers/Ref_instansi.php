<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_instansi extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_INSTANSI";
        //$this->checkAuthorization($this->MOD_ALIAS);
        $this->load->model("mref_instansi","mref");
    }
    
    public function index($msg=''){
        $this->list_instansi($msg);
    }
    
    public function list_instansi($msg){
        $data['titlehead'] = "Daftar Loket :: BAPENDA KOTA BOGOR";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'                          
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.rowGrouping.js',
            HTTP_MOD_JS.'modules/reference/ref_instansi.js'
            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                       $('#dataTables-listinstansi')
                            .DataTable({      
                                responsive: true,    
                                searching: true,
                                paging : true
                            });                       
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_instansi_list();
        $data['res_cnt'] = count($data['result']);
        if( !empty($msg) ){
            $data['message'] = $this->get_message($msg);
        }
        $this->template->load('tmpl/vwbacktmpl','vref_instansi',$data);     
    }

    public function insert_instansi(){
        
        $data = $this->input->post();
        $insert = array(
            'instansi_name' => $data['nama_instansi'],
            'instansi_address' => $data['alamat'],
            'postal_code' => $data['kodepos'],
            'instansi_email' => $data['email'],
            'instansi_phone' => $data['telp'],
            'instansi_fax' => $data['fax'],
            'instansi_city' => $data['kota'],
            'instansi_province' => $data['provinsi'],
            'instansi_country' => $data['negara'],
            
        );
        
        $exe = $this->mref->insert_record( $this->mref->_table, $insert );
        
        if($exe){
            redirect('reference/ref_instansi/index/inserted');
        }else{
            redirect('reference/ref_instansi/index/ins_failed');
        }
             
    }
    
    function delete($id){
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->delete_record( $this->mref->_table, $this->mref->_id, $ref_id );
        
        if($exe){
            redirect('reference/ref_instansi/index/deleted');
        }else{
            redirect('reference/ref_instansi/index/del_failed');
        }
    }
    
    function edit($id){
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->get_record( $this->mref->_table, $this->mref->_id, $ref_id);
        foreach($exe as $item){
            $item->instansi_id = $this->qsecure->encrypt($item->instansi_id);
        }
        echo json_encode($exe);
    }
    
    function update_instansi($id){
        $data = $this->input->post();
        $insert = array(
            'instansi_name' => $data['nama_instansi'],
            'instansi_address' => $data['alamat'],
            'postal_code' => $data['kodepos'],
            'instansi_email' => $data['email'],
            'instansi_phone' => $data['telp'],
            'instansi_fax' => $data['fax'],
            'instansi_city' => $data['kota'],
            'instansi_province' => $data['provinsi'],
            'instansi_country' => $data['negara'],
        );
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->update_record( $this->mref->_table, $insert, $this->mref->_id, $ref_id);
        if($exe){
            redirect('reference/ref_instansi/index/updated');
        }else{
            redirect('reference/ref_instansi/index/upd_failed');
        }
    }
    
    function get_message($message){
        
        switch($message){
            
            case 'deleted':
                $title = 'Hapus Berhasil';
                $message = 'Penghapusan Daftar Institusi berhasil dilakukan.';
                break;
            
            case 'inserted':
                $title = 'Tambah Berhasil';
                $message = 'Penambahan Daftar Institusi berhasil dilakukan.';
                break;
            
            case 'updated':
                $title = 'Update Berhasil';
                $message = 'Pembaharuan Daftar Institusi berhasil dilakukan.';
                break;
        }
        $icon = 'info';
        return message_box($title, $message, 'success', 'check', true );
    }
    
            
    
}
