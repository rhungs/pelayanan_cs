<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_puslitbalai extends Bss_controller {
            
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_BALAI";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_pusbalai","mref");
        $this->load->model( 'mref_puslitbang', 'mpuslit' );
        $this->load->model( 'mref_base', 'refbase' );
    }
    
    public function index($message=''){
        $this->list_balai($message);
    }
    
    public function list_balai($message=''){
        $data['titlehead'] = "Daftar Balai :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'               
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            HTTP_MOD_JS.'modules/reference/ref_puslitbalai.js'
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                        $('#dataTables-listbalai').DataTable({
                            responsive: true,                                
                            searching: true,
                            paging : true
                        });
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_balai_list();
        $data['res_cnt'] = count($data['result']);
        
        $data['list_puslitbang'] = $this->get_list_puslitbang();
        
        if( !empty($message) ){
            $data['message'] = $this->get_message($message);
        }
        
        $this->template->load('tmpl/vwbacktmpl','vref_balai',$data);     
    }
    
    function get_list_puslitbang(){
        
        $items = $this->mpuslit->get_puslit_list();
        
        foreach( $items as $item ){
            $result[$item->puslit_id] = $item->puslit_name;
        }
        
        return $result;
        
    }
    
    function add_new_balai(){
        $input = $this->input->post();
        
        $insert = array(
            'balai_alias' => $input['alias_balai'],
            'balai_name' => $input['nama_balai_full'],
            'balai_shortname' => $input['alias_balai'],
            'puslit_id' => $input['nama_puslitbang'],
            'aktif' => 1
        );
        
        $exe = $this->mref->insert_nama_balai($insert);
        
        if($exe){
            redirect('reference/ref_puslitbalai/index/inserted');
        }else{
            exit;
        }
    }
    
    function delete_balai($id){
        $del_id = $this->qsecure->decrypt($id);

        $delete = $this->refbase->delete_record( $this->mref->_table, $this->mref->_id, $del_id  );
        
        if( $delete ){
            redirect('reference/ref_puslitbalai/index/deleted');
        }else{
            exit;
        }
    }
    
    function get_message($message){
        
        switch($message){
            
            case 'deleted':
                $title = 'Hapus Berhasil';
                $message = 'Penghapusan data balai berhasil dilakukan.';
                break;
            
            case 'inserted':
                $title = 'Tambah Berhasil';
                $message = 'Penambahan data balai berhasil dilakukan.';
                break;
            
            case 'updated':
                $title = 'Update Berhasil';
                $message = 'Pembaharuan data balai berhasil dilakukan.';
                break;
        }
        $icon = 'info';
        
        return message_box($title, $message, 'success', 'check', true );
    }
    
    function edit_balai($id){
        
        $edit_id = $this->qsecure->decrypt($id);
        $balai_data = $this->mref->get_balai($edit_id);
        foreach($balai_data as $item){
            $item->balai_id = $this->qsecure->encrypt($item->balai_id);
        }
        echo json_encode($balai_data);
    }
    
    function update_balai($id){
        $input = $this->input->post();
        $data = array(
            'puslit_id' => $input['nama_puslitbang'],
            'balai_alias'  => $input['alias_balai'],
            'balai_name'  => $input['nama_balai_full'],
            'balai_shortname'  => $input['nama_balai_short']
        );
        $balai_id = $this->qsecure->decrypt($id);
        $update = $this->mref->update_balai( $data, $balai_id );
        if($update){
            redirect('reference/ref_puslitbalai/index/updated');
        }else{
            exit;
        }
    }
    
}
