<div class="content-wrapper">
    <section class="content">
        <?php
        if(isset($message)){
            echo $message;
        }
        ?>
        <div class="row">
            <div class="col-md-4">
                <div id="form_add" class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Daftar Klasifikasi Uji</h3>
                    </div><!-- /.box -->
                    <div class="box-body">
                        <?php echo form_open('reference/ref_ujiklasifikasi/add_ujiklasifikasi', array( 'id'=>'AddUjiKlasifikasi' ) ); ?>
                        <div class="form-group">
                        <?php
                            echo form_label('Klasifikasi Uji', 'klasifikasi_uji', array( 'class'=>'control-label' ) );
                            echo form_input( array( 'name'=>'klasifikasi_uji', 'id'=>'klasifikasi_uji', 'class'=>'form-control' ) );
                        ?>
                            <p class="description">Masukkan nama Klasifikasi Uji yang ingin ditambahkan.</p>
                        </div><!-- /.form-group -->
                        <?php
                            echo form_submit( array('class'=>'btn btn-primary'), 'Tambah Daftar' );
                        ?>
                        <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                
                <div id="form_edit" class="box box-info" style="display:none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Daftar Klasifikasi Uji</h3>
                    </div><!-- /.box -->
                    <div class="box-body">
                        <?php echo form_open('reference/ref_ujiklasifikasi/update', array( 'id'=>'EditUjiKlasifikasi' ) ); ?>
                        <div class="form-group">
                        <?php
                            echo form_label('Klasifikasi Uji', 'klasifikasi_uji_edit', array( 'class'=>'control-label' ) );
                            echo form_input( array( 'name'=>'klasifikasi_uji_edit', 'id'=>'klasifikasi_uji_edit', 'class'=>'form-control' ) );
                        ?>
                            <p class="description">Masukkan nama Klasifikasi Uji yang baru.</p>
                        </div><!-- /.form-group -->
                        <?php
                            echo form_submit( array('class'=>'btn btn-primary'), 'Edit Daftar' );
                            echo form_reset( array('id'=>'reset', 'value'=>'Cancel', 'class'=>'btn btn-warning' ) );
                        ?>
                        <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">DAFTAR KLASIFIKASI UJI</h3>

        <!--              <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                          <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                          <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </div>-->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">              
                      <table class="table table-striped table-bordered table-hover" id="dataTables-listujiklas" data-page-length='10'>
                        <thead>
                        <tr>
                            <th class="align-center">No.</th>
                            <th>Klasifikasi Uji</th>
                            <th class="align-center"># Aksi</th>
                        </tr>
                        </thead>
                        </tbody>
                        <?php
                        $seq = 1;
                        foreach($result as $row) : ?>

                            <tr>
                                <th class="align-center"><?php echo $seq; ?></th>
                                <td><?php echo $row->ujiklas_name; ?></td>
                                <td class="align-center">
                                <?php
                                    $anc_edit = array(
                                        'uri' => 'reference/ref_ujiklasifikasi/edit/'.$this->qsecure->encrypt($row->ujiklas_id),
                                        'title' => '<i class="fa fa-fw fa-edit"></i> Edit',
                                        'attr' => array(
                                            'class' => 'btn btn-info btn-xs edit_ref'
                                        )
                                    );
                                    
                                    $anc_delete = array(
                                        'uri' => 'reference/ref_ujiklasifikasi/delete/'.$this->qsecure->encrypt($row->ujiklas_id),
                                        'title' => '<i class="fa fa-fw fa-trash"></i> Delete',
                                        'attr' => array(
                                            'class' => 'btn btn-danger btn-xs delete_ref'
                                        )
                                    );
                                    echo anchor( $anc_edit['uri'], $anc_edit['title'], $anc_edit['attr'] );
                                    echo nbs();
                                    echo anchor( $anc_delete['uri'], $anc_delete['title'], $anc_delete['attr'] );
                                ?>
                                </td>
                            </tr>

                        <?php    
                            $seq++;
                        endforeach;

                        ?>
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                    <div class="overlay" style="display:none"></div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!--/.row -->
        
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

