<div class="content-wrapper">
    <section class="content">
        <?php if($this->session->flashdata('info')) { ?>
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">x</a>
                <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert">x</a>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
        <?php } ?>
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">DAFTAR PUSLITBANG</h3>               
              <a href="<?= base_url('reference/ref_puslitbang/form_puslit')?>" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Tambah Puslitbang</a>
<!--              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>-->
            </div>
            <!-- /.box-header -->
            <div class="box-body">              
              <table class="table table-striped table-bordered table-hover" id="dataTables-listpuslit" data-page-length='10'>
                <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Puslitbang</th>
                    <th>Nama Singkatan</th>
                    <th>Singkatan/Alias</th>
                    <th class="align-center"># Aksi</th>
                </tr>
                </thead>
                </tbody>
                <?php
                $seq = 1;
                foreach($result as $row) : ?>
                
                    <tr>
                        <td><?php echo $seq; ?></td>
                        <td><?php echo $row->puslit_name; ?></td>
                        <td><?php echo $row->puslit_shortname; ?></td>
                        <td><?php echo $row->puslit_alias; ?></td> 
                        <td class="align-center">
                            <?php 
                                echo anchor("reference/ref_puslitbang/form_puslit/".$this->qsecure->encrypt($row->puslit_id), "<i class='fa fa-fw fa-edit'></i>", array("class"=>"btn btn-info btn-xs"));
                             ?>  
                           
                            <a href="#" onClick="if(confirm('Delete data puslitbang ? ')){document.location='<?php echo base_url() ."reference/ref_puslitbang/delete_puslit/".$this->qsecure->encrypt($row->puslit_id); ?>'; }" title="Delete" class="btn btn-danger btn-xs">
                                <i class='fa fa-fw fa-trash-o'></i>
                            </a>
                        </td>
                    </tr>
                
                <?php    
                    $seq++;
                endforeach;
                
                ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

