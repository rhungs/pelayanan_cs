<div id="ModalBalai" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo form_open( 'reference/ref_puslitbalai', array( 'id' => 'FormBalai' ) ); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Daftar Balai</h4>
            </div>
            <div class="modal-body">                
                <div class="form-group">
                <?php
                    $input_data = array(
                        'name' => 'nama_balai_full',
                        'id' => 'nama_balai_full',
                        'value' => set_value('nama_balai_full'),
                        'class' => 'form-control'
                    );
                    echo form_label('Nama Lengkap Balai', 'nama_balai_full');
                    echo form_input($input_data);
                ?>
                </div><!-- /.form-group -->
                
                <div class="form-group">
                <?php
                    $input_data = array(
                        'name' => 'nama_balai_short',
                        'id' => 'nama_balai_short',
                        'value' => set_value('nama_balai_short'),
                        'class' => 'form-control'
                    );
                    echo form_label('Nama Pendek Balai', 'nama_balai_short');
                    echo form_input($input_data);
                ?>
                </div><!-- /.form-group -->
                
                <div class="form-group">
                <?php
                    $input_data = array(
                        'name' => 'alias_balai',
                        'id' => 'alias_balai',
                        'value' => set_value('alias_balai'),
                        'class' => 'form-control'
                    );
                    echo form_label('Singkatan/Alias', 'alias_balai');
                    echo form_input($input_data);
                ?>
                </div><!-- /.form-group -->
                
                <div class="form-group">
                <?php
                $att = array(
                    'class' => 'form-control',
                    'id' => 'nama_puslitbang'
                );
                    echo form_label('Puslitbang', 'nama_puslitbang');
                    echo form_dropdown( 'nama_puslitbang', $list_puslitbang, set_value('nama_puslitbang'), $att );
                ?>
                </div><!-- /.form-group -->                
            </div><!-- /.modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php echo form_submit('submit', 'Add Balai', array( 'class' => 'submit-btn btn btn-primary' ) ); ?>
            </div>
            <?php echo form_close(); ?>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="content-wrapper">
    <section class="content">
        <?php
        if(isset($message)){
            echo $message;
        }
        ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">DAFTAR BALAI</h3>
                <button type="button" class="add_ref btn btn-primary btn-sm pull-right">
                    <span class="fa fa-plus"></span>
                    Tambah Balai
                </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
              <table class="table table-striped table-bordered table-hover" id="dataTables-listbalai" data-page-length='10'>
                <thead>
                <tr>
                    <th class="align-center">No.</th>
                    <th>Nama Balai</th>                                        
                    <th>Singkatan/Alias</th>
                    <th>Nama Puslitbang</th>
                    <th class="align-center"># Aksi</th>
                </tr>
                </thead>
                </tbody>
                <?php
                $seq = 1;
                foreach($result as $row) : ?>
                
                    <tr>
                        <td class="align-center"><?php echo $seq; ?></td>
                        <td><?php echo $row->balai_name; ?></td>
                        <td><?php echo $row->balai_alias; ?></td>
                        <td><?php echo $row->puslit_shortname; ?></td> 
                        <td class="align-center"><?php
                            echo anchor('reference/ref_puslitbalai/edit_balai/'.$this->qsecure->encrypt($row->balai_id), '<i class="fa fa-fw fa-edit"></i>', array( 'class'=>'btn btn-info btn-xs edit_ref', 'title'=>'Edit', 'data-toggle' => 'tooltip', 'data-placement' => 'right' ) );
                            echo nbs(1);
                            echo anchor('reference/ref_puslitbalai/delete_balai/'.$this->qsecure->encrypt($row->balai_id), '<i class="fa fa-fw fa-trash"></i>', array( 'class'=>'btn btn-danger btn-xs', 'title'=>'Delete', 'data-toggle' => 'tooltip', 'data-placement' => 'left', "onclick"=>"if(!confirm('Ingin menghapus balai ini?')){event.preventDefault()}" ) );
                        ?></td>
                    </tr>
                
                <?php    
                    $seq++;
                endforeach;
                
                ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

