<style type="text/css">
	th { font-size: 17px; }
	td { font-size: 17px; font-weight:bold; }
</style>

<body>
<div class="content">
	<div class="col-md-12">
		<div class="row">
			<div class="pull-right">
			   <?php
			        echo anchor( 'auth/logout', '<span class="fa fa-sign-out"></span>&nbsp;Log Out', 'class="btn btn-default btn-flat" title="Log Out"' );
			    ?>
			</div>
			<div class="pull-right" style="padding-right:20px;">
				 <?php
			        echo anchor( 'penilaian/Cpenilaian/penilaian', '<span class="fa fa-sign-out"></span>&nbsp;Form Pengisian Kepuasan Pelanggan >>', 'class="btn btn-default btn-flat" title="Form Pengisian Kepuasan Pelanggan >>"' );
			    ?>
			</div>
			<div class="pull-left">
		        <div class="col-md-12">
					<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
		        </div>
		        <div class="col-md-12">
				<h1><b><?=$this->session->userdata(sess_prefix() . "instansi_name")?></b></h1>
					<div>
						<h3><b>Selamat Datang, <i><?=$this->session->userdata(sess_prefix() . "full_name")?></i></b></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<br/><br/><br/><br/>
	       	<div class="row">
				<input  type="hidden" name="flag_penilaian" id="flag_penilaian" class="form-control" value="<?=(isset($flag_penilaian) ? $flag_penilaian : "")?>">
	       		<table class="table no-border text-center">
		       		<tr>
		       			<td>
		       				<label class="pull-left" id="lblnomor" style="font-size:18pt;margin-right:10px;" >Masukan Nomor Antrian</label>
		       				<input type="text" name="no_antrian" id="no_antrian"  style="width:30%; height: 70px;font-size: 25pt;background-color: #fafeb1;
		       					input[type=text]:focus {
								    width: 250px;
								}
		       					" class="form-control" value="<?=(isset($no_antrian) ? $no_antrian : "")?>">
		       			</td>
		       			<td>
			       		</td>
			       		<td></td>
			       		<td align="right">	
			       			<button id="cancel" class="glyphicon pull-right" type="button">
			       				<div><img width="70px" src="<?php echo base_url().'assets/images/cancel.jpg' ?>"></img></div>
			       			</button>
			       			<button id="refresh" class="glyphicon pull-right" type="button">
			       				<div><img width="70px" src="<?php echo base_url().'assets/images/refresh-button.png' ?>"></img></div>
			       				<!-- <br/><h3>Refresh</h3><br/> -->
			       			</button>
			       		</td>
		       		</tr>
	       			<tr>
			       		
			       		<td colspan="4">
			       			<button id="selesai" class="btn btn-block btn-info" type="button">
			       				<div><h2>SELESAI PELAYANAN</h2></div>
			       			</button>
			       		</td>
<!-- 			       		<td colspan="4" >
			       			<button id="mulai" class="btn btn-block btn-success" type="button"> 
			       				<div><h2>MULAI PELAYANAN</h2></div>
			       			</button>
			       		</td> -->
		       		</tr>

	       		</table>
	       	</div>
       	</div>


		<div class="col-md-12">
	        <div class="box">
	            <div class="box-header">
	                 Data Kepuasan Pelanggan Hari Ini  	                       
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
					<table class="table table-striped table-bordered table-hover" id="dt-listindikator" >
						<thead>
						<tr>    
							<th>#</th>                
							<th>Tanggal</th>
							<th>Jenis Antrian</th>
							<th>Nomor Antrian</th>
							<th>Mulai Pelayanan</th>
							<th>Selesai Pelayanan</th>
							<th>Durasi</th>
							<th>Indikator Penilaian</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div><!-- /.box-body -->
	        </div><!-- /.box -->
        </div>




	</div>
</div>


<div class="modal fade  modal-info" id="msginfo" tabindex="-1" role="dialog" aria-labelledby="ct_masalah" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	  	<h3 class="modal-title">Info</h3>
	  </div>
	  <div class="modal-body">
	  	<div class="row">
			<span>
				<center>
					<h3>
					Status berhasil diubah ...
					</h3>
				</center>
			</span>
		</div>
	  </div>
	</div>
  </div>
</div>

</body>


