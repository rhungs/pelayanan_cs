<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mpenilaian extends Bss_model
{
	protected $_table = "ref_penilaian";
    protected $_id = "thang";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null)
    {
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $id_ref_jenisantrian = $this->session->userdata(sess_prefix()."id_ref_jenisantrian");
        $this->db->select("a.created_date, b.indikator_category_name, c.full_name, a.no_antrian, 
        a.start_time, a.end_time, TIMESTAMPDIFF(MINUTE,a.start_time ,  a.end_time) as durasi , d.nama as jenis_antrian");
        $this->db->from("indikator_penilaiann a");
        $this->db->join("ref_indikator_cat b","a.indikator_category_id=b.indikator_category_id","left");
        $this->db->join("res_sec_user c","a.user_id=c.id","left");
        $this->db->join("ref_jenisantrian d","a.id_ref_jenisantrian=d.id","inner");
        //$this->db->where("a.user_id",$user_id);
        $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");
        $this->db->where("a.id_ref_jenisantrian",$id_ref_jenisantrian);
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( b.indikator_category_name LIKE '%$search_string%'  OR
                                a.no_antrian LIKE '%$search_string%'
                                )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
                $this->db->order_by("b.indikator_category_name","ASC");  
                $this->db->order_by("a.no_antrian","ASC"); 
                $this->db->order_by("a.created_date","DESC");    
                

            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("a.indikator_category_id", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }

    function get_list_cnt($by=null, $search_string=null)
    {
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $id_ref_jenisantrian = $this->session->userdata(sess_prefix()."id_ref_jenisantrian");
        $this->db->select("count(a.indikator_category_id) _cnt");
        $this->db->from("indikator_penilaian a");
        $this->db->join("ref_indikator_cat b","a.indikator_category_id=b.indikator_category_id","left");
        $this->db->join("res_sec_user c","a.user_id=c.id","left");
        $this->db->join("ref_jenisantrian d","a.id_ref_jenisantrian=d.id","inner");
        //$this->db->where("a.user_id",$user_id);
        $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");
        $this->db->where("a.id_ref_jenisantrian",$id_ref_jenisantrian);

        if (! is_null($search_string) && $search_string != ""){
             $this->db->where("( b.indikator_category_name LIKE '%$search_string%'  OR
                                 a.no_antrian LIKE '%$search_string%'
                                )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }

    function insert_penilaian($data){
        $this->db->trans_begin();                 
        $this->db->insert("indikator_penilaian", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

    function update_penilaian($id,$data){
        $this->db->trans_begin();
        $this->db->where("no_antrian",$id);
        $this->db->where("DATE_FORMAT(created_date, '%d-%m-%Y') = DATE_FORMAT(NOW(), '%d-%m-%Y')");
        $this->db->update("indikator_penilaian", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }


    function UpdateFlag($id,$data){
        $this->db->trans_begin();
        $this->db->where("id",$id);
        $this->db->update("res_sec_user", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

    function CekStatusFlag($id){
        $query = $this->db->query("SELECT flag_penilaian FROM res_sec_user WHERE id=?",array($id));
        return $query->first_row()->flag_penilaian;
    }

    function CekLastNomorAntrian($id){
        $query = $this->db->query("SELECT no_antrian, start_time, end_time FROM res_sec_user WHERE id=?",array($id));
        return $query->first_row()->no_antrian.';'.$query->first_row()->start_time.';'.$query->first_row()->end_time;
    }

    function insert_queue($data){
        $this->db->trans_begin();                 
        $this->db->insert("t_queue", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

    function insert_queue_sound($data){
        $this->db->trans_begin();                 
        $this->db->insert("t_queue_sound", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

    function delete_queue_loket($loket){
        $this->db->where('loket', $loket);
        $exe = $this->db->delete("t_queue");
        return $exe;
    }


    function delete_queue($no_antrian){
        $this->db->where('no_antrian', $no_antrian);
        $exe = $this->db->delete("t_queue");
        return $exe;
    }

    function delete_queue_sound($no_antrian){
        $this->db->where('no_antrian', $no_antrian);
        $exe = $this->db->delete("t_queue_sound");
        return $exe;
    }


}
// 