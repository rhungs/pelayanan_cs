<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcommon extends CI_Model{
    protected $_data = null;
    
    function getMenuByRoleID($roleid)
    {
        $q = "CALL sp_SecRoleModule_get (?);";
        $query = $this->db->query($q, $roleid); 
        $result = $query->result_array();        
        $query->free_result();
        return $result;
    }
    
    function getImageUser($user_id)
    {		
        $this -> db ->select('photo ', FALSE);
        $this -> db -> from($this->db->dbprefix("user_profile"));
        $Q = $this->db->get()->result_array();
        $photo = $Q-> row()->photo;
        $Q-> free_result();
        return $photo;
    }
	     
    function checkMenuAccess($userid, $role_id, $menu_alias)
    {
        $isAllow = false;        
        // unusual query runs here        
        $this ->db->select('p.allow_view');
        $this ->db->from('v_sec_role_priv p');        
        $this->db->where('p.role_id', $role_id);
        $this->db->where('p.module_alias', $menu_alias);
        $Q = $this->db->get();        
        if($Q->num_rows() >= 1)
        {            
            $isAllow = $Q->row()->allow_view;            
        }        
        $Q-> free_result();
        return $isAllow;        
    }
    
    function checkMenuAccessCRUD($userid, $role_id, $menu_alias)
    {
        $data = false;
        $this ->db->select('p.allow_view, p.allow_new, p.allow_edit, p.allow_delete');
        $this ->db->from('v_sec_role_priv p');        
        $this->db->where('p.role_id', $role_id);
        $this->db->where('p.module_alias', $menu_alias);
        $Q = $this->db->get();
        
        if($Q->num_rows() >= 1)
        {
            $data = $Q->row();
        }
        $Q-> free_result();
        return $data;
        
    }
                          
    public function get_notif_list(){
        $userid=  $this->session->userdata( sess_prefix()."userid");        
        $this->db->select("n.notif_id, n.notif_date, n.src_uid, n.dest_uid, n.notif_subj, n.notif_msg, u.full_name as src_uname");
        $this->db->from( $this->db->dbprefix("notif")." n");
        $this->db->join( $this->db->dbprefix("sec_user")." u","n.src_uid = u.id", "INNER");      
        $this->db->where("n.is_read",0);        
        $this->db->where("n.dest_uid",$userid);        
        $this->db->order_by("n.notif_date","desc");
        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result(); 
        return $this->_data;
    }
    
    public function get_notif_count(){
        $userid=  $this->session->userdata( sess_prefix()."userid");
        $this->db->select("count(n.notif_id) as _cnt");
        $this->db->from( $this->db->dbprefix("notif")." n");
        $this->db->join( $this->db->dbprefix("sec_user")." u","n.src_uid = u.id", "INNER");        
        $this->db->where("n.is_read",0);
        $this->db->where("n.dest_uid",$userid);
        $this->db->order_by("n.notif_date","desc");
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result(); 
        return $this->_data;
    }
    
    public function update_notif($id,$data){        
       $this->db->trans_begin();        
        $this->db->where("notif_id",$id);
        $this->db->update( $this->db->dbprefix("notif"), $data );
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    
    public function get_instansi_list($by=null, $arr_id=null, $inst_type_id=1){
        $this->db->select("a.inst_id, a.inst_kd, a.inst_name, a.inst_address, a.inst_email, a.inst_phone, a.inst_fax, a.inst_postcode,
                           a.inst_city, a.inst_type_id");
        $this->db->from("_instansi a");
        $this->db->where("a.active", 1);
        $this->db->where("a.inst_type_id", $inst_type_id);
        if ($by != null)
            $this->db->where($by);

        if ($arr_id != null && is_array($arr_id))
            $this->db->where_in('a.inst_id',$arr_id);

        $this->db->order_by("a.inst_kd","asc");          
        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;
    }
    
    public function get_unit_list($by=nul){
        $this->db->select("a.unit_id, a.unit_kd, a.unit_name, a.inst_id");
        $this->db->from("_unit_pengelola a");
        $this->db->where("a.active", 1);         
        if ($by != null)
            $this->db->where($by);
     
        //$this->db->order_by("a.inst_kd","asc");          
        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;
    }
    
    public function get_role_instansi($role_id){
        $this->db->select("unit_id");
        $this->db->from("sec_role_unit");                
        $this->db->where('role_id',$role_id);  
        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        $result=array();
        foreach($this->_data as $row){
            $result[] = $row->unit_id;
        }
        return $result;

    }
    
    function get_kla_list($offset=null, $limit=null, $search_string=null, $by=null){
        $this->db->select("k.*, p.ref_name as jra_ket, bm.kla_name as kla_bm, mp.kla_name as kla_mp");
        $this->db->from("_klas k");
        $this->db->join("_klas bm","bm.kla_id=k.kla_pid","left");
        $this->db->join("_klas mp","mp.kla_id=bm.kla_pid","left");
        $this->db->join("ref_param p","k.jra_ket_id=p.ref_id","left");
        if (! is_null($search_string) && $search_string != ""){
            //$this->db->like('k.kla_name', $search_string);         
            $this->db->where("( k.kla_kd LIKE '%$search_string%' OR k.kla_name LIKE '%$search_string%' )");            
        }
        
        if ($by != null)
            $this->db->where($by);

        if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);

        $this->db->order_by("k.kla_kd");                     

        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;            
    }
    
    function get_kla_cnt($by=null, $search_string=null){
        $this->db->select("count(k.kla_id) _cnt");
        $this->db->from("_klas k");
        $this->db->join("ref_param p","k.jra_ket_id=p.ref_id","left");
        if (! is_null($search_string) && $search_string != ""){
            //$this->db->like('k.kla_name', $search_string);         
            $this->db->where("( k.kla_kd LIKE '%$search_string%' OR k.kla_name LIKE '%$search_string%' )");            
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
    
    function get_datarow( $table, $field, $where ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        
        $this->_result = $query->row();
        $query->free_result();
        return $this->_result;
    }
    
    function insert_log_vd($data){        
        $table = $this->db->dbprefix('_log_vd');        
        $this->db->insert($table, $data);
    }

	
	
	/*function checkMenuAccessCRUD($userid, $role_id, $menu_alias)
    {
        $data = false;
        $this ->db->select('p.allow_view, p.allow_new, p.allow_edit, p.allow_delete');
        $this ->db->from('v_sec_role_priv p');        
        $this->db->where('p.role_id', $role_id);
        $this->db->where('p.module_alias', $menu_alias);
        $Q = $this->db->get();
        
        if($Q->num_rows() >= 1)
        {
            $data = $Q->row();
        }
        $Q-> free_result();
        return $data;
        
    }
	*/
    
}