<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Menumodel extends CI_Model
{
    public $_tb_module = "res_sec_module";
    public $_tb_sec_role = "res_sec_role";
    public $_tb_sec_role_priv = "res_sec_role_priv";
    public $_tb_sec_user_profile = "res_user_profile";
         
    function getMenuByRoleID($roleid)
    {
        $q = "CALL sp_SecRoleModule_get (?);";
        $query = $this->db->query($q, $roleid); 
        $result = $query->result_array();        
        $query->free_result();
        return $result;
    }
    
    function getImageUser($user_id)
    {		
        $this -> db ->select('photo ', FALSE);
        $this -> db -> from($this->$_tb_sec_user_profile);
        $Q = $this->db->get()->result_array();
        $photo = $Q-> row()->photo;
        $Q-> free_result();
        return $photo;
    }
	     
    function checkMenuAccess($userid, $role_id, $menu_alias)
    {
        $isAllow = false;
        $this ->db->select('p.allow_view');
        $this ->db->from('v_sec_role_priv p');        
        $this->db->where('p.role_id', $role_id);
        $this->db->where('p.module_alias', $menu_alias);
        $Q = $this->db->get();
        
        if($Q->num_rows() >= 1)
        {
            $isAllow = $Q->row()->allow_view;            
        }
        $Q-> free_result();
        return $isAllow;        
    }
    
    function checkMenuAccessCRUD($userid, $role_id, $menu_alias)
    {
        $data = false;
        $this ->db->select('p.allow_view, p.allow_new, p.allow_edit, p.allow_delete, p.allow_print');
        $this ->db->from('v_sec_role_priv p');        
        $this->db->where('p.role_id', $role_id);
        $this->db->where('p.module_alias', $menu_alias);
        $Q = $this->db->get();
        
        if($Q->num_rows() >= 1)
        {
            $data = $Q->row();
        }
        $Q-> free_result();
        return $data;
        
    }
}
