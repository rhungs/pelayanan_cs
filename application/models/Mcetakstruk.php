<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mcetakstruk extends BSS_model
{
    protected $_table = "res_fpp";
    protected $_table_det = "res_fpp_det";
    protected $_id = "fpp_id";
    protected $_data = null;
               
    function getantrian($id_ref_jenisantrian){
        $this->db->trans_start();
        $q = "CALL FN_NoAntrian(?)";
        $result = $this->db->query($q, $id_ref_jenisantrian);
        $this->db->trans_complete();
        $retval = $result->row();
        return $retval;
    }

    function getlastno(){
        $this->db->trans_start();
        $q = "SELECT *
                FROM indikator_penilaian 
                WHERE DATE_FORMAT(created_date, '%Y-%m-%d') =  DATE_FORMAT(NOW(), '%Y-%m-%d')  ORDER BY no_antrian DESC LIMIT 1 ";
        $result = $this->db->query($q, 1);
        $this->db->trans_complete();
        $retval = $result->row();
        return $retval;
    }

    function insert($data){
        $this->db->trans_begin();            
        $this->db->insert("indikator_penilaiann", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return =false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return =true;            
        }
        return $return;
    }

    function get_list()
    {   
        $this->db->select("a.created_date, a.no_antrian, b.no_antrian AS no_antrian_sound, a.loket");
        $this->db->from("t_queue a");
        $this->db->join("t_queue_sound b","a.no_antrian=b.no_antrian","left");
        $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");
        $this->db->limit(6,"");
        $this->db->order_by("a.created_date","DESC");    
        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;      
    }

    function delete_queue_sound(){
        $this->db->where("DATE_FORMAT(created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");
        $this->db->delete("t_queue_sound");
    }

}
