<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Bss_controller
 *
 * @author KIQ
 */
class Bss_controller extends MX_Controller {
    protected $MOD_ALIAS = "";
    protected $SNAME_OPD_ID;    
    protected $SNAME_PROFILE_AVATAR;
    public $data;
    public $viewdata;
    
    protected $_userid;    
    protected $_username;
    protected $_useremail;
    protected $_userfullname;
    protected $_roleid;
    protected $_rolealias;
           
    function __construct()
    {
        parent::__construct();
        //$this->lang->load('auth');
        $this->SNAME_OPD_ID = sess_prefix() . "unit_id";        
        $this->SNAME_PROFILE_AVATAR = sess_prefix() . "avatar";        
        $this->load->model("Mcommon","mcommon");          
        $this->load->helper('cookie');
        
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login','refresh');
        }else{
            $this->_userid = $this->session->userdata(sess_prefix()."userid" );
            $this->_username = $this->session->userdata(sess_prefix()."username" );
            $this->_userfullname = $this->session->userdata(sess_prefix()."full_name" );
            $this->_roleid = $this->session->userdata(sess_prefix()."roleid" );            
            $this->_rolealias = $this->session->userdata(sess_prefix()."rolealias" );            
            $this->_useremail = $this->session->userdata(sess_prefix()."email" );  
        }    
    }
    
    public function checkAuthorization($MOD_ALIAS){
        $this->MOD_ALIAS = $MOD_ALIAS;
        $isAuthorized = false;
        $user_id = $this->session->userdata(sess_prefix()."userid" );        
        $role_id = $this->session->userdata(sess_prefix()."roleid" );                
        if(!$this->ion_auth->logged_in()){
            redirect('auth/login','refresh');
        }else{         
            if ($this->MOD_ALIAS == "MOD_HOME" OR $this->mcommon->checkMenuAccess($user_id, $role_id, $this->MOD_ALIAS)){
                $isAuthorized = true;                  
            }                                                         
        }
        
        if (! $isAuthorized){
             redirect('main/dashboard','refresh');
        }
    }
    
    function _render_page($view, $data=null, $render=false, $tmpl = 'tmpl/vwbacktmpl' )
    {

            $this->viewdata = (empty($data)) ? $this->data: $data;

            //$view_html = $this->load->view($view, $this->viewdata, $render);
            $view_html = $this->template->load($tmpl, $view, $this->viewdata, $render);
            if (!$render) return $view_html;
    }
            
    public function _set_sess_opd($unit_id) {
        $this->session->set_userdata( $this->SNAME_OPD_ID, $unit_id);
    }
    
    public function _get_sess_opd() {
        $this->session->userdata( $this->SNAME_OPD_ID);
    }
                    
    public function _get_sess_csrf()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_userdata('csrfkey', $key);		
        $this->session->set_userdata('csrfvalue', $value);
        return array($key => $value);
    }
        
    public function _valid_sess_csrf()
    {
        if ($this->input->post($this->session->userdata('csrfkey')) !== FALSE &&
                $this->input->post($this->session->userdata('csrfkey')) == $this->session->userdata('csrfvalue'))
        {
                $this->session->unset_userdata('csrfkey');	
                $this->session->unset_userdata('csrfvalue');	
                return TRUE;
        }
        else
        {
                return FALSE;
        }
    }
    
    /**public function download_file(){
       $this->load->helper('download');
       $module = $this->uri->segment(4,true);
       $id = $this->uri->segment(5,true);
       $flname = $this->uri->segment(6,true);
       if ($module != "" && $id != "" ){
           $flname = $this->qsecure->decrypt($flname);
           $id = $this->qsecure->decrypt($id);
           switch ($module) {
                case 'mod_arsipstatis':
                    $whr = sprintf("arsips_id=%d and nama_file='%s'",$id, $flname);
                    $row = $this->mcommon->get_datarow('arsipstatis_file','nama_file, lokasi_file, url_file',$whr);
                    if ($row && file_exists($row->lokasi_file)) {
                        $data = file_get_contents($row->lokasi_file);
                        force_download($flname,$data);
                    }else{
                           show_error("Maaf file yang diminta tidak ditemukan.");
                    }                    
                break;                
            default:
                //
            }                       
       }        
    }**/
   
    
}
 
