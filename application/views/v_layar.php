<style type="text/css">
.btn-xs {
    padding: 10px 200px;
    font-size: 19px;
    font-weight: bold;
    border-radius: 10px;
}

.antrian_left_frame{
	padding-top: 1.5%;
	background:#000080;
	color:#fff;
	text-align:left;
	/*padding-left:25%;*/
	font-weight:bold;
	font-size:30px;
	border-color:#000;
	border-width:1px;
	height:105%;
	width:100%;

}


.textframe{
	/*padding: 3.5rem 2rem 1.5rem 2rem;*/
	margin-top:2%;;
    padding-top: 2%;
    padding-right: 0rem;
    padding-bottom: 0rem;
    padding-left: 0rem;
	-moz-transition: opacity 1s ease-in-out, -moz-transform 1s ease-in-out;
	-webkit-transition: opacity 1s ease-in-out, -webkit-transform 1s ease-in-out;
	-ms-transition: opacity 1s ease-in-out, -ms-transform 1s ease-in-out;
	transition: opacity 1s ease-in-out, transform 1s ease-in-out;
	position: relative;
	width: 90%;
	max-width: 100%;
	min-width: 90%;
	min-height: 90%;
	margin: 0 auto;
	background-image: linear-gradient(90deg, #000, #000);
	border-radius: 0.325rem;
	text-align: center;
	z-index: 1;

	border-style: double solid;
	border-width: 4px 1px;
	line-height: 1.34em;
	margin: 0 auto 1em;
}

.antrian_center_frame{
	background:#000080;
	color:#fff;
	text-align:center;
	font-weight:bold;
	font-size:70px;
	height:274px;
	width:100%;
}

.textframecenter{
	/*padding: 3.5rem 2rem 1.5rem 2rem;*/
    padding-top: 5%;
    padding-right: 0rem;
    padding-bottom: 3rem;
    padding-left: 0rem;
	-moz-transition: opacity 1s ease-in-out, -moz-transform 1s ease-in-out;
	-webkit-transition: opacity 1s ease-in-out, -webkit-transform 1s ease-in-out;
	-ms-transition: opacity 1s ease-in-out, -ms-transform 1s ease-in-out;
	transition: opacity 1s ease-in-out, transform 1s ease-in-out;
	position: relative;
	width: 95%;
	max-width: 100%;
	min-width: 95%;
	min-height: 100%;
	margin: 0 auto;
	background-image: linear-gradient(90deg, #000, #000);
	border-radius: 0.325rem;
	text-align: center;
	z-index: 1;

	border-style: double solid;
	border-width: 4px 1px;
	font-size: 0.5em;
	line-height: 1.34em;
	margin: 0 auto 1em;



}




.textcenter{
	padding-top: 5.5%;
	font-size: 5em;
}

.textleft{
	font-size: 1.5em;
}




</style>

<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="pull-left">
			        <div class="col-md-12">
						<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
			        </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
		  <!-- Bar chart -->
		<div class="box box-primary">
			<div class="box-header with-border">
			</div>
			<div class="box-body">
				<div class="row">
					<table width="100%" >
<!-- 						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a6"></div>
										</div> 
										<div id="l6"></div>
									</div>
								</div>
							</td>
							<td height="100%" colspan="5" rowspan="2" width="100%" >
								<div class="antrian_center_frame">
									<div class="textframecenter">
										<div class="textcenter">
											<div id="a1"></div> 
										</div>
										<br/>
										<div id="l1"></div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a5"></div>
										</div> 
										<div id="l5"></div>
									</div>
								</div>
							</td>
						</tr> -->

						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a1">---</div>
										</div> 
										<div id="l1"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a2">---</div>
										</div> 
										<div id="l2"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a3">---</div>
										</div> 
										<div id="l3"></div>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a4">---</div>
										</div> 
										<div id="l4"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a5">---</div>
										</div> 
										<div id="l5"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a6">---</div>
										</div> 
										<div id="l6"></div>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a7">---</div>
										</div> 
										<div id="l7"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a8">---</div>
										</div> 
										<div id="l8"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a9">---</div>
										</div> 
										<div id="l9"></div>
									</div>
								</div>
							</td>
						</tr>

						<tr>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a10">---</div>
										</div> 
										<div id="l10"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a11">---</div>
										</div> 
										<div id="l11"></div>
									</div>
								</div>
							</td>
							<td height="110px" width="30%" >
								<div class="antrian_left_frame">
									<div class="textframe">
										<div class="textleft">
											<div id="a12">---</div>
										</div> 
										<div id="l12"></div>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</div>


		   </div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
	<div id="hasilprint"></div>
</section><!-- /.content -->






          

   


