<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
         <title><?php echo (isset($titlehead) ? $titlehead : "DISPENDA KOTA BOGOR"); ?></title>
        <link type="text/css" href="<?php echo assets_url('css'); ?>bootstrap.min.css?v=3.3.5" rel="stylesheet" />
        <link type="text/css" href="<?php echo assets_url('css'); ?>font-awesome.min.css?v=4.4.0" rel="stylesheet" />


        <script type="text/javascript">
         <!--
            var base_url  = '<?php echo base_url().index_page();?>';
            var asset_url  = '<?php echo HTTP_ASSET_PATH; ?>';            
            var FORMAT_DATE  = 'd-m-Y';
            var FDATE_SERVER = '<?php echo date('Y-m-d') ?>';            
         -->
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <style style="text/css">

            .parent{
                width: 100%; 
                height: 120px; 
                position: absolute;
                top: 30%;
                z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */
            }

            .inner{
                width: 350px; 
                height: 167px; 
                position: absolute;
                top: 40%;
                left: 110px;
                margin-top: -50px;
                margin-left: -100px;
                z-index:1;
                bottom:100px;
                right:20;
            }

            .textShadow {
                color: white; text-shadow: black 0.1em 0.1em 0.2em;
            }

            .textCenter {
                width: 600px; 
                position: absolute;
                top: 16%;
                left: 400px;
                white-space: wrap;
            }

              .right {
                float: right;
              }

            .btnPosition {
                float: left;
            }

            #background {
                width: 100%; 
                height: 100%; 
                position: fixed; 
                left: 0px; 
                top: 0px; 
                z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */
            }

            .centeredLogo {
                  width: 350px; 
                  height: 167px; 
                  position: absolute;
                  top: 40%;
                  left: 110px;
                  margin-top: -50px;
                  margin-left: -100px;
            }

            .centeredBG {
                  width: 100%; 
                  height: 200px; 
               
            }

            p {
                font-family: "calibri", Times, serif;
            }
            .stretch {
                width:100%;
                height:100%;
            }

            div.slide-down {
              width:100%;
              overflow:hidden;
              /*text-align: right;*/
            }
            div.slide-down p {
              animation: 2s slide-down;
              margin-top:10%;
              margin-left:45%;
              /*text-align: right;*/
            }

            @keyframes slide-down {
              from {
                margin-top: 0%;
              margin-left:45%;
                height: 100%; 
              }

              to {
                margin-top: 10%;
                height: 100%;
              margin-left:45%;
              }
            }

        </style>

    <?php echo $contents ?>
    </body>
</html>