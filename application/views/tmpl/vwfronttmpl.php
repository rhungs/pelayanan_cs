<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
         <title><?php echo (isset($titlehead) ? $titlehead : "DISPENDA BOGOR"); ?></title>

        <!-- Bootstrap -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>bootstrap.min.css?v=3.3.5" rel="stylesheet" />

        <!-- FontAwesome -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>font-awesome.min.css?v=4.4.0" rel="stylesheet" />
        
        <!-- IonIcons -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>ionicons.min.css?v=2.0.l" rel="stylesheet" />
        
        <!-- Select2 -->
        <link type="text/css" href="<?php echo assets_url('plugins/select2');?>select2.min.css?v=4.0.0" rel="stylesheet" />
        
        <!-- Theme style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>admin-lte.min.css" />
        
        <!-- Load All Skins -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css/skins'); ?>_all-skins.min.css">
        
        <!-- App Custom Style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>style.css" />
        <script type="text/javascript">
         <!--
            var base_url  = '<?php echo base_url().index_page();?>';                  
         -->
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery-2.1.4.min.js?v=2.1.4" type="text/javascript"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
         <?php 
            // load stylesheet or javascript at footer
            $this->load->view('tmpl/loader_header',(isset($loadhead) ? $loadhead : null));                         
        ?>
    </head>
    
    <body class="hold-transition skin-blue layout-top-nav">
        
        <div class="wrapper">
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="<?php echo base_url() ?>" class="navbar-brand">
                                <b>DISPENDA BOGOR</b> | 
                                <span class="header-desc">
                                    <strong>B</strong>adan
                                    <strong>P</strong>erlindungan
                                    <strong>S</strong>osial
                                    <strong>A</strong>suhan
                                    <strong>A</strong>nak
									<strong>P</strong>agaden
                                </span>
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        
                        
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <?php if( $this->session->userdata( sess_prefix()."isclogin" ) ){ ?>
                                <!-- Messages: style can be found in dropdown.less -->
                                <li class="dropdown dashboard-menu">
                                    <!-- Dashboard button -->
                                    <a href="<?php echo base_url_index() . 'main/dashboard' ?>" title="Dashboard">
                                        <i class="fa fa-dashboard"></i>                                                                               
                                    </a>
                                                                        
                                </li>
                                 
                                <!-- Messages: style can be found in dropdown.less-->
                                 <li class="dropdown messages-menu">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                         <i class="fa fa-bell-o"></i>
                                         <span class="label label-warning"><?php $cnt_notif = get_count_notif(); echo $cnt_notif; ?></span>
                                       </a>
                                       <ul class="dropdown-menu">
                                         <li class="header" id="countnotif"><?php echo ($cnt_notif > 0) ? "Anda memiliki ". $cnt_notif . " pemberitahuan" : "Tidak ada pemberitahuan"; ?></li>
                                         <li>
                                               <!-- inner menu: contains the actual data -->
                                               <ul class="menu" id="listnotif">
                                                 <?php
                                                 foreach(get_result_notif() as $notif) { ?>
                                                 <li><!-- start message -->
                                                       <a href="javascript:void(0)" onclick="ustat_notif(<?php echo $notif->notif_id?>)">
                                                         <h4 style="margin-left:0; margin-top:0;">
                                                               <?php echo $notif->src_uname; ?>
                                                               <small><i class="fa fa-clock-o"></i> <?php echo $notif->notif_date; ?></small>
                                                         </h4>
                                                         <p style="margin-left:0; margin-top:0;"><?php echo $notif->notif_subj; ?></p>
                                                       </a>
                                                 </li>
                                                 <!-- end message -->
                                                 <?php } ?>
                                               </ul>
                                         </li>
                                         <li class="footer"><a href="#"></a></li>
                                       </ul>
                                 </li>
								 
                                                                                                                             
                                <!-- User Account Menu -->
                                <li class="dropdown user user-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <!-- The user image in the navbar -->
                                        <img src="<?php echo HTTP_ASSET_IMG . $this->session->userdata(sess_prefix() . "avatar") ?>" class="user-image" alt="<?=$this->session->userdata(sess_prefix() . "username")?>" />
                                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                        <span class="hidden-xs"><?php echo $this->session->userdata( sess_prefix()."username"); ?></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- The user image in the menu -->
                                        <li class="user-header">                                            
                                            <img src="<?php echo HTTP_ASSET_IMG . $this->session->userdata(sess_prefix() . "avatar") ?>" class="img-circle" alt="<?=$this->session->userdata(sess_prefix() . "username")?>" />
                                            <p>
                                                <?php echo $this->session->userdata( sess_prefix()."full_name") ?>                                                
                                                <small>Terdaftar sejak, <?php echo date("M Y", strtotime($this->session->userdata( sess_prefix()."ucr_date"))); ?></small>
                                            </p>
                                        </li>
                                        
                                        <!-- Menu Body -->   
                                        <li class="user-body" style="padding:5px!important;">
                                            <div class="row">
                                              <div class="text-center">
                                                <a href="<?php echo base_url()."auth/change_password"; ?>" class="btn btn-default btn-flat"><span class="fa fa-key"></span>&nbsp;Ganti Password</a>
                                              </div>                              
                                            </div>
                                            <!-- /.row -->
                                        </li>
                                        
                                        <!-- Menu Footer -->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                            <?php
                                                echo anchor( 'auth/profile_user/', '<span class="fa fa-user"></span>&nbsp;Profile', 'class="btn btn-default btn-flat" title="Profile"' );
                                            ?>
                                            </div>
                                            <div class="pull-right">
                                            <?php
                                                echo anchor( 'auth/logout', '<span class="fa fa-sign-out"></span>&nbsp;Log Out', 'class="btn btn-default btn-flat" title="Log Out"' );
                                            ?>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <?php }else{ ?>
                                <li>
                                <?php
                                    echo anchor( 'auth/', '<span class="fa fa-sign-in"></span>&nbsp;Log In', 'class="pull-right"' );
                                ?>
                                <?php } ?>
                                </li>
                            </ul>
                        </div><!-- /.navbar-custom-menu -->
                        
                    </div><!-- /.container-fluid -->
                </nav>
            </header>
            
             <?php echo $contents ?>
            
             <footer class="main-footer">
                <div class="container">
                    <div class="pull-right hidden-xs">
                        <b>Version</b> 1.0 beta
                    </div>
                    Copyright &copy; 2016
                    <a href="<?php base_url() ?>">
                        DISPENDA BOGOR
                    </a>. All rights reserved.
                </div><!-- /.container -->
             </footer><!-- /.main-footer -->
        </div><!-- /.wrapper -->
        
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo assets_url('js'); ?>bootstrap.min.js?v=3.3.5"></script>
        
        <!-- SlimScroll -->
        <script src="<?php echo assets_url('plugins/slimscroll'); ?>jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo assets_url('plugins/fastclick'); ?>fastclick.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?php echo assets_url('plugins/select2') ?>select2.min.js?v=4.4.0"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo assets_url('js'); ?>app.min.js"></script>
        <!-- Custom Script -->
        <script type="text/javascript" src="<?php echo assets_url('js') ?>script.js"></script>
        
        
        <?php 
            $this->load->view('tmpl/loader_footer',(isset($loadfoot) ? $loadfoot : null)); // load stylesheet or javascript at footer
        ?>
        
    </body>
</html>
        