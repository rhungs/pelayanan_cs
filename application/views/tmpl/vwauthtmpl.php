<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->        
        <title><?php echo (isset($titlehead) ? $titlehead : "PULSA"); ?></title>

        <!-- Bootstrap -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>bootstrap.min.css?v=3.3.5" rel="stylesheet" />

        <!-- FontAwesome -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>font-awesome.min.css?v=4.4.0" rel="stylesheet" />
        
        <!-- IonIcons -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>ionicons.min.css?v=2.0.l" rel="stylesheet" />
        
        <!-- Theme style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>admin-lte.min.css" />
        
        <!-- App Custom Style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>style.css" />

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery-2.1.4.min.js?v=2.1.4" type="text/javascript"></script>

        <script type="text/javascript">
         <!--
            var base_url  = '<?php echo base_url().index_page();?>';            
         -->
        </script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
         <?php 
            $this->load->view('tmpl/loader_header',(isset($loadhead) ? $loadhead : null)); // load stylesheet or javascript at footer
        ?>
    </head>
    
    <body class="hold-transition login-page">
         
        <?php echo $contents ?>
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo assets_url('js'); ?>bootstrap.min.js?v=3.3.5"></script>
        
        <?php 
            $this->load->view('tmpl/loader_footer',(isset($loadfoot) ? $loadfoot : null)); // load stylesheet or javascript at footer
        ?>
    </body>
    
</html>

