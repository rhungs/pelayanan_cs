<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
         <title><?php echo (isset($titlehead) ? $titlehead : "DISPENDA KOTA BOGOR"); ?></title>

         <!-- Bootstrap -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>bootstrap.min.css?v=3.3.5" rel="stylesheet" />

        <!-- FontAwesome -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>font-awesome.min.css?v=4.4.0" rel="stylesheet" />
        
        <!-- IonIcons -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>ionicons.min.css?v=2.0.l" rel="stylesheet" />
        
        <!-- Select2 -->
        <link type="text/css" href="<?php echo assets_url('plugins/select2');?>select2.min.css?v=4.0.0" rel="stylesheet" />
        
        <!-- Theme style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>admin-lte.min.css" />
        
        <!-- Load All Skins -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css/skins'); ?>_all-skins.min.css">
        
        <!-- App Custom Style -->        
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('plugins/datepicker'); ?>datepicker3.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>style.css" />
        <script type="text/javascript">
         <!--
            var base_url  = '<?php echo base_url().index_page();?>';
            var asset_url  = '<?php echo HTTP_ASSET_PATH; ?>';            
            var FORMAT_DATE  = 'd-m-Y';
            var FDATE_SERVER = '<?php echo date('Y-m-d') ?>';            
         -->
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery-2.1.4.min.js?v=2.1.4" type="text/javascript"></script>        
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery.cookie.min.js" type="text/javascript"></script>        

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
                
         <?php 
            // load stylesheet or javascript at header
            $this->load->view('tmpl/loader_header',(isset($loadhead) ? $loadhead : null));                         
        ?>
    </head>
    
    <body class="hold-transition skin-blue sidebar-mini">
        
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href=<?php echo base_url() ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <img src="<?php echo assets_url('images'); ?>logo.png" width="35" alt="Logo PU" />
                    </span><!-- /.logo-mini -->
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <b>DISPENDA BOGOR</b>
                    </span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                  <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                      
                        <li class="dropdown dashboard-menu">
                            <!-- Home button -->
                            <a href="<?php echo base_url_index() . 'home' ?>" title="Home">
                                <i class="fa fa-desktop"></i>                                                                               
                            </a>

                        </li>
						
					<!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                 
                              <ul class="dropdown-menu">
                                <li>
                                      <!-- inner menu: contains the actual data -->
                                      <ul class="menu" id="listnotif">
                                     
                                       
                                      </ul>
                                </li>
                                <li class="footer"><a href="#"></a></li>
                              </ul>
                        </li>

          
                      <!-- User Account: style can be found in dropdown.less -->
                      <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">                                                    
                            <!-- The user image in the navbar -->
                            <img src="<?php echo HTTP_ASSET_IMG . $this->session->userdata(sess_prefix() . "avatar") ?>" class="user-image" alt="<?=$this->session->userdata(sess_prefix() . "username")?>" />                        
                            <span class="hidden-xs"><?php echo $this->session->userdata( sess_prefix()."username"); ?></span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                         <li class="user-header">
                            <img src="<?php echo HTTP_ASSET_IMG . $this->session->userdata(sess_prefix() . "avatar") ?>" class="img-circle" alt="<?=$this->session->userdata(sess_prefix() . "username")?>" />

                            <p>
                                <?php echo $this->session->userdata( sess_prefix()."full_name") ?>
                                <small>Terdaftar sejak, <?php echo date("M Y", strtotime($this->session->userdata( sess_prefix()."ucr_date"))); ?></small>
                            </p>
                        </li>
                          
                          <!-- Menu Body -->
                        <li class="user-body" style="padding:5px!important;">
                            <div class="row">
                              <div class="text-center">
                                <a href="<?php echo base_url()."auth/change_password"; ?>" class="btn btn-default btn-flat"><span class="fa fa-key"></span>&nbsp;Ganti Password</a>
                              </div>                              
                            </div>
                            <!-- /.row -->
                        </li>
                          
                          <!-- Menu Footer-->
                          <li class="user-footer">
                            <div class="pull-left">                              
                              <?php
                                echo anchor( 'auth/profile_user/', '<span class="fa fa-user"></span>&nbsp;Profile', 'class="btn btn-default btn-flat" title="Profile"' );
                              ?>
                            </div>
                            <div class="pull-right">
                               <?php
                                    echo anchor( 'auth/logout', '<span class="fa fa-sign-out"></span>&nbsp;Log Out', 'class="btn btn-default btn-flat" title="Log Out"' );
                                ?>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <!-- Control Sidebar Toggle Button -->
                      <li>
                        <!--a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a-->
                      </li>
                    </ul>
                  </div>
                </nav>
            </header>
            
            <!-- Side Bar Nav -->
            <?php  require_once("D:\\web_project\\bankbnp_hc\\application\\views\\tmpl\\vwbacksidebar.php"); // load left navigation  ?>
             
			<div class="content-wrapper">
            <?php echo $contents ?>
			</div>

            
            <footer class="main-footer">
                <strong>Copyright &copy; 2016
                    <a href="<?php base_url() ?>" >
                        DISPENDA BOGOR
                    </a>.                        
                </strong> All rights reserved.
            </footer>
          

        </div><!-- /.wrapper -->
        
        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo assets_url('js'); ?>bootstrap.min.js?v=3.3.5"></script>

        <!-- SlimScroll -->
        <script src="<?php echo assets_url('plugins/slimscroll'); ?>jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo assets_url('plugins/fastclick'); ?>fastclick.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?php echo assets_url('plugins/select2') ?>select2.min.js?v=4.4.0"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo assets_url('js'); ?>app.min.js"></script>
        <!-- Custom Script -->
        <script src="<?php echo assets_url('plugins/datepicker'); ?>bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo assets_url('js') ?>script.js"></script>
                         
        <?php 
            $this->load->view('tmpl/loader_footer',(isset($loadfoot) ? $loadfoot : null)); // load stylesheet or javascript at footer
        ?>
        
    </body>
</html>
        