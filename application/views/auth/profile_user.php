
<section class="content">    
    <div class="row">
        <div class="col-xs-12">
            
         <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Profile</h3>
            </div><!-- /.box-header -->
            <!-- form start -->            
             <?php
                $form_att = array(
                    'role' => 'form',
                    'class' => 'form-horizontal',
                );
            echo form_open(uri_string(), $form_att); ?>
            
            <div class="box-body">
                <div class="row">     
                                            
                    <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
                      <div class="alert alert-info alert-dismissable">
                          <a class="close" aria-hidden="true" data-dismiss="alert">x</a>
                          <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
                      </div>
                   <?php } ?>
                   <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
                      <div class="alert alert-danger alert-dismissable">
                          <a class="close" aria-hidden="true" data-dismiss="alert">x</a>
                          <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
                      </div>
                   <?php } ?>
                        
                    <div class="col-md-6">                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="username">Username</label>
                          <div class="col-sm-8">                              
                                <?php echo form_input($username);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="email">Email</label>
                          <div class="col-sm-8">                              
                                <?php echo form_input($email);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="nama_lengkap">Nama Lengkap</label>
                          <div class="col-sm-8">                              
                               <?php echo form_input($full_name);?>
                          </div>
                        </div>                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="pwd">Password</label>
                          <div class="col-sm-8">                              
                                 <?php echo form_input($password);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="conf_pwd">Konfirmasi Password</label>
                          <div class="col-sm-8">                              
                                 <?php echo form_input($password_confirm);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="alamat">Alamat</label>
                          <div class="col-sm-8">                              
                                 <?php echo form_textarea($address);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="telp">Telp/HP</label>
                          <div class="col-sm-8">                              
                                 <?php echo form_input($phone);?>
                          </div>
                        </div>
                         <div class="form-group">
                         <label class="col-sm-4 control-label" for="avatar">Avatar</label>
                          <div class="col-sm-8">                              
                              <select name="photo" id="photo"  class="form-control" >
                                  <?php
                                    foreach ($list_avatar as $la){                                                                                                                
                                        if ($la['img_name'] == (isset($user_photo) ? $user_photo : '')) {
                                            echo "<option value='".$la['img_name']."' data-icon='". HTTP_ASSET_IMG . $la['img_ico'] ."' selected>".$la['img_title']."</option>";
                                        }else{
                                            echo "<option value='".$la['img_name']."' data-icon='". HTTP_ASSET_IMG . $la['img_ico'] ."' >".$la['img_title']."</option>";
                                        } 
                                    }
                                  ?>
                              </select>
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="nama_perusahaan">Nama Perusahaan</label>
                          <div class="col-sm-8">
                                <div class="input-group">
                                    <?php echo form_input($instansi_name);?>
                                    <?php echo form_input($instansi_id);?>
									<?php $roleid = $role_id = $this->session->userdata(sess_prefix()."roleid");
										if($roleid==1){
									?>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" id="btn_cari_ins_pu">
                                            <span class="fa fa-search"></span>
                                            Cari
                                        </button>
                                    </span>
										<?php } ?>
                                </div>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="alamat_perusahaan">Alamat</label>
                          <div class="col-sm-8">                              
                                <?php echo form_input($instansi_address);?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="alamat_perusahaan">Telp/Fax</label>
                          <div class="col-md-4">
                                <?php echo form_input($instansi_phone);?>
                          </div>
                          <div class="col-md-4">
                                <?php echo form_input($instansi_fax);?>  
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="email_perusahaan">Email</label>
                          <div class="col-sm-8">                              
                                <?php echo form_input($instansi_email);?> 
                          </div>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div> 
                
            <?php echo form_hidden('id', $user->id);?>
            <?php echo form_hidden($csrf); ?>
            <!-- SUBMIT BUTTON -->
            <div class="box-footer">
                <?php echo form_submit('submit', 'Update',"class='btn btn-info'");?>
                <button type="button" class="btn btn-default" onClick="window.location='<?php echo base_url() ?>main/dashboard'" >Batal</button>                                                                   
            </div><!-- /.box-footer -->
                        
            <?php echo form_close();?>
 
         </div><!-- /.box box-info-->
        </div>
    </div>
</section>



<!-- modal-content Popup --> 
   <div class="modal fade" id="modal-instansi-pu" tabindex="-1" role="dialog" aria-labelledby="modal-instansi-pu" aria-hidden="true">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
           <h4 class="modal-title custom_align" id="Heading">Pilih nama perusahaan</h4>
         </div>
         <div class="modal-body overflow-edit">
             <div class="row" >

               <div class="box-body">
                 <table id="dt-popup-instansi-pu" class="table table-striped table-bordered table-hover" data-page-length="10">
                       <thead>
                       <tr>                      
                         <th>Nama Perusahaan</th>
                         <th>Alamat</th>
                         <th>Kota</th>
                         <th>Telp</th>                      
                       </tr>
                       </thead>
                       <tbody style="font-size: 12px">                       
                       </tbody>
                 </table>
               </div>

           </div>

         </div>
         <div class="modal-footer ">
           <button id="btn_select_ins_pu" type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Pilih</button>
           <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <!-- /.modal-content --> 

