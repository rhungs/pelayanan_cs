
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url(); ?>">
                    <b>Aplikasi Kepuasan Pelayanan Pelanggan</b><br />
                    <small>Bapenda kota Bogor</small>
                </a>
            </div><!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Silahkan login</p>
                <?php echo form_open("auth/login/returnurl/".$this->uri->segment(4)); ?>
                
                                    
                 <?php 
                  $has_error = array(
                        'identity' => '',
                        'password' => '',
                        'loket_id' => ''
                  );
                  
                  /*if( !is_null( form_error('identity') ) ){                       
                        $has_error['identity'] = 'has-error';                       
                  }
                  if( !is_null( form_error('password') ) ){
                        $has_error['password'] = 'has-error';                        
                  }*/
                  
                 if (isset($errmsg) && $errmsg != "") { ?>
                    <div class="alert alert-danger alert-sm">
                       <?php echo $errmsg;?>
                    </div>                 
                 <?php } else if($this->session->flashdata('info')) { ?>
                        <div class="alert alert-info alert-sm">
                            <a class="close" data-dismiss="alert">x</a>
                            <?php echo $this->session->flashdata('info'); ?>
                        </div>                    
                  <?php } else if($this->session->flashdata('message')) { ?>
                        <div class="alert alert-info alert-sm">
                            <a class="close" data-dismiss="alert">x</a>
                            <strong>Info! </strong><?php echo $this->session->flashdata('message'); ?>
                        </div>                    
                 <?php } ?>
                    
                    

                   <div class="form-group has-feedback <?php echo $has_error['loket_id']; ?>">   
                            <?php   
                                $disabled_loket  = "enabled";
                                $selected_loket = '';
                                $js = 'id="cbo_loket" class="form-control select2" '.$disabled_loket;
                                echo form_dropdown('cbo_loket', $list_loket, $selected_loket, $js);
                            ?>
                            <?php echo form_input($loket_id);?>
                    </div>

					<div style="margin-top:10px" class="form-group has-feedback <?php echo $has_error['identity']; ?>">                                    
                       <?php echo form_input($identity);?>
                       <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback <?php echo $has_error['password']; ?>">                                    
                        <?php echo form_input($password);?>
                         <span class="fa fa-key form-control-feedback"></span>
                    </div>
                    <div class="checkbox">
                        <label>                                        
                            <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
                            Remember Me
                        </label>
                    </div>
                    <!-- Change this to a button or input when using this as a form -->
                    <?php echo form_submit('submit', lang('login_submit_btn'), "class='btn btn-primary btn-block btn-flat'" );?>
                <?php echo form_close(); ?>


                   <br>
				 
                <!-- Belum punya akun? <?php //echo anchor('auth/register_form', 'Registrasi', 'title="Registrasi" class="text-center"'); ?>
				!-->
            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->

<script type="text/javascript">
    $('#cbo_loket')
        .focus(function() {
            prev_parent_unit = $(this).val();
    })
        .change(function() {
            $(this).blur() // Firefox fix as suggested by AgDude
            $("#loket_id").val($(this).val());                     
    });
</script>
        