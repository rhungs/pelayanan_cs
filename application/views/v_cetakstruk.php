<style type="text/css">
.btn-xs {
    padding: 10px 20px;
    font-size: 22px;
    font-weight: bold;
    border-radius: 10px;
}

</style>

<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="pull-left">
			        <div class="col-md-12">
						<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
			        </div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
		  <!-- Bar chart -->
		  <div class="box box-primary">
			<div class="box-header with-border">
			</div>
			<div class="box-body">

			    <div class="row" style="padding-top:0.5%">
					<div class="col-md-12">
						<div class="row" id="datainfo">
							<div class="col-md-12">
				       			<button id="cetak1" class="btn btn-default btn-xs col-sm-5" type="button" style="text-align: left;"> 
				       				<i class="fa fa-print"></i> PBB
				       			</button>          					
			            	</div>
		            	</div>
		    		</div>
				</div><!-- /.row-->

			    <div class="row" style="padding-top:0.5%">
					<div class="col-md-12">
						<div class="row" id="datainfo">
							<div class="col-md-12" align="center">
				       			<button id="cetak2" class="btn btn-default btn-xs col-sm-5" type="button" style="text-align: left;"> 
				       				<i class="fa fa-print"></i> BPHTB
				       			</button>          					
			            	</div>
		            	</div>
		    		</div>
				</div><!-- /.row-->

			    <div class="row" style="padding-top:0.5%">
					<div class="col-md-12">
						<div class="row" id="datainfo">
							<div class="col-md-12" align="center">
				       			<button id="cetak3" class="btn btn-default btn-xs col-sm-5" type="button" style="text-align: left;"> 
				       				<i class="fa fa-print"></i> PRINT OUT / SALINAN
				       			</button>          					
			            	</div>
		            	</div>
		    		</div>
				</div><!-- /.row-->

			    <div class="row" style="padding-top:0.5%">
					<div class="col-md-12">
						<div class="row" id="datainfo">
							<div class="col-md-12" align="center">
				       			<button id="cetak4" class="btn btn-default btn-xs col-sm-5" type="button"  style="text-align: left;"> 
				       				<i class="fa fa-print"></i> CUSTOMER SERVICE
				       			</button>          					
			            	</div>
		            	</div>
		    		</div>
				</div><!-- /.row-->

			    <div class="row" style="padding-top:0.5%">
					<div class="col-md-12">
						<div class="row" id="datainfo">
							<div class="col-md-12" align="center">
				       			<button id="cetak5" class="btn btn-default btn-xs col-sm-5" type="button" style="text-align: left;"> 
				       				<i class="fa fa-print"></i> BJB
				       			</button>          					
			            	</div>
		            	</div>
		    		</div>
				</div><!-- /.row-->

		  </div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
	<div id="hasilprint"></div>
</section><!-- /.content -->






          

   


