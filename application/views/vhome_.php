<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content-wrapper">
    <div class="container">
        
         <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
            <div class="alert alert-info alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
            </div>
         <?php } ?>
         <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
            </div>
         <?php } ?>
        
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Selamat Datang
            </h1>
        </section><!-- /.content-header -->
        
        <section class="content">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">DISPENDA BOGOR JAWA BARAT</h3>
                </div>
                <div class="box-body">
                    <strong>DISPENDA BOGOR</strong> bertugas Menyelamatkan dan melindungi anak balita, 
							anak terlantar dan anak yang mengalami disfungsi sosial, 
							agar dapat tumbuh kembang secara wajar untuk menjadi sumber daya manusia yang produktif. :
                    <ol type="number">
                        <li>
                            <strong>Terpenuhinya </strong> kebutuhan dasar anak terlantar mencakup pangan, sandang, tempat tinggal, kesehatan, 
							pendidikan perlindungan persamaan perlakuan dan mental spiritual.;
                        </li>
                        <li>
                            <strong>Terciptanya</strong> keberfungsian sosial anak balita terlantar, anak terlantar dan anak yang mengalami disfungsi 
							sosial sehingga dapat menampilkan kembali peran-peranya;
                        </li>
                        <li>
                            <strong>Teratasainya</strong> kesulitan kesulitan yang dialami anak sebagai akibat keterlantaran yang dialaminya;
                        </li>
						<li>
                            <strong>Terciptanya</strong> lingkungan dan situasi kehidupan yang mendukung keberfungsian sosial dan mencegah 
							terulangnya keterlantaran;
                        </li>
						<li>
                            <strong>Meningkatknya</strong> jangkauan pelayanan kesejahteraan sosial anak, baik dalam balai/sub unit rumah perlindungan 
							maupun di luar ordinat balai/sub unit rumah perlindungan.
                        </li>
                    </ol>                    
                </div><!-- /.box-body -->
            </div><!-- /.box -->
                
 
        </section><!-- /.content -->
        
    </div><!-- /.container -->
</div><!-- /.content-wrapper -->

