<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menulib {
    protected $obj;
    protected $flag;
    public function __construct(){
        $this->obj =& get_instance();
        $this->obj->load->helper('url');
        $this->obj->load->model('menumodel','menumodel');
    }
    
    public function show_menu(){             
       $dataMenu = $this->obj->menumodel->getMenuByRoleID($this->obj->session->userdata( $this->obj->config->item('sess_prefix', 'ion_auth') . 'roleid' ) );
       
       
        $menu = array(
                'menus' => array(),
                'parent_menus' => array()
        );

        foreach($dataMenu as $row){
                $menu['menus'][$row['module_id']] = $row;
                //creates entry into parent_menus array. parent_menus array contains a list of all menus with children
                $menu['parent_menus'][$row['module_pid']][] = $row['module_id'];
        }
		$this->flag = false;
        $menu = $this->buildMenu(0, $menu);
        //echo $menu; exit;
        return $menu;
       
          
    }

    private function buildMenu($parent, $menu) {
        $html = "";
		$space= "";
        if (isset($menu['parent_menus'][$parent])) {
                if($parent === 0){
                        $html .= "<ul class='sidebar-menu'> <li class='header'>MAIN NAVIGATION</li>";	
                }else{
                    if ($menu['menus'][$parent]['module_url'] == "#" && $menu['menus'][$parent]['mod_group'] == $this->obj->uri->segment(1)) { // module reference, utility, main fpp  
						//$this->flag = true;			
						//print_r($menu['parent_menus']); 
						//print_r($menu['parent_menus'][$parent]); 
						//print_r($menu['menus'][$parent]); 
						//exit;
                        $html .= "<ul class='treeview-menu menu open' style=\"display:block;\" >";
                    }else{
                        $html .= "<ul class='treeview-menu' >";
                    }
                    //$html .= "<ul class='treeview-menu'>";
                }
                
                $clsActive = "";
                $cActive = "";
                foreach ($menu['parent_menus'][$parent] as $menu_id) {                        
                        $clsActive = "";
                        $cActive = "";
						$space = "";
						
						// class active child (not treeview)
						if ( ($this->obj->uri->uri_string() == $menu['menus'][$menu_id]['module_url']) ) {
							$clsActive = "class='active'";							
						}
						// class active parent (treeview)
						if ( $menu['menus'][$menu_id]['module_url'] == "#" && $menu['menus'][$menu_id]['mod_group'] ==  $this->obj->uri->segment(1) ) {							
							$cActive = "active";
						}												
						if ($menu['menus'][$menu_id]['module_alias'] != "MOD_HOME")
							$space = "&nbsp;&nbsp;";
													
                        if (!isset($menu['parent_menus'][$menu_id])) {                               
                                $html .= "<li $clsActive><a href='" . base_url($menu['menus'][$menu_id]['module_url']) . "'>$space<i class='fa ".$menu['menus'][$menu_id]['mod_icon_cls']."'></i> " . $menu['menus'][$menu_id]['module_name'] . "</a></li>"; 
                                //$html .= "<li><a href=\"javascript:getContent('" . $menu['menus'][$menu_id]['module_url'] . "')\" ><i class='fa ".$menu['menus'][$menu_id]['mod_icon_cls']."'></i> " . $menu['menus'][$menu_id]['module_name'] . "</a></li>";
                        }
                        if (isset($menu['parent_menus'][$menu_id])) {                                								 					
								$html .= "<li class='treeview $cActive'><a href='" . base_url($menu['menus'][$menu_id]['module_url']) . "'><i class='fa ".$menu['menus'][$menu_id]['mod_icon_cls']."'></i> <span>" . $menu['menus'][$menu_id]['module_name'] . "</span><i class='fa fa-angle-left pull-right'></i></a>";
                                //$html .= "<li class='treeview'><a href=\"javascript:getContent('" . $menu['menus'][$menu_id]['module_url'] . "')\" ><i class='fa ".$menu['menus'][$menu_id]['mod_icon_cls']."'></i> <span>" . $menu['menus'][$menu_id]['module_name'] . "</span><i class='fa fa-angle-left pull-right'></i></a>";
                                $html .= $this->buildMenu($menu_id, $menu); //--== recursive
                                $html .= "</li>";
                        }
                }
                $html .= "</ul>";
        }
        return $html;
}
}

/* End of file MenuLib.php */
/* Location: ./application/libraries/MenuLib.php */