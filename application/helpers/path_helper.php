<?php
/**
 * 
 */
if ( !function_exists('asset_url') ){
    function assets_url($dir = null){
        // the helper function doesn't have access to $this, so we need to get a reference to the
        // CodeIgniter instance.  We'll store that reference as $CI and use it instead of $this
        $CI =& get_instance();

        // return the asset_url
        if( !empty( $dir ) ){
            return base_url() . $CI->config->item('assets_path') . $dir .'/';
        }else{
            return base_url() . $CI->config->item('assets_path');
        }
        
    }
}

if ( !function_exists('base_url_index') ){
    function base_url_index(){        
        //$CI =& get_instance();
        return base_url() . index_page();        
    }
}

function debug_var($vars){
    
    echo '<pre>';
    var_dump($vars);
    echo '</pre>';
    
}
?>
