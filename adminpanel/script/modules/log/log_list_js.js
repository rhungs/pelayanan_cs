"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;
$(document).ready(function() {  
    
	var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listlog').DataTable( {
        "aLengthMenu": [[5,10,11,15,20,25,50,75,100, 0], [5,10,11,15,20,25,50,75,100, "All"]],
        "iDisplayLength": 11,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center","width":"15%"},
            {"class" : "align-left","width":"45%"},
            {"class" : "align-left","width":"40%"}
        ],
        "ajax":{
            url : base_url + "log/Clog/list_log",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listlog > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listlog_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listlog').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
  
});



