"use_strict";

var prev_ujikat_id;
var prev_puslit_id;
var prev_balai_id;
$(document).ready(function() {  
   
   $('#DataTables-listfpp tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
   });  
   
   //init
   $('#dt-popupju').DataTable({
        responsive: true,                                
        searching: true,
        paging : true ,
        scrollY: '40vh',
        scrollCollapse: true,                            
        'ajax': base_url + 'main/fpp/get_ujijenis_by/0'
   });  
   
   $('#fpp_tgl').datepicker({
        format: 'yyyy-mm-dd',
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
   });
   
   $('#ujikat_id').focus(function() {
        prev_ujikat_id = $(this).val();        
   });
   
   
   $('#puslit_name')
        .focus(function() {
            prev_puslit_id = $(this).val();
    })
        .change(function() {
             
            $(this).blur() // Firefox fix as suggested by AgDude
                                    
            var conf;
            if ( $("#det_ju_id").val() !== "") {
                conf = confirm("Ganti Puslitbang ?\nJenis pengujian yang telah dipilih akan di reset !");            
            }else{
                conf = true;
            }
            if (conf) {
            
                $("#puslit_id").val($(this).val());
                // reset select option balai
                GetBalaiList($(this).val());
                                                
                // reset select option kategori                 
                $('#ujikat_id').empty();                     
                $('#ujikat_id').append("<option value=''>- pilih kategori -</option>");
                                                                
                // reset jenis pengujian yang telah ada/dipilih
                $("#det_ju_id").val("");
                $("#total_harga").val(0);  
                $("#total_lamauji").val(0); 
                $("#daftar_det tr").remove();                
            }else{
                $(this).val(prev_puslit_id);                
                return false;    
            }                                    
    });
    
    $('#balai_name')
        .focus(function() {
            prev_balai_id = $(this).val();
    })
        .change(function() {
            $(this).blur() // Firefox fix as suggested by AgDude
                                    
            var conf;
            if ( $("#det_ju_id").val() !== "") {
                conf = confirm("Ganti Balai ?\nJenis pengujian yang telah dipilih akan di reset !");            
            }else{
                conf = true;
            }
            if (conf) {                                
                $("#balai_id").val($(this).val());
                GetUjiKatList($(this).val());
                                                                                
                // reset jenis pengujian yang telah ada/dipilih
                $("#det_ju_id").val("");
                $("#total_harga").val(0);  
                $("#total_lamauji").val(0); 
                $("#daftar_det tr").remove();                
            }else{
                $(this).val(prev_balai_id);                
                return false;    
            }                                    
    });
   
   $('#ujikat_id')
        .focus(function() {
            prev_ujikat_id = $(this).val();
    })
        .change(function() {
            $(this).blur() // Firefox fix as suggested by AgDude
                                    
            var conf;
            if ( $("#det_ju_id").val() !== "") {
                conf = confirm("Ganti kategori pengujian ?\nJenis pengujian yang telah dipilih akan di reset !");            
            }else{
                conf = true;
            }
            if (conf) {
                // reset jenis pengujian yang telah ada/dipilih
                $("#det_ju_id").val("");
                $("#total_harga").val(0);  
                $("#total_lamauji").val(0); 
                $("#daftar_det tr").remove();
                //alert('changed');
            }else{
                $(this).val(prev_ujikat_id);
                //alert('unchanged');
                return false;    
            }                                    
    });
   
   
   GetBalaiList(vlPUSLIT_ID);
   GetUjiKatList(vlBALAI_ID);
   UjiKatChange(vlUJIKAT_ID);
             
   // on show popup modal jenis pengujian
   //$('#modal-ju').on('shown.bs.modal', function () {                                                  
   //});         
    
    //--== on click button tambah jenis uji, show popup modal jenis pengujian   
     $('#tambah_ju').click( function () {
         if ($('#ujikat_id').val() === "" || $('#ujikat_id').val() === null){
             alert("Silakan pilih dulu kategori pengujian !");
             return false;
         }
        $('#modal-ju').modal('show');         
        //var dt_popupju = $('#dt-popupju').DataTable();
        //dt_popupju.fnStandingRedraw();
    });
                   
    //--== on click button delete detail jenis pengujian
    $(document).on('click', 'button.removebutton', function () {
        //$(this).data("id"); //get HTML5 data Attributes        
        //$(this).data("id",5); //set HTML5 data Attributes
        var ju_id = $(this).data("id");
        var attrju = $(this).data("attrju");
        if (confirm("Delete jenis pengujian ?")) {
            $(this).closest('tr').remove();            
            // replace hide det_ju_id value and remove from array
            var det_ju_id = $("#det_ju_id").val();
            if (det_ju_id !== null && det_ju_id !== ""){            
                JenisUji = det_ju_id.split(";");
                // remove from array
                JenisUji = jQuery.grep(JenisUji, function(value) { return value != ju_id; });
                $("#det_ju_id").val(JenisUji.join(";"));
                
                // adjust total harga dan lama uji
                attrjuArr = attrju.split("~"); 
                var _total_lamauji = $("#total_lamauji").val(); //) - parseInt(attrjuArr[1]);
                var _total_harga = $("#total_harga").val();
                _total_lamauji = parseInt(_total_lamauji) - parseInt(attrjuArr[1]);
                _total_harga = js_str_replace(",", "", _total_harga);
                _total_harga = parseFloat(_total_harga) - parseFloat(attrjuArr[0]);                
                $("#total_lamauji").val( _total_lamauji );
                $("#total_harga").val( js_number_format(_total_harga,0, ".", ",")  );
            }    
            return false;
        }
    });      
    
    
    
});

function GetSelectedJU(id){       
    //var oTable = $('#tableDet').DataTable();
    var obj = document.getElementById(id);
    var data = obj.getAttribute('data-select');        
    var dataArr = data.split(";");    
    var rowsJU = $('table#tableDet').find('tr');
    var rownum = $("#daftar_det > tr").size();  
    var _totharga = $("#total_harga").val();
    var _totlamauji = $("#total_lamauji").val();    
    if (_totharga !== null && _totharga !== "" && _totharga !== undefined){
        _totharga = js_str_replace(",", "", _totharga);
        _totharga = parseFloat(_totharga);              
    }else{
        _totharga = 0;
    }
    if (_totlamauji !== null && _totlamauji !== "" && _totlamauji !== undefined){        
        _totlamauji = parseInt(_totlamauji);
    }else{
        _totlamauji = 0;
    }
    //for (var i = 0; i < rowsJU.length; i++) {
    //  rownum = $(rowsJU[i]).find('td:eq(0)').html();
    //}
    if(rownum === undefined) rownum = 0;
    //$number = parseInt(rownum) + 1;
    $number = parseInt(rownum) + 1;
    if (dataArr.length > 0 && ValidateSelectedJU(dataArr[0]) == true) {
        var _haragsat = parseFloat(dataArr[3]);
        var _lamauji  = parseFloat(dataArr[4]);
        _totharga += _haragsat;
        _totlamauji += _lamauji;
        var _attrju = _haragsat + "~" + _lamauji;
        var _rowTbl = "<tr>"
                + "<td><input id='ju_id" + $number + "' type='hidden' name='ju_id[]' value='" + dataArr[0] + "' /> " + dataArr[1] + "</td>"
                + "<td>" + dataArr[2] + "</td>"
                + "<td>" + dataArr[3] + "</td>"
                + "<td>" + dataArr[4] + "</td>"
                + "<td><button data-id='"+dataArr[0]+"' data-attrju='"+_attrju+"' type='button' class='removebutton btn btn-danger btn-xs' title='Remove this row'><span class='glyphicon glyphicon-trash'></span> Delete</button></td>";
                + "</tr>";
        
       /*oTable.row.add( [
            "<input id='ju_id" + number + "' type='hidden' name='ju_id[]' value='" + dataArr[0] + "' />" + dataArr[1],
            dataArr[2],
            dataArr[3],
            dataArr[4],
            "<button data-id='"+dataArr[0]+"' type='button' class='removebutton btn btn-danger btn-xs' title='Remove this row'><span class='glyphicon glyphicon-trash'></span> Delete</button>"
        ]).draw( false );
        */
        //$('#tableDet tr:last').after(_rowTbl);
        $('#tableDet > tbody:last-child').append(_rowTbl);
        
        $("#det_ju_id").val( $("#det_ju_id").val() + dataArr[0] + ";");          
        $("#total_harga").val( js_number_format(_totharga,0, ".", ",") );  
        $("#total_lamauji").val( _totlamauji );  
        
        
    }                
    $('#modal-ju').modal('hide');
}

function ValidateSelectedJU(val){
    var arrJU = $("#det_ju_id").val();    
    if (arrJU == null || arrJU == undefined || arrJU == ""){return true;}
    arrJU = arrJU.split(";");
    var z=0;var found = false;
    while(z < arrJU.length && ! found){         
        if (arrJU[z] == val){found = true;} z++;
    }    
    return (found == true ? false : true);	
}

function GetBalaiList(vl) {
    $('body').css('cursor', 'wait');
    var resHtml = "";
    //$('#balai_id').empty();     
    $('#balai_name').empty(); 
    if (vl !== "") {
        $.getJSON(base_url + "main/fpp/get_balai_by/"+vl,
            function(data){
                resHtml = BuildSelectOptions(data, 'balai_id', 'balai_shortname', vlBALAI_ID, '- pilih balai -');                
                $('#balai_name').append(resHtml);
            }
        );
    }else {
        resHtml = "<option value=''>- pilih balai -</option>";
        $('#balai_name').append(resHtml);
    }	
    $('body').css('cursor', 'default');
}

function GetUjiKatList(vl) {
    $('body').css('cursor', 'wait');
    $('#balai_id').empty(vl);  // balai id
    var resHtml = "";
    $('#ujikat_id').empty(); 
    if (vl !== "") {
        $.getJSON(base_url + "main/fpp/get_ujikat_by/"+vl,
            function(data){
                resHtml = BuildSelectOptions(data, 'ujikat_id', 'ujikat_name', vlUJIKAT_ID, '- pilih kategori -');                
                $('#ujikat_id').append(resHtml);
            }
        );     
    }else {
        resHtml = "<option value=''>- pilih kategori -</option>";
        $('#ujikat_id').append(resHtml);
    }	
    $('body').css('cursor', 'default');
}


function UjiKatChange(vl) {
    if (vl > 0){                    
        $('body').css('cursor', 'wait');    
        /*$.ajax({
            url: base_url + 'main/fpp/get_ujijenis_vw/'+vl,
            type: 'POST',
            success: function( response ) {
                $('div#popup_jenisuji').html('');
                $('div#popup_jenisuji').html(response);
            }
        });*/
        var table = $('#dt-popupju').DataTable();
        table.ajax.url( base_url + 'main/fpp/get_ujijenis_by/'+ vl ).load();
        //table.columns.adjust().draw(); // Redraw the DataTable    
        $('body').css('cursor', 'default');        
    }
}

