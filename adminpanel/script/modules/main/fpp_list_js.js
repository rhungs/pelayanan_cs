"use_strict";
 var oTable;
 var _status_fpp_id=-1;
 var _fpp_id=-1;
 var _fpp_kode="";
$(document).ready(function() {  
    
   oTable = $('#dataTables-listfpp').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
   $('#dataTables-listfpp tbody').on( 'click', 'tr', function () {
        //$(this).toggleClass('selected');
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');                      
        }
        else {
            oTable.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
        
        //--=== get selected row data
        _fpp_id = $(this).data("id"); //get HTML5 data Attributes       
        _fpp_kode = $(this).data("code");
        _status_fpp_id = $(this).data("status");
        //alert(_fpp_id);
        if (_status_fpp_id === 1){ // Created
            $("#btnSendOrder").removeAttr('disabled');
            $("#btnReceipt").attr('disabled','disabled');
            $("#btnConfirmPay").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled');
            if ($("#btnFinish") !== undefined)
                $("#btnFinish").attr('disabled','disabled');
        }else if (_status_fpp_id === 2){ // Request
            $("#btnSendOrder").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled');
            $("#btnReceipt").removeAttr('disabled');
            $("#btnConfirmPay").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled');            
            $("#btnFinish").attr('disabled','disabled');
        }else if (_status_fpp_id === 3){ // Received
            $("#btnSendOrder").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled'); 
            $("#btnReceipt").attr('disabled','disabled'); 
            $("#btnConfirmPay").removeAttr('disabled');
            $("#btnVerify").attr('disabled','disabled');            
            if ($("#btnFinish") !== undefined)
                $("#btnFinish").attr('disabled','disabled');
        }else if (_status_fpp_id === 4){ // Paid
            $("#btnSendOrder").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled'); 
            $("#btnReceipt").attr('disabled','disabled'); 
            $("#btnConfirmPay").attr('disabled','disabled'); 
            $("#btnVerify").removeAttr('disabled');
            $("#btnFinish").attr('disabled','disabled');
        }else if (_status_fpp_id >= 5){ // Verified
            $("#btnSendOrder").attr('disabled','disabled');
            $("#btnVerify").attr('disabled','disabled'); 
            $("#btnReceipt").attr('disabled','disabled'); 
            $("#btnConfirmPay").attr('disabled','disabled'); 
            $("#btnVerify").attr('disabled','disabled');
            $("#btnFinish").removeAttr('disabled');
        }
   });  
   
    $('#submit_confirmpay').on( 'click', function(e){
        e.preventDefault();
       btnSubmitConfirmPay_Click();
    });
   
   
   
});


function btnSendOrder_Click(){
     if (oTable.rows('.selected').data().length > 0) {
        if (confirm("Kirim permohonan uji "+ _fpp_kode +" ?")){
            window.location.replace(base_url + "main/fpp/update_fpp/request/" + _fpp_id);
        }        
    }else{
        alert("Silakan pilih dulu datanya !");
        return false;
    }
    
}

function btnReceipt_Click(){
    //var table = $('#dataTables-listfpp').DataTable();
    //var ids = $.map(table.rows('.selected').data(), function (item) {
    //    return item[0];
    //});    
    //console.log(ids);    
    //table.row('.selected').remove().draw( false );
    //alert(oTable.rows('.selected').data().length + ' row(s) selected');    
    
    if (oTable.rows('.selected').data().length > 0) {        
        if (confirm("Terima permohonan uji "+ _fpp_kode+" ?")){
             window.location.replace(base_url + "main/fpp/update_fpp/received/" + _fpp_id);
        }        
    }else{
        alert("Silakan pilih dulu datanya !");
        return false;
    }        
}

function btnConfirmPay_Click(){
    if (oTable.rows('.selected').data().length > 0) {                      
        $("#ModalFppConfPay").modal('show');
        $("#FormConfirm").trigger('reset');
        $("#confirmpay_fppid").val(_fpp_id);
    }else{
        alert("Silakan pilih dulu datanya !");
        return false;
    }  
}

function btnSubmitConfirmPay_Click(){    
    if ($("#no_bukti").val() === "" || $("#no_bukti").val() === null) {
        alert("Nomor bukti transfer harus diisi !!");
        $("#frmg_no_bukti").addClass('has-error');        
        $("#no_bukti").focus();
        return false;
    }else{
        if (confirm("Nomor bukti transfer sudah benar ?")){
            var _dataFrm = $("#FormConfirm").serialize();
            $('body').css('cursor', 'wait');
            $.ajax({
                url: base_url + 'main/fpp/confirm_pay',
                type: 'POST',
                dataType: 'json',
                data : _dataFrm,
                success: function( response ) {
                    if (response["success"]=== true){
                        $('#ModalFppConfPay').modal('hide');
                        $('#confirmpay_fppid').val('');
                        $('#no_bukti').val('');
                        alert(response["msg"]);
                        location.reload();
                    }else{
                        alert(response["msg"]);
                        return false;
                    }                
                },
                error : function(msg){ 
                    alert('Submit Failed !!');
                    return false;
                }
            });
            $('body').css('cursor', 'default');
        }
    }
}

function btnVerify_Click(){        
    if (oTable.rows('.selected').data().length > 0) {
        if (confirm("Verifikasi pembayaran permohonan uji "+ _fpp_kode+" ?")){
            window.location.replace(base_url + "main/fpp/update_fpp/verified/" + _fpp_id);
        }         
    }else{
        alert("Silakan pilih dulu datanya !");
        return false;
    }        
}

function btnFinish_Click(){
     if (oTable.rows('.selected').data().length > 0) {
        if (confirm("Permohonan uji "+ _fpp_kode +" selesai dilakukan ?")){
            window.location.replace(base_url + "main/fpp/update_fpp/finished/" + _fpp_id);
        }        
    }else{
        alert("Silakan pilih dulu datanya !");
        return false;
    }
    
}

