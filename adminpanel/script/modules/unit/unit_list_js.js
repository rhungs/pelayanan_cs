"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;
$(document).ready(function() {  
    
	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listunit').DataTable( {
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center"},
			{"class" : "align-left"},
            {"class" : "align-left"},
			{"class" : "align-left"},
			{"class" : "align-left"},
			{"class" : "align-left"},
        ],
        "ajax":{
            url : base_url + "unit/Cunit/list_unit",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listunit > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listunit_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listunit').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
  
});



