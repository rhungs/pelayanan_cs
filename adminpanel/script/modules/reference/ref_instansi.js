"use_strict";

jQuery(document).ready(function($){
    
    var modalForm = $('#ModalInstansi');
    var theForm = $('#addForm');
    var formTarget = theForm.attr('action');
    var addBtn = $('#addInstansi');
    var delBtn = $('.deleteButton');
    var edtBtn = $('.editButton');
    
    addBtn.on( 'click', function(e){
        e.preventDefault();
        modalForm.find('.modal-title').html('Form Tambah Daftar Instansi');
        modalForm.modal('show');
        $('#reset').trigger('click');
        theForm.attr('action', formTarget+'/insert_instansi');
    });
    
    delBtn.on( 'click', function(e){
        var conf = confirm( 'Apakah anda yakin ingin menghapus institusi ini?');
        if( !conf ){
            e.preventDefault();
        }
    });
    
    edtBtn.on( 'click', function(e){
        e.preventDefault();
        
        var target = $(this).attr('href');
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                //console.log(field);
                modalForm.find('.modal-title').html('Form Edit Daftar Instansi');
                modalForm.modal('show');
                var ins_id = field.instansi_id;
                theForm.attr('action', formTarget+'/update_instansi/'+ins_id);
                $('#nama_instansi').val(field.instansi_name);
                $('#nama_instansi').focus();
                $('#id_ref_jenisantrian').val(field.id_ref_jenisantrian);
                $('#alamat').val(field.instansi_address);
                $('#email').val(field.instansi_email);
                $('#telp').val(field.instansi_phone);
                $('#fax').val(field.instansi_fax);
                $('#kota').val(field.instansi_city);
                $('#kodepos').val(field.postal_code);
                $('#provinsi').val(field.instansi_province);
                $('#negara').val(field.instansi_country);
                
            });
            
        });
        
        
    });
    
});