"use_strict";

jQuery(document).ready(function($){
    var boxAdd = '#form_add',
        boxEdit = '#form_edit',
        formEdit = '#EditUjiKlasifikasi',
        formEditAction = $(formEdit).attr('action'),
        inputField = '#klasifikasi_uji_edit';
    
    $('.edit_ref').on( 'click', function(e){
        e.preventDefault();
        
        $('.overlay').fadeIn(10);
        $(boxAdd).fadeOut(10);
        $(boxEdit).fadeIn(10);
        
        var target = $(this).attr('href');
        
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                $(formEdit).attr('action', formEditAction+'/'+field.ujiklas_id);
                $(inputField).val(field.ujiklas_name);
                $(inputField).focus();
                
            });
            
        });
        
    });
    
    $('.delete_ref').on( 'click', function(e){
        var conf = confirm( 'Yakin ingin menghapus daftar uji klasifikasi ini?');
        if(!conf){
            e.preventDefault();
        }
    });
    
    $('#reset').on( 'click', function(e){
        $(boxAdd).fadeIn(10);
        $(boxEdit).fadeOut(10);
        $('.overlay').fadeOut(10);
    });
});


