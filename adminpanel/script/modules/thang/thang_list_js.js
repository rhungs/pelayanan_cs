"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;
$(document).ready(function() {  
    
	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listthang').DataTable( {
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center"},
            {"class" : "align-left"},
        ],
        "ajax":{
            url : base_url + "thang/Cthang/list_thang",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listthang > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listthang_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listthang').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
  
});



