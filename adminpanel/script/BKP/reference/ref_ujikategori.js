"use_strict";

jQuery(document).ready(function($){
    var boxAdd = '#add_kategori_box',
        boxEdit = '#edit_kategori_box',
        formEdit = '#editForm',
        formEditAction = $(formEdit).attr('action'),
        inputField = '#cat_name_edit',
        textField = '#cat_desc_edit';
    
    $('.edit_ref').on( 'click', function(e){
        e.preventDefault();
        
        $('.overlay').fadeIn(10);
        $(boxAdd).fadeOut(10);
        $(boxEdit).fadeIn(10);
        
        var target = $(this).attr('href');
        
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                $(formEdit).attr('action', formEditAction+'/'+field.ujikat_id);
                $(inputField).val(field.ujikat_name);
                $(textField).val(field.ujikat_desc);
                $(inputField).focus();
                
            });
            
        });
        
    });
    
    $('.delete_ref').on( 'click', function(e){
        var conf = confirm( 'Yakin ingin menghapus kategori ini?');
        if(!conf){
            e.preventDefault();
        }
    });
    
    $('#reset').on( 'click', function(e){
        $(boxAdd).fadeIn(10);
        $(boxEdit).fadeOut(10);
        $('.overlay').fadeOut(10);
    });
});


