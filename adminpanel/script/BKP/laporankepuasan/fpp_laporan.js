"use_strict";
    var label_ = [];
	var data_karyawan_ = [];
	var chartJsData = [];
	var chartJsLabels = [];
    var prev_parent_unit;
    var prev_cs;
    var oTable;
    var dataTable;
$(document).ready(function() {  



    dataTable = $('#dt-listindikator').DataTable( {
        "aLengthMenu": [[5,10,15,20,25,50,75,100, 0], [5,10,15,20,25,50,75,100, "All"]],
        "iDisplayLength": 5,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-left"},
            {"class" : "align-left"},
            {"class" : "align-left"},
            {"class" : "align-left"},
            {"class" : "align-center"},
            {"class" : "align-center"},
            {"class" : "align-center"},
            {"class" : "align-center"}
        ],
        "ajax":{
            url : "list_penilaian",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listindikator > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listindikator_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
    
   oTable = $('#dataTables-listindikator').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 

    
    $('#cbo_loket')
        .focus(function() {
            prev_parent_unit = $(this).val();
    })
        .change(function() {
            $(this).blur() // Firefox fix as suggested by AgDude
            $("#loket_id").val($(this).val());                     
    });

    $('#cbo_cs')
        .focus(function() {
            prev_cs = $(this).val();
    })
        .change(function() {
            $(this).blur() // Firefox fix as suggested by AgDude
            $("#cs").val($(this).val());                     
    });

    $('#tglmulai').datepicker({
        format: 'yyyy-mm-dd',
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    });

    $('#tglakhir').datepicker({
        format: 'yyyy-mm-dd',
        clearBtn: true,
        autoclose: true,
        todayHighlight: true
    });

    ///Barchart();
    BarchartAll();
    $("#bar-chart").UseTooltip();



    $(document).delegate('#generate','click',function(){

        var tglmulai = $('#tglmulai').val();
        var tglakhir = $('#tglakhir').val();
        var loket_id = $('#loket_id').val();
        var cs = $('#cs').val();


        $.ajax({
             type: "POST",
             url: "Laporankepuasan/generate",
             data: "loket_id="+loket_id+"&cs="+cs+"&tglmulai="+tglmulai+"&tglakhir="+tglakhir,
             success: function(res){
                /*$("#msginfo").modal("show");
                setTimeout(function() {
                    $('#msginfo').modal("hide");
                }, 1000); */
                $("#datainfo").html(res);
                Barchart();
                 $("#bar-chart").UseTooltip();
                 dataTable.ajax.reload( null, false );
             },
              error: function(xhr, textStatus, errorThrown) {
                 alert(xhr.status);
              }
        });
    }); 


    $(document).delegate('#refresh_all','click',function(){
        BarchartAll();
        $("#bar-chart").UseTooltip();
        $('#tglmulai').val("");
        $('#tglakhir').val("")
        dataTable.ajax.reload( null, false );
    });


});



function InfoJml(){
    var tglmulai = $('#tglmulai').val();
    var tglakhir = $('#tglakhir').val();
    var loket_id = $('#loket_id').val();
    var cs = $('#cs').val();


    $.ajax({
         type: "POST",
         url: "Laporankepuasan/generate",
         data: "loket_id="+loket_id+"&cs="+cs+"&tglmulai="+tglmulai+"&tglakhir="+tglakhir,
         success: function(res){
            $("#datainfo").html(res);

            Barchart();
            $("#bar-chart").UseTooltip();

         },
          error: function(xhr, textStatus, errorThrown) {
             alert(xhr.status);
          }
    });
}

function BarchartAll(){


        var tglmulai = $('#tglmulai').val();
        var tglakhir = $('#tglakhir').val();
        var loket_id = $('#loket_id').val();
        var cs = $('#cs').val();

         
        $.ajax({
        url: base_url + 'laporankepuasan/Laporankepuasan/GetBarchartAll',
        method: "POST",
        dataType: "json",
        success: function(res) {
                
            var data = [{data: res.data_sangat_puas, color: "#00a65a"}, 
                        {data: res.data_puas, color: "#f39c12"},
                        {data: res.data_tidak_puas, color: "#dd4b39"}];

            $.plot("#bar-chart", data, {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.3,
                        align: "center",
                        lineWidth: 0,
                        fill:.75
                    }
                },xaxis: {
                    //ticks: [[0,"Red"],[1,"Yellow"],[2,"Green"]]
                },grid: {
                    borderWidth: 1,
                    borderColor: "#f3f3f3",
                    tickColor: "#f3f3f3"
                },xaxis: {
                    axisLabelUseCanvas: true,
                    mode: "categories",
                    font:{
                        size:15,
                        weight:"bold",
                        color:"black"
                    }
                },yaxis: {
                    axisLabel: "Jumlah",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 18,
                    axisLabelFontWeight: "bold",
                    axisLabelPadding: 3,
                    tickFormatter: function (v, axis) {
                        return v;
                    }
                },legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },grid: {
                    hoverable: true,
                    borderWidth: 2,
                    backgroundColor: { colors: ["#000000", "#ffffff"] }
                }
            });

            
        },error: function(data) {
                //console.log(data);
                alert('Gagal terhubung, silakan Cek Koneksi');
            }
        });


        var previousPoint = null, previousLabel = null;

        $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();
 
                        var x = item.datapoint[0];
                        var y = item.datapoint[1];
 
                        var color = item.series.color;
 
                        //console.log(item.series.xaxis.ticks[x].label);                
 
                        showTooltip(item.pageX,
                        item.pageY,
                        color,
                        item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong>");
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };
 
        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '18px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }


        var tglmulai = $('#tglmulai').val();
        var tglakhir = $('#tglakhir').val();
        var loket_id = $('#loket_id').val();
        var cs = $('#cs').val();


        $.ajax({
             type: "POST",
             url: "Laporankepuasan/generateAll",
             data: "loket_id="+loket_id+"&cs="+cs+"&tglmulai="+tglmulai+"&tglakhir="+tglakhir,
             success: function(res){
                $("#datainfo").html(res);
             },
              error: function(xhr, textStatus, errorThrown) {
                 alert(xhr.status);
              }
        });
        }


function Barchart(){


        var tglmulai = $('#tglmulai').val();
        var tglakhir = $('#tglakhir').val();
        var loket_id = $('#loket_id').val();
        var cs = $('#cs').val();

         
        $.ajax({
        url: base_url + 'laporankepuasan/Laporankepuasan/GetBarchart',
        data: "loket_id="+loket_id+"&cs="+cs+"&tglmulai="+tglmulai+"&tglakhir="+tglakhir,
        method: "POST",
        dataType: "json",
        success: function(res) {

                var bar_data = {
                    data: res.data_grafik,
                    color: "#3c8dbc"
                };

                
                $.plot("#bar-chart", [bar_data], {
                grid: {
                    borderWidth: 1,
                    borderColor: "#f3f3f3",
                    tickColor: "#f3f3f3"
                },
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.6,
                        align: "center"
                    }
                },
                xaxis: {
                    axisLabelUseCanvas: true,
                    mode: "categories",
                    font:{
                      size:15,
                      weight:"bold",
                      color:"black"
                   }

                },
                yaxis: {
                    axisLabel: "Jumlah",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 18,
                    axisLabelFontWeight: "bold",
                    axisLabelPadding: 3,
                    tickFormatter: function (v, axis) {
                        return v;
                    }
                },
                legend: {
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: true,
                    borderWidth: 2,
                    backgroundColor: { colors: ["#000000", "#ffffff"] }
                },

            });
            /* END BAR CHART */
        },error: function(data) {
                console.log(data);
            }
        });


        var previousPoint = null, previousLabel = null;

        $.fn.UseTooltip = function () {
            $(this).bind("plothover", function (event, pos, item) {
                if (item) {
                    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                        previousPoint = item.dataIndex;
                        previousLabel = item.series.label;
                        $("#tooltip").remove();
 
                        var x = item.datapoint[0];
                        var y = item.datapoint[1];
 
                        var color = item.series.color;
 
                        //console.log(item.series.xaxis.ticks[x].label);                
 
                        showTooltip(item.pageX,
                        item.pageY,
                        color,
                        item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong>");
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        };
 
        function showTooltip(x, y, color, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 120,
                border: '2px solid ' + color,
                padding: '3px',
                'font-size': '18px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }

}





