"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;
$(document).ready(function() {  
    
	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listindikator').DataTable( {
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center"},
            {"class" : "align-left"},
			{"class" : "align-center"}
        ],
        "ajax":{
            url : base_url + "indikatorcore/Cindikatorcore/list_indikatorcore",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listindikator > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listindikator_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listindikator').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
  
});



