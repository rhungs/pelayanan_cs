"use_strict";

jQuery(document).ready(function($){
	$('#divQLoading').hide();
    var modal = '#modal-tambahhistory';
    var form = '#FormAddHistory';
    var form_action = $(form).attr('action');
    $('.edit_outcome').on( 'click', function(e){
        e.preventDefault();
        var target = $(this).attr('href'); 
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                var outcome_id = field.id;
                $('#indikator_id').val(field.indikator_id);
                $('#sort').val(field.sort);
                $('#indikator_name').val(field.indikator_name);
				$('#thang').val(field.thang);
				$('#semester').val(field.semester);
            });
        });
        $(modal).modal('show');
    });
    
    //--== on click button tambah , show popup modal 
    $('#tambah_history').click( function () {
        $('#modal-tambahhistory').modal('show');   
        $('#indikator_id').val('');
        $('#sort').val('');
        $('#indikator_name').val('');
    });
    
    //--== on click button tambah , show popup modal 
     $('#add_deskripsi').click( function () {
		 $('#divQLoading').show();
         var id = document.getElementById('indikator_id').value;
         var sort = document.getElementById('sort').value;
         var indikator_name = document.getElementById('indikator_name').value;
        $.ajax({
            type: 'POST',
            url: base_url + 'indikatorcore/Cindikatorcore/save_indikator',
            data: jQuery('#FormAddHistory').serialize(),
            cache: false,
            success:  function(data){
                
                /*if json obj. alert(JSON.stringify(data));*/
                if(data == 1){
                    //alert('Data Berhasil diupdate');
                    location.reload(); 
                    //clear form add history
                    document.getElementById('indikator_id').value = '';
                    document.getElementById('sort').value = '';
                    document.getElementById('indikator_name').value = '';
                }      
            }
        });
		$('#divQLoading').hide();
        $('#modal-tambahhistory').modal('hide');
    });
    

//--== on click button delete detail jenis pengujian
    $(document).on('click', 'button.removebutton', function () {
        var indikator_id = $(this).data("id");
        if (confirm("Hapus History ?")) {
            //$(this).closest('tr').remove();  
            $.ajax({
                type: 'POST',
                url: base_url + 'indikatorcore/Cindikatorcore/delete_indikator',
                data: {
                    'indikator_id' : indikator_id
                },
                cache: false,
                success:  function(data){
                    if(data == 1){
                        location.reload(); 
                    }      
                }
            });
            return false;
        }
    });      

});

