"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;

$(document).ready(function() {  

	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listindikator').DataTable( {
        "aLengthMenu": [[5,10,15,20,25,50,75,100, 0], [5,10,15,20,25,50,75,100, "All"]],
        "iDisplayLength": 5,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-left"},
            {"class" : "align-left"},
            {"class" : "align-left"},
			{"class" : "align-left"}
        ],
        "ajax":{
            url : "Cpenilaian/list_penilaian",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listindikator > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listindikator_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listindikator').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 



	
	var flag_penilaian= $('#flag_penilaian').val();

	if(flag_penilaian==1){
		$('#mulai').prop('disabled', true);
		$('#selesai').prop('disabled', false);
		$('#refresh').prop('disabled', true);
	}else if(flag_penilaian==2){
		$('#mulai').prop('disabled', true);
		$('#selesai').prop('disabled', true);
		$('#refresh').prop('disabled', false);
	}else if(flag_penilaian==0){
		$('#mulai').prop('disabled', false);
		$('#selesai').prop('disabled', true);
		$('#refresh').prop('disabled', true);
	}


    var check_session;
    function CheckForSession() {
        var str="chksession=true";
        jQuery.ajax({
            type: "POST",
            url: "Cpenilaian/cek_session",
            data: str,
            cache: false,
            success: function(res){
            	if(res==0){
            		$('#mulai').prop('disabled', false);
            		$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', true);
					$('#no_antrian').val("");
					$('#no_antrian').prop('disabled', true);

            	}
            }
        });
    }
    check_session = setInterval(CheckForSession, 2000); 


	$(document).delegate('#btnclosefeedback','click',function(){
		$("#msginfo").modal("hide");
	});	

	$(document).delegate('#mulai','click',function(){

		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=1;

		//if(no_antrian!=""){
		    $.ajax({
		        type: "POST",
		        url: "Cpenilaian/statuspenilaian",
		        data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian,
		        success: function(data){
		            $("#msginfo").modal("show");
		            setTimeout(function() {
					    $('#msginfo').modal("hide");
					}, 1000); 
					$('#mulai').prop('disabled', true);
					$('#selesai').prop('disabled', false);
					$('#refresh').prop('disabled', true);
					$('#no_antrian').val("");
					$('#no_antrian').prop('disabled', false);
					$('#no_antrian').focus();
					dataTable.ajax.reload( null, false );
		         },
				  error: function(xhr, textStatus, errorThrown) {
				     alert(xhr.status);
				  }
		    });
		//}else{
		//	alert("Nomor antrian harus diisi !");
		//}

	});	

	$(document).delegate('#selesai','click',function(){


		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=2;

		if(no_antrian!=""){
		    $.ajax({
		         type: "POST",
		         url: "Cpenilaian/statuspenilaian",
		         data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian,
		         success: function(data){
		            $("#msginfo").modal("show");
		            setTimeout(function() {
					    $('#msginfo').modal("hide");
					}, 1000); 
					$('#mulai').prop('disabled', true);
					$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', false);
					$('#no_antrian').prop('disabled', false);
		         },
				  error: function(xhr, textStatus, errorThrown) {
				     alert(xhr.status);
				  }
		    });
		}else{
			alert("Nomor antrian harus diisi !");
		}
	});	
	



	$(document).delegate('#refresh','click',function(){

		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=0;
		if(no_antrian!=""){
		    $.ajax({
		         type: "POST",
		         url: "Cpenilaian/statuspenilaian",
		         data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian,
		         success: function(data){
		            $("#msginfo").modal("show");
		            setTimeout(function() {
					    $('#msginfo').modal("hide");
					}, 1000); 
					$('#mulai').prop('disabled', false);
					$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', true);
		         },
				  error: function(xhr, textStatus, errorThrown) {
				     	alert(xhr.status);
				  }
		    });
		}else{
			alert("Nomor antrian harus diisi !");
		}
	});	

	$(document).delegate('#clickform','click',function(){
		window.location.href = "Cpenilaian/penilaian";
	});




	
});






