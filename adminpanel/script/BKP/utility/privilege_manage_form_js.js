"use_strict";

$(document).ready(function() {  
    //--==== Init ===--//  
    
    
     
    $('#role_id').select2()
        .on("change", function(e) {
            // mostly used event, fired to the original element when the value changes            
            var $this = $(this);              
            var newUrlPriv = base_url+'utility/privilege_manage/get_priv_by/' + $this.val();
            $('#dt-listpriv').DataTable().ajax.url(newUrlPriv).load();
    });
      
    //--==== Event ===--//    
    // on click button delete detail unit
    $(document).on('click', 'button.bt_unitremove', function () {        
        var unid = $(this).data("id");        
        if (confirm("Hapus instansi ?")) {
            $(this).closest('tr').remove();          
            // replace hide det_aksesunit value and remove from array
            var _det_aksesunit = $("#det_aksesunit").val();
            if (_det_aksesunit !== null && _det_aksesunit !== ""){            
                var _aksesUnit = _det_aksesunit.split(";");
                // remove from array
                var _aksesUnit = jQuery.grep(_aksesUnit, function(value) { return value != unid; });
                $("#det_aksesunit").val(_aksesUnit.join(";"));                                
            }    
            return false;
        }
    });  
    

                                         
});



function unSelectAllChk(obName){
    var checkboxes = document.getElementsByName(obName);
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = false; //source.checked;
    }    
}

