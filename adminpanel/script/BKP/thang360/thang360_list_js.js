"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;
$(document).ready(function() {  
    
	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listthang360').DataTable( {
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center"},
            {"class" : "align-center"},
			{"class" : "align-center"},
			{"class" : "align-center"}
        ],
        "ajax":{
            url : base_url + "thang360/Cthang360/list_thang360",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listthang360 > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listthang360_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listthang360').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
        
  
});



