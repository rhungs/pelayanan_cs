<?php
class Qsecure {
	
	private $CRYPT_KEY = '25qz@53crEtxwK';
	
	function __construct()
	{
		//Get the CodeIgniter super object $this->ci =& get_Instance();
		
	}

/* -----------------------------------------------
   @Name: Encrypt()
   @Args: $txt-> String to encrypt.
   @Args: $CRYPT_KEY -> String used to generate a encryption key.
   @Returns: $estr -> Encrypted string.
  ----------------------------------------------- */
	function encrypt($txt)
	{
		if (!$txt && $txt != "0"){
			return false;
			exit;
		}
		
		if (!$this->CRYPT_KEY){
			return false;
			exit;
		}
		
		$kv = $this->keyvalue();
		$estr = "";
		$enc = "";

                for ($i=0; $i<strlen($txt); $i++) {
			$e = ord(substr($txt, $i, 1));
			$e = $e + $kv[1];
			$e = $e * $kv[2];
			(double)microtime()*1000000;
			$rstr = chr(rand(65, 90));
			$estr .= "$rstr$e";
		}

		return $estr;
	}

/* -----------------------------------------------
   @Name: Decrypt()
   @Args: $txt-> String to decrypt.
   @Args: $CRYPT_KEY -> String used to encrypt the string.
   @Returns: $estr -> Decrypted string.
  ----------------------------------------------- */
	
	function decrypt($txt)
	{
		if (!$txt && $txt != "0"){
			return false;
			exit;
		}
		
		if (!$this->CRYPT_KEY){
			return false;
			exit;
		}
		
		$kv = $this->keyvalue($this->CRYPT_KEY);
		$estr = "";
		$tmp = "";

		for ($i=0; $i<strlen($txt); $i++) {
			if ( ord(substr($txt, $i, 1)) > 64 && ord(substr($txt, $i, 1)) < 91 ) {
				if ($tmp != "") {
					$tmp = $tmp / $kv[2];
					$tmp = $tmp - $kv[1];
					$estr .= chr($tmp);
					$tmp = "";
				}
			} else {
				$tmp .= substr($txt, $i, 1);
			}
		}

					$tmp = $tmp / $kv[2];
					$tmp = $tmp - $kv[1];
		$estr .= chr($tmp);

		return $estr;
	}

/* -----------------------------------------------
   @Name: keyvalue()
   @Args: $CRYPT_KEY -> String used to generate a encryption key.
   @Returns: $keyvalue -> Array containing 2 encryption keys.
  ----------------------------------------------- */
	
	function keyvalue()
	{
            $keyvalue = "";
            $keyvalue[1] = "0";
            $keyvalue[2] = "0";
            for ($i=1; $i<strlen($this->CRYPT_KEY); $i++) {
                $curchr = ord(substr($this->CRYPT_KEY, $i, 1));
                $keyvalue[1] = $keyvalue[1] + $curchr;
                $keyvalue[2] = strlen($this->CRYPT_KEY);
            }
            return $keyvalue;
	}
	
}
/* End of file Qsecure.php */
/* Location: ./application/libraries/Qsecure.php */