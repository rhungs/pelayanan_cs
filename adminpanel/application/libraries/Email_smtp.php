<?php
/** Author : KiQ 
   Created Date 2012.06.23 
   Modified Date 2016.02.04
**/
require_once (APPPATH . "third_party/phpmailer/class.phpmailer.php");
require_once (APPPATH . "third_party/phpmailer/class.smtp.php");

class Email_smtp extends phpmailer 
{
	//const newline = "\r\n";
	
	// Set default variables for all new objects
        public $From     = ""; 
        public $FromName = "Admin PULSA";
        public $Host = "";
        public $Mailer   = "smtp"; // Alternative to IsSMTP()    
        public $SMTPAuth = false;
        public $SMTPSecure = 'tls';        
        public $Username = '';
        public $Password = '';        	        	
        public $ContentType;
        public $Subject;
        public $Message;        
			
	protected $obj; 

	function __construct()
	{	
		$this->obj =& get_instance();
		$this->obj->load->config('email_cnf', TRUE); 				
		$this->isSMTP();
		//$this->Headers['MIME-Version'] = "1.0";
		//$this->Headers['Content-type'] = "text/plain; charset=iso-8859-1";										 
	}
		
 
  
	public function Initialize()
	{      	 
		$this->From = $this->obj->config->item('fromemail', 'email_cnf');
		$this->Fromname = $this->obj->config->item('fromname', 'email_cnf');
		$this->Host = $this->obj->config->item('smtp_host', 'email_cnf');
		$this->SMTPAuth = $this->obj->config->item('smtp_auth', 'email_cnf');
		$this->SMTPSecure = $this->obj->config->item('smtp_secure', 'email_cnf');
		$this->Username = $this->obj->config->item('smtp_user', 'email_cnf');
		$this->Password = $this->obj->config->item('smtp_pass', 'email_cnf');			
		$this->WordWrap = $this->obj->config->item('word_wrap', 'email_cnf');
		//$this->ContentType = $this->obj->config->item('content_type', 'email_cnf');
		$this->isHTML(true);				 	
	}
			 	 	 
	function SendEmail()
	{				 		
            try {			
                $this->Body = $this->Message;
                if(!$this->send()) {				 				
                    return false;
                }
                else{
                    return true;				
                }
            }catch(Exception $e) { 
                    return false;
            }		
	}
}

