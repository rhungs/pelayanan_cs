
        <div class="register-box">
            <div class="register-logo">
                <a href="<?php echo base_url(); ?>">
                    <b>PULSA</b><br />
                    <small>Pelayanan Uji Laboratorium
                        Sertifikasi dan Advis Teknis</small>
                </a>
            </div><!-- /.login-logo -->
            <div class="register-box-body">
                <p class="login-box-msg">Silahkan isi form dibawah untuk melakukan registrasi.</p>
<!--                <form class="form-transition" action="" method="post">-->
                <?php echo form_open('auth/register_form'); ?>  
                    <div class="nav-tabs-custom">
                        
                        <div class="tab-content">
                            <div id="data-diri" class="tab-pane active">
                                                                                               
                                 <?php if($this->session->flashdata('info')) { ?>
                                    <div class="alert alert-info alert-sm">
                                        <a class="close" data-dismiss="alert">x</a>
                                        <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>
                                    </div>
                                    <?php } else if($this->session->flashdata('err')) { ?>                                    
                                        <div class="alert alert-error alert-sm">
                                            <a class="close" data-dismiss="alert">x</a>
                                            <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                                        </div>
                                <?php } else { ?>
                                     <div class="form-header alert alert-info alert-sm alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <p>Mohon masukkan data diri anda dengan benar dan lengkap.</p>
                                    </div><!-- /.form-header -->
                                <?php } ?>
                                
                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('full_name'); ?>" name="full_name" id="full_name" type="text" class="form-control" placeholder="Nama Lengkap *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input autocomplete="off" value="<?php set_value('identity') ?>" name="identity" id="identity" type="text" class="form-control" placeholder="Username *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('email') ?>" name="email" id="email" type="email" class="form-control" placeholder="Email *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input autocomplete="off" name="password" type="password" id="password" class="form-control" placeholder="Password * (min 8 char)" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input autocomplete="off" name="password_confirm" type="password" id="password_confirm" class="form-control" placeholder="Ketik ulang password *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <textarea rows="4" name="address" id="address" class="form-control" placeholder="Alamat Lengkap *" required><?php set_value('address') ?></textarea>
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('phone') ?>" name="phone" id="telp" type="text" class="form-control" placeholder="Nomor telepon/hp">
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group clearfix">
                                    <a href="#data-perusahaan" data-toggle="tab" aria-expanded="true" class="btn btn-primary btn-flat pull-right">
                                        Selanjutnya <span class="fa fa-angle-double-right"></span>
                                    </a>
                                </div><!-- /.form-group -->
                            </div><!-- /#data-diri -->
                            
                            <div id="data-perusahaan" class="tab-pane">
                                
                                <div class="form-header alert alert-warning alert-sm alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        Mohon masukkan data perusahaan anda dengan benar dan lengkap.
                                    </p>
                                </div><!-- /.form-header -->
                                
                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <input name="instansi_name" id="instansi_name" value="<?php set_value('instansi_name') ?>" type="text" class="form-control" placeholder="Nama Perusahaan *" required />
                                        <input name="instansi_id" id="instansi_id" value="<?php set_value('instansi_id') ?>" type="hidden" readonly="true" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" id="btn_cari_ins">
                                                <span class="fa fa-search"></span>
                                                Cari
                                            </button>
                                        </span>
                                    </div><!-- /.input-group -->
                                    <span class="form-control-feedback"></span>
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <textarea name="instansi_address" id="instansi_address" value="<?php set_value('instansi_address') ?>" class="form-control" placeholder="Alamat Perusahaan *" rows="5" required></textarea>
                                    <span class="form-control-feedback"></span>
                                </div><!-- /.form-group -->

                                <div class="form-group clearfix row">
                                    <div class="col-md-7">
                                        <input name="instansi_city" id="instansi_city" type="text" value="<?php set_value('instansi_city') ?>" class="form-control" placeholder="Kota" required />
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-5">
                                        <input name="postal_code" id="postal_code" type="text" value="<?php set_value('postal_code') ?>" class="form-control" placeholder="Kode Pos" />
                                    </div><!-- /.col-md-5 -->
                                </div><!-- /.form-group -->
                                
                                <div class="form-group clearfix row">
                                    <div class="col-md-6">
                                        <input name="instansi_phone" id="instansi_phone" type="text" value="<?php set_value('instansi_phone') ?>" class="form-control" placeholder="Telepon *" required />
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <input name="instansi_fax" id="instansi_fax" type="text" value="<?php set_value('instansi_fax') ?>" class="form-control" placeholder="Fax" />
                                    </div><!-- /.col-md-6 -->
                                </div><!-- /.form-group -->
                                
                                <div class="form-group">
                                    <input type="email" name="instansi_email" id="instansi_email" class="form-control" value="<?php set_value('instansi_email') ?>" placeholder="Email perusahaan" />
                                </div><!-- /.form-group -->
                                
                                <div class="form-group">
                                    <div class="formcol">						 
                                           <img src="<?php echo base_url()?>auth/captcha" id="imgcaptcha">
                                           <br/>
                                           <small><a href="javascript:void(0)" onclick="
                                               document.getElementById('imgcaptcha').src='<?php echo base_url()?>auth/captcha/'+Math.random();
                                               document.getElementById('captcha').focus();"
                                               id="change-image">Tidak dapat dibaca? ganti teks.</a>
                                           </small>
                                           <input type="text" name="captcha" id="captcha" value="" size="20" class="form-control" autocomplete="off" placeholder="ketika kata di atas" />                               
                                       </div>
                                </div><!-- /.form-group -->


                                <div class="form-group clearfix">
                                    <a href="#data-diri" data-toggle="tab" aria-expanded="true" class="btn btn-default btn-flat pull-left">
                                         <span class="fa fa-angle-double-left"></span> Sebelumnya
                                    </a>
                                    <button type="submit" name="submit" class="pull-right btn btn-flat btn-primary"><span class="fa fa-user">&nbsp;</span>Registrasi</button>
                                </div><!-- /.form-group -->
                            </div><!-- /#data-diri -->
                            
                        </div><!-- /.tab-content -->
                    </div><!-- /.nav-tabs-custom -->
                    
                    
                <?php echo form_close(); ?>


                Sudah punya akun? <?php echo anchor('auth/login', 'Log In', 'title="Log In" class="text-center"' ); ?>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->
        

 <!-- modal-content Popup --> 
   <div class="modal fade" id="modal-instansi" tabindex="-1" role="dialog" aria-labelledby="modal-instansi" aria-hidden="true">
     <div class="modal-dialog modal-lg">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
           <h4 class="modal-title custom_align" id="Heading">Pilih nama perusahaan</h4>
         </div>
         <div class="modal-body overflow-edit">
             <div class="row" >

               <div class="box-body">
                 <table id="dt-popup-instansi" class="table table-striped table-bordered table-hover" data-page-length="10">
                       <thead>
                       <tr>                      
                         <th>Nama Perusahaan</th>
                         <th>Alamat</th>
                         <th>Kota</th>
                         <th>Telp</th>                      
                       </tr>
                       </thead>
                       <tbody style="font-size: 12px">                       
                       </tbody>
                 </table>
               </div>

           </div>

         </div>
         <div class="modal-footer ">
           <button id="btn_select_ins" type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Pilih</button>
           <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
         </div>
       </div>
     </div>
   </div>
   <!-- /.modal-content --> 
         