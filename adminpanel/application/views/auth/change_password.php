
<section class="content">    
    <div class="row">
        <div class="col-xs-12">
            
         <div class="box box-info" class="col-md-12">
            <div class="box-header with-border">
              <h3 class="box-title">Ganti Password</h3>
            </div><!-- /.box-header -->
            <!-- form start -->            
             <?php
                $form_att = array(
                    'role' => 'form',
                    'class' => 'form-horizontal',
                );
            echo form_open("auth/change_password", $form_att); ?>
            
            <div class="box-body">
                <div class="row">                  
                    <div class="col-md-9">
                        
                      <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
                        <div class="alert alert-info alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
                        </div>
                     <?php } ?>
                     <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
                        </div>
                     <?php } ?>
                        
                        <?php
                            $cols = '5;6';
                            $form_items = array(
                                
                                // Old Password
                                array(
                                    'type' => 'text',
                                    'name' => 'old_password',
                                    'label' => 'change_password_old_password_label',
                                    'default' => $old_password,
                                ),
                                
                                // New Password
                                array(
                                    'type' => 'text',
                                    'name' => 'new_password',
                                    'label' => 'change_password_new_password_label',
                                    'pass_length' => $min_password_length,
                                    'default' => $new_password,
                                    'desc' => 'at least %s characters long',
                                ),
                                
                                // New Password Confirm
                                array(
                                    'type' => 'text',
                                    'name' => 'new_password_confirm',
                                    'label' => 'change_password_new_password_confirm_label',
                                    'default' => $new_password_confirm,
                                ),
                            );
                            
                            render_inline_form( $form_items, $cols );
                            
                            echo form_input($user_id);
                            
                        ?>
                        
                         <div class="row">
                            <div class="col-md-6 col-md-offset-5">
                                <?php echo form_submit('submit', lang('change_password_submit_btn'),"class='btn btn-info'");?>
                                <button type="button" class="btn btn-default" onClick="window.location='<?php echo base_url() ?>home'" >Batal</button>
                            </div>
                        </div><!-- /.row -->
                        
                    </div>
                </div>
            </div>
           <?php echo form_close();?>
         </div><!-- /.box-info -->                         
        </div><!-- /col-xs-12 -->
     </div>    
</section>

 