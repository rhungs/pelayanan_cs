
        <div class="register-box">
            <div class="register-logo">
                <a href="<?php echo base_url(); ?>">
                    <b>PULSA</b><br />
                    <small>Pelayanan Uji Laboratorium
                        Sertifikasi dan Advis Teknis</small>
                </a>
            </div><!-- /.login-logo -->
            <div class="register-box-body">
                <p class="login-box-msg">Silahkan isi form dibawah untuk melakukan registrasi.</p>
<!--                <form class="form-transition" action="" method="post">-->
                <?php echo form_open('user_auth/register_user'); ?>  
                    <div class="nav-tabs-custom">
                        
                        <div class="tab-content">
                            <div id="data-diri" class="tab-pane active">
                                
                                <div class="form-header alert alert-info alert-sm alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <p>Mohon masukkan data diri anda dengan benar dan lengkap.</p>
                                </div><!-- /.form-header -->
                                
                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('fullname'); ?>" name="fullname" id="fullname" type="text" class="form-control" placeholder="Nama Lengkap *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('usename') ?>" name="username" id="username" type="text" class="form-control" placeholder="Username *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('email') ?>" name="email" id="email" type="email" class="form-control" placeholder="Email *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input name="password" type="password" id="password" class="form-control" placeholder="Password *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input name="re-password" type="password" id="re-password" class="form-control" placeholder="Ketik ulang password *" required />
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <textarea rows="4" name="address" id="address" class="form-control" placeholder="Alamat Lengkap *"><?php set_value('address') ?></textarea>
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <input value="<?php set_value('telp') ?>" name="telp" id="telp" type="text" class="form-control" placeholder="Nomor telepon/hp *">
                                    
                                </div><!-- /.form-group -->

                                <div class="form-group clearfix">
                                    <a href="#data-perusahaan" data-toggle="tab" aria-expanded="true" class="btn btn-primary btn-flat pull-right">
                                        Selanjutnya <span class="fa fa-angle-double-right"></span>
                                    </a>
                                </div><!-- /.form-group -->
                            </div><!-- /#data-diri -->
                            
                            <div id="data-perusahaan" class="tab-pane">
                                
                                <div class="form-header alert alert-warning alert-sm alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        Mohon masukkan data perusahaan anda dengan benar dan lengkap.
                                    </p>
                                </div><!-- /.form-header -->
                                
                                <div class="form-group has-feedback">
                                    <div class="input-group">
                                        <input name="comp_name" id="comp_name" type="text" class="form-control" placeholder="Nama Perusahaan *" />
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default">
                                                <span class="fa fa-search"></span>
                                                Cari
                                            </button>
                                        </span>
                                    </div><!-- /.input-group -->
                                    <span class="form-control-feedback"></span>
                                </div><!-- /.form-group -->

                                <div class="form-group has-feedback">
                                    <textarea name="comp_addr" id="comp_addr" class="form-control" placeholder="Alamat Perusahaan *" rows="5"></textarea>
                                    <span class="form-control-feedback"></span>
                                </div><!-- /.form-group -->

                                <div class="form-group clearfix row">
                                    <div class="col-md-7">
                                        <input name="city" id="city" type="text" class="form-control" placeholder="Kota" required />
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-5">
                                        <input name="kodepos" id="kodepos" type="text" class="form-control" placeholder="Kode Pos" />
                                    </div><!-- /.col-md-5 -->
                                </div><!-- /.form-group -->
                                
                                <div class="form-group clearfix row">
                                    <div class="col-md-6">
                                        <input name="comp_tlp" id="comp_tlp" type="text" class="form-control" placeholder="Telepon *" />
                                    </div><!-- /.col-md-6 -->
                                    <div class="col-md-6">
                                        <input name="fax" id="fax" type="text" class="form-control" placeholder="Fax" />
                                    </div><!-- /.col-md-6 -->
                                </div><!-- /.form-group -->
                                
                                <div class="form-group">
                                    <input type="email" name="comp_email" class="form-control" placeholder="email" />
                                </div><!-- /.form-group -->


                                <div class="form-group clearfix">
                                    <a href="#data-diri" data-toggle="tab" aria-expanded="true" class="btn btn-default btn-flat pull-left">
                                         <span class="fa fa-angle-double-left"></span> Sebelumnya
                                    </a>
                                    <button type="submit" name="submit" class="pull-right btn btn-flat btn-primary"><span class="fa fa-user">&nbsp;</span>Registrasi</button>
                                </div><!-- /.form-group -->
                            </div><!-- /#data-diri -->
                            
                        </div><!-- /.tab-content -->
                    </div><!-- /.nav-tabs-custom -->
                    
                    
                <?php echo form_close(); ?>


                Sudah punya akun? <?php echo anchor('user_auth/login_form', 'Log In', 'title="Log In" class="text-center"' ); ?>

            </div><!-- /.login-box-body -->
        </div><!-- /.login-box -->