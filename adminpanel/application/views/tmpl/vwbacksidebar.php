<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">                
                <img src="<?php echo HTTP_ASSET_IMG . $this->session->userdata(sess_prefix() . "avatar") ?>" class="img-circle" alt="<?=$this->session->userdata(sess_prefix() . "full_name")?>" />
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata( sess_prefix()."full_name") ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
      
        <!-- sidebar menu: : style can be found in sidebar.less -->
          <?php
                //if ($this->input->cookie('pls_leftNav', TRUE)) {                                    
                echo $this->menulib->show_menu();
          ?>
        <?php 
        
      ?>
    </section>
    <!-- /.sidebar -->
  </aside>