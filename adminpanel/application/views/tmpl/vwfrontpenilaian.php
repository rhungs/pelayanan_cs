<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo (isset($titlehead) ? $titlehead : "BPSAA"); ?></title>

        <!-- iCheck Style -->        
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('plugins/iCheck'); ?>all.css" />
        
         <!-- Bootstrap -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>bootstrap.min.css?v=3.3.5" rel="stylesheet" />

        <!-- FontAwesome -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>font-awesome.min.css?v=4.4.0" rel="stylesheet" />
        
        <!-- IonIcons -->
        <link type="text/css" href="<?php echo assets_url('css'); ?>ionicons.min.css?v=2.0.l" rel="stylesheet" />
        
        <!-- Select2 -->
        <link type="text/css" href="<?php echo assets_url('plugins/select2');?>select2.min.css?v=4.0.0" rel="stylesheet" />
        
        <!-- Theme style -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css'); ?>admin-lte.min.css" />
        
        <!-- Load All Skins -->
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css/skins'); ?>_all-skins.min.css">
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('css/skins'); ?>_all-skins.min.css">
        
        <!-- App Custom Style -->        
        <link type="text/css" rel="stylesheet" href="<?php echo assets_url('plugins/datepicker'); ?>datepicker3.css" />

        <script type="text/javascript">
         <!--
            var base_url  = '<?php echo base_url().index_page();?>';
            var asset_url  = '<?php echo HTTP_ASSET_PATH; ?>';            
            var FORMAT_DATE  = 'd-m-Y';
            var FDATE_SERVER = '<?php echo date('Y-m-d') ?>';            
         -->
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery-2.1.4.min.js?v=2.1.4" type="text/javascript"></script>        
        <script src="<?php echo assets_url('plugins/jquery'); ?>jquery.cookie.min.js" type="text/javascript"></script>     
            
            <style type="text/css">
                .bodynya {
                    background-color: #34495e;
                    /*height: 100px;*/
                }
                .fontColor {
                    color:white;
                     text-align: left;
                }

                .fontFrontColor {
                    color:#34495e;
                     /*text-align: left;*/
                }

                .btn-lg{
                    width: 260px;
                }
                .tblpenilaian{
                    width: 100%;
                }
            </style>

        <?php echo $contents ?>

        
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo assets_url('js'); ?>bootstrap.min.js?v=3.3.5"></script>
        <!-- SlimScroll -->
        <script src="<?php echo assets_url('plugins/slimscroll'); ?>jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo assets_url('plugins/fastclick'); ?>fastclick.js"></script>
        <!-- Select2 -->
        <script type="text/javascript" src="<?php echo assets_url('plugins/select2') ?>select2.min.js?v=4.4.0"></script>
        <!-- iCheck -->
        <script type="text/javascript" src="<?php echo assets_url('plugins/iCheck') ?>icheck.js"></script>
        <script type="text/javascript" src="<?php echo assets_url('plugins/iCheck') ?>icheck.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo assets_url('js'); ?>app.min.js"></script>
        <!-- Custom Script -->
        <script src="<?php echo assets_url('plugins/datepicker'); ?>bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?php echo assets_url('js') ?>script.js"></script>            
        <?php 
            $this->load->view('tmpl/loader_footer',(isset($loadfoot) ? $loadfoot : null)); // load stylesheet or javascript at footer
        ?>
    </body>
</html>