<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>:: DISPENDA BOGOR ::</title>
  <link href="<?php echo assets_url('plugins/template/css'); ?>bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo assets_url('plugins/template/css'); ?>animate.min.css" rel="stylesheet"> 
  <link href="<?php echo assets_url('plugins/template/css'); ?>font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo assets_url('plugins/template/css'); ?>lightbox.css" rel="stylesheet">
  <link href="<?php echo assets_url('plugins/template/css'); ?>main.css" rel="stylesheet">
  <link id="css-preset" href="<?php echo assets_url('plugins/template/css'); ?>presets/preset1.css" rel="stylesheet">
  <link href="<?php echo assets_url('plugins/template/css'); ?>responsive.css" rel="stylesheet">
  <link href="<?php echo assets_url('plugins/template/css'); ?>jquery-ui.css" rel="stylesheet">
  
  
  


  <!--[if lt IE 9]>
    <script src="<?php echo assets_url('plugins/template/js'); ?>html5shiv.js"></script>
    <script src="<?php echo assets_url('plugins/template/js'); ?>respond.min.js"></script>
  <![endif]-->
  
  <link rel="shortcut icon" href="<?php echo assets_url('plugins/template/images'); ?>favicon_.ico">
</head><!--/head-->





<body>


  <!--.preloader-->
  <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
  <!--/.preloader-->

  <header id="home">
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(<?=base_url('assets/plugins/template/images/slider/1.jpg'); ?>) ">
          <div class="caption">
			<a href="<?php echo base_url()."pendaftaran/pendaftaran/form_fpp/" ?>"><h1 class="animated fadeInLeftBig">Penilaian  <span>360</span></h1></a>
            <p class="animated fadeInRightBig"> DISPENDA BOGOR </p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Mulai</a>
          </div>
        </div>
        <div class="item" style="background-image: url(<?=base_url('assets/plugins/template/images/slider/2.jpg'); ?>) ">
          <div class="caption">
            <a href="<?php echo base_url()."pendaftaranbalita/pendaftaranbalita/form_fpp/" ?>"><h1 class="animated fadeInLeftBig">Penilaian  <span>360</span></h1></a>
            <p class="animated fadeInRightBig"> DISPENDA BOGOR </p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Mulai</a>
          </div>
        </div>
        <div class="item" style="background-image: url(<?=base_url('assets/plugins/template/images/slider/3.jpg'); ?>) ">
          <div class="caption">
            <a href="<?php echo base_url()."pendaftarancota/pendaftarancota/form_fpp/" ?>"><h1 class="animated fadeInLeftBig">Penilaian  <span>360</span></h1></a>
            <p class="animated fadeInRightBig"> DISPENDA BOGOR </p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Mulai</a>
          </div>
        </div>
      </div>
      <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
      <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

      <a id="tohash" href="#services"><i class="fa fa-angle-down"></i></a>

    </div><!--/#home-slider-->
    <div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">
            <h1><img class="img-responsive"  src="<?=base_url('assets/plugins/template/images/logo.png');?>"  alt="logo"></h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class="scroll active"><a href="#home">Beranda</a></li>
            <li class="scroll"><a href="#services">Penilaian 360</a></li> 
			<li class="scroll">
				<?php if( $this->session->userdata( sess_prefix()."isclogin" ) ){ 
							echo anchor( 'auth/logout', '<span class="fa fa-sign-out"></span>&nbsp;Log Out', 'class="pull-right" title="Log Out"' );
					  }else{
							echo anchor( 'auth/', '<span class="fa fa-sign-in"></span>&nbsp;Log In', 'class="pull-right"' );
					  } ?>
            </li>
          </ul>
        </div>
      </div>
    </div><!--/#main-nav-->
  </header><!--/#home-->
  <section id="services">
    <div class="container">
      <div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="row">
          <div class="text-center col-sm-8 col-sm-offset-2">
            <h2>Penilaian 360</h2>
          </div>
        </div> 
      </div>
      <div class="text-center our-services">
        <div class="row">
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
            <div class="service-icon">
              <i class="fa fa-cog fa-spin fa-3x fa-fw margin-bottom"></i>
            </div>
            <div class="service-info">
              <a href="<?php echo base_url()."pendaftaran/pendaftaran/form_fpp/" ?>"><h3>Penilaian 360</h3></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!--/#services-->
  <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
        <div class="footer-logo">
          <a href="index.html"><img class="img-responsive" src="<?=base_url('assets/plugins/template/images/logo.png'); ?>"  alt=""></a>
        </div>
        <div class="social-icons">
          <ul>
            <li><a class="envelope" href="#"><i class="fa fa-envelope"></i></a></li>
            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li> 
            <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="tumblr" href="#"><i class="fa fa-tumblr-square"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>&copy; 2016 PT. Bank Nusantara Parahyangan.</p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- /.modal-content --> 

  
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>jquery.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>jquery.inview.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>wow.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>mousescroll.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>smoothscroll.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>jquery.countTo.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>lightbox.min.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>main.js"></script>
  <script type="text/javascript" src="<?php echo assets_url('plugins/template/js'); ?>jquery-ui.js"></script>

  
  
  </body>
</html>
