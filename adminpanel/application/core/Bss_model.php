<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of BSS_Model
 *
 * @author KIQ
 * 
 */
class Bss_model extends CI_Model {
    //put your code here
    protected $_result= null;    
    protected $_tblparam = "ref_param";
    public $error = null;
    
    public function get_user_profile(){
        $user_id  = (int)$this->session->userdata( sess_prefix() . 'userid' );
        $this->db->select("a.full_name, a.email, b.instansi_id, b.address, b.phone, b.photo, c.instansi_name, a.created_date, a.password_changed, a.flag_penilaian ");
        $this->db->from( $this->db->dbprefix("res_sec_user")." a" );
        $this->db->join( $this->db->dbprefix("res_user_profile")." b","b.userid=a.id","inner" );
        $this->db->join( $this->db->dbprefix("res_ref_instansi")." c","c.instansi_id=b.instansi_id","inner");             
        $this->db->or_like('a.id', $user_id);                      
        $Q = $this->db->get();
        $this->_result = $Q->row();
        $Q->free_result();
        return $this->_result;
    }
    
    protected function _by($by){
        $par = '';
        $c = ' AND';
        $i = 0;
        $where = "";
        $cntrec = count($by);        
        foreach($by as $key=>$value){
            if($i == $cntrec-1){$c = '';}

            $ex = explode(" ",$key);
            if(count($ex)>1){
                $key = $ex[0]." ".$ex[1];
            }
            else{
                $key = $ex[0]." =";
            }

            if(is_int($value)){
                $value = $value;
            }else{
                $value = "'".$value."'";
            }

            $where .=" ".$key." ".$value."".$c;
            $i++;
        }
        return $where;
    }
    
    protected function _set_error($err){
        $this->error = $err;
    }
    
    protected function get_error($err){
        return $this->error;
    }
    
    /*
     * This is method below for reference model. delete_record(), get_record(), update_record(), insert_record()
     * Contain common function to reduce repeated same function writing such as inserting, editing and deleting records.
     * @author Fazri Alfan Muaz <fazri@inov8-software.com>
     * @since 1.0
     */
    
    /**
     * This is delete function to delete record(s) on single table
     * $id can be passed as array to delete multiple rows
     * 
     * @param string $table
     * @param string $column
     * @param int|string|array $id
     */
    public function delete_record( $table, $column, $id ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        if( is_array($id) ){
            $this->db->where_in( $column, $id );
        }else{
            $this->db->where( $column, $id );
        }
        
        $return = $this->db->delete( $table );
        
        return $return;
    }
    
    public function get_record( $table, $column, $id ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        
        $this->db->select('*');
        $this->db->where($column, $id);
        $query = $this->db->get($table);
        
        $this->_result = $query->result();
        $query->free_result();
        return $this->_result;
    }
    
	public function changed_password_flag($id){
	    $data = array(
			'password_changed' => 1
		); 
        $exec = $this->db->update("res_sec_user", $data, array( "username" => $id ) );
        return $exec;
    }
	
	
    public function update_record($table, $data, $column, $id){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        $exec = $this->db->update( $table, $data, array( $column => $id ) );
        return $exec;
    }
    
    public function insert_record( $table, $data ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        $exec = $this->db->insert( $table, $data );
        return $exec;
    }
    
    public function insert_record_getid( $table, $data ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        $this->db->insert( $table, $data );
        return $this->db->insert_id();
    }
    
    public function get_row( $table, $column, $id ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        
        $this->db->select('*');
        $this->db->where($column, $id);
        $query = $this->db->get($table);
        
        $this->_result = $query->row();
        $query->free_result();
        return $this->_result;
    }
    
    public function get_rowdata( $table, $field, $where ){
        if (substr($table, 0, 5) != $this->db->dbprefix){
            $table = $this->db->dbprefix($table);
        }
        
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        
        $this->_result = $query->row();
        $query->free_result();
        return $this->_result;
    }
    
    public function get_refparam( $param_type, $by=null, $sortby="ref_seq", $sortmode="asc" ){        
        $this->db->select('ref_id, ref_code, ref_name, ref_descript, ref_seq');
        $this->db->where('ref_active', 1);
        $this->db->where('ref_type', $param_type);
        $this->db->order_by($sortby, $sortmode);
        if ($by != null){
            $this->db->where($by);                                                                
        } 
        $query = $this->db->get($this->db->dbprefix($this->_tblparam));        
        $this->_result = $query->result();
        $query->free_result();
        return $this->_result;
    }
    
    
   
    
    public function send_email($fromEmail = "", $fromName = "", $toEmail ="", $toName = "", $subjectEmail = "", $bodyEmail = ""){
        $this->load->config('email_cnf', TRUE);
        if (! $this->config->item('allow_sendmail', 'email_cnf')) {
             return true;
        }
      
        require_once (APPPATH . "third_party/Mailerfactory.php");
        $mail = Mailerfactory::init();
        $mail->Subject = $subjectEmail;
        $mail->Body    = $bodyEmail;
        if($fromEmail!="") $mail->From = $fromEmail;
        $mail->FromName = $fromName;	
        $mail->SetWordWrap();
        $mail->IsHTML(TRUE);
        //$mail->addAttachment($attachment);

        $mail->AddAddress($toEmail, $toName);
//		if ($cc != false) {
//                    if (! is_array($cc)) {
//                        $cc = array($cc);
//                    }
//                    foreach ($cc as $each) {
//                        $mail->AddCC($each);
//                    }
//                }

        if(!$mail->Send())
        {
            return $mail->ErrorInfo;
        }else{
            return true;
        }
    }
}
 
