<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Extended version of HTML helper.
 * 
 * @author Fazri Alfan Muaz <fazri@inov8-software.com>
 * @since 1.0
 */

function message_box( $title, $message, $mode='info', $icon='', $fadeout=false, $close=true ){
    
    $html = '';
    $html .= '<div class="alert alert-'.$mode;
    if($close){
        $html .=' alert-dismissible';
    }
    if($fadeout){
        $html .= ' fade in';
    }
    $html .= '">';
    
    if($close){
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    }
    $html .= '<h4>';
    if( !empty($icon) ){
        $html .= '<i class="icon fa fa-'.$icon.'"></i>'.nbs();
    }
    $html .= $title;
    $html .= '</h4>';
    $html .= $message;
    $html .= '</div><!-- /.alert -->';
    if($fadeout){
        $html .= '<script type="text/javascript">';
        $html .= 'window.setTimeout(function(){'
                . 'jQuery(".alert").alert(\'close\');'
                . '},3000);';
        $html .= '</script>';
    }
    
    return $html;
}
