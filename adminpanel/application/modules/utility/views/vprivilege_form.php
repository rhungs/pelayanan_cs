<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
    <li><i class="fa fa-share"></i> Utility</li>
    <li class="active">Pengaturan Otorisasi</li>    
  </ol>
</section>
<section class="content">
    <div class="row">       
        <!--form id='frmPriv' method="POST" name="frmPriv"-->
        <?php echo form_open(base_url().  index_page()."utility/privilege_manage/save_privilege"); ?>
        <?php
            $input = array(
                'class' => 'form-control'
            );

            $label = array(
                'class' => 'control-label col-sm-2'
            );
        ?>

        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-info-circle"></i> Pilih Role, tentukan instansi dan akses modul. 
                    <button id="submit" type="submit" class='btn btn-primary btn-sm pull-right'><span class="fa fa-save"></span> Update</button>
                </div><!-- /.box-header -->
                <div class="box-body">
                     <?php if (isset($message) && $message != "" OR $this->session->flashdata('message')) { ?>
                            <script type="text/javascript">
                                window.setTimeout(function(){
                                    $(".alert").alert('close');
                                },3000);
                            </script>
                            <div class="alert alert-success alert-dismissable">
                                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                                <?php echo (isset($message) && $message != "") ? $message : $this->session->flashdata('message');?>
                            </div>
                         <?php } ?>
                         <?php if (isset($errmsg) && $errmsg != "" OR $this->session->flashdata('errmsg')) { ?>
                            <script type="text/javascript">
                                window.setTimeout(function(){
                                    $(".alert").alert('close');
                                },5000);
                            </script>
                            <div class="alert alert-danger alert-dismissable">
                                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                                <?php echo (isset($errmsg) && $errmsg != "") ? $errmsg : $this->session->flashdata('errmsg'); ?>
                            </div>
                    <?php } ?>

                    <div class="row">                   
                        <div class="col-md-6">                                                
                            <div class="form-group">
                                <?php echo form_label( 'Nama Role*', 'role_id' ) ?>
                                
                                <?php                                   
                                    $selectedrole = '';
                                    if(isset($priv->role_id) && trim($priv->role_id) != '') $selectedmod = $priv->role_id;
                                    $js = 'id="role_id" class="form-control select2" ';
                                    echo form_dropdown('role_id', $list_roles, $selectedrole, $js);
                                ?>
                                
                            </div><!-- /.form-group -->                                                        
                        </div><!-- /.col-md-6 -->                     
                    </div> 
                   
 

                   
                    <!-- SPACE -->
                    <div class="row"> 
                        <div class="col-md-6">                                                
                            <div class="form-group"></div>                            
                        </div>                        
                    </div>
                    
                    <?php echo form_label( 'Hak Akses Modul', 'lbl_aksesmodul' ) ?>                                         
                    <table class="table table-striped table-bordered table-hover" id="dt-listpriv" data-page-length="25">
                    <thead>
                        <tr>                                                                     
                            <th class="align-center" width="10%">#</th>
                            <th>Nama Modul</th>                                                                                                                                            
                        </tr>
                    </thead>
                    <tbody id="modul_det">                    
                    </tbody>
                    </table>
                     
                </div><!-- /.box-body -->
                
                <div class="box-footer">                     
                </div><!-- /.box-footer -->
                 
            </div><!-- /.box -->
        </div><!-- /col-xs-12 -->
                           
        <?php echo form_hidden($csrf); ?>
        <?php echo form_close(); ?>
        
    </div><!-- /.row -->
</section><!-- /.content -->

<!-- modal-content Popup Unit --> 
<div class="modal fade" id="modal-unit-pv" tabindex="-1" role="dialog" aria-labelledby="modal-unit-pv" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn btn-default btn-sm pull-right margin-left-10 bt_clselectunit"><span class="fa fa-close"></span> Tutup</button>        
        <button type="button" class="btn btn-default btn-sm pull-right bt_selectunit"><span class="fa fa-check"></span> Pilih</button>        
        <h4 class="modal-title custom_align" id="Heading">Pilih nama instansi/OPD</h4>
        
      </div>
      <div class="modal-body overflow-edit">          
          <div class="row" >
            <div class="box-body">                
              <table id="dt-popup-unit-pv" class="table table-striped table-bordered table-hover" data-page-length="25">
                    <thead>
                    <tr>                      
                      <th width="5%">#</th>
                      <th width="15%">Kode OPD</th>
                      <th width="80%">Nama OPD</th>                                          
                    </tr>
                    </thead>
                    <tbody style="font-size: 13px">                       
                    </tbody>
              </table>
            </div>

        </div>

      </div>
      <div class="modal-footer ">           
        
      </div>
    </div>
  </div>
</div>
<!-- /.modal-content Popup Unit --> 