<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
    <li><i class="fa fa-share"></i> Utility</li>
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
<section class="content">   
    <div class="box box-primary">
        <div class="box-header with-border">           
            <a href="<?= base_url('utility/role_manage/edit_form')?>" class="btn btn-primary btn-sm pull-right"><span class="fa fa-plus"></span> Tambah Role</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            
            <?php if($this->session->flashdata('message')) { ?>
                <script type="text/javascript">
                    window.setTimeout(function(){
                        $(".alert").alert('close');
                    },3000);
                </script>
                <div class="alert alert-success">            
                    <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                    <strong>Info! </strong><?php echo $this->session->flashdata('message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <script type="text/javascript">
                    window.setTimeout(function(){
                        $(".alert").alert('close');
                    },5000);
                </script>
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert" aria-hidden="true" type="button">x</button>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
            <?php } ?>
       
            <table class="table table-striped table-bordered table-hover" id="dt-listrole" data-page-length="25">
                <thead>
                    <tr>                                                                                                    
                        <th>Nama Role</th>
                        <th>Alias</th>                        
                        <th class="align-center">Aktif</th>
                        <th class="align-center">#</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $seq = 1;
                foreach($roles as $role) : ?>

                    <tr>                                                                   
                        <td><?php echo htmlspecialchars($role->role_name, ENT_QUOTES,'UTF-8'); ?></td>
                        <td><?php echo htmlspecialchars($role->role_alias,ENT_QUOTES,'UTF-8'); ?></td>                        
                        <td class="align-center"><?php echo ($role->active) ? anchor("utility/role_manage/deactivate/".$this->qsecure->encrypt($role->role_id), "<span class='fa fa-check text-green'>&nbsp;</span>".lang('index_active_link')."", array("onclick"=>"return confirm('Non-aktifkan role ?');") ) : 
                                                         anchor("utility/role_manage/activate/". $this->qsecure->encrypt($role->role_id), "<span class='fa fa-times text-red'>&nbsp;".lang('index_inactive_link')."</span>", array("onclick"=>"return confirm('Aktifkan role ?');"));?></td>
                        <td class="align-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-gear"></i> Aksi <span class="caret"></span>
                                </button>                             
                                <ul class="dropdown-menu" role="menu">                                 
                                    <li>                                    
                                        <?php
                                            $id = $this->qsecure->encrypt($role->role_id);
                                            $att = array(
                                                'data-toggle' => 'tooltip',
                                                'data-placement' => 'top',
                                            );
                                            $att['title'] = 'Edit';                                        
                                            echo anchor('utility/role_manage/edit_form/'.$id, '<i class="fa fa-fw fa-edit"></i>Edit',$att);
                                        ?>
                                    </li>                                    
                                </ul>
                            </div>    
                        </td>
                    </tr>

                <?php    
                    $seq++;
                endforeach;

                ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->

</section><!-- /.content -->

