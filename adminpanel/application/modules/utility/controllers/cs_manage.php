<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cs_manage extends Bss_controller {
  
    function __construct() {
        parent::__construct();
       
        $this->MOD_ALIAS = "MOD_USERMANAGE";
        $this->checkAuthorization($this->MOD_ALIAS);    
        $this->load->library('form_validation');
        $this->load->helper(array('url','language'));
        $this->load->model("Ion_auth_model","mauth");  
        $this->load->model("Mcs_manage","mcs_manage");       
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

    public function index(){
     
        $this->data['titlehead'] = "Daftar User :: DISPENDA KOTA BOGOR";
           
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'                         
        );
        $this->data['loadhead'] = $loadhead; 

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(   
             /*- DataTables JavaScript -*/           
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',               
            HTTP_MOD_JS.'modules/utility/cs_manage_js.js'          
        ); 
        $this->data['loadfoot'] = $loadfoot;

        if (!$this->ion_auth->logged_in())
        {
                //redirect them to the login page
                redirect('auth/login', 'refresh');
        }
        /*elseif (!$this->ion_auth->is_admin() && !$this->ion_auth->is_superadmin()) //remove this elseif if you want to enable this for non-admins
        {
                //redirect them to the home page because they must be an administrator to view this
                return show_error('You must be an administrator to access this page.');
        }*/
        else
        {
                //set the flash data error message if there is one                   
                $this->data['errmsg'] = (validation_errors() ? validation_errors() : "");
                $this->data['message'] = $this->session->flashdata('message');

                //list the users
                $this->data['users'] = $this->mauth->mcs_manage->get_users();
        }
        $this->_render_page('utility/vcs_list', $this->data, false, 'tmpl/vwbacktmpl');
        //$this->template->load('tmpl/vwbacktmpl','vuser_list',$data);
    }
	
    public function list_users(){
	    $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
		$role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
		
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
		
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mcs_manage->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res; //$this->mref->sort_parentchild($res);
        $totalfiltered =  $this->mcs_manage->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
		$btn_edit = "";
		$btn_group ="";
		$stat_visiting="";
		$btn_delete="";
		$no=1;
        foreach($results as $row) {       

			

			
			$btn_group="";
			$btn_active="";
			($row->active) ? 
				$btn_active .= "<a class='fa fa-check' href='#' onClick='if(confirm(\"Non-aktifkan user ? \")){ document.location=\" ".base_url()."utility/Cs_manage/deactivate/".$this->qsecure->encrypt($row->id)." \";}' title='Non-aktifkan user' ></a>" :
				$btn_active .= "<a class='fa fa-times' href='#' onClick='if(confirm(\"Aktifkan user ? \")){ document.location=\" ".base_url()."utility/Cs_manage/activate/".$this->qsecure->encrypt($row->id)." \";}' title='Non-aktifkan user' ></a>";
			
					//$btn_active .="anchor('utility/Cs_manage/deactivate/'".$this->qsecure->encrypt($row->id), '<span class="fa fa-check">&nbsp;</span>'".lang('index_active_link')."", array('onclick'=>'return confirm('Non-aktifkan user ?');') )" : 
					//$btn_active .="anchor('utility/Cs_manage/deactivate/'".$this->qsecure->encrypt($row->id), '<span class="fa fa-check">&nbsp;</span>'".lang('index_active_link')."", array('onclick'=>'return confirm('Non-aktifkan user ?');') )"	 
					//anchor("utility/Cs_manage/activate/". $this->qsecure->encrypt($user->id), "<span class='fa fa-times'>&nbsp;".lang('index_inactive_link')."</span>", array("onclick"=>"return confirm('Aktifkan user ?');"));
			
			$id = $this->qsecure->encrypt($row->id);
			$att = array(
				'data-toggle' => 'tooltip',
				'data-placement' => 'top',
			);
			$att['title'] = 'Edit';
			$att['class'] = 'btn btn-xs btn-info editButton';
			$btn_group .= anchor('utility/Cs_manage/form_user/'.$id, '<i class="fa fa-fw fa-edit"></i>', $att);
			$btn_group .= nbs();
			$att['class'] = 'btn btn-xs btn-danger deleteButton';
			$att['title'] = 'Delete';
			$att['onclick'] = "return confirm('Delete user ?');";
			$btn_group .=anchor('utility/Cs_manage/delete/'.$id, '<i class="fa fa-fw fa-trash"></i>', $att);
			
			
			
			array_push($build_array["data"],
                array(   	
					htmlspecialchars($row->username,ENT_QUOTES,'UTF-8'),
					htmlspecialchars($row->full_name, ENT_QUOTES,'UTF-8'),
                    htmlspecialchars($row->email,ENT_QUOTES,'UTF-8'),                       
                    htmlspecialchars($row->address,ENT_QUOTES,'UTF-8'),          
					$btn_active ,
					$btn_group
                )
            );   
			$no++;			
        }      
                    
        echo json_encode($build_array);   
    }
   
    public function form_user(){
        $id = 0;
        $this->data['id'] = $this->uri->segment(4);
        if (trim($this->data['id']) != ""){
            $id = $this->qsecure->decrypt($this->data['id']);
              $this->data['titlehead'] = "Edit User Loket :: DISPENDA KOTA BOGOR";
        }else{
              $this->data['titlehead'] = "Tambah User Loket :: DISPENDA KOTA BOGOR";
        }
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(                      
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css',    
            HTTP_ASSET_PATH.'plugins/wselect/wSelect.css'
        );
        $this->data['loadhead'] = $loadhead; 

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(                               
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',           
            HTTP_ASSET_PATH.'plugins/wselect/wSelect.min.js',
            HTTP_MOD_JS.'modules/utility/cs_manage_form_js.js'          
        ); 
        $this->data['loadfoot'] = $loadfoot;
       
        if (!$this->ion_auth->logged_in())
        {
                //redirect them to the login page
                redirect('auth/login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin() && !$this->ion_auth->is_superadmin()) //remove this elseif if you want to enable this for non-admins
        {
                //redirect them to the home page because they must be an administrator to view this
                return show_error('You must be an administrator to access this page.');
        }
        
       
        $tables = $this->config->item('tables','ion_auth');

        //validate form input
        $this->form_validation->set_rules('full_name', 'Nama Lengkap', 'required|trim');       
        $this->form_validation->set_rules('username', 'Username', 'required|trim|callback_whitespace_unamecheck');
        //$this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('role_id', 'Grup User', 'required');

        $this->form_validation->set_rules('phone', 'Telp/Hp', 'trim');
        $this->form_validation->set_rules('instansi_id', "Nama Perusahaan", 'trim|required');
        $this->form_validation->set_rules('address', "Alamat", 'trim');
        $this->form_validation->set_rules('photo', "Avatar", 'trim');

        /*$this->form_validation->set_rules('instansi_name', "Perusahaan", 'trim');
        $this->form_validation->set_rules('instansi_address', "Alamat Perusahaan", 'trim');
        $this->form_validation->set_rules('instansi_phone', "Telp. Perusahaan", 'trim');
        $this->form_validation->set_rules('instansi_fax', "Fax. Perusahaan", 'trim');
        $this->form_validation->set_rules('instansi_email', "Fax. Perusahaan", 'trim');*/
       
        if (trim($this->data['id']) == ""){ // new mode
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Konfirmasi password', 'required');           
            //$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique['.$tables['users'].'.email]');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        }else{ // edit mode
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        }
       
        if ($this->input->post("username_edit") AND trim($this->input->post("username_edit")) != "" AND
            trim($this->input->post("username")) != trim($this->input->post("username_edit")) ) {
             $this->form_validation->set_rules('username', 'Username', 'required|trim|callback_whitespace_unamecheck|callback_username_check');
        }
       
        if ($this->input->post("email_edit") AND trim($this->input->post("email_edit")) != "" AND
            trim($this->input->post("email")) != trim($this->input->post("email_edit")) ) {
             $this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_check');
        }
       
        $this->form_validation->set_message('required', '%s harus diisi !');
                        
        if( $id > 0 AND !$this->input->post('id')) {
            // retrieve data for edit
            $user = $this->mcs_manage->get_users($id);  
            $this->data['user_photo'] = $user->photo;
            $this->data['role_id'] = $user->role_id;
        }else{
            $user = new stdClass();
            $user->username = '';
            $user->full_name = '';
            $user->email = '';
            $user->phone = '';
            $user->instansi_id = 1;
            $user->address = '';
            $user->photo = '';
            $user->instansi_name = '';
            $user->instansi_address = '';
            $user->instansi_phone = '';
            $user->instansi_fax = '';
            $user->instansi_email = '';
            $user->role_id = '';    
            $user->agama = '';
            $user->asal_daerah = '';
            $user->jenis_kelamin = '';
            $user->tgl_lahir = '';
        }

        if (isset($_POST) && !empty($_POST))
        {                     
            if ($this->_valid_sess_csrf() === FALSE AND $this->data['id'] != $this->input->post("id"))
            {
                  show_error($this->lang->line('error_csrf'));
            }
           
            //update the password if it was posted
            if ($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', 'Konfirmasi password', 'required');
            }
           
            // POSTING VARIABLE
            $username = strtolower($this->input->post('username'));
            $full_name = $this->input->post('full_name');
            $email    = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $role_id = $this->input->post('role_id');
           
            $additional_data = array(
                'full_name' => $full_name,
                'role_id' => $role_id
            );

            $group_ids=array();
            $user_profile = array(                   
                'phone' => $this->input->post('phone'),
                'address' => $this->input->post('address'),
                'instansi_id'  => $this->input->post('instansi_id'),                          
                'photo'      => $this->input->post('photo'),
                'role_id' => $role_id
            );
           
            $data_pengasuh = array(                   
                'agama' => $this->input->post('agama'),
                'asal_daerah' => $this->input->post('asal_daerah'),
                'jenis_kelamin'  => $this->input->post('jenis_kelamin'),                          
                'tgl_lahir'      => date("Y-m-d",strtotime($this->input->post("tgl_lahir")))
            );  
                      
           
            //check to see if we are updating the user
            if( $id > 0 AND $this->input->post('id')) { // update
                $data = array (
                  'username' => $username,
                  'full_name' => $full_name,
                  'email' => $email,
                  'role_id' => $role_id
                );
               
                if ($this->input->post('password')) {
                    $data['password'] = trim($this->input->post('password'));
                }
               
                if ($this->form_validation->run($this) === TRUE AND $this->mauth->update($id, $data))
                {
                   
                    $this->mauth->update_profile($id, $user_profile);
                    $this->mauth->update_role($id,$role_id);
                    if ($user_profile["photo"] !== ""){
                        $this->session->set_userdata(sess_prefix() . "avatar", $user_profile["photo"]);
                    }
                    $this->session->set_flashdata('message', "Update user berhasil.." );
                    redirect("utility/Cs_manage", 'refresh');
                   
                }else{
                    //set the flash data error message if there is one
                    $this->data['errmsg'] = (validation_errors() ? validation_errors() : ($this->mauth->errors() ? $this->mauth->errors() : ""));
                    $this->data['message'] = $this->session->flashdata('message');
                    $this->session->set_flashdata('err', $this->data['errmsg']);
                   
                    $this->data['user_photo'] = $this->input->post('photo'); 
                    $this->data['role_id'] = $this->input->post('role_id'); 
                    $this->data['username_edit'] = $username;
                }
               
                               
            }else{ // insert
                    //$this->ion_auth->register($username, $password, $email, $additional_data, $group_ids, $user_profile);
                    //if ($this->form_validation->run($this) === TRUE AND  $retid = $this->ion_auth->register($username, $password, $email, $additional_data, $group_ids, $user_profile) )
                    if ($this->form_validation->run($this) === TRUE AND  $retid = $this->ion_auth->register($username, $password, $email, $additional_data, $user_profile) )
                    {
                        $this->session->set_flashdata('message', ($this->mauth->messages() ? $this->mauth->messages() : "Tambah user berhasil.."));
                            redirect("utility/Cs_manage", 'refresh');
                    }else{
                       
                        //set the flash data error message if there is one
                        $this->data['errmsg'] = (validation_errors() ? validation_errors() : ($this->mauth->errors() ? $this->mauth->errors() : ""));
                        $this->data['message'] = $this->session->flashdata('message');
                        $this->session->set_flashdata('err', $this->data['errmsg']);      
                        $user = new stdClass();
                        $user->username = $this->input->post('username');
                        $user->full_name = $this->input->post('full_name');
                        $user->email = $this->input->post('email');
                        $user->phone = $this->input->post('phone');
                        $user->instansi_id = $this->input->post('instansi_id');
                        $user->address = $this->input->post('address');
                        $user->photo = $this->input->post('photo');
                        $user->instansi_name = $this->input->post('instansi_name');
                        $user->instansi_address = $this->input->post('instansi_address');
                        $user->instansi_phone = $this->input->post('instansi_phone');
                        $user->instansi_fax = $this->input->post('instansi_fax');
                        $user->instansi_email = $this->input->post('instansi_email');
                        $user->role_id = $this->input->post('role_id');
                        $user->agama = $this->input->post('agama');
                        $user->asal_daerah = $this->input->post('asal_daerah');
                        $user->jenis_kelamin = $this->input->post('jenis_kelamin');
                        $user->tgl_lahir = $this->input->post('tgl_lahir');
                }
            }
                                      
        }//endif POST

      
        //display the create user form                                             
        $this->data['full_name'] = array(
                'name'  => 'full_name',
                'id'    => 'full_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('full_name', $user->full_name),
                'class' => 'form-control',
                'placeholder' => 'nama lengkap'
        );                  
        $this->data['password'] = array(
                'name' => 'password',
                'id'   => 'password',
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => 'password min 8 char',
        );
        $this->data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id'   => 'password_confirm',
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => 'konfirmasi password'
        );

        $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'email',
                'value' => $this->form_validation->set_value('email', $user->email),
                'class' => 'form-control',
                'placeholder' => 'email'
        );
        $this->data['email_edit'] =  $user->email;
       
 
        $this->data['username'] = array(
                'name'  => 'username',
                'id'    => 'username',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('username', $user->username),
                'class' => 'form-control',
                'placeholder' => 'username'                  
        );
        $this->data['username_edit'] =  $user->username;

        $this->data['address'] = array(
                'name'  => 'address',
                'id'    => 'address',
                //'type'  => 'text',
                'value' => $this->form_validation->set_value('address', $user->address),
                'class' => 'form-control',
                'rows' => '4',
                'placeholder' => 'alamat'                       
        );

        $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone', $user->phone),
                'class' => 'form-control',
                'placeholder' => 'telp/hp',
        );

        $this->data['instansi_name'] = array(
                'name'  => 'instansi_name',
                'id'    => 'instansi_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('instansi_name', $user->instansi_name),
                'class' => 'form-control',
                'placeholder' => 'nama perusahaan',
                'readonly'=>'true'
        );

        $this->data['instansi_id'] = array(
                'name'  => 'instansi_id',
                'id'    => 'instansi_id',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('instansi_id', $user->instansi_id),                                               
                'readonly'=>'true'
        );

        $this->data['instansi_address'] = array(
                'name'  => 'instansi_address',
                'id'    => 'instansi_address',
                //'type'  => 'text',
                'value' => $this->form_validation->set_value('instansi_address', $user->instansi_address),
                'class' => 'form-control',
                'placeholder' => 'alamat perusahaan',
                'rows' => '6',
                'readonly'=>'true'
        );

        $this->data['instansi_email'] = array(
                'name'  => 'instansi_email',
                'id'    => 'instansi_email',
                'type'  => 'email',
                'value' => $this->form_validation->set_value('instansi_email', $user->instansi_email),
                'class' => 'form-control',
                'placeholder' => 'email perusahaan',                       
                'readonly'=>'true'
        );

        $this->data['instansi_phone'] = array(
                'name'  => 'instansi_phone',
                'id'    => 'instansi_phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('instansi_phone', $user->instansi_phone ),
                'class' => 'form-control',
                'placeholder' => 'no telp',                       
                'readonly'=>'true'
        );

         $this->data['instansi_fax'] = array(
                'name'  => 'instansi_fax',
                'id'    => 'instansi_fax',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('instansi_fax', $user->instansi_fax ),
                'class' => 'form-control',
                'placeholder' => 'no fax',                       
                'readonly'=>'true'
        );
      
        // option list avatar               
        $list_avatar = array(
            array (
                'img_name' => '160x160.png',
                'img_ico' => '160x160_ico.png',
                'img_title' => '- Pilih Avatar -'
            ),
            array (
                'img_name' => 'avatar1.png',
                'img_ico' => 'avatar_ico1.png',
                'img_title' => 'Avatar-1'
            ),
            array (
                'img_name' => 'avatar2.png',
                'img_ico' => 'avatar_ico2.png',
                'img_title' => 'Avatar-2'
            ),
            array (
                'img_name' => 'avatar3.png',
                'img_ico' => 'avatar_ico3.png',
                'img_title' => 'Avatar-3'
            ),
            array (
                'img_name' => 'avatar4.png',
                'img_ico' => 'avatar_ico4.png',
                'img_title' => 'Avatar-4'
            ),
            array (
                'img_name' => 'avatar5.png',
                'img_ico' => 'avatar_ico5.png',
                'img_title' => 'Avatar-5'
            ),
        );

        //$this->data['user_photo'] = $user->photo;
        //$this->data['role_id'] = $user->role_id;
        $this->data['csrf'] = $this->_get_sess_csrf();
        $this->data['list_avatar'] = $list_avatar;


        // option role or group user
        $data_group  = $this->mcs_manage->get_groups();
        $list_role ['0'] = '- Pilih role -';
        foreach ($data_group as $row) {
            $list_role[$row->role_id] = $row->role_name;
        }
        $this->data['list_role'] =  $list_role;

        $this->_render_page('utility/vcs_form', $this->data, false, 'tmpl/vwbacktmpl');
         
                     
    }
   
    //activate the user
    public function activate($id){       
        if (!$this->ion_auth->logged_in() OR (! $this->ion_auth->is_admin() && ! $this->ion_auth->is_superadmin()) )
        {
             return show_error('You must be an administrator to action this method.');                       
        }       
        if ($id != null && $id != ""){ $id = $this->qsecure->decrypt($id); }
        $id= (int)$id;
        $activation = $this->mauth->activate($id);
        if ($activation)
        {                       
            $this->session->set_flashdata('message', $this->mauth->messages());          
        }
        else
        {        
            $this->session->set_flashdata('err', $this->mauth->errors());       
        }
        redirect("utility/Cs_manage", 'refresh');
        
    }

    //deactivate the user
    public function deactivate($id = NULL)
    {
        if (!$this->ion_auth->logged_in() OR (! $this->ion_auth->is_admin() && ! $this->ion_auth->is_superadmin()) ) {               
            return show_error('You must be an administrator to action this method.');
        }   
               
        if ($id != null && $id != ""){ $id = $this->qsecure->decrypt($id); }
        $id= (int)$id;
        if ($id == 1) {
            redirect('utility/Cs_manage/list_user', 'refresh');
        }
           
        $deactivate = $this->mauth->deactivate($id);
    if ($deactivate)
        {           
            $this->session->set_flashdata('message', $this->mauth->messages());          
        }
        else
        {        
            $this->session->set_flashdata('err', $this->mauth->errors());       
        }
        redirect("utility/Cs_manage", 'refresh');
    }
   
    //delete the user
    public function delete($id = NULL)
    {
        if (!$this->ion_auth->logged_in() OR (! $this->ion_auth->is_admin() && ! $this->ion_auth->is_superadmin()) ) {               
            return show_error('You must be an administrator to action this method.');
        }   
               
        if ($id != null && $id != ""){ $id = $this->qsecure->decrypt($id); }
        $id= (int)$id;
        if ($id == 1) {
            redirect('utility/Cs_manage', 'refresh');
        }            
        $res = $this->mauth->delete_user($id);
        if ($res)
        {           
            $this->session->set_flashdata('message', $this->mauth->messages());          
        }
        else
        {        
            $this->session->set_flashdata('err', $this->mauth->errors());       
        }
        //redirect them back to the auth page
        redirect('utility/Cs_manage', 'refresh');       
    }
   
    function whitespace_unamecheck($str) {
        //$str = $this->input->post("username");
        if (ctype_space($str)) {                         
            $this->form_validation->set_message('whitespace_unamecheck', '%s tidak boleh berisi spasi !');
            return FALSE;
        }else{
            return TRUE;
        }
    }
   
    function username_check($username) {          
        if ($this->mcs_manage->username_exists($username, $this->qsecure->decrypt($this->input->post("id"))) ) {
            $this->form_validation->set_message('username_check', '%s sudah digunakan (duplikat) !');
            return FALSE;
        }else{
            return TRUE;
        }
       
    }
   
    function email_check($email) {               
        if ($this->mcs_manage->email_exists($email, $this->qsecure->decrypt($this->input->post("id"))) ) {
            $this->form_validation->set_message('email_check', '%s sudah digunakan (duplikat) !');
            return FALSE;
        }else{
            return TRUE;
        }
    }
   
}