<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Bss_controller {
        
	public function __construct(){
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_HOME";
        $this->checkAuthorization($this->MOD_ALIAS);
    }

	public function index(){            
        $data['titlehead'] = "Dashboard :: Dispenda Kota Bogor";
        //$this->load->model("mcommon","mcom");
		$this->load->model("Mdashboard","mcom"); 
		// custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.min.js',
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.resize.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.pie.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.categories.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.axislabels.js',
            HTTP_MOD_JS.'modules/main/fpp_dashboard.js'            
        );
		
		// load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);
		
		//$data['loadhead'] = $loadhead;

		$data['tglmulai'] = date('Y-m-d');
		$data['tglakhir'] = date('Y-m-d');
		$data['auto'] = 1;


        // option list thang
        $data_loket  = $this->mcom->get_loket();
        $list_loket ['0'] = '- Semua Loket -';
        foreach ($data_loket as $row) {
            $list_loket[$row->id] = $row->name;
        }
        $data['list_loket'] = $list_loket;
        $data['loket_id'] = 0;

        // option list thang
        $data_cs  = $this->mcom->get_cs();
        $list_cs ['0'] = '- Semua Customer Service -';
        foreach ($data_cs as $row) {
            $list_cs[$row->id] = $row->name;
        }
        $data['list_cs'] = $list_cs;
        $data['cs'] = 0;


		$data['datajml'] =  $this->mcom->getDataJml(null,null,null,null);
        $data['loadfoot'] = $loadfoot;     
        $this->template->load('tmpl/vwbacktmpl','vdashboard',$data);                        
	}
	
	function GetBarchart(){
		$this->load->model("Mdashboard","mcom");      

		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");

        $results  =  $this->mcom->getDataGrafik($tglmulai ,$tglakhir ,$loket_id,$cs); 
		$ttl = count($results);
		$build_array = array ("data_grafik"=>array(), "jml"=>$ttl);
        foreach($results as $row) {         
			array_push($build_array["data_grafik"],
				array(
					$row->indikator_name, 
					$row->jumlah
				)
			);
        }       
        echo json_encode($build_array); 
	}

	function GetBarchartAll(){
		$this->load->model("Mdashboard","mcom");      

		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");

        $results  =  $this->mcom->getDataGrafik(null ,null ,null,null); 
		$ttl = count($results);
		$build_array = array ("data_grafik"=>array(), "jml"=>$ttl, "data_sangat_puas"=>array(), "data_puas"=>array(), "data_tidak_puas"=>array());
        foreach($results as $row) {      
        	if($row->indikator_category_id==1){
				array_push($build_array["data_sangat_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	}else if($row->indikator_category_id==2){
				array_push($build_array["data_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	}else if($row->indikator_category_id==3){
				array_push($build_array["data_tidak_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	} 

        }       
        echo json_encode($build_array); 
	}


	public function generateAll(){
		$this->load->model("Mdashboard","mcom"); 
		$datajml  = $this->mcom->getDataJml(null,null,null,null);
		$hasil="";     
		$cnt = count($datajml);	

		if($cnt > 0 ){
			$total=0;
			foreach($datajml as $rowjml) :
				$total = $total + $rowjml['jumlah'];
			endforeach; 


	        foreach($datajml as $row) :

	        	$persen = (($row['jumlah']/$total) * 100 ) ;
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>".$row['jumlah']."</h3>
					  <h3>".round($persen,2)."%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }else{
	    	$datajmlkosong   = $this->mcom->getDataJmlKosong();
	        foreach($datajmlkosong as $row) :
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>0</h3>
					  <h3>0%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }


        echo $hasil;
	}

	public function generate(){
		$this->load->model("Mdashboard","mcom"); 
		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");
		$datajml   = $this->mcom->getDataJml($tglmulai ,$tglakhir ,$loket_id,$cs);
		$hasil="";     
		$cnt = count($datajml);	

		if($cnt > 0 ){
			$total=0;
			foreach($datajml as $rowjml) :
				$total = $total + $rowjml['jumlah'];
			endforeach; 


	        foreach($datajml as $row) :

	        	$persen = (($row['jumlah']/$total) * 100 ) ;
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>".$row['jumlah']."</h3>
					  <h3>".round($persen,2)."%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }else{
	    	$datajmlkosong   = $this->mcom->getDataJmlKosong();
	        foreach($datajmlkosong as $row) :
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>0</h3>
					  <h3>0%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }


        echo $hasil;
	}

        
}
