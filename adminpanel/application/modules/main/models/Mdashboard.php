<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mdashboard extends BSS_model
{
    protected $_table = "res_fpp";
    protected $_table_det = "res_fpp_det";
    protected $_id = "fpp_id";
    protected $_data = null;
               
	public function getDataGrafik($tglmulai=null,$tglakhir=null,$loket_id=null,$cs=null)
    {


        $this->db->select("b.indikator_category_id, b.icon, b.indikator_category_name as indikator_name,
                          count(a.indikator_category_id) as jumlah", FALSE);
        $this->db->from('indikator_penilaian a');
        $this->db->join('ref_indikator_cat b', 'a.indikator_category_id=b.indikator_category_id');
        $this->db->join('res_ref_instansi c', 'a.instansi_id=c.instansi_id');
        $this->db->join('res_sec_user d', 'a.user_id=d.id');


        if($tglmulai !=null && $tglakhir !=null){
            $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') >= DATE_FORMAT('".$tglmulai."', '%Y-%m-%d')");
            $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') <= DATE_FORMAT('".$tglakhir."', '%Y-%m-%d')");
        }

        if($loket_id != 0){
            $this->db->where("a.instansi_id",$loket_id);
        }

        if($cs != 0){
            $this->db->where("a.user_id",$cs);
        }

        $this->db->where("DATE_FORMAT(a.created_date, '%Y') = DATE_FORMAT(NOW(), '%Y')");

        $this->db->group_by('a.indikator_category_id');

        $Q = $this->db->get();
        $this->_data = $Q->result();
        $Q->free_result();
        return $this->_data;
    }
    

    public function getDataJml($tglmulai=null,$tglakhir=null,$loket_id=null,$cs=null){

        $this->db->select("b.indikator_category_id, b.icon, b.indikator_category_name as indikator_name,
                          count(a.indikator_category_id) as jumlah", FALSE);
        $this->db->from('indikator_penilaian a');
        $this->db->join('ref_indikator_cat b', 'a.indikator_category_id=b.indikator_category_id');
        $this->db->join('res_ref_instansi c', 'a.instansi_id=c.instansi_id');
        $this->db->join('res_sec_user d', 'a.user_id=d.id');


        if($tglmulai !=null && $tglakhir !=null){
            $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') >= DATE_FORMAT('".$tglmulai."', '%Y-%m-%d')");
            $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') <= DATE_FORMAT('".$tglakhir."', '%Y-%m-%d')");
        }

         if($loket_id != 0){
            $this->db->where("c.instansi_id",$loket_id);
        }

        if($cs != 0){
            $this->db->where("a.user_id",$cs);
        }

        $this->db->group_by('a.indikator_category_id');

        $this->db->where("DATE_FORMAT(a.created_date, '%Y') = DATE_FORMAT(NOW(), '%Y')");


        $query = $this->db->get();
        return $query->result_array();  
    }

    public function getDataJmlKosong(){

        $this->db->select("b.indikator_category_id, b.icon, b.indikator_category_name as indikator_name,
                          0 as jumlah", FALSE);
        $this->db->from('ref_indikator_cat b');
        $query = $this->db->get();
        return $query->result_array();  
    }



    function get_loket($instansi_id=null){
          $this->db->select("a.instansi_id as id, a.instansi_name as name ");
          $this->db->from("res_ref_instansi a");          
          if (! is_null($instansi_id) && $instansi_id != ""){
              $this->db->where('a.instansi_id', $instansi_id);
          }                                                           
          $Q = $this->db->get();
          if (! is_null($instansi_id) && $instansi_id != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }

    function get_cs($id=null){
            $this->db->select("a.id as id, a.full_name as name ");
            $this->db->from("res_sec_user a");       
            $this->db->where("a.role_id",3);    
            $this->db->where("a.active",1);      
            $this->db->order_by("a.full_name");       
            if (! is_null($id) && $id != ""){
                $this->db->where('a.id', $id);
            }                                                           
            $Q = $this->db->get();
            if (! is_null($id) && $id != ""){
                $this->_data = $Q->row();
            }else{
                $this->_data = $Q->result();  
            }
            $Q->free_result();
            return $this->_data;
    }

}
