<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
	<li><i class="fa fa-fw fa-files-o"></i>Tahun Anggaran</li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                 <!-- form start -->
				<?php
				$attributes = array('class' => 'form-horizontal', 'id' => 'form_input');
				echo form_open_multipart(base_url() . 'thang/Cthang/form_thang/'.(isset($thang) ? $thang :''), $attributes); 
				?>
			    <div class="box-body">
					<center>
						<div id="divQLoading">
							<div class="loading-screen">
								<span class="fa fa-refresh fa-spin"></span>
							</div>
						</div><!-- /#divQLoading -->
					</center>
					<?php if (isset($err_msg) && $err_msg != "") { ?><div class="alert alert-danger"><?=$err_msg?> </div>  <?php } ?>
				        <div class="panel box box-solid">
					        <div class="box-header with-border">
						        <h4 class="box-title">
						            <a data-toggle="collapse" data-parent="#accordion" href="#identitas_unit"><small><strong>Edit/Tambah</strong></small> </a>
						        </h4>
					        </div>
					<div class="row">
                        <div class="col-md-11">
                            <input name="thang" type="hidden" value="<?php echo set_value('thang', (isset($thang) ? $thang :'')) ; ?>">
                            </br>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fpp_tgl">Tahun Anggaran*</label>
								<div class="col-sm-8">
									<input name="thang" id="thang" class="form-control" value="<?=(isset($thang) ? $thang : "")?>"  />
								</div>
                            </div>
						</div><!-- /.col-md-11 -->
					</div><!-- /.col-md-11 -->                                         
				</div><!-- /.panel -->
				<div class="box-footer">
					<button type="submit" id="simpan" class="btn btn-info" onclick="return confirm('Simpan data permohonan ?')">Simpan</button>
					<a onclick="return confirm('Batalkan pengisian data tahun anggaran ?')" href="<?php echo base_url()."thang/Cthang"?>" class="btn btn-default" type="submit">Batal</a>                
				</div><!-- /.box-footer --> 
			</form>   
			<?php echo form_close();?>
			</div><!-- /.box-info -->
        </div><!-- /col-xs-12 -->
    </div>
</section>
