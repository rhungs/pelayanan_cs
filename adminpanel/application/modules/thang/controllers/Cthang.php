<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cthang extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_TAHUNANGGARAN";
        $this->checkAuthorization($this->MOD_ALIAS);            
        $this->load->model("Mthang","mfpp");
    }
    
    public function index(){
		$data['titlehead'] = "Tahun Anggaran :: DISPENDA KOTA BOGOR";
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css',
			HTTP_ASSET_PATH.'css/style.css'
        );
        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
			HTTP_ASSET_PATH.'plugins/jQuery/jquery.form.min.js',
            HTTP_MOD_JS.'modules/thang/thang_list_js.js'            
        );
		// load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);
		$data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
		$this->template->load('tmpl/vwbacktmpl','v_thang_list',$data);     
    }
    
    public function list_thang(){
	    $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
		$role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
		
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
		
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mfpp->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res; //$this->mref->sort_parentchild($res);
        $totalfiltered =  $this->mfpp->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
		$btn_edit = "";
		$btn_group ="";
		$stat_visiting="";
		$btn_delete="";
		$no=1;
        foreach($results as $row) {         
			$btn_edit = anchor("thang/Cthang/form_thang/".$this->qsecure->encrypt($row->thang), "<span class='fa fa-edit'></span>", array('class'=>'btn btn-info btn-xs','title'=>'Edit')) ;
			if($role_id=="1" || $role_id=="2" || $role_id=="3"){
				$btn_delete = " <a class='btn btn-danger btn-xs' href='#' onClick='if(confirm(\"Hapus tahun anggaran ini ? \")){ document.location=\" ".base_url()."thang/Cthang/delete_thang/".$this->qsecure->encrypt($row->thang)." \";}' title='Delete' ><span class='fa fa-trash-o'></span>&nbsp;</a>";
			}else{
				$btn_delete="";
			}
			$hiden_text=" <input id='thang' type='hidden' value='". $this->qsecure->encrypt($row->thang) ."' />";
			$btn_group  = $btn_edit.$btn_delete.$hiden_text;
			array_push($build_array["data"],
                array(   
					$btn_group,		
                    $row->thang
                )
            );   
			$no++;			
        }      
                    
        echo json_encode($build_array);   
    }
    
    public function form_thang(){
        $id = $this->uri->segment(4); 
        if ($id == "0") { $id = ""; }
        
        //--== parameetr redirect reservasi halaman home
 
        $data['unit_id'] = $id;
        if ( is_null($id) || trim($id) == '' ) {
            $data['titlehead'] = 'Form Tahun Anggaran :: DISPENDA KOTA BOGOR';
            $data['fpp_status_id'] = -1;
        } else {
            $data['titlehead'] = 'Edit Tahun Anggaran :: DISPENDA KOTA BOGOR';
            $id = $this->qsecure->decrypt($id);            
        }
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',    			
            HTTP_MOD_JS.'modules/thang/thang_form_js.js',            
        );
             
        //$loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
        
        // config rules validation
        $config = array (	
            array(
                'field'=>'thang',
                'rules'=>'trim|required'
            )
        );
	
	                    
       if( ! is_null($id) && (trim($id) != '')) {
            $result = $this->mfpp->get_view_edit($id);
            if (count($result) > 0 ) {
                $data['thang'] = $result->thang;
            }        
       }else{
            $data['thang'] = "";
		}
   
       if ($_POST) { 
            $this->form_validation->set_rules($config); 
            $this->form_validation->set_message('required', '%s harus diisi !');
            if($this->form_validation->run() === FALSE) {
                $data['err_msg'] = validation_errors();
            }else{
                $res = $this->mfpp->thang_save($id);
                if ($res["success"] == true)
                    $this->session->set_flashdata('info', $res['msg']);
                else
                    $this->session->set_flashdata('err', $res['msg']);
                redirect('thang/Cthang');                                
            }
        }
	    $this->template->load('tmpl/vwbacktmpl','v_thang_form',$data);     
    }
 
    
	public function approve()
    {
        $id = $this->qsecure->decrypt($this->input->post('id'));    
        $email = $this->input->post('email');
        $msg = $this->input->post('msg');
        $cat = array('unit_id' => $id,
                     'catatan_tgl' => date('Y-m-d H:i:s'),
                     'catatan' => $msg,
                     'status' => 2,
                     'logins_user_id' => $this->session->userdata( sess_prefix()."userid"));
        $this->mfpp->insertHis($cat);
		$this->mfpp->approve($id, $cat);
		$res['status'] = 'Approve berhasil';
        echo json_encode($res);
    }
	
	public function visiting()
    {
        $id = $this->qsecure->decrypt($this->input->post('id'));    
		$this->mfpp->visiting($id);
		$res['status'] = 'Visiting berhasil';
        echo json_encode($res);
    }     
    public function delete_thang($id){
        $id = $this->qsecure->decrypt($id);  
        $res = $this->mfpp->delete_thang($id);
        if ($res["success"] == true)
            $this->session->set_flashdata('info', $res['msg']);
        else
            $this->session->set_flashdata('err', $res['msg']);
			redirect('thang/Cthang');
    } 
    
}
