<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mindikatorcore extends Bss_model
{
	protected $_table = "ref_indikator_cat";
    protected $_id = "indikator_category_id";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("indikator_category_id,indikator_category_name,active");
        $this->db->from("ref_indikator_cat");
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( indikator_category_name LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
            $this->db->order_by("indikator_category_id");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("indikator_category_id", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }
    function get_list_cnt($by=null, $search_string=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("count(indikator_category_id) _cnt");
		$this->db->from("ref_indikator_cat");
        if (! is_null($search_string) && $search_string != ""){
             $this->db->where("( indikator_category_name LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
    public function get_view_edit($id=null,$by=null){                 
        $this->db->select("indikator_category_id,indikator_category_name,active");
        $this->db->from('ref_indikator_cat');    
		$this->db->where("indikator_category_id",$id);		
        if ($by != null) {
            $bye = $this->_by($by);          
            $this->db->where($bye,"",false);
        }
        $Q = $this->db->get();
        if ($id == null && $id == ""){
            $this->_data = $Q->result();
        }else{
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;
    }
    public function save($id){       
		if ($id == null OR $id == ""){
			$dataIn = array(    
				'indikator_category_id' => $this->input->post("indikator_category_id"),
				'indikator_category_name' => $this->input->post("indikator_category_name"),
				'active' => 1
			);  
			$this->insert_indikator($dataIn);
		}else{
			$dataIn = array(    
				'indikator_category_name' => $this->input->post("indikator_category_name")
			);  
			$this->update_indikator($id,$dataIn);
		}
    }
    function insert_indikator($data){
        $this->db->trans_begin();                 
        $this->db->insert("ref_indikator_cat", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function update_indikator($id,$data){
        $this->db->trans_begin();
        $this->db->where("indikator_category_id",$id);
        $this->db->update("ref_indikator_cat", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function delete_indikator_category_id360($id,$indikator_category_name){
		$data= array(    
			'active' => false
		);  
        $this->db->trans_begin();
		$this->db->where("indikator_category_id",$id);
		$this->db->where("indikator_category_name",$indikator_category_name);
        $this->db->update('ref_indikator_cat',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="T.A Anggaran & indikator_category_name gagal dinonaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="T.A Anggaran & indikator_category_name berhasil dinonaktifkan...";
        }
        return $return;
    }
	function active($id){
		$data= array(    
			'active' => true
		);  
        $this->db->trans_begin();
		$this->db->where("indikator_category_id",$id);
        $this->db->update('ref_indikator_cat',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="Perilaku gagal diaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="Perilaku berhasil diaktifkan...";
        }
        return $return;
    }
	
	function deactive($id){
		$data= array(    
			'active' => false
		);  
        $this->db->trans_begin();
		$this->db->where("indikator_category_id",$id);
        $this->db->update('ref_indikator_cat',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="Perilaku gagal diaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="Perilaku berhasil diaktifkan...";
        }
        return $return;
    }
    function insertHis($msg){
        $this->db->insert('pend_his',$msg);
    }
    function insert_notif($data){
        $this->db->trans_begin();                 
        $this->db->insert("res_notif", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

}
