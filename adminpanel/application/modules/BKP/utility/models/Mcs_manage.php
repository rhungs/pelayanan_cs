<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mcs_manage extends BSS_Model
{
	protected $tables;
	protected $table_instansi = "res_ref_instansi";
	protected $_data = null;
	protected $_id = "u.id";
        
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->config('ion_auth', TRUE);
		$this->load->helper('cookie');
		$this->load->helper('date');
		$this->lang->load('ion_auth');

		//initialize db tables data
		$this->tables  = $this->config->item('tables', 'ion_auth');
	}
	
	
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null){
		$this->db->select("u.id, u.username, u.full_name, u.email, u.created_date, u.last_login, u.active,  "
				. "        p.instansi_id, p.address, p.phone, p.photo, u.role_id, r.role_name, i.instansi_name, "
				. "        i.instansi_address, i.instansi_email, i.instansi_phone, i.instansi_fax");
		$this->db->from($this->tables['users'] . " u");
		$this->db->join($this->tables['users_profile'] . " p","u.id=p.userid","inner");
		//$this->db->join($this->tables['users_groups'] . " ur","ur.userid=u.id","inner");
		$this->db->join($this->tables['groups'] . " r","r.role_id=u.role_id","inner");
		$this->db->join($this->table_instansi . " i","i.instansi_id=p.instansi_id","inner");
        $this->db->where("u.role_id",3);
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( u.full_name LIKE '%$search_string%' OR   u.username  LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
            $this->db->order_by("u.id");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("unit_id", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }
	
    function get_list_cnt($by=null, $search_string=null){
		$this->db->select("count(u.id) as _cnt");
		$this->db->from($this->tables['users'] . " u");
		$this->db->join($this->tables['users_profile'] . " p","u.id=p.userid","inner");
		//$this->db->join($this->tables['users_groups'] . " ur","ur.userid=u.id","inner");
		$this->db->join($this->tables['groups'] . " r","r.role_id=u.role_id","inner");
		$this->db->join($this->table_instansi . " i","i.instansi_id=p.instansi_id","inner");
        $this->db->where("u.role_id",3);
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( u.full_name LIKE '%$search_string%' OR   u.username  LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
	

	function get_users($id=null){
            $this->db->select("u.id, u.username, u.full_name, u.email, u.created_date, u.last_login, u.active,  "
                    . "        p.instansi_id, p.address, p.phone, p.photo, u.role_id, r.role_name, i.instansi_name, "
                    . "        i.instansi_address, i.instansi_email, i.instansi_phone, i.instansi_fax");
            $this->db->from($this->tables['users'] . " u");
            $this->db->join($this->tables['users_profile'] . " p","u.id=p.userid","inner");
            //$this->db->join($this->tables['users_groups'] . " ur","ur.userid=u.id","inner");
            $this->db->join($this->tables['groups'] . " r","r.role_id=u.role_id","inner");
            $this->db->join($this->table_instansi . " i","i.instansi_id=p.instansi_id","inner");
            if ($id == null OR $id == "") {
                /*if ($this->session->userdata (sess_prefix() . "rolealias") == "superadmin"){
                    $this->db->where("r.role_id >=", 1);
                }
                else if ($this->session->userdata (sess_prefix() . "rolealias") == "admin"){
                    $this->db->where("r.role_id >=", 2);
                }*/
                $this->db->order_by("u.username","asc");          
                
                $Q = $this->db->get();
                $this->_data = $Q->result();
            }else{
                $this->db->where("u.id", $id);
                
                $Q = $this->db->get();
                $this->_data = $Q->row();
            }
        
        
            $Q->free_result();
            return $this->_data;            
        }
        
		
		/*function get_users($id=null){
            $this->db->select("u.ida, u.username, u.full_name, u.email, u.created_date, u.last_login, u.active,DATE_FORMAT(p.tgl_lahir,'%Y-%m-%d') AS tgl_lahir, "
                    . "        p.instansi_id, p.address, p.phone, p.photo, u.role_id, r.role_name, i.instansi_name, "
                    . "        i.instansi_address, i.instansi_email, i.instansi_phone, i.instansi_fax,p.jenis_kelamin,p.asal_daerah,p.agama");
            $this->db->from($this->tables['users'] . " u");
            $this->db->join($this->tables['users_profile'] . " p","u.id=p.userid","inner");
            $this->db->join($this->tables['users_groups'] . " ur","ur.userid=u.id","inner");
            $this->db->join($this->tables['groups'] . " r","r.role_id=u.role_id","inner");
            $this->db->join($this->table_instansi . " i","i.instansi_id=p.instansi_id","inner");
            if ($id == null OR $id == "") {
                if ($this->session->userdata (sess_prefix() . "rolealias") == "superadmin"){
                    $this->db->where("r.role_id >=", 1);
                }
                else if ($this->session->userdata (sess_prefix() . "rolealias") == "admin"){
                    $this->db->where("r.role_id >=", 2);
                }
                $this->db->order_by("u.username","asc");          
                
                $Q = $this->db->get();
                $this->_data = $Q->result();
            }else{
                $this->db->where("u.id", $id);
                
                $Q = $this->db->get();
                $this->_data = $Q->row();
            }
        
        
            $Q->free_result();
            return $this->_data;            
        }*/
        
        function get_groups(){
            $this->db->select("role_id, role_name");            
            $this->db->from($this->tables['groups']);
            $this->db->where("active", 1);
            //if ($this->session->userdata (sess_prefix() . "rolealias") == "admin"){
            $this->db->where("role_id >=", 2);
            //}
            $this->db->order_by("role_id","asc");          
            $Q = $this->db->get();
            $this->_data = $Q->result();
            $Q->free_result();
            return $this->_data;
            
        }
        
        function get_jenis_kelamin(){
            $this->db->select("id AS id_jenis_kelamin, jenis_kelamin");            
            $this->db->from('master_jenis_kelamin');
//            $this->db->where("aktif", 1);
            //if ($this->session->userdata (sess_prefix() . "rolealias") == "admin"){
//            $this->db->where("role_id >=", 2);
            //}
            $this->db->order_by("id","asc");          
            $Q = $this->db->get();
            $this->_data = $Q->result();
            $Q->free_result();
            return $this->_data;
            
        }
        
        function get_agama(){
            $this->db->select("agama_id, agama");            
            $this->db->from('ref_agama');
            $this->db->where("aktif", 1);
            //if ($this->session->userdata (sess_prefix() . "rolealias") == "admin"){
//            $this->db->where("role_id >=", 2);
            //}
            $this->db->order_by("agama_id","asc");          
            $Q = $this->db->get();
            $this->_data = $Q->result();
            $Q->free_result();
            return $this->_data;
            
        }
        
        public function username_exists($username, $id=null)
		{		 
            if (empty($username))
            {
                return FALSE;
            }
		 
            $this->db->select("count(id) as _cnt");
            $this->db->from($this->tables['users']);
            $this->db->where('username', $username);
            if ($id != null){
                $this->db->where('id!=', $id);
            }
            $Q = $this->db->get();
            $this->_data = $Q->row()->_cnt;
            $Q->free_result();
            if ($this->_data > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        public function email_exists($email, $id=null)
	{		 
            if (empty($email))
            {
                return FALSE;
            }
		 
            $this->db->select("count(id) as _cnt");
            $this->db->from($this->tables['users']);
            $this->db->where('email', $email);
            if ($id != null){
                $this->db->where('id!=', $id);
            }
            $Q = $this->db->get();
            $this->_data = $Q->row()->_cnt;
            $Q->free_result();
            if ($this->_data > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
                
  
 
}
