<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mthang extends Bss_model
{
	protected $_table = "ref_thang";
    protected $_id = "thang";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("thang");
        $this->db->from("ref_thang");
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( thang LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
            $this->db->order_by("thang");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("thang", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }
    function get_list_cnt($by=null, $search_string=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("count(thang) _cnt");
		$this->db->from("ref_thang");
        if (! is_null($search_string) && $search_string != ""){
             $this->db->where("( thang LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
    public function get_view_edit($id=null,$by=null){                 
        $this->db->select("thang");
        $this->db->from('ref_thang');        
        if ($by != null) {
            $bye = $this->_by($by);          
            $this->db->where($bye,"",false);
        }
        if ($id == null && $id == ""){                         
            $this->db->order_by("thang","asc");          
        }else{
            $this->db->where("thang",$id);
        }
        $Q = $this->db->get();
        if ($id == null && $id == ""){
            $this->_data = $Q->result();
        }else{
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;
    }
	public function get_level_unit($thang=null){
          $this->db->select("a.thang as id, a.unit_name_str as name ");
          $this->db->from("v_unit a");          
          $this->db->where('a.active', 1);
		  $this->db->where('a.level IN (0,1,2)');
          if (! is_null($thang) && $thang != ""){
              $this->db->where('a.thang', $thang);
          }                                                           
          $Q = $this->db->get();
          if (! is_null($thang) && $thang != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }
    public function thang_save($id){
        $dataIn = array(    
			'thang' => $this->input->post("thang")
        );         
		if ($id == null OR $id == ""){
			$this->insert_thang($dataIn);
		}else{
			$this->update_thang($id,$dataIn);
		}
    }
    function insert_thang($data){
        $this->db->trans_begin();                 
        $this->db->insert("ref_thang", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function update_thang($id,$data){
        $this->db->trans_begin();
        $this->db->where("thang",$id);
        $this->db->update("ref_thang", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function delete_thang($id){
        $this->db->trans_begin();
		$this->db->where("thang",$id);
         $this->db->delete('ref_thang');
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="Unit gagal dihapus !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="Unit berhasil dihapus...";
        }
        return $return;
    }
    function insertHis($msg){
        $this->db->insert('pend_his',$msg);
    }
	function approve($id, $msg){
        $this->db->query("UPDATE pend_data SET status=2 WHERE pend_id=?",array($id));
        $this->db->insert('pend_his',$msg);
    }
    function insert_notif($data){
        $this->db->trans_begin();                 
        $this->db->insert("res_notif", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

}
