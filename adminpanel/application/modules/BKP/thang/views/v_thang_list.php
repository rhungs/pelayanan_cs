<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i>Referensi</a></li>    
	<li><i class="fa fa-fw fa-files-o"></i>Master Unit</li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>

    <section class="content">
         <?php if($this->session->flashdata('info')) { ?>
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">x</a>
                <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert">x</a>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
        <?php } ?>
        <div class="box">
            <div class="box-header with-border">
               <?php 
                    if ($this->session->userdata(sess_prefix() . "rolealias") == "registered") { ?>
                        <a href="<?= base_url('main/fpp/form_fpp')?>" class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
               <?php } ?>
            </div>
            <div class="box-header">
            <div class="modal-header">
                <a class="btn btn-primary btn-xs pull-right" href="<?= base_url('thang/Cthang/form_thang')?>">
                <span class="glyphicon glyphicon-plus"></span> Tambah Tahun Anggaran
                </a>
            </div>
                                               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="box-body table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dt-listthang" data-page-length="50">
						<thead>
						<tr>                    
							<th>Aksi</th>
							<th>Tahun Anggaran</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
	

