<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>laporankepuasan/Laporankepuasan" ><i class="fa fa-dashboard"></i> Laporan Kepuasan Pelanggan</a></li>    
  </ol>
</section>
    <?php
    if ($this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR
              $this->session->userdata(sess_prefix() . "rolealias") == "admin") 
          { ?>
			<section>
				<div class="row">
					<div class="col-md-12">
					  <!-- Bar chart -->
					  <div class="box box-primary">
						<div class="box-header with-border">
						</div>
						<div class="box-body">			
				            <div class="col-md-6" >
								<div class="row">
									 <div class="form-group">
						                <label class="col-sm-3 control-label" for="tgl">Dari Tanggal</label>
						                <div class="col-sm-3">
						                    <input name="tglmulai" id="tglmulai" class="form-control" data-provide="datepicker"  value="<?=set_value('tglmulai',(isset($tglmulai) ? $tglmulai : ""))?>" />
						                </div>
						                <div class="col-sm-2">
						                <center>s/d</center>
						                </div>
						                <div class="col-sm-3">
						                    <input name="tglakhir" id="tglakhir" class="form-control" data-provide="datepicker" value="<?=set_value('tglakhir',(isset($tglakhir) ? $tglakhir : ""))?>"  />
						                </div>
					                </div>
					       		 </div>
					       		 <div>
					       		 <div class="row" style="margin-top:10px;" >
					       		 	<div class="form-group">
		                                <label class="col-sm-3 control-label" for="loket">Loket
		                                <?php echo $this->session->userdata('username_report'); ?>
		                                </label>
		                                <div class="col-md-8">
		                                    <?php   
		                                        $disabled_loket  = "enabled";
		                                        $selected_loket = '';
		                                        if(isset($loket) && trim($loket) != '') $selected_loket = $loket;
		                                        $js = 'id="cbo_loket" class="form-control select2" '.$disabled_loket;
		                                        echo form_dropdown('cbo_loket', $list_loket, $selected_loket, $js);
		                                    ?>
											<input name="loket_id" id="loket_id" value="<?=set_value('loket_id',(isset($loket_id) ? $loket_id : ""))?>" type="hidden">
										</div>
		                            </div>
					       		 </div>
 					       		 <div class="row" style="margin-top:5px;">
					       		 	<div class="form-group">
		                                <label class="col-sm-3 control-label" for="loket">Nama CS</label>
		                                <div class="col-md-8">
		                                    <?php   
		                                        $disabled_cs  = "enabled";
		                                        $selected_cs = '';
		                                        if(isset($cs) && trim($cs) != '') $selected_cs = $cs;
		                                        $js = 'id="cbo_cs" class="form-control select2" '.$disabled_cs;
		                                        echo form_dropdown('cbo_cs', $list_cs, $selected_cs, $js);
		                                    ?>
											<input name="cs" id="cs" value="<?=set_value('cs',(isset($cs) ? $cs : ""))?>" type="hidden">
										</div>
		                            </div>
					       		 </div>
					       		 </div>
								<div class="row" style="margin-top:5px;">
						            <div class="form-group">
				                		<label class="col-sm-3 control-label" for="tgl"></label>
					                	<div class="col-sm-4">
					       					<button id="generate" class="btn btn-block btn-info" type="button">Tampilkan Filter</button>
					       				</div>
						                <div class="col-sm-4" >
					       					<button id="refresh_all" class="btn btn-block btn-info" type="button">Tampilkan Semua</button>
					       				</div>
			       					</div>
			       				</div>
		       				</div>
						</div><!-- /.box-body-->
					  </div><!-- /.box -->
					</div><!-- /.col -->
				  </div><!-- /.row -->
			</section><!-- /.content -->

			<section class="content">
				<div class="row">
					<div class="col-md-12">
					  <!-- Bar chart -->
					  <div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-bar-chart-o"></i>
						  <h3 class="box-title">Statisik Kepuasan Pelanggan Bapenda Kota Bogor</h3>
						  <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div>
						</div>
						<div class="box-body">
						<div class="col-md-12">
							<div class="row" id="datainfo">
				                <?php 
				                foreach($datajml as $row) :
				                ?>
									<div class="col-lg-4 col-xs-6">
									  <!-- small box -->
									  <div class="<?php echo $row['icon'];?>">
										<div class="inner">
										  <h3><?php echo $row['jumlah'];?></h3>
										  <p><?php echo $row['indikator_name'];?> </p>
										</div>
									  </div>
									</div><!-- ./col -->
				                <?php
				                endforeach;    
				                ?>
				            </div>
			            </div>
				            <div class="row">
					            <div class="col-md-12">
								  	<div id="bar-chart" style="height: 350px;"></div>
								</div>
							</div>
						</div><!-- /.box-body-->
					  </div><!-- /.box -->
					</div><!-- /.col -->



				  </div><!-- /.row -->
			</section><!-- /.content -->




			<section class="content">
				<div class="row">
					<div class="col-md-12">
					  <!-- Bar chart -->
					  <div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-bar-chart-o"></i>
						  <h3 class="box-title">List laporan Kepuasan Pelanggan Bapenda Kota Bogor</h3>
						  <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div>
						</div>
						<div class="box-body">
							<div class="col-md-12">
								<div class="row" >
	                	            <div class="box-body">
							            <div class="box-body">
											<table class="table table-striped table-bordered table-hover" id="dt-listindikator" >
												<thead>
												<tr>                    
													<th>Tanggal</th>
													<th>Loket</th>
													<th>Nama CS</th>
													<th>Nomor Antrian</th>
													<th>Mulai Pelayanan</th>
													<th>Selesai Pelayanan</th>
													<th>Durasi</th>
													<th>Indikator Penilaian</th>
												</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div><!-- /.box-body -->
									</div><!-- /.box-body -->
					            </div>
				            </div>
						</div><!-- /.box-body-->
					  </div><!-- /.box -->
					</div><!-- /.col -->
				  </div><!-- /.row -->
			</section><!-- /.content -->

            
<?php   } ?>

   


