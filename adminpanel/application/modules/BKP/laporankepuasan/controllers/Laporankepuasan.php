<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporankepuasan extends Bss_controller {
        
	public function __construct(){
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_LAP_KEPUASAN";
        $this->checkAuthorization($this->MOD_ALIAS);
        $this->load->library('session');
        $this->load->model("Mlaporankepuasan","mcom"); 
    }

	public function index(){            
        $data['titlehead'] = "Laporan :: Bapenda Kota Bogor";
        //$this->load->model("mcommon","mcom");
		
		// custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.min.js',
            HTTP_ASSET_PATH.'plugins/flot/jquery.flot.resize.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.pie.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.categories.min.js',
			HTTP_ASSET_PATH.'plugins/flot/jquery.flot.axislabels.js',
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',               
            HTTP_MOD_JS.'modules/laporankepuasan/fpp_laporan.js'            
        );

        $loadhead['stylesheet'] = array(
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );
		
		// load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);
		
		//$data['loadhead'] = $loadhead;

		$data['tglmulai'] = date('Y-m-d');
		$data['tglakhir'] = date('Y-m-d');
		$data['auto'] = 1;


        // option list thang
        $data_loket  = $this->mcom->get_loket();
        $list_loket ['0'] = '- Semua Loket -';
        foreach ($data_loket as $row) {
            $list_loket[$row->id] = $row->name;
        }
        $data['list_loket'] = $list_loket;
        $data['loket_id'] = 0;

        // option list thang
        $data_cs  = $this->mcom->get_cs();
        $list_cs ['0'] = '- Semua Customer Service -';
        foreach ($data_cs as $row) {
            $list_cs[$row->id] = $row->name;
        }
        $data['list_cs'] = $list_cs;
        $data['cs'] = 0;


		$this->session->unset_userdata('tglmulai_rep');
		$this->session->unset_userdata('tglakhir_rep');
		$this->session->unset_userdata('loket_id_rep');
		$this->session->unset_userdata('cs_rep');
        $newdata = array(
        	'tglmulai_rep'  => '',
        	'tglakhir_rep' 	=> '',
           	'loket_id_rep'  => '0',
           	'cs_rep'  => '0'
        );
		$this->session->set_userdata($newdata);



		$data['datajml'] =  $this->mcom->getDataJml(null,null,null,null);
        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;    
        $this->template->load('tmpl/vwbacktmpl','vlaporankepuasan',$data);                        
	}
	
	function GetBarchart(){
   
		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");

        $results  =  $this->mcom->getDataGrafik($tglmulai ,$tglakhir ,$loket_id,$cs); 
		$ttl = count($results);
		$build_array = array ("data_grafik"=>array(), "jml"=>$ttl);
        foreach($results as $row) {         
			array_push($build_array["data_grafik"],
				array(
					$row->indikator_name, 
					$row->jumlah
				)
			);
        }       
        echo json_encode($build_array); 
	}

	function GetBarchartAll(){

		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");

		$this->session->unset_userdata('tglmulai_rep');
		$this->session->unset_userdata('tglakhir_rep');
		$this->session->unset_userdata('loket_id_rep');
		$this->session->unset_userdata('cs_rep');
        $newdata = array(
        	'tglmulai_rep'  => '',
        	'tglakhir_rep' 	=> '',
           	'loket_id_rep'  => '0',
           	'cs_rep'  => '0'
        );

        $results  =  $this->mcom->getDataGrafik(null ,null ,null,null); 
		$ttl = count($results);
		$build_array = array ("data_grafik"=>array(), "jml"=>$ttl, "data_sangat_puas"=>array(), "data_puas"=>array(), "data_tidak_puas"=>array());
        foreach($results as $row) {      
        	if($row->indikator_category_id==1){
				array_push($build_array["data_sangat_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	}else if($row->indikator_category_id==2){
				array_push($build_array["data_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	}else if($row->indikator_category_id==3){
				array_push($build_array["data_tidak_puas"],
					array(
						$row->indikator_name, 
						$row->jumlah
					)
				);
        	} 

        }       
        echo json_encode($build_array); 
	}


	public function generateAll(){


		$this->session->unset_userdata('tglmulai_rep');
		$this->session->unset_userdata('tglakhir_rep');
		$this->session->unset_userdata('loket_id_rep');
		$this->session->unset_userdata('cs_rep');
        $newdata = array(
        	'tglmulai_rep'  => '',
        	'tglakhir_rep' 	=> '',
           	'loket_id_rep'  => '0',
           	'cs_rep'  => '0'
        );
		$this->session->set_userdata($newdata);

		$datajml  = $this->mcom->getDataJml(null,null,null,null);
		$hasil="";     
		$cnt = count($datajml);	

		if($cnt > 0 ){
			$total=0;
			foreach($datajml as $rowjml) :
				$total = $total + $rowjml['jumlah'];
			endforeach; 


	        foreach($datajml as $row) :

	        	$persen = (($row['jumlah']/$total) * 100 ) ;
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>".$row['jumlah']."</h3>
					  <h3>".round($persen,2)."%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }else{
	    	$datajmlkosong   = $this->mcom->getDataJmlKosong();
	        foreach($datajmlkosong as $row) :
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>0</h3>
					  <h3>0%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }


        echo $hasil;
	}

	public function generate(){

		$loket_id = $this->input->post("loket_id");
		$cs = $this->input->post("cs");
		$tglmulai = $this->input->post("tglmulai");
		$tglakhir = $this->input->post("tglakhir");


		$this->session->unset_userdata('tglmulai_rep');
		$this->session->unset_userdata('tglakhir_rep');
		$this->session->unset_userdata('loket_id_rep');
		$this->session->unset_userdata('cs_rep');
        $newdata = array(
        	'tglmulai_rep'  => $tglmulai,
        	'tglakhir_rep' 	=> $tglakhir,
           	'loket_id_rep'  => $loket_id,
           	'cs_rep'  => $cs
        );
		$this->session->set_userdata($newdata);



		$datajml   = $this->mcom->getDataJml($tglmulai ,$tglakhir ,$loket_id,$cs);
		$hasil="";     
		$cnt = count($datajml);	

		if($cnt > 0 ){
			$total=0;
			foreach($datajml as $rowjml) :
				$total = $total + $rowjml['jumlah'];
			endforeach; 


	        foreach($datajml as $row) :

	        	$persen = (($row['jumlah']/$total) * 100 ) ;
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>".$row['jumlah']."</h3>
					  <h3>".round($persen,2)."%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }else{
	    	$datajmlkosong   = $this->mcom->getDataJmlKosong();
	        foreach($datajmlkosong as $row) :
	        	$hasil .="
				<div class='col-lg-4 col-xs-6'>
				  <div class='". $row['icon']."'>
					<div class='inner'>
					  <h3>0</h3>
					  <h3>0%</h3>
					  <p style='font-size:17pt;'>". $row['indikator_name']."</p>
					</div>
					<div class='icon'>
					  <i class='fa fa-bar-chart'></i>
					</div>
				  </div>
				</div>";
	        endforeach; 
	    }


        echo $hasil;
	}

    public function datediff($tgl1,$tgl2){

        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        /*$data = array( "Tahun" => date("Y", $diff) - $base_year, 
                      "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, 
                      "Bulan" => date("n", $diff) - 1, 
                      "days_total" => floor($diff_secs / (3600 * 24)), 
                      "Hari" => date("j", $diff) - 1, 
                      "hours_total" => floor($diff_secs / 3600), 
                      "Jam" => date("G", $diff), 
                      "minutes_total" => floor($diff_secs / 60), 
                      "Menit" => (int) date("i", $diff), 
                      "seconds_total" => $diff_secs, 
                      "Detik" => (int) date("s", $diff) 
                    );*/

        $hasil = floor($diff_secs / 60)." Menit ".(int) date("s", $diff)." Detik";
        return  $hasil;
    }


    public function list_penilaian(){
        $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
        $role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
        
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
        
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mcom->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res;
        $totalfiltered =  $this->mcom->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
        $btn_edit = "";
        $btn_group ="";
        $stat_visiting="";
        $btn_delete="";
        $no=1;
        foreach($results as $row) {         
            array_push($build_array["data"],
                array(    
                    $row->created_date,
                    $row->instansi_name,
                    $row->full_name,
                    $row->no_antrian,
                    $row->start_time,
                    $row->end_time,
                    $this->datediff($row->start_time,$row->end_time),
                    $row->indikator_category_name
                )
            );   
            $no++;          
        }      
                    
        echo json_encode($build_array);   
    }

        
}
