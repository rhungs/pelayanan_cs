<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i>Perencanaan 360</a></li>    
	<li><i class="fa fa-fw fa-files-o"></i>Thn.Anggaran & Semester</li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
                 <!-- form start -->
				<?php
				$attributes = array('class' => 'form-horizontal', 'id' => 'form_input');
				echo form_open_multipart(base_url() . 'thang360/Cthang360/form_thang360/'.(isset($thang) ? $thang :''), $attributes); 
				?>
			    <div class="box-body">
					<center>
						<div id="divQLoading">
							<div class="loading-screen">
								<span class="fa fa-refresh fa-spin"></span>
							</div>
						</div><!-- /#divQLoading -->
					</center>
					<?php if (isset($err_msg) && $err_msg != "") { ?><div class="alert alert-danger"><?=$err_msg?> </div>  <?php } ?>
				<div class="panel box box-solid">
					<div class="box-header with-border">
						<h4 class="box-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#identitas_unit"><small><strong>Edit/Tambah</strong></small> </a>
						</h4>
					</div>
					<div class="row">
                        <div class="col-md-11">
                            <input name="thang" type="hidden" value="<?php echo set_value('thang', (isset($thang) ? $thang :'')) ; ?>">
                            </br>
							<div class="form-group">
                                <label class="col-sm-2 control-label" for="kategori_anak_id">Tahun Anggaran *</label>
                                <div class="col-md-8">
                                    <?php   
                                        $disabled_thang  = "enabled";
                                        $selected_thang = '';
                                        if(isset($thang) && trim($thang) != '') $selected_thang = $thang;
                                        $js = 'id="cbo_thang" class="form-control select2" '.$disabled_thang;
                                        echo form_dropdown('cbo_thang', $list_thang, $selected_thang, $js);
                                    ?>
									<input name="thang" id="thang" value="<?=set_value('thang',(isset($thang) ? $thang : ""))?>" type="hidden">
								</div>
                            </div>
							<div class="form-group">
                                <label class="col-sm-2 control-label" for="i_jenis_kelamin">Semester*</label>
                                <div class="col-md-8">
                                    <?php   
										$disabled_semester  = "enabled";
										$selected_semester = '';
										if(isset($semester) && trim($semester) != '') $selected_semester = $semester;
										$js = 'id="cbo_semester" class="form-control select2" '.$disabled_semester;
										echo form_dropdown('cbo_semester', $list_semester, $selected_semester, $js);
									?>
									<input name="semester" id="semester" value="<?=set_value('semester',(isset($semester) ? $semester : ""))?>" type="hidden">
								</div>
                            </div>	
						</div><!-- /.col-md-11 -->
					</div><!-- /.col-md-11 -->    
				</div><!-- /.panel -->
				<div class="box-footer">
					<button type="submit" id="simpan" class="btn btn-info" onclick="return confirm('Simpan data permohonan ?')">Simpan</button>
					<a onclick="return confirm('Batalkan pengisian data tahun anggaran ?')" href="<?php echo base_url()."thang360/Cthang360"?>" class="btn btn-default" type="submit">Batal</a>                
				</div><!-- /.box-footer --> 
			</form>   
			<?php echo form_close();?>
			</div><!-- /.box-info -->
        </div><!-- /col-xs-12 -->
    </div>
</section>
