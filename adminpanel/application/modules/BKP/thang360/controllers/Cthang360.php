<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Cthang360 extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_THNSMSTER";
        $this->checkAuthorization($this->MOD_ALIAS);            
        $this->load->model("Mthang360","mfpp");
    }
    
    public function index(){
		$data['titlehead'] = "Tahun Anggaran - Semester :: DISPENDA KOTA BOGOR";
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css',
			HTTP_ASSET_PATH.'css/style.css'
        );
        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
			HTTP_ASSET_PATH.'plugins/jQuery/jquery.form.min.js',
            HTTP_MOD_JS.'modules/thang360/thang360_list_js.js'            
        );
		// load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);
		$data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
		$this->template->load('tmpl/vwbacktmpl','v_thang360_list',$data);     
    }
    
    public function list_thang360(){
	    $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
		$role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
		
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
		
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mfpp->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res; //$this->mref->sort_parentchild($res);
        $totalfiltered =  $this->mfpp->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
		$btn_edit = "";
		$btn_group ="";
		$stat_visiting="";
		$btn_delete="";
		$no=1;
        foreach($results as $row) {         
			$btn_edit = anchor("thang360/Cthang360/form_thang360/".$this->qsecure->encrypt($row->thang)."/".$this->qsecure->encrypt($row->semester), "<span class='fa fa-edit'></span>", array('class'=>'btn btn-info btn-xs','title'=>'Edit')) ;
			
			if($row->active==true){
				$btn_delete = " <a class='fa fa-check text-green' href='#' onClick='if(confirm(\"Non Aktif tahun anggaran ini ? \")){ document.location=\" ".base_url()."thang360/Cthang360/delete_thang360/".$this->qsecure->encrypt($row->thang)."/".$this->qsecure->encrypt($row->semester)." \";}' title='Delete' ></a>";
			}else{
				$btn_delete = " <a class='fa fa-times text-red' href='#' onClick='if(confirm(\"Aktif tahun anggaran ini ? \")){ document.location=\" ".base_url()."thang360/Cthang360/active_thang360/".$this->qsecure->encrypt($row->thang)."/".$this->qsecure->encrypt($row->semester)." \";}' title='Delete' ></a>";
			}
			$hiden_text=" <input id='thang' type='hidden' value='". $this->qsecure->encrypt($row->thang) ."' />";
			$btn_group  = $btn_edit.$hiden_text;
			array_push($build_array["data"],
                array(   
					$btn_group,		
                    $row->thang,
					$row->semester,
					$btn_delete,
                )
            );   
			$no++;			
        }      
                    
        echo json_encode($build_array);   
    }
    
    public function form_thang360(){
        $id = $this->uri->segment(4); 
		$semester= $this->uri->segment(5); 
        if ($id == "0") { $id = ""; }
        
        //--== parameetr redirect reservasi halaman home
 
        $data['unit_id'] = $id;
        if ( is_null($id) || trim($id) == '' ) {
            $data['titlehead'] = 'Form Thn Anggaran Semester :: DISPENDA KOTA BOGOR';
            $data['fpp_status_id'] = -1;
        } else {
            $data['titlehead'] = 'Edit Thn Anggaran Semester :: DISPENDA KOTA BOGOR';
            $id = $this->qsecure->decrypt($id);  
			$semester = $this->qsecure->decrypt($semester);  
        }
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'           
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',    			
            HTTP_MOD_JS.'modules/thang360/thang360_form_js.js',            
        );
             
        //$loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
        
        // config rules validation
        $config = array (	
            array(
                'field'=>'thang',
                'rules'=>'trim|required'
            )
        );
		
		$config = array (	
            array(
                'field'=>'semester',
                'rules'=>'trim|required'
            )
        );
		
	                    
       if( ! is_null($id) && (trim($id) != '')) {
            $result = $this->mfpp->get_view_edit($id,$semester);
            if (count($result) > 0 ) {
                $data['thang'] = $result->thang;
				$data['semester'] = $result->semester;
            }        
       }else{
            $data['thang'] = "";
		}
   
       if ($_POST) { 
            $this->form_validation->set_rules($config); 
            $this->form_validation->set_message('required', '%s harus diisi !');
            if($this->form_validation->run() === FALSE) {
                $data['err_msg'] = validation_errors();
            }else{
                $res = $this->mfpp->thang360_save($id);
                if ($res["success"] == true)
                    $this->session->set_flashdata('info', $res['msg']);
                else
                    $this->session->set_flashdata('err', $res['msg']);
                redirect('thang360/Cthang360');                                
            }
        }
		
		// option list thang
        $data_thang  = $this->mfpp->get_thang();
        $list_thang [''] = '- Pilih Tahun Anggaran -';
        foreach ($data_thang as $row) {
            $list_thang[$row->id] = $row->name;
        }
        $data['list_thang'] = $list_thang;
		
	    // option select semester
        $list_semester[''] = '- Pilih Semester -';
        $list_semester[1] = 1;
		$list_semester[2] = 2;
        $data['list_semester'] = $list_semester;
		
	    $this->template->load('tmpl/vwbacktmpl','v_thang360_form',$data);     
    }    
    public function delete_thang360($id,$semester){
        $id = $this->qsecure->decrypt($id);
		$semester = $this->qsecure->decrypt($semester);		
        $res = $this->mfpp->delete_thang360($id,$semester);
        if ($res["success"] == true)
            $this->session->set_flashdata('info', $res['msg']);
        else
            $this->session->set_flashdata('err', $res['msg']);
			redirect('thang360/Cthang360');
    } 
	
	public function active_thang360($id,$semester){
        $id = $this->qsecure->decrypt($id);
		$semester = $this->qsecure->decrypt($semester);	
		$res_decative = $this->mfpp->deactive_thang360($id,$semester);
        $res = $this->mfpp->active_thang360($id,$semester);
        if ($res["success"]  == true && $res_decative["success"] == true)
            $this->session->set_flashdata('info', $res['msg']);
        else
            $this->session->set_flashdata('err', $res['msg']);
			redirect('thang360/Cthang360');
    } 
    
}
