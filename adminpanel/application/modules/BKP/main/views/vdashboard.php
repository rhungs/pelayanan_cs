<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
  </ol>
</section>
    <?php
    if ($this->session->userdata(sess_prefix() . "rolealias") == "superadmin" OR
              $this->session->userdata(sess_prefix() . "rolealias") == "admin") 
          { ?>

			<section class="content">
				<div class="row">
					<div class="col-md-12">
					  <!-- Bar chart -->
					  <div class="box box-primary">
						<div class="box-header with-border">
						  <i class="fa fa-bar-chart-o"></i>
						  <h3 class="box-title">Statisik Kepuasan Pelayanan Pelanggan Bapenda Kota Bogor</h3>
						  <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div>
						</div>
						<div class="box-body">
						    <div class="row">
								<div class="col-md-12">
									<div class="row" id="datainfo">
						                <?php 
						                foreach($datajml as $row) :
						                ?>
											<div class="col-lg-4 col-xs-6">
											  <!-- small box -->
											  <div class="<?php echo $row['icon'];?>">
												<div class="inner">
												  <h3><?php echo $row['jumlah'];?></h3>
												  <p><?php echo $row['indikator_name'];?> </p>
												</div>
												<div class="icon">
												  <i class="fa fa-bar-chart"></i>
												</div>
											  </div>
											</div><!-- ./col -->
						                <?php
						                endforeach;    
						                ?>
						            </div>
					            </div>
						    </div>
						    <div class="row">
						   		<div class="col-md-12">
						  			<div id="bar-chart" style="height: 350px;"></div>
						  		</div>
						  	</div>
						</div><!-- /.box-body-->
					  </div><!-- /.box -->
					</div><!-- /.col -->
				  </div><!-- /.row -->


			</section><!-- /.content -->

            
<?php   } ?>

   


