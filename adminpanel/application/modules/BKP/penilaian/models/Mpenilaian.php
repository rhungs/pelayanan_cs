<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mpenilaian extends Bss_model
{
	protected $_table = "ref_penilaian";
    protected $_id = "thang";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null)
    {
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");

        $this->db->select("a.created_date, b.indikator_category_name, c.full_name, a.no_antrian");
        $this->db->from("indikator_penilaian a");
        $this->db->join("ref_indikator_cat b","a.indikator_category_id=b.indikator_category_id");
        $this->db->join("res_sec_user c","a.user_id=c.id");
        $this->db->where("a.user_id",$user_id);
        $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");

        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( b.indikator_category_name LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
                $this->db->order_by("a.created_date","DESC");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("a.indikator_category_id", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }

    function get_list_cnt($by=null, $search_string=null)
    {
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("count(a.indikator_category_id) _cnt");
        $this->db->from("indikator_penilaian a");
        $this->db->join("ref_indikator_cat b","a.indikator_category_id=b.indikator_category_id");
        $this->db->join("res_sec_user c","a.user_id=c.id");
        $this->db->where("a.user_id",$user_id);
         $this->db->where("DATE_FORMAT(a.created_date, '%Y-%m-%d') = DATE_FORMAT(NOW(), '%Y-%m-%d')");

        if (! is_null($search_string) && $search_string != ""){
             $this->db->where("( b.indikator_category_name LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }

    function insert_penilaian($data){
        $this->db->trans_begin();                 
        $this->db->insert("indikator_penilaian", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }


    function UpdateFlag($id,$data){
        $this->db->trans_begin();
        $this->db->where("id",$id);
        $this->db->update("res_sec_user", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

    function CekStatusFlag($id){
        $query = $this->db->query("SELECT flag_penilaian FROM res_sec_user WHERE id=?",array($id));
        return $query->first_row()->flag_penilaian;
    }

    function CekLastNomorAntrian($id){
        $query = $this->db->query("SELECT no_antrian FROM res_sec_user WHERE id=?",array($id));
        return $query->first_row()->no_antrian;
    }


}
// 