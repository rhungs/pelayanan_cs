<body>
<div class="content">
	<div class="col-md-12">
		<div class="row">
			<div class="pull-right">
			   <?php
			        echo anchor( 'auth/logout', '<span class="fa fa-sign-out"></span>&nbsp;Log Out', 'class="btn btn-default btn-flat" title="Log Out"' );
			    ?>
			</div>
			<div class="pull-right" style="padding-right:20px;">
				 <?php
			        echo anchor( 'penilaian/Cpenilaian/penilaian', '<span class="fa fa-sign-out"></span>&nbsp;Form Pengisian Kepuasan Pelanggan >>', 'class="btn btn-default btn-flat" title="Form Pengisian Kepuasan Pelanggan >>"' );
			    ?>
			</div>
			<div class="pull-left">
				<h1><b>Selamat Datang, <?=$this->session->userdata(sess_prefix() . "full_name")?></b></h1>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<br/><br/><br/><br/>
	       	<div class="row">
				<input  type="hidden" name="flag_penilaian" id="flag_penilaian" class="form-control" value="<?=(isset($flag_penilaian) ? $flag_penilaian : "")?>">
	       		<table class="table no-border text-center">
	       			<tr>
			       		<td>
			       			<button id="mulai" class="btn btn-block btn-success" type="button"> 
			       				<div><h2>MULAI PELAYANAN</h2></div>
			       			</button>
			       		</td>
			       		<td>
			       			<button id="selesai" class="btn btn-block btn-info" type="button">
			       				<div><h2>SELESAI PELAYANAN</h2></div>
			       			</button>
			       		</td>
		       		</tr>
		       		<tr>
		       			<td>
		       				<input  type="text" name="no_antrian" id="no_antrian" placeholder="MASUKAN NOMOR ANTRIAN" style="height: 70px;font-size: 20pt" class="form-control" value="<?=(isset($no_antrian) ? $no_antrian : "")?>">
		       			</td>
		       			<td>
		       				<button id="refresh" class="btn btn-block btn-default" type="button">
			       				<div><h2>REFRESH</h2></div>
			       			</button>
			       		</td>
		       		</tr>
	       		</table>
	       	</div>
       	</div>


		<div class="col-md-12">
	        <div class="box">
	            <div class="box-header">
	                 Data Kepuasan Pelanggan Customer Hari Ini  	                       
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
					<table class="table table-striped table-bordered table-hover" id="dt-listindikator" >
						<thead>
						<tr>                    
							<th>Tanggal</th>
							<th>Nomor Antrian</th>
							<th>Indikator Penilaian</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div><!-- /.box-body -->
	        </div><!-- /.box -->
        </div>

        <div class="col-md-12">
	       	<div class="footer">
				<div><img src="<?php echo base_url().'assets/images/logo.png' ?>"></img></div>
			</div>
        </div>


	</div>
</div>


<div class="modal fade  modal-info" id="msginfo" tabindex="-1" role="dialog" aria-labelledby="ct_masalah" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	  	<h3 class="modal-title">Info</h3>
	  </div>
	  <div class="modal-body">
	  	<div class="row">
			<span>
				<center>
					<h3>
					Status berhasil diubah ...
					</h3>
				</center>
			</span>
		</div>
	  </div>
	</div>
  </div>
</div>

</body>


