<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i>log</a></li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>

    <section class="content">
         <?php if($this->session->flashdata('info')) { ?>
            <div class="alert alert-info">
                <a class="close" data-dismiss="alert">x</a>
                <strong>Info! </strong><?php echo $this->session->flashdata('info'); ?>
            </div>
            <?php } ?>
            <?php if($this->session->flashdata('err')) { ?>
                <div class="alert alert-error">
                    <a class="close" data-dismiss="alert">x</a>
                    <strong>Warning! </strong><?php echo $this->session->flashdata('err'); ?>
                </div>
        <?php } ?>
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
				<div class="box-body table-responsive">
					<table class="table table-striped table-bordered table-hover" id="dt-listlog" >
						<thead>
						<tr>                    
							<th>Tanggal</th>
							<th>Nama</th>
                            <th>Loket</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div><!-- /.box-body -->
        </div><!-- /.box -->
    </section><!-- /.content -->
	

