<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mlog extends Bss_model
{
	protected $_table = "bnp_log";
    protected $_id = "id";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("a.tanggal,b.full_name,c.instansi_name");
        $this->db->from("log_akses a");
        $this->db->join("res_sec_user b","a.userid=b.id");
        $this->db->join("res_ref_instansi c","a.instansi_id=c.instansi_id");
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( b.full_name LIKE '%$search_string%' or c.instansi_name LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
                $this->db->order_by("tanggal","DESC");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("tanggal", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }
    function get_list_cnt($by=null, $search_string=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("count(a.tanggal) _cnt");
        $this->db->from("log_akses a");
        $this->db->join("res_sec_user b","a.userid=b.id");
        $this->db->join("res_ref_instansi c","a.instansi_id=c.instansi_id");
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( b.full_name LIKE '%$search_string%' or c.instansi_name LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
}
