<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Clog extends Bss_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_LOG";
        $this->checkAuthorization($this->MOD_ALIAS);            
        $this->load->model("Mlog","mfpp");
    }
    
    public function index(){
		$data['titlehead'] = "log :: BAPENDA KOTA BOGOR";
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css',
			HTTP_ASSET_PATH.'css/style.css'
        );
        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
			HTTP_ASSET_PATH.'plugins/jQuery/jquery.form.min.js',
            HTTP_MOD_JS.'modules/log/log_list_js.js'            
        );
		// load tag js 
        //$tagjs1 = "";
        //$loadfoot['tagjs'] = array($tagjs1);
		$data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
		$this->template->load('tmpl/vwbacktmpl','v_log_list',$data);     
    }
    
    public function list_log(){
	    $draw = intval( $this->input->post('draw') );
        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        $search = $this->input->post('search');
        $columns = $this->input->post('columns');
		$role_id = $this->session->userdata(sess_prefix()."roleid");
        $search_string = null;
		
        if (!empty($search['value'])){
            $search_string = trim($search['value']);            
        }
		
        $totaldata = 0;
        $totalfiltered = 0;             
        $res  = $this->mfpp->get_list(null,$start,$limit,$search_string);
        $totaldata  = count($res);
        $results  = $res; //$this->mref->sort_parentchild($res);
        $totalfiltered =  $this->mfpp->get_list_cnt(null,$search_string);
        
        $build_array = array (
            "draw" => $draw,
            "recordsTotal" =>  $totaldata,
            "recordsFiltered" =>  $totalfiltered,
            "data"=>array()
        );
		$no=1;
        foreach($results as $row) {     
			array_push($build_array["data"],
                array(   
                    $row->tanggal,
                    $row->full_name,
                    $row->instansi_name
                )
            );   
			$no++;			
        }      
                    
        echo json_encode($build_array);   
    }
    

}
