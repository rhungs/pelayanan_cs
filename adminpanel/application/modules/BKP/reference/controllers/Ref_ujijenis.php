<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_ujijenis extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_JENISUJI";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_ujijenis","mref");
        $this->load->model('mref_ujikategori', 'mkat');
        $this->load->model('mref_ujiklasifikasi', 'mklas');
    }
    
    public function index($msg=''){
        $this->list_ujijenis($msg);
    }
    
    public function list_ujijenis($msg){
        $data['titlehead'] = "Daftar Jenis Pengujian :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'             
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.rowGrouping.js',
            HTTP_MOD_JS.'modules/reference/ref_ujijenis.js',
            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                        $('#dataTables-listujijenis')
                            .dataTable({ 
                                'bLengthChange': false, 'bPaginate': false,
                                 searching: true
                            })
                            .rowGrouping({ 
                                bExpandableGrouping: true, 
                                iGroupingColumnIndex: 4,
                                sGroupingColumnSortDirection: 'asc'
                                //iGroupingOrderByColumnIndex: 0
                            });                       
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_ujijenis_list();
        $data['res_cnt'] = count($data['result']);
        $data['list'] = array(
            'klas_uji' => $this->get_klasifikasi_uji(),
            'kat_uji' => $this->get_kategori_uji()
        );
        if( !empty($msg) ){
            $data['message'] = $this->get_message($msg);
        }
        $this->template->load('tmpl/vwbacktmpl','vref_ujijenis',$data);     
    }
    
    public function get_kategori_uji(){
        $data = $this->mkat->get_ujikat_list();
        foreach($data as $item){
            $return[$item->ujikat_id] = $item->ujikat_name;
        }
        return $return;
    }
    
    public function get_klasifikasi_uji(){
        $data = $this->mklas->get_ujiklas_list();
        foreach($data as $item){
            $return[$item->ujiklas_id] = $item->ujiklas_name;
        }
        return $return;
        
    }
    
    public function insert_ujijenis(){
        $input = $this->input->post();
        $data = array(
            'jenisuji_code' => $input['kode_jenisuji'],
            'jenisuji_name' => $input['nama_jenisuji'],
            'jenisuji_shortname' => $input['singkatan'],
            'jenisuji_standard' => $input['standar'],
            'jenisuji_lama' => $input['lama_uji'],
            'jenisuji_harga' => $input['harga_uji'],
            'ujikat_id' => $input['kat_uji'],
            'ujiklas_id' => $input['klas_uji']
                
        );
        
        $exe = $this->mref->insert_record( $this->mref->_table, $data );
        
        if($exe){
            redirect('reference/ref_ujijenis/index/inserted');
        }else{
            redirect('reference/ref_ujijenis/index/ins_failed');
        }
    }
    
    function delete($id){
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->delete_record( $this->mref->_table, $this->mref->_id, $ref_id );
        
        if($exe){
            redirect('reference/ref_ujijenis/index/deleted');
        }else{
            redirect('reference/ref_ujijenis/index/del_failed');
        }
    }
    
    function edit($id){
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->get_record( $this->mref->_table, $this->mref->_id, $ref_id);
        foreach($exe as $item){
            $item->jenisuji_id = $this->qsecure->encrypt($item->jenisuji_id);
        }
        echo json_encode($exe);
    }
    
    function update_jenisuji($id){
        $input = $this->input->post();
        $insert = array(
            'jenisuji_code' => $input['kode_jenisuji'],
            'jenisuji_name' => $input['nama_jenisuji'],
            'jenisuji_shortname' => $input['singkatan'],
            'jenisuji_standard' => $input['standar'],
            'jenisuji_lama' => $input['lama_uji'],
            'jenisuji_harga' => $input['harga_uji'],
            'ujikat_id' => $input['kat_uji'],
            'ujiklas_id' => $input['klas_uji']
                
        );
        $ref_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->update_record( $this->mref->_table, $insert, $this->mref->_id, $ref_id);
        if($exe){
            redirect('reference/ref_ujijenis/index/updated');
        }else{
            redirect('reference/ref_ujijenis/index/upd_failed');
        }
    }
    
    function get_message($message){
        
        switch($message){
            
            case 'deleted':
                $title = 'Hapus Berhasil';
                $message = 'Penghapusan Daftar Jenis Uji berhasil dilakukan.';
                break;
            
            case 'inserted':
                $title = 'Tambah Berhasil';
                $message = 'Penambahan Daftar Jenis Uji berhasil dilakukan.';
                break;
            
            case 'updated':
                $title = 'Update Berhasil';
                $message = 'Pembaharuan Daftar Jenis Uji berhasil dilakukan.';
                break;
        }
        $icon = 'info';
        return message_box($title, $message, 'success', 'check', true );
    }
    
}
