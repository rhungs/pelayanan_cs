<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_ujiklasifikasi extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_KLASUJI";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_ujiklasifikasi","mref");
        //$this->load->model('mref_base', 'mbase');
    }
    
    public function index($msg=''){
        $this->list_ujiklas($msg);
    }
    
    public function list_ujiklas($msg){
        $data['titlehead'] = "Daftar Klasifikasi Uji :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'              
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',            
            HTTP_MOD_JS.'modules/reference/ref_ujiklasifikasi.js'            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                        $('#dataTables-listujiklas').DataTable({
                            responsive: true,                                
                            searching: true,
                            paging : false
                        });
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_ujiklas_list();
        $data['res_cnt'] = count($data['result']);
        if( !empty($msg) ){
            $data['message'] = $this->get_message($msg);
        }
        $this->template->load('tmpl/vwbacktmpl','vref_ujiklas',$data);     
    }
    
    function add_ujiklasifikasi(){
        $datainput = $this->input->post();
        $input = $this->mref->insert_uji($datainput);
        if($input){
            redirect('reference/ref_ujiklasifikasi/index/inserted');
        }else{
            redirect('reference/ref_ujiklasifikasi/index/ins_failed');
        }
    }
    
    function delete($id){
        $klas_id = $this->qsecure->decrypt($id);
        $exe = $this->mref->delete_record( $this->mref->_table, $this->mref->_id, $klas_id );
        if($exe){
            redirect('reference/ref_ujiklasifikasi/index/deleted');
        }else{
            redirect('reference/ref_ujiklasifikasi/index/del_failed');
        }
    }
    
    function edit($id){
        $ref_id = $this->qsecure->decrypt($id);
        $ref = $this->mref->get_record( $this->mref->_table, $this->mref->_id, $ref_id );
        foreach($ref as $item){
            $item->ujiklas_id = $this->qsecure->encrypt($item->ujiklas_id);
        }
        echo json_encode($ref);
    }
    
    function update($id){
        $ref_id = $this->qsecure->decrypt($id);
        $input = $this->input->post();
        $data = array(
            'ujiklas_name' => $input['klasifikasi_uji_edit']
        );
        $update = $this->mref->update_record( $this->mref->_table, $data, $this->mref->_id, $ref_id );
        if($update){
            redirect('reference/ref_ujiklasifikasi/index/updated');
        }else{
            redirect('reference/ref_ujiklasifikasi/index/upd_failed');
        }
    }
    
    function get_message($message){
        
        switch($message){
            
            case 'deleted':
                $title = 'Hapus Berhasil';
                $message = 'Penghapusan Daftar Klasifikasi Uji berhasil dilakukan.';
                break;
            
            case 'inserted':
                $title = 'Tambah Berhasil';
                $message = 'Penambahan Daftar Klasifikasi Uji berhasil dilakukan.';
                break;
            
            case 'updated':
                $title = 'Update Berhasil';
                $message = 'Pembaharuan Daftar Klasifikasi Uji berhasil dilakukan.';
                break;
        }
        $icon = 'info';
        return message_box($title, $message, 'success', 'check', true );
    }
    
}
