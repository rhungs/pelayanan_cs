<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends Bss_controller {
    
    function __construct() {
        parent::__construct();        
        $this->MOD_ALIAS = "MOD_REF_COMMON";        
        $this->load->model("mcommon","mcom");
    }
    
    public function get_instansi_ref()
    {            
        //$ctype = $this->uri->segment(4,true);  
        $this->load->model("mref_instansi","mref");
        $data = $this->mcom->get_instansi_list();
        $build_array = array ("data"=>array());
        if (count($data) > 0) {            
            $i=1;
            foreach($data as $row) {
               $link = $row->instansi_id . ";".
                        $row->instansi_name  . ";".
                        $row->instansi_address  . ";".
                        $row->instansi_phone . ";".
                        $row->instansi_fax . ";".
                        $row->instansi_email . ";".
                        $row->postal_code . ";".
                        $row->instansi_city . ";".
                        $row->instansi_province . ";".
                        $row->instansi_country;

               $param = "sl_ins_".$i;

               $instansi_name = "<a id='".$param."' href='javascript:GetSelectedInstansi(\"".$param."\");' data-select='".$link."'>".$row->instansi_name."</a>";

                array_push($build_array["data"],
                    array(
                        $instansi_name,
                        $row->instansi_address, 
                        $row->instansi_city,
                        $row->instansi_phone                      
                    )
                );
                $i++;
            }
        }  else{

        }       
        echo json_encode($build_array);        
    }
    
     public function ustat_notif(){                  
        $notif_id = $this->input->post("notif_id");            
        $data['is_read'] = "1";
        $res = $this->mcom->update_notif($notif_id, $data);
        echo json_encode($res);
    }
    
      
    
}
