<div class="content-wrapper">
    <section class="content">
        <?php
        if(isset($message)){
            echo $message;
        }
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary" id="add_kategori_box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Jenis Uji Kategori</h3>
                    </div><!-- /.bos-header -->
                    <div class="box-body">
                    <?php echo form_open('reference/ref_ujikategori/add_kategori', array( 'id'=>'addForm' )); ?>
                        <div class="form-group">
                        <?php
                            echo form_label('Nama Kategori Uji', 'cat_name', array( 'class'=>'control-label' ) );
                            echo form_input( array( 'name'=>'cat_name', 'id'=>'cat_name', 'class'=>'form-control' ) );
                        ?>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                        <?php
                            echo form_label('Deskripsi Kategori', 'cat_desc', array( 'class'=>'control-label' ) );
                            echo form_textarea( array( 'class'=>'form-control','id'=>'cat_desc','name'=>'cat_desc') );
                        ?>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                        <?php
                            echo form_submit( array('class'=>'btn btn-primary pull-right'), 'Tambah Kategori' );
                        ?>
                        </div><!-- /.form-group -->
                    <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                
                <div class="box box-primary" id="edit_kategori_box" style="display:none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Jenis Uji Kategori</h3>
                    </div><!-- /.bos-header -->
                    <div class="box-body">
                    <?php echo form_open('reference/ref_ujikategori/update', array( 'id'=>'editForm' )); ?>
                        <div class="form-group">
                        <?php
                            echo form_label('Nama Kategori Uji', 'cat_name_edit', array( 'class'=>'control-label' ) );
                            echo form_input( array( 'name'=>'cat_name_edit', 'id'=>'cat_name_edit', 'class'=>'form-control' ) );
                        ?>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                        <?php
                            echo form_label('Deskripsi Kategori', 'cat_desc_edit', array( 'class'=>'control-label' ) );
                            echo form_textarea( array( 'class'=>'form-control','id'=>'cat_desc_edit','name'=>'cat_desc_edit') );
                        ?>
                        </div><!-- /.form-group -->
                        <div class="form-group" style="text-align: right">
                        <?php
                            echo form_reset( array('id'=>'reset', 'class'=>'btn btn-warning'), 'Cancel' );
                            echo nbs();
                            echo form_submit( array('class'=>'btn btn-primary'), 'Update Kategori' );
                        ?>
                        </div><!-- /.form-group -->
                    <?php echo form_close(); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">DAFTAR KATEGORI UJI</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">              
                      <table class="table table-striped table-bordered table-hover" id="dataTables-listujiklas" data-page-length='10'>
                        <thead>
                        <tr>
                            <th class="align-center">No.</th>
                            <th>Kategori Uji</th>
                            <th class="align-center"># Aksi</th>
                        </tr>
                        </thead>
                        </tbody>
                        <?php
                        $seq = 1;
                        foreach($result as $row) : ?>

                            <tr>
                                <th class="align-center"><?php echo $seq; ?></th>
                                <td><?php echo $row->ujikat_name; ?></td>
                                <td class="align-center">
                                <?php
                                    $anc_edit = array(
                                        'uri' => 'reference/ref_ujikategori/edit/'.$this->qsecure->encrypt($row->ujikat_id),
                                        'title' => '<i class="fa fa-fw fa-edit"></i> Edit',
                                        'attr' => array(
                                            'class' => 'btn btn-info btn-xs edit_ref'
                                        )
                                    );
                                    
                                    $anc_delete = array(
                                        'uri' => 'reference/ref_ujikategori/delete/'.$this->qsecure->encrypt($row->ujikat_id),
                                        'title' => '<i class="fa fa-fw fa-trash"></i> Delete',
                                        'attr' => array(
                                            'class' => 'btn btn-danger btn-xs delete_ref'
                                        )
                                    );
                                    echo anchor( $anc_edit['uri'], $anc_edit['title'], $anc_edit['attr'] );
                                    echo nbs();
                                    echo anchor( $anc_delete['uri'], $anc_delete['title'], $anc_delete['attr'] );
                                ?>
                                </td>
                            </tr>

                        <?php    
                            $seq++;
                        endforeach;

                        ?>
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                    <div class="overlay" style="display:none"></div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

