<div id="ModalInstansi" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    Form Tambah Daftar Loket
                </h4>
            </div><!-- /.modal-header -->
            
            <div class="modal-body">
                <?php echo form_open( 'reference/ref_instansi', array( 'id'=>'addForm', 'class'=>'form-horizontal' ) );?>
                    <?php
                        $label = array(
                            'class' => 'control-label col-md-3'
                        );
                        $input = array(
                            'class' => 'form-control'
                        );
                    ?>
                    <div class="form-group" >
                        <?php echo form_label('Nama Loket', 'nama_instansi', $label); ?>
                        <div class="col-md-8">
                            <?php echo form_input( array( 'name'=> 'nama_instansi', 'id'=>'nama_instansi' ), set_value('nama_instansi'), $input); ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-3">
                        <?php
                            echo form_reset( array('id'=>'reset'), 'Reset', array( 'class'=>'btn btn-danger' ) );
                            echo form_submit( array('id'=>'submit'), 'Submit', array( 'class'=>'btn btn-primary' ) );
                        ?>
                        </div><!-- /.col -->
                    </div><!-- /.form-group -->
                <?php echo form_close(); ?>
            </div><!-- /.modal-body -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<section class="content-header">
  <h1>    
    <?=strtoupper($titlehead)?>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url() ?>main/dashboard" ><i class="fa fa-dashboard"></i> Dashboard</a></li>    
	<li><i class="fa fa-table"></i> Referensi</li>    
    <li class="active"><?=($titlehead)?></li> 
  </ol>
</section>
    <section class="content">
        <?php
        if(isset($message)){
            echo $message;
        }
        ?>
        <div class="box">
            <div class="box-header with-border">
                <button id="addInstansi" class="pull-right btn btn-primary btn-sm">
                    <span class="fa fa-plus"></span>
                    Tambah Instansi
                </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
              <table class="table table-bordered table-hover table-condensed" id="dataTables-listinstansi" data-page-length='10'>
                <thead>
                <tr>  
                    <th class="align-center" width="10%">No</th>
                    <th>Nama Loket</th>
                    <th class="align-center" width="20%"># Aksi</th>                                                   
                </tr>
                </thead>
                </tbody>
                <?php
                $seq = 1;
                foreach($result as $row) : ?>
                
                    <tr>       
                        <th class="align-center"><?php echo $seq ?></th>
                        <td><?php echo $row->instansi_name; ?></td>
                        <td align="center">
                        <?php
                            $id = $this->qsecure->encrypt($row->instansi_id);
                            $att = array(
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            );
                            $att['title'] = 'Edit';
                            $att['class'] = 'btn btn-xs btn-info editButton';
                            echo anchor('reference/ref_instansi/edit/'.$id, '<i class="fa fa-fw fa-edit"></i>', $att);
                            echo nbs();
                            $att['class'] = 'btn btn-xs btn-danger deleteButton';
                            $att['title'] = 'Delete';
                            echo anchor('reference/ref_instansi/delete/'.$id, '<i class="fa fa-fw fa-trash"></i>', $att);
                        ?>
                        </td>
                    </tr>
                
                <?php    
                    $seq++;
                endforeach;
                ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->


