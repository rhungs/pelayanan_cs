<div id="ModalUjiJenis" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    Form Tambah Daftar Jenis Pengujian
                </h4>
            </div><!-- /.modal-header -->
            <div class="modal-body">
                <?php echo form_open( 'reference/ref_ujijenis', array( 'id'=>'addForm', 'class'=>'form-horizontal' ) );?>
                <?php
                    $label = array(
                        'class' => 'control-label col-md-4'
                    );
                    $input = array(
                        'class' => 'form-control'
                    );
                ?>
                <div class="form-group">
                    <?php echo form_label('Kode Jenis Uji', 'kode_jenisuji', $label); ?>
                    <div class="col-md-8">
                        <?php echo form_input( array( 'name'=> 'kode_jenisuji', 'id'=>'kode_jenisuji' ), set_value('kode_jenisuji'), $input); ?>
                    </div><!-- /.col -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Nama Jenis Uji', 'nama_jenisuji', $label); ?>
                    <div class="col-md-8">
                        <?php echo form_input( array( 'name'=> 'nama_jenisuji', 'id'=>'nama_jenisuji' ), set_value('nama_jenis_uji'), $input ); ?>
                    </div><!-- /.col-md-8 -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Singkatan', 'singkatan', $label); ?>
                    <div class="col-md-8">
                        <?php echo form_input( array( 'name'=> 'singkatan', 'id'=>'singkatan' ), set_value('singkatan'), $input ); ?>
                    </div><!-- /.col-md-8 -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Standar', 'standar', $label); ?>
                    <div class="col-md-8">
                        <?php echo form_input( array( 'name'=> 'standar', 'id'=>'standar' ), set_value('standar'), $input ); ?>
                    </div><!-- /.col-md-8 -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Lama Uji', 'lama_uji', $label); ?>
                    <div class="col-md-5">
                        <div class="input-group">
                            <?php echo form_input( array( 'name'=> 'lama_uji', 'id'=>'lama_uji' ), set_value('lama_uji'), $input ); ?>
                            <span class="input-group-addon">
                                Hari
                            </span>
                        </div><!-- /.input-group -->
                    </div><!-- /.col-md-8 -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Harga Uji', 'harga_uji', $label); ?>
                    <div class="col-md-5">
                        <div class="input-group">
                            <span class="input-group-addon">
                                Rp.
                            </span>
                            <?php echo form_input( array( 'name'=> 'harga_uji', 'id'=>'harga_uji' ), set_value('harga_uji'), $input ); ?>
                        </div><!-- /.input-group -->
                    </div><!-- /.col -->
                            
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Kategori Pengujian', 'kat_uji', $label); ?>
                    <div class="col-md-5">
                        <div class="input-group">
                        <?php
                            $input['id'] = 'kat_uji';
                            echo form_dropdown( 'kat_uji', $list['kat_uji'], set_select('kat_uji'), $input );
                        ?>
                        </div><!-- /.input-group -->
                    </div><!-- /.col -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <?php echo form_label('Klasifikasi Pengujian', 'klas_uji', $label); ?>
                    <div class="col-md-5">
                        <div class="input-group">
                        <?php
                            $input['id'] = 'klas_uji';
                            echo form_dropdown( 'klas_uji', $list['klas_uji'], set_select('klas_uji'), $input );
                        ?>
                        </div><!-- /.input-group -->
                    </div><!-- /.col -->
                </div><!-- /.form-group -->
                
                <div class="form-group">
                    <div class="col-md-5 col-md-offset-4">
                    <?php
                        echo form_reset( array('id'=>'reset'), 'Reset', array( 'class'=>'btn btn-danger' ) );
                        echo form_submit( array('id'=>'submit'), 'Submit', array( 'class'=>'btn btn-primary' ) );
                    ?>
                    </div><!-- /.col -->
                </div>
                
                
                <?php echo form_close(); ?>
            </div><!-- /.modal-body -->
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="content-wrapper">
    <section class="content">
        <?php
        if(isset($message)){
            echo $message;
        }
        ?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">DAFTAR JENIS PENGUJIAN</h3>
                <button id="addFormBtn" class="btn btn-xs pull-right btn-primary">
                    <span class="fa fa-plus"></span>
                    Tambah Daftar Pengujian
                </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
                <table class="table table-condensed table-striped table-bordered table-hover" id="dataTables-listujijenis" data-page-length='10'>
                    <thead>
                        <tr>                    
                            <!--th>No.</th-->
                            <th>Kode</th>                                        
                            <th>Jenis Pengujian</th>
                            <!--<th>Singkatan</th>-->
                            <th>Standar</th>                    
                            <th>Kategori Pengujian</th>
                            <th>Klasifikasi Pengujian</th>
                            <th>Lama Uji</th>
                            <th class="align-center"># Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $seq = 1;
                    foreach($result as $row) : ?>

                        <tr>                         
                            <!--td><?php //echo $seq; ?></td-->
                            <td><?php echo $row->jenisuji_code; ?></td>
                            <td><?php echo $row->jenisuji_name; ?></td>
                            <!--<td><?php echo $row->jenisuji_shortname; ?></td>--> 
                            <td><?php echo $row->jenisuji_standard; ?></td>                         
                            <td><?php echo $row->ujikat_name; ?></td>
                            <td><?php echo $row->ujiklas_name; ?></td> 
                            <td><?php echo $row->jenisuji_lama.nbs().'hari'; ?></td>
                            <td class="align-center">
                            <?php
                                $id = $this->qsecure->encrypt($row->jenisuji_id);
                                $att = array(
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                );
                                $att['title'] = 'Edit';
                                $att['class'] = 'btn btn-xs btn-info editButton';
                                echo anchor('reference/ref_ujijenis/edit/'.$id, '<i class="fa fa-fw fa-edit"></i>', $att);
                                echo nbs();
                                $att['class'] = 'btn btn-xs btn-danger deleteButton';
                                $att['title'] = 'Delete';
                                echo anchor('reference/ref_ujijenis/delete/'.$id, '<i class="fa fa-fw fa-trash"></i>', $att);

                            ?>
                            </td>
                        </tr>

                    <?php    
                        $seq++;
                    endforeach;

                    ?>
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

