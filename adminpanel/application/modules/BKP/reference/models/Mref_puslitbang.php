<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_puslitbang extends BSS_model
{
    protected $_table = "res_ref_puslit";
    protected $_id = "puslit_id";
    protected $_data = null;
                
    public function get_puslit_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.puslit_id, a.puslit_alias, a.puslit_name, a.puslit_shortname, a.aktif");
          $this->db->from($this->_table. " a");        
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.puslit_shortname', $search_string);
              $this->db->or_like('a.puslit_name', $search_string);
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by($this->_id,"asc");
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_puslit_count($search_string=null){
        $this->db->select("count(puslit_id) as _cnt");
        $this->db->from($this->_table);        
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.puslit_shortname', $search_string);
              $this->db->or_like('a.puslit_name', $search_string);
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
    public function get_puslit_row($id=null){
          $this->db->select("a.puslit_id, a.puslit_alias, a.puslit_name, a.puslit_shortname, a.aktif");
          $this->db->from($this->_table. " a");                            
          $this->db->where("a.puslit_id",$id);
          $Q = $this->db->get();
          $this->_data = $Q->row();
          $Q->free_result();
          return $this->_data;
    }
    
    function puslit_save($id=null){
        $dataIn = array (
          'puslit_name' => $this->input->post("puslit_name"),
          'puslit_shortname' => $this->input->post("puslit_shortname"),
          'puslit_alias' => $this->input->post("puslit_alias"),
          'aktif' => ($this->input->post("aktif") == null ? 0 : $this->input->post("aktif"))
        );
        
        if (is_null($id) OR $id == ""){
            return $this->insert($dataIn);
        }else{
            return $this->update($id, $dataIn);
        }
        
    }
    
    function insert($data){
        $this->db->trans_begin();
        
        $this->db->insert($this->_table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
            $return["msg"]="Data Puslitbang gagal disimpan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;  
            $return["msg"]="Data Puslitbang berhasil disimpan..";
        }
        return $return;
    }
    
    function update($id,$data){
        $this->db->trans_begin();
        $this->db->where("puslit_id",$id);
        $this->db->update($this->_table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
            $return["msg"]="Data Puslitbang gagal diupdate !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;  
            $return["msg"]="Data Puslitbang berhasil diupdate..";
        }
        return $return;
    }
    
    function delete($id){
        $this->db->trans_begin();                        
        $this->db->where("puslit_id",$id);
        $this->db->delete($this->_table);
               
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="Data Puslitbang gagal dihapus !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="Data Puslitbang berhasil dihapus..";
        }
        return $return;
    }
    
    
    
}
