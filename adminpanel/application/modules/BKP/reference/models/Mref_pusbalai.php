<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_pusbalai extends BSS_model
{
    var $_table = "res_ref_balai";
    var $_id = "balai_id";
    var $_data = null;
                
    public function get_balai_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.balai_id, a.puslit_id, a.balai_alias, a.balai_name, a.balai_shortname, a.aktif, b.puslit_shortname");
          $this->db->from($this->_table. " a");        
          $this->db->join("res_ref_puslit b","a.puslit_id=b.puslit_id","inner");
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.balai_name', $search_string);
              $this->db->or_like('a.balai_shortname', $search_string);
              $this->db->or_like('b.puslit_shortname', $search_string);
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.puslit_id","asc");
          $this->db->order_by("a.seq_balai","asc");
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_balai_count($search_string=null){
        $this->db->select("count(a.balai_id) as _cnt");
        $this->db->from($this->_table . " a");
        $this->db->join("res_ref_puslit b","a.puslit_id=b.puslit_id","inner");
        if (! is_null($search_string) && $search_string != ""){
             $this->db->like('a.balai_name', $search_string);
             $this->db->or_like('a.balai_shortname', $search_string);
             $this->db->or_like('b.puslit_shortname', $search_string);
         }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
    public function insert_nama_balai($data){   
        $insert = $this->db->insert( $this->_table, $data );   
        return $insert;   
    }
    
    public function get_balai($id){
        $this->db->select('*');
        $this->db->where($this->_id, $id);
        $query = $this->db->get($this->_table);
        
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    public function update_balai($data, $id){
        $exec = $this->db->update( $this->_table, $data, array( $this->_id => $id ) );
        return $exec;
    }
    
    
    
}
