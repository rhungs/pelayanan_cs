<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_ujijenisbalai extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_BALAI_JENISUJI";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_ujijenisbalai","mref");
        $this->load->model('mref_ujijenis','muji');
        
        $this->load->model('mref_pusbalai','mbalai');
        $this->load->model('mref_puslitbang', 'mpuslit');
    }
    
    public function index(){
        $this->list_ujijenisbalai();
    }
    
    public function list_ujijenisbalai(){
        $data['titlehead'] = "Daftar Mapping Balai dan Jenis Pengujian :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css',
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.rowGrouping.js',
            HTTP_ASSET_PATH.'plugins/select2/select2.min.js',
            HTTP_MOD_JS.'modules/reference/ref_ujijenisbalai.js'
            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                       $('#dataTables-listujijenisbl')
                            .dataTable({ 
                                'bLengthChange': false, 'bPaginate': true,
                                 searching: true,
                                 paging : true
                            })
                            .rowGrouping({ 
                                bExpandableGrouping: true, 
                                iGroupingColumnIndex: 3,
                                sGroupingColumnSortDirection: 'asc'                                
                            });                       
                   });";

        $loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
        $data['list'] = array(
            'nama_balai' => $this->get_list_balai(),
        );        
        $data['result'] = $this->mref->get_ujijenisbalai_list();
        $data['res_cnt'] = count($data['result']);
        
        $this->template->load('tmpl/vwbacktmpl','vref_ujijenisbalai',$data);     
    }
    
    public function get_list_balai(){
        $data = $this->mbalai->get_balai_list();
        $return[] = '';
        foreach( $data as $item ){
            $puslit = '';
            $puslit = $this->mpuslit->get_puslit_row($item->puslit_id);
            //debug_var($puslit);
            $return[$item->balai_id] = $puslit->puslit_alias.' - '.$item->balai_shortname;
        }
        return $return;
    }
    
    public function get_jenis_uji($balai_id=''){
        $data = $this->muji->get_ujijenis_list();
        $balai_uji = $this->mref->get_balaiuji($balai_id);
        foreach($data as $item){
            $dis = '';
            foreach( $balai_uji as $value ){
                
                if( $value->jenisuji_id == $item->jenisuji_id){
                    $dis = ' disabled="disabled"';
                    break;
                }
            }
            //echo nbs().$item->jenisuji_id."]<br />";
            echo '<option value="'.$item->jenisuji_id.'"'.$dis.'>'.$item->jenisuji_name.'</option>';
        }
    }
    
    public function insert_ujijenisbalai(){
        $input = $this->input->post();
        $data = array(
            'balai_id' => $input['nama_balai'],
            'jenisuji_id' => $input['jenis_uji']
        );
        $exe = $this->mref->insert_record( $this->mref->_table, $data );
        if($exe){
            redirect('reference/ref_ujijenisbalai/index/inserted');
        }else{
            redirect('reference/ref_ujijenisbalai/index/ins_failed');
        }
        
    }
    
    public function delete($balai_id,$uji_id){
        $b_id = $this->qsecure->decrypt($balai_id);
        $u_id = $this->qsecure->decrypt($uji_id);
        
        $exe = $this->mref->delete_balaiuji($b_id, $u_id);
        
        if($exe){
            redirect('reference/ref_ujijenisbalai/index/deleted');
        }else{
            redirect('reference/ref_ujijenisbalai/index/del_failed');
        }
    }
    
    public function edit($balai_id, $uji_id){
        $bid = $this->qsecure->decrypt($balai_id);
        $uid = $this->qsecure->decrypt($uji_id);
        
        $data = $this->mref->get_ujijenisbalai($bid,$uid);
        echo json_encode($data);
    }
    
    
}
