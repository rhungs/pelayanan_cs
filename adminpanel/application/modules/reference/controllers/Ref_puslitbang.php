<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ref_puslitbang extends Bss_controller {
    
    function __construct() {
        parent::__construct();
        
        $this->MOD_ALIAS = "MOD_REF_PUSLITBANG";
        $this->checkAuthorization($this->MOD_ALIAS);
        
        $this->load->model("mref_puslitbang","mref");
    }
    
    public function index(){
        $this->list_puslit();
    }
    
    public function list_puslit(){
        $data['titlehead'] = "Daftar Puslitbang :: PULSA";
        
        // custom load stylesheet, place at header
        $loadhead['stylesheet'] = array(
            /* DataTables CSS */
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.css'              
        );

        // custom load javascript, place at footer
        $loadfoot['javascript'] = array(
            /*- DataTables JavaScript -*/            
            HTTP_ASSET_PATH.'plugins/datatables/jquery.dataTables.min.js',
            HTTP_ASSET_PATH.'plugins/datatables/dataTables.bootstrap.min.js',
            
        );

        // load tag js 
        $tagjs1 = "$(document).ready(function() {                            
                        $('#dataTables-listpuslit').DataTable({
                            responsive: true,                                
                            searching: true,
                            paging : true
                        });
                   });";

        //$loadfoot['tagjs'] = array($tagjs1);

        $data['loadhead'] = $loadhead;
        $data['loadfoot'] = $loadfoot;
                
        $data['result'] = $this->mref->get_puslit_list();
        $data['res_cnt'] = count($data['result']);
        
        $this->template->load('tmpl/vwbacktmpl','vref_puslit',$data);     
    }
    
    public function form_puslit(){
        $id = $this->uri->segment(4);        
        $data['puslit_id'] = $id;
        if ( is_null($id) || trim($id) == '' ) {
            $data['titlehead'] = 'Form Referensi Puslitbang :: PULSA';
            $data['aktif'] = "1";
        } else {
            $data['titlehead'] = 'Edit Referensi Puslitbang :: PULSA';
            $id = $this->qsecure->decrypt($id);            
        }
        
        $data['loadhead'] = array();
        $data['loadfoot'] = array();
        
        // config rules validation
        $config = array (		
            array(
                    'field'=>'puslit_name',
                    'label'=>'Nama Puslitbang',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'puslit_shortname',
                    'label'=>'Nama Singkatan',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'puslit_alias',
                    'label'=>'Singkatan/Alias',
                    'rules'=>'trim|required'
                    ),
            array(
                    'field'=>'aktif',
                    'label'=>'Aktif',
                    'rules'=>'trim'
                    )
        );
               	                    
       if( ! is_null($id) && (trim($id) != '') && ! $this->input->post('puslit_name')) {
            // retrieve data;
            $result = $this->mref->get_puslit_row($id);
            if (count($result) > 0 ) {
                $data['puslit_name'] = $result->puslit_name;
                $data['puslit_shortname'] = $result->puslit_shortname;
                $data['puslit_alias'] = $result->puslit_alias;
                $data['aktif'] = $result->aktif;                 
            }
                        
       } 
       
       if ($_POST) { // POST
          
            // Form submited, check rules
            $this->form_validation->set_rules($config); 
            $this->form_validation->set_message('required', '%s harus diisi !');
            if($this->form_validation->run() === FALSE) {
                $data['err_msg'] = validation_errors();
            }else{
                $res = $this->mref->puslit_save($id);
                if ($res["success"] == true)
                    $this->session->set_flashdata('info', $res['msg']);
                else
                    $this->session->set_flashdata('err', $res['msg']);
                redirect('reference/ref_puslitbang/list_puslit');                                
            }
                
            
       }
                                                    
        $this->template->load('tmpl/vwbacktmpl','vref_puslit_form',$data);     
    }
    
    public function delete_puslit($id){
        $id = $this->qsecure->decrypt($id);             
        $res = $this->mref->delete($id);
        if ($res["success"] == true)
            $this->session->set_flashdata('info', $res['msg']);
        else
            $this->session->set_flashdata('err', $res['msg']);
        redirect('reference/ref_puslitbang');
    }
    
}
