<div class="content-wrapper">
<section class="content">    
    <div class="row">
        <div class="col-xs-12">
            
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Form Referensi Puslitbang</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="<?php echo base_url() . "reference/ref_puslitbang/form_puslit/" . (isset($puslit_id) ? $puslit_id :''); ?>" >
              <div class="box-body">
                <div class="row">
                    <?php
                        if (isset($err_msg) && $err_msg != "") { ?>
                            <div class="alert alert-danger">
                                <label for="inputError" class="control-label"><?=$err_msg?></label>
                            </div>
                    <?php
                        }
                    ?>
                  <div class="col-md-9">
                        <input name="puslit_id" type="hidden" value="<?php echo set_value('puslit_id', (isset($puslit_id) ? $puslit_id :'')) ; ?>">
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_kode">Nama Puslitbang* :</label>
                          <div class="col-sm-8">
                              <input type="text"  id="puslit_name" name="puslit_name" class="form-control" value="<?=(isset($puslit_name) ? $puslit_name : "")?>" >                             
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_kode">Nama Singkatan* :</label>
                          <div class="col-sm-8">
                              <input type="text"  id="puslit_shortname" name="puslit_shortname" class="form-control" value="<?=(isset($puslit_shortname) ? $puslit_shortname : "")?>" >                             
                          </div>
                        </div>
                        
                         
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="fpp_kode">Alias* :</label>
                          <div class="col-sm-8">
                              <input type="text"  id="puslit_alias" name="puslit_alias" class="form-control" value="<?=(isset($puslit_alias) ? $puslit_alias : "")?>" >                             
                          </div>
                        </div>
 
                    
                        <div class="form-group">
                          <label class="col-sm-4 control-label" for="aktif">Aktif :</label>
                          <div class="col-sm-8">
                               <div class="checkbox">
                              <?php
                                $yes = FALSE;
                                $no = TRUE;
                                if(isset($aktif) && trim($aktif) == '1'){
                                    $yes = TRUE;
                                    $no = FALSE;
                                }else if(isset($aktif) && trim($aktif) == '0'){
                                    $yes = FALSE;
                                    $no = TRUE;
                                }
                                
                                $dataChk = array(
                                    'name' => 'aktif',
                                    'id' => 'aktif',
                                    'value' => '1',
                                    'checked' => $yes,
                                    'style' => 'margin:5px'                                     
                                );

                                echo form_checkbox($dataChk);
                                //echo "Yes";
                              ?>
                               </div>
                          </div>
                        </div>
                                   
                  </div><!-- /.col-md-9 -->                  
                  
                </div>
                <hr />
               
              </div><!-- /.box-body -->
              <div class="box-footer">                
                <button type="submit" id="simpan" class="btn btn-info pull-left">Simpan</button>
                &nbsp;&nbsp;
                <a href="<?php echo base_url()."reference/ref_puslitbang/list_puslit"?>" class="btn btn-default" type="submit">Batal</a>
              </div><!-- /.box-footer -->
                                          
            </form>
          </div><!-- /.box-info -->
                         
        </div><!-- /col-xs-12 -->
     </div>
    

</section>
</div>