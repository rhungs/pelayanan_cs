<div id="ModalUjiJenisBalai" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    Form Tambah Daftar Jenis Uji Balai
                </h4>
            </div><!-- /.modal-header -->
            <div class="modal-body">
                <?php echo form_open( 'reference/ref_ujijenisbalai', array( 'id'=>'addForm' ) );?>
                <?php
                    $label = array(
                        //'class' => 'control-label'
                    );
                    $input = array(
                        'class' => 'form-control select2',
                        'style' => 'width:100%'
                    );
                ?>
                <div class="form-group">
                <?php
                    $input['id'] = 'nama_balai';
                    echo form_label('Nama Balai', 'nama_balai', $label);
                    echo form_dropdown( 'nama_balai', $list['nama_balai'], set_select('nama_balai'), $input );
                ?>
                </div><!-- /.form-group -->
                <div class="form-group">
                <?php
                    $input['id'] = 'jenis_uji';
                    echo form_label('Jenis Uji', 'jenis_uji', $label);
                    echo form_dropdown( 'jenis_uji', '', '', $input );
                ?>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <?php
                        echo form_reset( array('id'=>'reset'), 'Reset', array( 'class'=>'btn btn-danger' ) );
                        echo form_submit( array('id'=>'submit'), 'Submit', array( 'class'=>'btn btn-primary' ) );
                    ?>
                    
                </div>
                
                
                <?php echo form_close(); ?>
            </div><!-- /.modal-body -->
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="content-wrapper">
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">DAFTAR BALAI DAN JENIS PENGUJIAN</h3>
                <button id="addFormBtn" class="pull-right btn btn-xs btn-primary">
                    <span class="fa fa-plus"></span>
                    Tambah Daftar
                </button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!--<div class="box-body table-responsive">-->
              <table class="table table-bordered table-hover" id="dataTables-listujijenisbl" data-page-length='10'>
                <thead>
                <tr>                                                                                       
                    <th>Jenis Pengujian</th>
                    <th>Kategori Pengujian</th>
                    <th>Klasifikasi Pengujian</th>
                    <th>Nama Balai</th>
                    <th>Nama Puslitbang</th>
                    <th class="align-center"># Aksi</th>
                </tr>
                </thead>
                </tbody>
                <?php
                $seq = 1;
                foreach($result as $row) : ?>
                
                    <tr>                                                 
                        <td><?php echo nbs(5).$row->jenisuji_name; ?></td>
                        <td><?php echo $row->ujikat_name; ?></td>
                        <td><?php echo $row->ujiklas_name; ?></td> 
                        <td><b>Nama Balai :</b> <?php echo $row->balai_name; ?></td>
                        <td><?php echo $row->puslit_shortname?></td>
                        <td class="align-center">
                        <?php
                            $uji_id = $this->qsecure->encrypt($row->jenisuji_id);
                            $balai_id = $this->qsecure->encrypt($row->balai_id);
                            $att = array(
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            );
                            $att['class'] = 'btn btn-xs btn-danger deleteButton';
                            $att['title'] = 'Delete';
                            echo anchor('reference/ref_ujijenisbalai/delete/'.$balai_id.'/'.$uji_id, '<i class="fa fa-fw fa-trash"></i> '.$att['title'], $att);

                        ?>      
                        </td>
                    </tr>
                
                <?php    
                    $seq++;
                endforeach;
                
                ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

