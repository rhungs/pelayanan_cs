<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_ujijenis extends BSS_model
{
    var $_table = "res_ref_ujijenis";
    var $_id = "jenisuji_id";
    protected $_data = null;
                
    public function get_ujijenis_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.jenisuji_id, a.jenisuji_code, a.jenisuji_name, a.jenisuji_shortname, a.jenisuji_standard, 
                             a.jenisuji_lama, a.ujikat_id, a.ujikat_id, b.ujikat_name, c.ujiklas_name, a.aktif");
          $this->db->from($this->_table. " a");        
          $this->db->join("res_ref_ujikategori b","b.ujikat_id=a.ujikat_id","inner");
          $this->db->join("res_ref_ujiklasifikasi c","c.ujiklas_id=a.ujiklas_id","inner");
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.jenisuji_name', $search_string);
              $this->db->or_like('a.jenisuji_standard', $search_string);              
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.jenisuji_code","asc");          
          $Q = $this->db->get();          
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;
    }

    public function get_ujijenis_count($search_string=null){
        $this->db->select("count(a.jenisuji_id) as _cnt");
        $this->db->from($this->_table . " a");
        $this->db->join("res_ref_ujikategori b","b.ujikat_id=a.ujikat_id","inner");
        $this->db->join("res_ref_ujiklasifikasi c","c.ujiklas_id=a.ujiklas_id","inner");
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.jenisuji_name', $search_string);
              $this->db->or_like('a.jenisuji_standard', $search_string);              
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
    
    
}
