<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  KiQ
*/ 

class Mref_ujiklasifikasi extends BSS_model
{
    var $_table = "res_ref_ujiklasifikasi";
    var $_id = "ujiklas_id";
    protected $_data = null;
                
    public function get_ujiklas_list($offset=null, $limit=null,$search_string=null){
          $this->db->select("a.ujiklas_id, a.ujiklas_name, a.aktif");
          $this->db->from($this->_table. " a");        
          if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.ujiklas_name', $search_string);              
          }
          
          if ($offset != null && $limit != null)
            $this->db->limit($limit,$offset);
          
          $this->db->order_by("a.ujiklas_id","asc");          
          $Q = $this->db->get();
          $this->_data = $Q->result();
          $Q->free_result();
          return $this->_data;

    }

    public function get_ujiklas_count($search_string=null){
        $this->db->select("count(a.ujiklas_id) as _cnt");
        $this->db->from($this->_table . " a");        
        if (! is_null($search_string) && $search_string != ""){
              $this->db->like('a.ujiklas_name', $search_string);              
        }
        $Q = $this->db->get();
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;

    }
    
    public function insert_uji($ins){
        $data = array(
            'ujiklas_name' => $ins['klasifikasi_uji'],
        );
        $insert = $this->db->insert( $this->_table, $data );   
        return $insert; 
    }
    
    public function get_ujiklas($id){
        $this->db->select('*');
        $this->db->where($this->_id, $id);
        $query = $this->db->get($this->_table);
        
        $result = $query->result();
        $query->free_result();
        return $result;
    }
    
    
    
}
