<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Author:  Nur
*/ 

class Mthang360 extends Bss_model
{
	protected $_table = "ref_thang360";
    protected $_id = "thang";
    protected $_data = null;
   
	function get_list($id=null, $offset=null, $limit=null, $search_string=null, $by=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("thang,semester,active");
        $this->db->from("ref_thang360");
        if (! is_null($search_string) && $search_string != ""){
            $this->db->where("( thang LIKE '%$search_string%' )");
        }
        if ($id == null OR $id == "") {
            if ($by != null)
                $this->db->where($by);
            if ($offset != null && $limit != null)
                $this->db->limit($limit,$offset);
            $this->db->order_by("thang");         
            $Q = $this->db->get();
            $this->_data = $Q->result();
        }else{
            $this->db->where("thang", $id);
            if ($by != null)
                $this->db->where($by);
            $Q = $this->db->get();
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;            
    }
    function get_list_cnt($by=null, $search_string=null){
		$role_id = $this->session->userdata(sess_prefix()."roleid");
		$user_id = $this->session->userdata(sess_prefix()."userid");
        $this->db->select("count(thang) _cnt");
		$this->db->from("ref_thang360");
        if (! is_null($search_string) && $search_string != ""){
             $this->db->where("( thang LIKE '%$search_string%' )");
        }
        if ($by != null){
            $this->db->where($by);                                                                
        }        
        $Q = $this->db->get();            
        $this->_data = $Q->row()->_cnt;
        $Q->free_result();
        return $this->_data;            
    }
    public function get_view_edit($id=null,$semester=null,$by=null){                 
        $this->db->select("thang,semester");
        $this->db->from('ref_thang360');    
		$this->db->where("thang",$id);		
		$this->db->where("semester",$semester);		
        if ($by != null) {
            $bye = $this->_by($by);          
            $this->db->where($bye,"",false);
        }
        if ($id == null && $id == ""){                         
            $this->db->order_by("thang","asc");          
        }else{
            $this->db->where("thang",$id);
        }
        $Q = $this->db->get();
        if ($id == null && $id == ""){
            $this->_data = $Q->result();
        }else{
            $this->_data = $Q->row();
        }
        $Q->free_result();
        return $this->_data;
    }
	public function get_thang($thang=null){
          $this->db->select("a.thang as id, a.thang as name ");
          $this->db->from("ref_thang a");          
          if (! is_null($thang) && $thang != ""){
              $this->db->where('a.thang', $thang);
          }                                                           
          $Q = $this->db->get();
          if (! is_null($thang) && $thang != ""){
            $this->_data = $Q->row();
          }else{
            $this->_data = $Q->result();  
          }
          $Q->free_result();
          return $this->_data;
    }
    public function thang360_save($id){       
		if ($id == null OR $id == ""){
			$dataIn = array(    
				'thang' => $this->input->post("thang"),
				'semester' => $this->input->post("semester"),
				'active' => 1
			);  
			$this->insert_thang360($dataIn);
		}else{
			$dataIn = array(    
				'thang' => $this->input->post("thang"),
				'semester' => $this->input->post("semester")
			);  
			$this->update_thang360($id,$dataIn);
		}
    }
    function insert_thang360($data){
        $this->db->trans_begin();                 
        $this->db->insert("ref_thang360", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function update_thang360($id,$data){
        $this->db->trans_begin();
        $this->db->where("thang",$id);
        $this->db->update("ref_thang360", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }
    function delete_thang360($id,$semester){
		$data= array(    
			'active' => false
		);  
        $this->db->trans_begin();
		$this->db->where("thang",$id);
		$this->db->where("semester",$semester);
        $this->db->update('ref_thang360',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="T.A Anggaran & Semester gagal dinonaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="T.A Anggaran & Semester berhasil dinonaktifkan...";
        }
        return $return;
    }
	function active_thang360($id,$semester){
		$data= array(    
			'active' => true
		);  
        $this->db->trans_begin();
		$this->db->where("thang",$id);
		$this->db->where("semester",$semester);
        $this->db->update('ref_thang360',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="T.A Anggaran & Semester gagal diaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="T.A Anggaran & Semester berhasil diaktifkan...";
        }
        return $return;
    }
	
	function deactive_thang360($id,$semester){
		$data= array(    
			'active' => false
		);  
        $this->db->trans_begin();
        $this->db->update('ref_thang360',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;   
            $return["msg"]="T.A Anggaran & Semester gagal diaktifkan !";
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;   
            $return["msg"]="T.A Anggaran & Semester berhasil diaktifkan...";
        }
        return $return;
    }
	
	
    function insertHis($msg){
        $this->db->insert('pend_his',$msg);
    }
	function approve($id, $msg){
        $this->db->query("UPDATE pend_data SET status=2 WHERE pend_id=?",array($id));
        $this->db->insert('pend_his',$msg);
    }
    function insert_notif($data){
        $this->db->trans_begin();                 
        $this->db->insert("res_notif", $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            $return["success"]=false;            
        }
        else
        {
            $this->db->trans_commit();           
            $return["success"]=true;            
        }
        return $return;
    }

}
