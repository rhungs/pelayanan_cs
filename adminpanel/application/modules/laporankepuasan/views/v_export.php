<?php 
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment;Filename=ExportLaporanKepuasanPelangan-".date('Y-m-d').".xls");
?>

<html>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">

<body>
          
<h4>List laporan Kepuasan Pelanggan Bapenda Kota Bogor</h4>
<table class="table-bordered" border="1px">
  <thead>
    <tr>
      <th align="center" >No</th>
      <th align="center" >Tanggal</th>
      <th align="center" >Loket</th>
      <th align="center" >Jenis Pelayanan</th>
      <th align="center" >Nama CS</th>
      <th align="center" >Nomor Antrian</th>
      <th align="center" >Mulai Pelayanan</th>
      <th align="center" >Selesai Pelayanan</th>
      <th align="center" >Durasi</th>
      <th align="center" >Indikator Pelayanan</th>
    </tr>
  </thead>

  <tbody>
    <?php 
      if (!empty($list)) {
        $no = 1;
        foreach ($list as $row) {
          echo '<tr>
                  <td Valign="center" valign="top">'.$no.'</td>
                  <td valign="top">'.$row->created_date.'</td>
                  <td valign="top">'.$row->instansi_name.'</td>
                  <td valign="top">'.$row->jenis_pelayanan.'</td>
                  <td valign="top">'.$row->full_name.'</td>
                  <td valign="top">'.$row->no_antrian.'</td>
                  <td valign="top">'.$row->start_time.'</td>
                  <td valign="top">'.$row->end_time.'</td>
                  <td valign="top">'.datediff($row->start_time,$row->end_time).'</td>
                  <td valign="top">'.$row->indikator_category_name.'</td>
                </tr>';
        $no++;
        }
      } else {
        echo '<tr><td colspan="10">Belum ada data</td></tr>';
      }
    ?>
  </tbody>
</table>

</body>
</html>


<?php 
    function to_rupiah($bil=null){
        $rupiah = number_format($bil, 0, ',', '.');
        return $rupiah;
    }

    function datediff($tgl1,$tgl2){

        $tgl1 = strtotime($tgl1);
        $tgl2 = strtotime($tgl2);
        $diff_secs = abs($tgl1 - $tgl2);
        $base_year = min(date("Y", $tgl1), date("Y", $tgl2));
        $diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);
        /*$data = array( "Tahun" => date("Y", $diff) - $base_year, 
                      "months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1, 
                      "Bulan" => date("n", $diff) - 1, 
                      "days_total" => floor($diff_secs / (3600 * 24)), 
                      "Hari" => date("j", $diff) - 1, 
                      "hours_total" => floor($diff_secs / 3600), 
                      "Jam" => date("G", $diff), 
                      "minutes_total" => floor($diff_secs / 60), 
                      "Menit" => (int) date("i", $diff), 
                      "seconds_total" => $diff_secs, 
                      "Detik" => (int) date("s", $diff) 
                    );*/

        $hasil = floor($diff_secs / 60)." Menit ".(int) date("s", $diff)." Detik";
        return  $hasil;
    }

?>