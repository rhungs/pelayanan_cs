"use_strict";

$(document).ready(function() {  
       
   //init
   $('#dt-popup-instansi-pu').DataTable({
        responsive: true,                                
        searching: true,
        paging : true ,
        'ajax': base_url + 'reference/common/get_instansi_ref/'
   });  
      
    $('#btn_cari_ins_pu').click( function () {
        $('#modal-instansi-pu').modal('show');            
    });
    
    $("select#photo").wSelect();
                                     
});

function GetSelectedInstansi(id){           
    var obj = document.getElementById(id);
    var data = obj.getAttribute('data-select');        
    var dataArr = data.split(";");    
       
    if (dataArr.length > 0 ) {               
        $("#instansi_id").val(dataArr[0]);  
        $("#instansi_name").val(dataArr[1]);                  
        $("#instansi_address").val(dataArr[2]);
        $("#instansi_phone").val(dataArr[3]);
        $("#instansi_fax").val(dataArr[4]);
        $("#instansi_email").val(dataArr[5]);
        //$("#postal_code").val(dataArr[6]);
        //$("#instansi_city").val(dataArr[7]);                 
    }                
    $('#modal-instansi-pu').modal('hide');
}
 