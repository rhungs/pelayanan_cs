"use_strict";

jQuery(document).ready(function($){
    var modal = '#ModalBalai';
    var form = '#FormBalai';
    var form_action = $(form).attr('action');
    $('.edit_ref').on( 'click', function(e){
        e.preventDefault();
        
        var target = $(this).attr('href');
        
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                var balai_id = field.balai_id;
                
                $('#nama_balai_full').val(field.balai_name);
                $('#nama_balai_short').val(field.balai_shortname);
                $('#alias_balai').val(field.balai_alias);
                $('#nama_puslitbang').val(field.puslit_id);
                
                $(form).attr('action', form_action+'/update_balai/'+balai_id);
                $(modal+' .modal-title').html('Form Edit Daftar Balai');
                $(modal+' .submit-btn').val('Update Balai');
            });
            
        });
       
        $(modal).modal('show');
        
        
        
    });
    
    $('.add_ref').on( 'click', function(e){
        e.preventDefault();
        $(modal).modal('show');
        $(form).trigger('reset');
        $(form).attr('action', form_action+'/add_new_balai');
        $(modal+' .modal-title').html('Form Tambah Daftar Balai');        
        $(modal+' .submit-btn').val('Add Balai');
    });
});

