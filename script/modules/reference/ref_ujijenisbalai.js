"use_strict";

jQuery(document).ready(function($){
    
    var modalForm = $('#ModalUjiJenisBalai');
    var theForm = $('#addForm');
    var formTarget = theForm.attr('action');
    var addBtn = $('#addFormBtn');
    var delBtn = $('.deleteButton');
    var edtBtn = $('.editButton');
    
    $('.select2').select2({
        placeholder : "Please select..."
    });
    
    addBtn.on( 'click', function(e){
        e.preventDefault();
        modalForm.find('.modal-title').html('Form Tambah Daftar Uji Jenis Balai');
        modalForm.modal('show');
        $('#reset').trigger('click');
        theForm.attr('action', formTarget+'/insert_ujijenisbalai');
    });
    
    delBtn.on( 'click', function(e){
        var conf = confirm('Apakah anda yakin ingin menghapus Uji Jenis Balai ini?');
        if( !conf ){
            e.preventDefault();
        }
    });
    
    $('#nama_balai').on( 'change', function(e){
        $('#jenis_uji').html("");
        $('#jenis_uji').select2({
            placeholder : "pilih jenis uji..."
        });
        var select_id = $(this).val();
        var target = formTarget+'/get_jenis_uji/'+select_id;        
        $.ajax({
            url: target,
            success: function(result){
                //console.log(result);
                $('#jenis_uji').html(result);
            }
        });
    });
    
});