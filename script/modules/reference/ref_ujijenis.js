"use_strict";

jQuery(document).ready(function($){
    
    var modalForm = $('#ModalUjiJenis');
    var theForm = $('#addForm');
    var formTarget = theForm.attr('action');
    var addBtn = $('#addFormBtn');
    var delBtn = $('.deleteButton');
    var edtBtn = $('.editButton');
    
    addBtn.on( 'click', function(e){
        e.preventDefault();
        modalForm.find('.modal-title').html('Form Tambah Daftar Uji Jenis');
        modalForm.modal('show');
        $('#reset').trigger('click');
        theForm.attr('action', formTarget+'/insert_ujijenis');
    });
    
    delBtn.on( 'click', function(e){
        var conf = confirm('Apakah anda yakin ingin menghapus Uji Jenis ini?');
        if( !conf ){
            e.preventDefault();
        }
    });
    
    edtBtn.on( 'click', function(e){
        e.preventDefault();
        
        var target = $(this).attr('href');
        $.getJSON(target, function(result){
            $.each(result, function(i, field){
                //console.log(field);
                modalForm.find('.modal-title').html('Form Edit Daftar Uji Jenis');
                modalForm.modal('show');
                var ins_id = field.jenisuji_id;
                theForm.attr('action', formTarget+'/update_jenisuji/'+ins_id);
                $('#kode_jenisuji').val(field.jenisuji_code);
                $('#nama_jenisuji').val(field.jenisuji_name);
                $('#singkatan').val(field.jenisuji_shortname);
                $('#standar').val(field.jenisuji_standard);
                $('#lama_uji').val(field.jenisuji_lama);
                $('#harga_uji').val(field.jenisuji_harga);
                $('#kat_uji').val(field.ujikat_id);
                $('#klas_uji').val(field.ujiklas_id);
                
            });
            
        });
        
        
    });
    
});