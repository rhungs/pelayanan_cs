"use_strict";
 var oTable;
 var pend_id=0;
 var dataTable;

$(document).ready(function() {  



	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listindikator').DataTable( {
        "aLengthMenu": [[5,10,15,20,25,50,75,100, 0], [5,10,15,20,25,50,75,100, "All"]],
        "iDisplayLength": 5,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
        	{"class" : "align-center"},
            {"class" : "align-left"},
            {"class" : "align-left"},
            {"class" : "align-center"},
            {"class" : "align-center"},
            {"class" : "align-center"},
			{"class" : "align-center"},
			{"class" : "align-center"}
        ],
        "ajax":{
            url : "Cpenilaian/list_penilaian",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listindikator > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listindikator_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listindikator').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 

	var flag_penilaian= $('#flag_penilaian').val();

	if(flag_penilaian==1){
		$('#selesai').prop('disabled', false);
		$('#refresh').prop('disabled', true);
		$('#no_antrian').show();
		$('#lblnomor').show();
		$('#no_antrian').focus();
	}else if(flag_penilaian==2){
		$('#selesai').prop('disabled', true);
		$('#refresh').prop('disabled', false);
		$('#lblnomor').show();
		$('#no_antrian').show();
		$('#no_antrian').focus();
	}else if(flag_penilaian==0){
		$('#selesai').prop('disabled', true);
		$('#refresh').prop('disabled', true);
		$('#no_antrian').hide();
		$('#lblnomor').hide();
	}


    var check_session;
    function CheckForSession() {
        var str="chksession=true";
        jQuery.ajax({
            type: "POST",
            url: "Cpenilaian/cek_session",
            data: str,
            cache: false,
            success: function(res){
            	if(res==0){
            		$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', true);
					$('#no_antrian').val("");
					//$('#no_antrian').prop('disabled', true);
					//dataTable.ajax.reload( null, false );
					$('#no_antrian').hide();
					$('#lblnomor').hide();
            	}
            }
        });
    }
    check_session = setInterval(CheckForSession, 2000); 


    var check_session_list;
    function CheckForSessionList() {
    	dataTable.ajax.reload( null, false );
    }
    check_session_list = setInterval(CheckForSessionList, 10000); 


	$(document).delegate('#btnclosefeedback','click',function(){
		$("#msginfo").modal("hide");
	});	

	// $(document).delegate('#mulai','click',function(){

	// 	var no_antrian= $('#no_antrian').val();
	// 	var flag_penilaian=1;
	// 	var flag="mulai";

	// 	//if(no_antrian!=""){
	// 	    $.ajax({
	// 	        type: "POST",
	// 	        url: "Cpenilaian/statuspenilaian",
	// 	        data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian+"&flag="+flag,
	// 	        success: function(data){
	// 	           $("#msginfo").modal("show");
	// 	            setTimeout(function() {
	// 				   $('#msginfo').modal("hide");
	// 				   $('#no_antrian').focus();
	// 				}, 1000); 
	// 				$('#mulai').prop('disabled', true);
	// 				$('#selesai').prop('disabled', false);
	// 				$('#refresh').prop('disabled', true);
	// 				$('#no_antrian').val("");
	// 				$('#no_antrian').prop('disabled', false);
	// 				$('#no_antrian').show();
	// 				$('#no_antrian').focus();
	// 				$('#lblnomor').show();

	// 				//dataTable.ajax.reload( null, false );
	// 	         },
	// 			  error: function(xhr, textStatus, errorThrown) {
	// 			    //alert(xhr.status);
	// 			     alert('Gagal terhubung, silakan Cek Koneksi');
	// 			  }
	// 	    });
	// 	//}else{
	// 	//	alert("Nomor antrian harus diisi !");
	// 	//}
	// 	$('#no_antrian').focus();
	// });	

	$(document).delegate('#selesai','click',function(){


		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=2;
		var flag="selesai";

		if(no_antrian!=""){
		    $.ajax({
		         type: "POST",
		         url: "Cpenilaian/statuspenilaian",
		         data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian+"&flag="+flag,
		         success: function(data){
		            $("#msginfo").modal("show");
		            setTimeout(function() {
					    $('#msginfo').modal("hide");
					}, 1000); ;
					$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', false);
					$('#no_antrian').prop('disabled', false);
					$('#no_antrian').show();
					$('#lblnomor').show();
		         },
				  error: function(xhr, textStatus, errorThrown) {
				    //alert(xhr.status);
				    alert('Gagal terhubung, silakan Cek Koneksi');
				  }
		    });
		}else{
			alert("Nomor antrian harus diisi !");
		}
	});	



	$(document).delegate('#refresh','click',function(){

		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=0;
		var flag="refresh";
		
		if(no_antrian!=""){
		    $.ajax({
		         type: "POST",
		         url: "Cpenilaian/statuspenilaian",
		         data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian+"&flag="+flag,
		         success: function(data){
		            $("#msginfo").modal("show");
		            setTimeout(function() {
					    $('#msginfo').modal("hide");
					}, 1000); 
					$('#selesai').prop('disabled', true);
					$('#refresh').prop('disabled', true);
					$('#no_antrian').hide();
					$('#lblnomor').hide();
		         },
				  error: function(xhr, textStatus, errorThrown) {
				    //alert(xhr.status);
				    alert('Gagal terhubung, silakan Cek Koneksi');
				  }
		    });
		}else{
			alert("Nomor antrian harus diisi !");
		}
	});	

	$(document).delegate('#cancel','click',function(){


		var no_antrian= $('#no_antrian').val();
		var flag_penilaian=0;
		var flag="cancel";

	    $.ajax({
	         type: "POST",
	         url: "Cpenilaian/statuspenilaian",
	         data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian+"&flag="+flag,
	         success: function(data){
	            $("#msginfo").modal("show");
	            setTimeout(function() {
				    $('#msginfo').modal("hide");
				}, 1000); 
				$('#selesai').prop('disabled', true);
				$('#refresh').prop('disabled', true);
				$('#no_antrian').hide();
				$('#lblnomor').hide();
	         },
			  error: function(xhr, textStatus, errorThrown) {
			    //alert(xhr.status);
			    alert('Gagal terhubung, silakan Cek Koneksi');
			  }
	    });
	});	

	$(document).delegate('#clickform','click',function(){
		window.location.href = "Cpenilaian/penilaian";
	});

	
});

function f_mulai_penilaian(noantrian){

	var no_antrian= noantrian;
	var flag_penilaian=1;
	var flag="mulai";

	//if(no_antrian!=""){
	    $.ajax({
	        type: "POST",
	        url: "Cpenilaian/statuspenilaian",
	        data: "flag_penilaian="+flag_penilaian+"&no_antrian="+no_antrian+"&flag="+flag,
	        success: function(data){
	           $("#msginfo").modal("show");
	            setTimeout(function() {
				   $('#msginfo').modal("hide");
				   $('#no_antrian').focus();
				}, 1000); 
				$('#selesai').prop('disabled', false);
				$('#refresh').prop('disabled', true);
				$('#no_antrian').val(no_antrian);
				$('#no_antrian').prop('disabled', true);
				$('#no_antrian').show();
				$('#no_antrian').focus();
				$('#lblnomor').show();

				//dataTable.ajax.reload( null, false );
	         },
			  error: function(xhr, textStatus, errorThrown) {
			    //alert(xhr.status);
			     alert('Gagal terhubung, silakan Cek Koneksi');
			  }
	    });
	//}else{
	//	alert("Nomor antrian harus diisi !");
	//}
	$('#no_antrian').focus();


}






