"use_strict";
 var oTable;
 var dataTable;
$(document).ready(function() {  
       
   //init
   $('#dataTables-listuser').DataTable({
        responsive: true,                                
        searching: true,
        paging : true       
   });  
   
   	 var dataTable;
	//--=== #REGION GRID LIST
    dataTable = $('#dt-listuser').DataTable( {
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "stateSave": false,
        "columns": [
            {"class" : "align-center"},
			{"class" : "align-left"},
            {"class" : "align-left"},
			{"class" : "align-left"},
            {"class" : "align-left"},
			{"class" : "align-left"}
        ],
        "ajax":{
            url : base_url + "utility/User_manage/list_users",
            type: "post",            
            error: function(){  // error handling                
                $("#dt-listuser > tbody").append('<tr class="odd"><td valign="top" colspan="8" class="dataTables_empty">No data available in table</td></tr>');
                $("#dt-listuser_processing").css("display","none");
            }
        }
    });    
    //--=== #END REGION GRID LIST
	
   oTable = $('#dataTables-listuser').DataTable({
            responsive: true,                                
            searching: true,
            paging : true
   }); 
      
    
                                     
});
 