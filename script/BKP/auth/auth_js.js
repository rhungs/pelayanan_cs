"use_strict";

$(document).ready(function() {  
       
   //init
   $('#dt-popup-instansi').DataTable({
        responsive: true,                                
        searching: true,
        paging : true ,
        //scrollY: '40vh',
        //scrollCollapse: true,                            
        'ajax': base_url + 'reference/common/get_instansi_ref/'
   });  
      
    $('#btn_cari_ins').click( function () {
        $('#modal-instansi').modal('show');            
    });
                                     
});

function GetSelectedInstansi(id){           
    var obj = document.getElementById(id);
    var data = obj.getAttribute('data-select');        
    var dataArr = data.split(";");    
       
    if (dataArr.length > 0 ) {               
        $("#instansi_id").val(dataArr[0]);  
        $("#instansi_name").val(dataArr[1]);                  
        $("#instansi_address").val(dataArr[2]);
        $("#instansi_phone").val(dataArr[3]);
        $("#instansi_fax").val(dataArr[4]);
        $("#instansi_email").val(dataArr[5]);
        $("#postal_code").val(dataArr[6]);
        $("#instansi_city").val(dataArr[7]); 
        
        if (dataArr[0] > 0) {
            $("#instansi_address").prop('readonly', true);
            $("#instansi_phone").prop('readonly', true);
            $("#instansi_fax").prop('readonly', true);
            $("#instansi_email").prop('readonly', true);
            $("#postal_code").prop('readonly', true);
            $("#instansi_city").prop('readonly', true);
        }
    }                
    $('#modal-instansi').modal('hide');
}
 